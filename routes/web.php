<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
    
Route::group(['middleware' => ['web']], function () {
    
    
    /* FRONTPAGE ROUTE start */
    
        Route::get('/', 'Frontend\FrontpageController@frontpage')->middleware('guest');
        
        Route::get('/home', 'Frontend\FrontpageController@frontpage')->middleware('guest');
        
    /* FRONTPAGE ROUTE end */
    
    /* AUTH ROUTES start */

        //Login form
        Route::get('/login', function () {
            Auth::logout();
            
            Session::forget('isAdmin');
            
            $apploc = App::getLocale();
            if($userlanguage = Session::get('locale')){
            }else{
                $userlanguage = Session::put('locale', $apploc);
            }
            App::setLocale($userlanguage);
            Session::put('locale', $userlanguage);
            
            return view('auth.login');
        });
        
        //Login process
        Route::post('/login', [
            'uses'          => 'Auth\LoginController@login',
            'middleware'    => 'checkstatus',
        ]);
        
        //Logout must be done with LoginController because it deletes admin privileges!!!
        Route::get('/logout', 'Auth\LoginController@logout'); 
        
        // Registration Routes
        Route::get('/register/{usertype}', function($usertype)
        {
            $apploc = App::getLocale();
            if($userlanguage = Session::get('locale')){
            }else{
                $userlanguage = Session::put('locale', $apploc);
            }
            App::setLocale($userlanguage);
            Session::put('locale', $userlanguage);
            
            return view('auth.register',compact('usertype'));
        });
        Route::post('/register/{usertype?}', 'Auth\RegisterController@register');
        
        //Mail verification Route
        Route::get('/verify/{activationCode}', 'Auth\RegisterController@activateUser');
        
        //User enabling Route
        Route::get('/enable/{enablingCode}', 'Auth\RegisterController@enableUser');
    
        // Password Reset Routes...
        Route::get('password/forgotpassword', 'Auth\PasswordController@showEmailForm');
        Route::post('password/email', 'Auth\PasswordController@sendResetLinkEmail');
        
        Route::get('password/resetpass/{token}/{email}', 'Auth\PasswordController@showResetForm');
        Route::post('password/reset', 'Auth\PasswordController@reset');
        
        //Route for page after login
        Route::get('/authhome', 'Backend\HomeController@index')->middleware('auth');
        
    /* AUTH ROUTES end */
    
    /* USER (Logged in user) ROUTES start */
        // Profile view and edit
        Route::get('/myprofile/{userid}', 'Backend\UserController@show')->middleware('auth');
        Route::get('/profile/{userid}', 'Backend\UserController@edit')->middleware('auth');
        Route::patch('/profile/{userid}', 'Backend\UserController@update')->middleware('auth');
        
        // Modules (index, view, add, add new lang...)
        Route::get('/modules', 'Backend\ModuleController@index')->middleware('auth');
        Route::get('/newmodule', 'Backend\ModuleController@create')->middleware('auth');
        Route::post('/module', 'Backend\ModuleController@store')->middleware('auth');
        Route::get('/moduled/{moduleid}', 'Backend\ModuleController@destroy')->middleware('auth');
        Route::get('/module/{module}', 'Backend\ModuleController@edit')->middleware('auth');
        Route::patch('/module/{module}', 'Backend\ModuleController@update')->middleware('auth');
        Route::patch('/modulel/{module}', 'Backend\ModuleController@updatelang')->middleware('auth');
        Route::patch('modulepublic/{module}', 'Backend\ModuleController@public')->middleware('auth');
        Route::get('/modulenew/{moduleid}/{module}/{modlang}', 'Backend\ModuleController@createlang')->middleware('auth');
        Route::patch('/modulelang', 'Backend\ModuleController@addnewlang')->middleware('auth');
        
        
        // Images (gallery) for modules
        Route::get('/images/{moduleid}', 'Backend\GalleryController@index')->middleware('auth');
        Route::get('/imagenew/{moduleid}', 'Backend\GalleryController@create')->middleware('auth');
        Route::post('/image', 'Backend\GalleryController@store')->middleware('auth');
        Route::get('/imaged/{imageid}', 'Backend\GalleryController@destroy')->middleware('auth');
        
        
        // Schedule for modules
        Route::get('/schedules/{moduleid}', 'Backend\ScheduleController@index')->middleware('auth');
        Route::get('/schedulenew/{moduleid}', 'Backend\ScheduleController@create')->middleware('auth');
        Route::post('/schedule', 'Backend\ScheduleController@store')->middleware('auth');
        Route::get('/scheduled/{scheduleid}', 'Backend\ScheduleController@destroy')->middleware('auth');
        
        // Itineraries (index, view, add, add new lang...)
        Route::get('/itineraries', 'Backend\ItineraryController@index')->middleware('auth');
        Route::get('/newitinerary', 'Backend\ItineraryController@create')->middleware('auth');
        Route::post('/itinerary', 'Backend\ItineraryController@store')->middleware('auth');
        Route::get('/itineraryd/{itineraryid}', 'Backend\ItineraryController@destroy')->middleware('auth');
        Route::get('/itinerary/{itinerary}', 'Backend\ItineraryController@edit')->middleware('auth');
        Route::patch('/itinerary/{itinerary}', 'Backend\ItineraryController@update')->middleware('auth');
        Route::patch('/itineraryl/{itinerary}', 'Backend\ItineraryController@updatelang')->middleware('auth');
        Route::patch('itinerarypublic/{itinerary}', 'Backend\ItineraryController@public')->middleware('auth');
        Route::get('/itinerarynew/{itineraryid}/{itinerary}/{itilang}', 'Backend\ItineraryController@createlang')->middleware('auth');
        Route::patch('/itinerarylang', 'Backend\ItineraryController@addnewlang')->middleware('auth');
        Route::get('/itinerarypdf/{lang}/{itinerary}', 'Backend\ItineraryController@pdf')->middleware('auth');

        // Change Itineraries status
        Route::get('/confirm/{itineraryid}', 'Backend\ItineraryController@confirm')->middleware('auth');
        Route::get('/cancel/{itineraryid}', 'Backend\ItineraryController@cancel')->middleware('auth');
        Route::get('/completed/{itineraryid}', 'Backend\ItineraryController@completed')->middleware('auth');
        
        // Rating
        Route::get('/rate/{token}', 'Backend\RatingController@showRatingFormProvider')->middleware('auth');
        Route::post('/rate/{token}', 'Backend\RatingController@RatingProvider')->middleware('auth');
        Route::get('/ratea/{token}', 'Backend\RatingController@showRatingFormAgency')->middleware('auth');
        Route::post('/ratea/{token}', 'Backend\RatingController@RatingAgency')->middleware('auth');
        //Rating with token (no login needed) for providers and agency
        Route::get('/rating/{token}', 'Backend\RatingController@showRatingFormProvider');
        Route::post('/rating/{token}', 'Backend\RatingController@RatingProvider');
        Route::get('/ratinga/{token}', 'Backend\RatingController@showRatingFormAgency');
        Route::post('/ratinga/{token}', 'Backend\RatingController@RatingAgency');
        
        // Itinerary Modules 
        Route::get('/modulesiti/{itineraryid}', 'Backend\ItineraryModuleController@index')->middleware('auth');
        Route::get('/modulesitinew/{itineraryid}', 'Backend\ItineraryModuleController@create')->middleware('auth');
        Route::post('/modulesiti', 'Backend\ItineraryModuleController@store')->middleware('auth');
        Route::get('/modulesitid/{moduleid}', 'Backend\ItineraryModuleController@destroy')->middleware('auth');
        Route::post('/modulesitidate/{itineraryid}', 'Backend\ItineraryModuleController@storedates')->middleware('auth');
        
        //Routes to exchange communication between providers and agencies
        //Messages with token (no login needed)
        Route::get('/msg/{answer}/{token}', 'Backend\MessageController@showResponseForm');
        Route::post('/msg/{answer}/{token}', 'Backend\MessageController@providerResponse');
        //Message module in  backend
        Route::get('/messages/{itineraryid}', 'Backend\MessageController@index')->middleware('auth');
        
        
    /* USER ROUTES end */
    
    /* ADMIN ROUTES start */
    
        // Modules
        Route::get('Admin/modules', 'Admin\AModuleController@index')->middleware('auth');
        Route::get('Admin/modules/{catid}', 'Admin\AModuleController@indexFiltered')->middleware('auth');
        Route::get('Admin/newmodule', 'Admin\AModuleController@create')->middleware('auth');
        Route::post('Admin/module', 'Admin\AModuleController@store')->middleware('auth');
        Route::delete('Admin/module/{moduleid}', 'Admin\AModuleController@destroy')->middleware('auth');
        Route::get('Admin/module/{module}', 'Admin\AModuleController@edit')->middleware('auth');
        Route::patch('Admin/module/{module}', 'Admin\AModuleController@update')->middleware('auth');
        Route::patch('Admin/moduleenable/{module}', 'Admin\AModuleController@enable')->middleware('auth');
        Route::get('Admin/modulenew/{moduleid}/{modlang}', 'Admin\AModuleController@createlang')->middleware('auth');
        Route::patch('Admin/modulelang', 'Admin\AModuleController@addnewlang')->middleware('auth');
        Route::get('Admin/moduleimg/{moduleid}', 'Admin\AModuleController@changeImageForm')->middleware('auth');
        Route::patch('Admin/modimage', 'Admin\AModuleController@changeImage')->middleware('auth');
        // Users
        Route::get('Admin/users', 'Admin\AUserController@index')->middleware('auth');
        Route::get('Admin/users/{filter}', 'Admin\AUserController@indexFiltered')->middleware('auth');
        Route::delete('Admin/user/{userid}', 'Admin\AUserController@destroy')->middleware('auth');
        Route::patch('Admin/userenable/{module}', 'Admin\AUserController@enable')->middleware('auth');
        Route::patch('Admin/userverify/{module}', 'Admin\AUserController@verify')->middleware('auth');
        Route::post('Admin/ratings/{userid}', 'Admin\AUserController@rating')->middleware('auth');
        // Itineraries
        Route::get('Admin/itineraries', 'Admin\AItineraryController@index')->middleware('auth');
        Route::patch('Admin/itinerarypublic/{itinerary}', 'Admin\AItineraryController@public')->middleware('auth');
        Route::patch('Admin/itineraryenable/{itinerary}', 'Admin\AItineraryController@enable')->middleware('auth');
        Route::get('Admin/itinerary/{itinerary}', 'Admin\AItineraryController@viewItinerary')->middleware('auth');
        Route::get('Admin/modulesiti/{itineraryid}', 'Admin\AItineraryController@viewModules')->middleware('auth');
    
    /* ADMIN ROUTES end */

    /* PAGES ROUTES start */

        Route::get('/how-to-register', function () {
            $apploc = App::getLocale();
            if($userlanguage = Session::get('locale')){
            }else{
                $userlanguage = Session::put('locale', $apploc);
            }
            App::setLocale($userlanguage);
            Session::put('locale', $userlanguage);
            
            return view('frontend.register-options');
        })->middleware('guest');
    
        // Change Language links
        Route::get('/lang/{lang?}', 'LanguageController@update');
        Route::get('/lang', 'LanguageController@update');
        Route::get('/lang/', 'LanguageController@update');
        
        //Module categories pages
        Route::get('/food', 'Frontend\ModCategoryController@index1');
        Route::get('/experiences', 'Frontend\ModCategoryController@index2');
        Route::get('/events', 'Frontend\ModCategoryController@index3');
        
        //Module view page
        Route::get('/offer/{moduleid}', 'Frontend\ModuleViewController@showModule');
        
        //Search page
        Route::get('/search/', 'Frontend\SearchController@showForm');
        Route::post('/search/', 'Frontend\SearchController@makeSearch');
        
        //Packages (Itineraries)
        Route::get('/packages', 'Frontend\ItineraryViewController@index');
        Route::get('/package/{itineraryid}', 'Frontend\ItineraryViewController@showItinerary');
        
    /* PAGES ROUTES end */
        
});

