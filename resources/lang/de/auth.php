<?php
return [
    /*
     |--------------------------------------------------------------------------
     | Authentication Language Lines
     |--------------------------------------------------------------------------
     |
     | The following language lines are used during authentication for various
     | messages that we need to display to the user. You are free to modify
     | these language lines according to your application's requirements.
     |
     */
    
    'failed'   => 'Diese Kombination aus Zugangsdaten wurde nicht in unserer Datenbank gefunden.',
    'throttle' => 'Zu viele Loginversuche. Versuchen Sie es bitte in :seconds Sekunden nochmal.',
    'logout' => 'Ausloggen',
    'login' => 'Anmeldung',
    'register' => 'Registrieren',
    'resetpass' => 'Klicken Sie hier um Ihr Passwort zurückzusetzen',
    'reset' => 'Passwort zurücksetzen',
    'email' => 'E-Mail Addresse',
    'sendlink' => 'Link zum Zurücksetzen des Passworts senden',
    'pass' => 'Passwort',
    'passconf' => 'Passwort bestätigen',
    'username' => 'Nutzername',
    'remember' => 'Erinnere dich an mich',
    'forgot' => 'Haben Sie Ihr Passwort vergessen',
    'title' => 'Titel',
    'address' => 'Addresse',
    'logo' => 'Logo',
    'telephone' => 'Telefon',
    'cellphone' => 'Handy',
    'www' => 'Webseite',
    'emoruser' => 'E-mail oder Nutzername',
    'logged' => 'Sie sind eingeloggt!',
    'successreg' => 'Ein neues Konto wurde erfolgreich erstellt. Bitte überprüfen Sie Ihre E-Mail und aktivieren Sie Ihr Konto.',
    'nousercreated' => 'Neuer Benutzer kann nicht erstellt werden.',
    'notverified' => 'E-Mail nicht bestätigt Bitte überprüfen Sie Ihre E-Mail-Adresse, bevor Sie sich anmelden.',
    'notenabled' => 'Dieser Benutzer wurde deaktiviert. Bitte kontaktieren Sie den Administrator für weitere Informationen.',
    'usertype' => 'Benutzertyp',
    'agency' => 'Reisenagentur',
    'provider' => 'Dienstanbieter',
    'group' => 'Gruppe',
    'filenotfound' => 'Fehler beim Hochladen der Datei.',
    'unauthorized' => 'Nicht autorisierte Handlung',
    'logindata' => 'Bitte einloggen Sie sich mit Ihren Zugangsdaten an',
    'fillindata' => 'Bitte füllen Sie alle Felder aus, um sich zu registrieren',
    'passchanged' => 'Dein Passwort wurde erfolgreich geändert.',
    'passnotchanged' => 'Ihr Passwort wurde nicht geändert. Bitte versuch es erneut.',
        
];