<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'logout' => 'Logout',
    'login' => 'Login',
    'register' => 'Register',
    'resetpass' => 'Click here to reset your password',
    'reset' => 'Reset Password',
    'email' => 'E-Mail Address',
    'sendlink' => 'Send Password Reset Link',
    'pass' => 'Password',
    'passconf' => 'Confirm Password',
    'username' => 'Username',
    'remember' => 'Remember Me',
    'forgot' => 'Forgot Your Password',
    'title' => 'Title',
    'address' => 'Address',
    'logo' => 'Logo',
    'telephone' => 'Telephone',
    'cellphone' => 'Cellphone',
    'www' => 'Web page',
    'emoruser' => 'E-mail or username',
    'logged' => 'You are logged in!',
    'successreg' => 'Successfully created a new account. Please check your e-mail and activate your account.',
    'nousercreated' => 'Unable to create new user account.',
    'notverified' => 'E-mail not verified. Please verify your e-mail before attempting to log in.',
    'notenabled' => 'This user has been disabled. Please contact the admin for further information.',
    'usertype' => 'User type',
    'agency' => 'Tourist Agency',
    'provider' => 'Service provider',
    'group' => 'Group',
    'filenotfound' => 'Error uploading file.',
    'unauthorized' => 'Unauthorized action',
    'logindata' => 'Please sign in with your credentials',
    'fillindata' => 'Please fill in all the fields in order to register',
    'passchanged' => 'Your password was successfully changed.',
    'passnotchanged' => 'Your password was not changed. Please try again.',
    
];
