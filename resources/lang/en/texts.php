<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Text Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'designpackages' => 'Create your itinerary',
    'madepackages' => 'Find experiences',
    'beaprovider' => 'Become a provider',
    'banner1' => 'create your tourist experience',
    'sklopi' => 'Offered categories',
    'sklop1' => 'Food',
    'sklop2' => 'Experiences',
    'sklop3' => 'Events',
    'sklop4' => 'Prepared packages',
    'search' => 'Search',
    'locations' => 'Locations',
    'aboutus' => 'About us',
    'groups' => 'Make a program<br/>for a group',
    'individuals' => 'Programs <br/>for individuals',
    'packages' => 'Prepared packages',
    'sponsors' => 'The »ECooL-Tour« platform is co-financed by the European Union through the European Regional Development Fund.<br />
                 The operation is carried out within the cooperation program INTERREG V-A Slovenia - Croatia.',
    'projectsponsors' => 'This project is co-financed by',
    'projectpartners' => 'Project partners',
    'aboutustext' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam egestas est at elit efficitur, non egestas metus tempor. Suspendisse sit amet feugiat erat. Duis sed fringilla augue, at feugiat libero. In turpis tortor, laoreet in commodo in, egestas vel massa. In et porta tortor, in semper ligula. Phasellus tempor et mi non pharetra. Nulla lobortis eleifend consequat. Nulla ipsum felis, gravida quis consectetur quis, ultrices vel felis. Quisque ullamcorper eros ut dolor ultricies viverra. Cras imperdiet tortor et tellus pellentesque aliquam ac ac arcu.',
    'from' => 'from',
    'to' => 'to',
    'duration' => 'Duration',
    'geolocation' => 'Geolocation',
    'contact' => 'Contact',
    'register2' => 'Agency',
    'register3' => 'Provider',
    'register4' => 'Group',
    'newuser' => 'A new user has registered',
    'linkenable' => 'Click the following link to enable the user',
    'haveregistered' => 'You have registered in Site name',
    'linkverify' => 'Click the following link to verify your e-mail',
    'modulerequest' => 'An agency or group is interested in one of your modules',
    'requestconfirm' => 'If you would like to confirm this reservation please follow this link',
    'requestreject' => 'If you would like to reject this reservation please follow this link',
    'providerresponse' => 'A provider has sent you a response',
    'providerresponse2' => 'Provider\'s response',
    'abouttoreject' => 'You are about to reject the next reservation',
    'abouttoconfirm' => 'You are about to confirm the next reservation',
    'miskakeinresponse' => 'There is a mistake in your response',
    'agencyname' => 'Agency\'s or group\'s name',
    'confirm' => 'Confirm',
    'reject' => 'Reject',
    'responsesent' => 'Your response was sent. Thank you!',
    'messageexists' => 'You already replied to this agency or group for this module',
    'providername' => 'Provider\'s name',
    'providerhasconfirmed' => 'The provider has confirmed your reservation',
    'providerhasrejected' => 'The provider has rejected your reservation',
    'providermsg' => 'Additional message from the provider',
    'sent' => 'Sent',
    'nomsg' => 'No messages',
    'userratings' => 'User rating',
    'showingratings' => 'Ratings for user',
    'ratingform' => 'Rating Form',
    'ratingformp' => 'The agency or group that recently booked one of your modules',
    'ratingforma' => 'Providers booked in this itinerary',
    'howwouldyou' => 'Questionaire',
    'ratingsent' => 'Thank you for filling the evaluation form',
    'ratingexistsp' => 'You already evaluated this agency for this itinerary',
    'ratingexistsa' => 'You already evaluated this itinerary',
    'actions' => 'Actions',
    'agencyconfirmation' => 'An agency or group has confirmed one of your modules',
    'moreinfocontact' => 'For more information please contact the agency or group',
    'agencycancel' => 'An agency or group has cancelled an itinerary with one of your modules',
    'evaluationlink' => 'Site name has sent you an evaluation link',
    'pleaseevaluate' => 'Your feedback is very important for us. It will help us to keep offering you only the best.',
    'pleaseevaluateprovider' => 'Please follow the link to evaluate the providers of an itinerary you recently realized',
    'pleaseevaluateagency' => 'Please follow the link to evaluate the agency or group that recently booked one of your modules',
    'ratingquestion1' => 'How satisfied are you with the responsiveness and communication of the user (the agency, the organizer of the group)?',
    'ratingquestion2' => 'How satisfied are you with the placement of your module in the entire package of the itinerary?',
    'ratingquestion3' => 'How satisfied are you with the organization of the visit by the user (agency, group organizer)?',
    'ratingquestiona1' => 'How satisfied are you with the responsiveness and communication of the provider?',
    'ratingquestiona2' => 'How satisfied are you with the content of the tourist product (tourist service or module)?',
    'ratingquestiona3' => 'How satisfied are you with the provider\'s service?',
    'saverating' => 'Rate',
    'alltosaverating' => 'You must choose an answer for all the questions to send a rating for an itinerary',
    'question' => 'Question',
    'rating' => 'Rating',
    'ratingsnumber' => 'Number of times rated',
    'noratings' => 'This user has no ratings yet',
    'average' => 'Average',
    'ratingmail' => 'You have been rated in Site name',
    'ratingmailp' => 'You have been rated in Site name by a provider',
    'ratingmaila' => 'You have been rated in Site name by an agency or group',
    'offeredby' => 'This module if offered by',
    'registernow' => 'Register now',
    'capacity' => 'Capacity',
    'searchresults' => 'Search results',
    'showingall' => 'Showing all offers in map',
    'about1' => 'Tourist agencies',
    'about2' => 'Organized groups',
    'about3' => 'Individuals and small groups',
    'about4' => 'Providers',
    'regagency' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam egestas est at elit efficitur, non egestas metus tempor. Suspendisse sit amet feugiat erat. Duis sed fringilla augue, at feugiat libero. In turpis tortor, laoreet in commodo in, egestas vel massa. In et porta tortor, in semper ligula. Phasellus tempor et mi non pharetra. Nulla lobortis eleifend consequat. Nulla ipsum felis, gravida quis consectetur quis, ultrices vel felis. Quisque ullamcorper eros ut dolor ultricies viverra. Cras imperdiet tortor et tellus pellentesque aliquam ac ac arcu.',
    'regprovider' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam egestas est at elit efficitur, non egestas metus tempor. Suspendisse sit amet feugiat erat. Duis sed fringilla augue, at feugiat libero. In turpis tortor, laoreet in commodo in, egestas vel massa. In et porta tortor, in semper ligula. Phasellus tempor et mi non pharetra. Nulla lobortis eleifend consequat. Nulla ipsum felis, gravida quis consectetur quis, ultrices vel felis. Quisque ullamcorper eros ut dolor ultricies viverra. Cras imperdiet tortor et tellus pellentesque aliquam ac ac arcu.',
    'reggroup' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam egestas est at elit efficitur, non egestas metus tempor. Suspendisse sit amet feugiat erat. Duis sed fringilla augue, at feugiat libero. In turpis tortor, laoreet in commodo in, egestas vel massa. In et porta tortor, in semper ligula. Phasellus tempor et mi non pharetra. Nulla lobortis eleifend consequat. Nulla ipsum felis, gravida quis consectetur quis, ultrices vel felis. Quisque ullamcorper eros ut dolor ultricies viverra. Cras imperdiet tortor et tellus pellentesque aliquam ac ac arcu.',
    'individuals1' => 'Information for individuals and small groups',
    'individuals2' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam egestas est at elit efficitur, non egestas metus tempor. Suspendisse sit amet feugiat erat. Duis sed fringilla augue, at feugiat libero. In turpis tortor, laoreet in commodo in, egestas vel massa. In et porta tortor, in semper ligula. Phasellus tempor et mi non pharetra. Nulla lobortis eleifend consequat. Nulla ipsum felis, gravida quis consectetur quis, ultrices vel felis. Quisque ullamcorper eros ut dolor ultricies viverra. Cras imperdiet tortor et tellus pellentesque aliquam ac ac arcu.',
    'msg1subject' => 'Reminder: An agency or group is interested in one of your modules',
    'msg2subject' => 'Notification: One of your modules has been marked as rejected',
    'msg3subject' => 'Notification: A module you included in one of your itineraries was not answered',
    'msg1' => 'This mail is merely a reminder that an agency or group is interested in one of your modules and you have yet to answer.<br/>
               When 72 hours have past from the first mail the module will be marked as rejected and the agency or group will choose another provider.',
    'msg2' => '72 hours have passed since an agency or group interested in one of your modules sent you an e-mail. <br/>As there has been no answer this module has now be marked as rejected.',
    'msg3' => '72 hours have passed since a mail was sent to a provider for a module you included in one of your itineraries. <br/>As there has been no answer this module has now be marked as rejected.',
    'msggroup1subject' => 'Reminder: Your Site name account will be deleted due to inactivity',
    'msggroup2subject' => 'Notification: Your Site name account was deleted due to inactivity',
    'msggroup1' => 'You have not logged in into your acount for almost a year.<br/>If you do not log in within the next week it will be deleted.',
    'msggroup1login' => 'You can login using the next link',
    'msggroup2' => 'Your Site name account was deleted because you did not log in for a year.<br/>
               For more information please contact the page administrator at admin@sitename.com.',
    'descriptionitinerary' => 'Itinerary details',
    'descriptionmodules' => 'Activities in itinerary',
    'status0i' => 'Itinerary created',
    'status1i' => 'Mails sent to providers',
    'status2i' => 'A module has been rejected',
    'status3i' => 'Some providers have confirmed their modules',
    'status4i' => 'All providers have confirmed their modules',
    'status5i' => 'Itinerary ready for realization',
    'status6i' => 'Waiting for agency rating.',
    'status7i' => 'The itinerary is archived',
    'status8i' => 'The itinerary was cancelled',
    'status0m' => 'Module added to itinerary',
    'status1m' => 'Module added to itinerary',
    'status2m' => 'Waiting for response',
    'status3m' => 'Not yet confirmed',
    'status4m' => 'Confirmed by providers',
    'status5m' => 'Confirmed by agency',
    'status6m' => 'Waiting for provider rating.',
    'status7m' => 'The module is archived',
    'status8m' => 'The itinerary was cancelled',
    'statusitinerary' => 'Lifecycle of an itinerary. Hover over a circle to see its meaning.',
    'statusmodule' => 'Lifecycle of a module. Hover over a circle to see its meaning.',
    'itineraries' => 'Itineraries',
    'welcomemsg' => 'Welcome!',
    'welcomeagency' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam egestas est at elit efficitur, non egestas metus tempor. Suspendisse sit amet feugiat erat. Duis sed fringilla augue, at feugiat libero. In turpis tortor, laoreet in commodo in, egestas vel massa. In et porta tortor, in semper ligula. Phasellus tempor et mi non pharetra. Nulla lobortis eleifend consequat. Nulla ipsum felis, gravida quis consectetur quis, ultrices vel felis. Quisque ullamcorper eros ut dolor ultricies viverra. Cras imperdiet tortor et tellus pellentesque aliquam ac ac arcu.',
    'welcomeprovider' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam egestas est at elit efficitur, non egestas metus tempor. Suspendisse sit amet feugiat erat. Duis sed fringilla augue, at feugiat libero. In turpis tortor, laoreet in commodo in, egestas vel massa. In et porta tortor, in semper ligula. Phasellus tempor et mi non pharetra. Nulla lobortis eleifend consequat. Nulla ipsum felis, gravida quis consectetur quis, ultrices vel felis. Quisque ullamcorper eros ut dolor ultricies viverra. Cras imperdiet tortor et tellus pellentesque aliquam ac ac arcu.',
    'welcomegroup' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam egestas est at elit efficitur, non egestas metus tempor. Suspendisse sit amet feugiat erat. Duis sed fringilla augue, at feugiat libero. In turpis tortor, laoreet in commodo in, egestas vel massa. In et porta tortor, in semper ligula. Phasellus tempor et mi non pharetra. Nulla lobortis eleifend consequat. Nulla ipsum felis, gravida quis consectetur quis, ultrices vel felis. Quisque ullamcorper eros ut dolor ultricies viverra. Cras imperdiet tortor et tellus pellentesque aliquam ac ac arcu.',
    'welcomeadmin' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam egestas est at elit efficitur, non egestas metus tempor. Suspendisse sit amet feugiat erat. Duis sed fringilla augue, at feugiat libero. In turpis tortor, laoreet in commodo in, egestas vel massa. In et porta tortor, in semper ligula. Phasellus tempor et mi non pharetra. Nulla lobortis eleifend consequat. Nulla ipsum felis, gravida quis consectetur quis, ultrices vel felis. Quisque ullamcorper eros ut dolor ultricies viverra. Cras imperdiet tortor et tellus pellentesque aliquam ac ac arcu.',
    
    /**/

];
