<?php

return [
    'message' => 'Za boljšo spletno izkušnjo uporabljamo piškotke in vam nudimo ustrezne informacije. <br/>Če boste še naprej ostali na naši strani sklepamo, da se strinjate z našim pravilnikom o piškotkih.',
    'agree' => 'Ok',
];
