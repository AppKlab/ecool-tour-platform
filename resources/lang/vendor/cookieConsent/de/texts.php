<?php

return [
    'message' => 'Diese Seite verwendet Cookies f�r ein besseres Browser-Erlebnis und bietet Ihnen entsprechende Informationen dar�ber. Wenn Sie weiterhin auf unserer Webseite bleiben, werten wir dies als Zustimmung zu unserer Cookie-Richtlinie.',
    'agree' => 'Ok',
];
