<?php

return [
    'message' => 'We use cookies to improve your experience and provide you with relevant information. <br/>If you continue to browse our site we\'ll assume you agree with our Cookie Policy.',
    'agree' => 'Ok',
];
