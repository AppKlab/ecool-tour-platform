<?php

return [
    'message' => 'Koristimo kolačiće kako bismo poboljšali vaše iskustvo i pružili vam relevantne informacije. <br/>Ako nastavite pregledavati našu web stranicu pretpostavljamo da se slažete s našim Pravilima o kolačićima.',
    'agree' => 'Ok',
];
