<?php

return [
    
    /*
     |--------------------------------------------------------------------------
     | Text Language Lines
     |--------------------------------------------------------------------------
     |
     | The following language lines are used during authentication for various
     | messages that we need to display to the user. You are free to modify
     | these language lines according to your application's requirements.
     |
     */
    
    'designpackages' => 'Oblikuj itinerar',
    'madepackages' => 'Pronađi doživljaj',
    'beaprovider' => 'Postani ponuditelj',
    'banner1' => 'oblikuj vlastiti turististički doživljaj',
    'banner2' => 'lokalno.održivo.uključujući.',
    'banner3' => 'Upoznaj ponudu',
    'sklopi' => 'Sustavi ponude',
    'sklop1' => 'Hrana',
    'sklop2' => 'Doživljaji',
    'sklop3' => 'Događaji',
    'sklop4' => 'Spremni paketi',
    'search' => 'Traži',
    'see' => 'Upoznaj',
    'locations' => 'Lokacije',
    'aboutus' => 'About us',
    'groups' => 'Oblikuj program <br/>za grupe',
    'individuals' => 'Programi <br/>za pojedince',
    'packages' => 'Spremni paketi',
    'sponsors' => 'Platforma »ECooL-Tour« sufinanciraju Europska unija putem Europskega fonda za regionalni razvoj. <br />
                Operacija se provodi u okviru Programa sudelovanja INTERREG V-A Slovenija - Hrvatska.',
    'projectsponsors' => 'Projekt sufinanciraju',
    'projectpartners' => 'Partneri projekta',
    'projectecooltour' => 'ECooL-Tour je turistički proizvod kojeg korisnik dizajnira prema vlastitim željama.
                            ECooL-Tour povezuje postojeće inicijative održivog turizma i uspostavlja novi zajednički
                            prekogranični turistički proizvod koji je modularan i sastoji od turističkih usluga vezanih
                            uz očuvanje prirodne i kulturne baštine na području Prekmurja, Podravlja, Međimurja i Varaždinske regije.
                            <br/>Produkt je prvenstveno namijenjen organiziranim skupinama i turističkim agencijama, a neizravno
                            i za pojedince i male skupine koji samostalno izrađuju vlastiti plan jednodnevnih ili višednevnih putovanja
                            koja su puna izvornih, autentičnih i jednokratnih doživljaja.',
    'from' => 'Od',
    'to' => 'Za',
    'duration' => 'Trajanje',
    'geolocation' => 'Geo lokacija',
    'contact' => 'Kontakt',
    'register2' => 'Agencija',
    'register3' => 'Ponuditelj',
    'register4' => 'Grupa',
    'newuser' => 'Registrirao se novi korisnik',
    'linkenable' => 'Kliknite sljedeći link da biste potvrdili korisnika',
    'haveregistered' => 'Registrirali ste se u ECooL-Tour',
    'linkverify' => 'Kliknite sljedeći link da biste potvrdili svoju e-adresu',
    'modulerequest' => 'Agencija ili grupa zainteresirana je za jedan od vaših modula',
    'requestconfirm' => 'Ako želite potvrditi ovu rezervaciju, slijedite ovaj link',
    'requestreject' => 'Ako želite odbiti ovu rezervaciju, slijedite ovaj link',
    'providerresponse' => 'Ponuditelj vam je poslao odgovor',
    'providerresponse2' => 'Odgovor ponuditelja',
    'abouttoreject' => 'Spremate se odbiti sljedeću rezervaciju',
    'abouttoconfirm' => '‘Spremate se potvrditi sljedeću rezervaciju',
    'miskakeinresponse' => 'U odgovoru postoji greška',
    'agencyname' => 'Ime agencije ili grupe',
    'confirm' => 'Potvrdi',
    'reject' => 'Odbij',
    'responsesent' => 'Vaš odgovor je poslan. Hvala vam!',
    'messageexists' => 'Već ste odgovorili ovoj agenciji ili grupi za ovaj modul',
    'providername' => 'Ime ponuditelja',
    'providerhasconfirmed' => 'Ponuditelj je potvrdio vašu rezervaciju',
    'providerhasrejected' => 'Ponuditelj je odbio vašu rezervaciju',
    'providermsg' => 'Dodatna poruka ponuditelja',
    'sent' => 'Poslano',
    'nomsg' => 'Nema poruka',
    'userratings' => 'Korisnička ocijena',
    'showingratings' => 'Ocjene za korisnika',
    'ratingform' => 'Obrazac za ocjenu',
    'ratingformp' => 'Agencija ili grupa koja je nedavno rezervirala jedan od vaših modula',
    'ratingforma' => 'Ponuditelji izabrani u ovom intineraru',
    'howwouldyou' => 'Upitnik',
    'ratingsent' => 'Hvala što ste ispunili obrazac za ocjenjivanje',
    'ratingexistsp' => 'Već ste ocijenili ovu agenciju za ovaj itinerar',
    'ratingexistsa' => 'Već ste ocijenili ovaj itinerar',
    'actions' => 'Akitvnosti',
    'agencyconfirmation' => 'Agencija ili grupa potvrdila je jedan od vaših modula',
    'moreinfocontact' => 'Za više informacija kontaktirajte agenciju ili grupu',
    'agencycancel' => 'Agencija ili grupa otkazala je itinerar s jednim od vaših modula',
    'evaluationlink' => 'ECooL-Tour poslao vam je link za ocjenu',
    'pleaseevaluate' => 'Vaše nam je mišljenje vrlo važno. To će nam pomoći da vam ponudimo samo najbolje.',
    'pleaseevaluateprovider' => 'Slijedite link da biste ocijenili ponuditelje itinerara koji ste nedavno realizirali',
    'pleaseevaluateagency' => 'Slijedite link da biste ocijenili agenciju ili grupu koja je nedavno rezervirala jedan od vaših modula',
    'ratingquestion1' => 'Koliko ste zadovoljni komunikacijom korisnika (agencije, organizatora grupe)?',
    'ratingquestion2' => 'Koliko ste zadovoljni plasmanom vašega modula u cijeli plan turističkog paketa?',
    'ratingquestion3' => 'Koliko ste zadovoljni organizacijom posjeta od strane korisnika (agencija, organizator grupe)?',
    'ratingquestiona1' => 'Koliko ste zadovoljni odazivom i komunikacijom ponuditelja usluga?',
    'ratingquestiona2' => 'Koliko ste zadovoljni sadržajem turističkog proizvoda (turistička usluga/modul)?',
    'ratingquestiona3' => 'Koliko ste zadovoljni provedbom usluga od strane ponuđača usluga?',
    'saverating' => 'Ocjena',
    'alltosaverating' => 'Morate odabrati odgovor za sva pitanja kako biste poslali ocjenu za initerar',
    'question' => 'Pitanje',
    'rating' => 'Ocjena',
    'ratingsnumber' => 'Ocjenjeno broj puta',
    'noratings' => 'Ovaj korisnik još nema ocjene',
    'average' => 'Prosjek',
    'ratingmail' => 'Bili ste ocijenjeni u ECooL-Tour-u',
    'ratingmailp' => 'Bili ste ocijenjeni u ECooL-Tour-u od ponuditelja',
    'ratingmaila' => 'Bili ste ocijenjeni u ECooL-Tour-u od agencije ili grupe',
    'offeredby' => 'Ovaj modul nudi',
    'registernow' => 'Registriraj se',
    'capacity' => 'Kapacitet',
    'searchresults' => 'Traži rezultate',
    'showingall' => 'Prikaz svih ponuda na karti',
    'about1' => 'Turističke agencije',
    'about2' => 'Organizirane grupe',
    'about3' => 'Pojedinci i male grupe',
    'about4' => 'Ponuditelji',
    'regagency' => 'Registrirane turističke agencije pristupaju u aplikaciju za kreiranje jednodnevnih i višednevnih
                    planova putovanja. Ulazite u izravnu komunikaciju s ponuđačima usluga i provodite rezervacije.
                    Agencije mogu pripremati planove putovanja za zatvorene skupine ili javno objaviti gotove planove u dio
                    “pripremljeni paketi”, gdje se pojedinci i male skupine prijavljuju za putovanja.  <br/>Aplikacija
                    je organizacijski pripomoček za turistične agencije, ki želite ponuditi itinerarije s vsebinskega in
                    geografskega področja produkta ECooL-Tour. <br/>Registracijom postajete član “mreže ECooL-Tour”.',
    'regprovider' => 'Ponuditelji turističkih usluga (modula) registriranjem mogu dobiti pristup za upis svojih modula u
                        zajedničku ponudu za izradu modularno sastavljenih turističkih itinerara, koje kreiraju agencije i grupe.
                        <br/>Aplikaciju možete koristiti za unos i nadogradnju modula iz sadržajnog i geografskog područja ECooL-Tour
                        te za komunikaciju s korisnicima i implementaciju rezervacijskog sustava. <br/>Registracijom postajete član mreže “ECooL-Tour”.',
    'reggroup' => 'Registrirane organizirane grupe (npr. škole, udruge, neformalne skupine i sl.) pristupaju aplikaciji za oblikovanje
                    jednodnevnih i višednevnih planova putovanja. Ulazite u izravnu komunikaciju s ponuditeljima usluga i provodite
                    rezerviranje.<br/> Aplikacija je organizacijski alat za organizirane skupine koje želite oblikovati izlete sa
                    sadržajem i iz geografskog područja proizvoda ECooL-Tour.',
    'individuals1' => 'Informacije za pojedince i male grupe',
    'individuals2' => 'Pojedinci i male grupe na web stranici ECooL-Tour mogu pronaći pojedinačne turističke module ili oblikovane
                        programe koje nude registrirane turističke agencije u dijelu "pripremljeni paketi". Pojedinci i male skupine,
                        kao što su, na primjer, malo društvo na putovanju, grupe biciklista ili obitelji i sl., mogu koristiti web
                        stranicu kako bi pronašli informacije o ponuditeljima usluga iz sadržajnog i geografskog područja ECooL-Tour i
                        kontaktirati ih pojedinačno. Nažalost, zbog prirode određenih aktivnosti kao što su organizirane radionice ili
                        kulturni i gastronomski događaji, pojedincima i malim skupinama svi moduli ECooL-Tour produkta nisu dostupni
                        osim ako nisu stavljeni u “pripremljeni paketi”.',
    'msg1subject' => 'Podsjetnik: agenciju ili grupu zanima jedan od vaših modula ',
    'msg2subject' => 'Obavijest: jedan od vaših modula označen je kao odbijen',
    'msg3subject' => 'Obavijest: na modul koji ste uključili u jedan od vaših itinerara nije bilo odgovoreno',
    'msg1' => 'Ova poruka samo je podsjetnik da je agencija ili grupa zainteresirana za jedan od vaših modula i da još niste odgovorili.<br/>
               Kada je prođe 72 sata od prve poruke, modul će biti označen kao odbijen, a agencija ili grupa će odabrati drugog ponuditelja.',
    'msg2' => '72 sata je prošlo otkako vam je agencija ili grupa zainteresirana za jedan od vaših modula poslala poruku putem e-pošte. <br/>Budući da nema odgovora, ovaj modul sada je označen kao odbijen.',
    'msg3' => 'Prošlo je 72 sata od prve poruke ponuditelju modula koji ste uključili u vaš itinerar. <br/> Budući da nema odgovora, ovaj modul sada je označen kao odbijen.',
    'msggroup1subject' => 'Podsjetnik: Vaš račun ECooL-Tour bit će izbrisan zbog neaktivnosti',
    'msggroup2subject' => 'Obavijest: Vaš račun ECooL-Tour izbrisan je zbog neaktivnosti',
    'msggroup1' => 'Gotovo godinu dana niste se prijavili u svoj račun.<br/>Ako se ne prijavite u sljedećem tjednu, bit će izbrisan.',
    'msggroup1login' => 'Možete se prijaviti pomoću sljedećog linka',
    'msggroup2' => 'Vaš ECooL-Tour račun je izbrisan jer se niste prijavili godinu dana.<br/>
               Ako želite ponovo aktivirati i koristiti račun, obratite se administratoru na admin@sitename.com.',
    'descriptionitinerary' => 'Opis itinerara',
    'descriptionmodules' => 'Aktivnosti u itineraru',
    'status0i' => 'Stvoren je itinerar',
    'status1i' => 'Poruke su poslane ponuditeljima',
    'status2i' => 'Modul je odbijen',
    'status3i' => 'Neki ponuditelji potvrdili su svoje module',
    'status4i' => 'Svi ponuditelji potvrdili su svoje module',
    'status5i' => 'Itinerar spreman za realizaciju',
    'status6i' => 'Čeka se ocjena agencije',
    'status7i' => 'Itinerar je arhiviran',
    'status8i' => 'Itinerar je otkazan',
    'status0m' => 'Modul je dodan u itinerar',
    'status1m' => 'Modul je dodan u itinerar',
    'status2m' => 'Čeka se odgovor',
    'status3m' => 'Još nije potvrđeno',
    'status4m' => 'Potvrđeno od ponuditelja',
    'status5m' => 'Potvrđeno od agencije',
    'status6m' => 'Čeka se ocjena ponuditelja.',
    'status7m' => 'Modul je arhiviran',
    'status8m' => 'Itinerar je otkazan',
    'statusitinerary' => 'Životni ciklus itinerara. Zadržite pokazivač iznad kruga da biste vidjeli njegovo značenje.',
    'statusmodule' => 'Životni ciklus modula. Zadržite pokazivač iznad kruga da biste vidjeli njegovo značenje.',
    'itineraries' => 'Itinerari',
    'welcomemsg' => 'Dobrodošli!',
    'welcomeagency' => 'Registrirane turističke agencije pristupate u aplikaciju za kreiranje jednodnevnih i višednevnih
                        planova putovanja (itinerara). Ulazite u izravnu komunikaciju s ponuđačima usluga i provodite rezervacije.
                        <br/>Agencije mogu pripremati planove putovanja za zatvorene skupine ili javno objaviti gotove planove u dio
                        “pripremljeni paketi”, gdje se pojedinci i male skupine prijavljuju za putovanja. <br/>Aplikacija je
                        organizacijski alat za turističke agencije koje žele ponuditi planove putovanja iz sadržaja i
                        geografskog opsega ECooL-Tour produkta. Registracijom postajete član “mreže ECooL-Tour”.',
    'welcomeprovider' => 'Ponuditelji turističkih usluga (modula) registriranjem možete dobiti pristup za upis svojih modula
                        u zajedničku ponudu za izradu modularno sastavljenih turističkih itinerara, koje kreiraju agencije i grupe.
                        Aplikaciju možete koristiti za unos i nadogradnju modula iz sadržajnog i geografskog područja ECooL-Tour te
                        za komunikaciju s korisnicima i implementaciju rezervacijskog sustava. Registracijom postajete član “mreže ECooL-Tour”.',
    'welcomegroup' => 'Registrirane organizirane grupe (npr. škole, udruge, neformalne skupine i sl.) pristupaju aplikaciji za
                        oblikovanje jednodnevnih i višednevnih planova putovanja. Ulazite u izravnu komunikaciju s ponuditeljima
                        usluga i provodite rezerviranje.<br/> Aplikacija je organizacijski alat za organizirane skupine koje želite
                        oblikovati izlete sa sadržajem i iz geografskog područja proizvoda ECooL-Tour.',
    'welcomeadmin' => 'Kao administrator sada imate pristup administrativnim alatima proizvoda ECool-Tour.',
    
];
