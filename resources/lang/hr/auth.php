<?php
return [
    /*
     |--------------------------------------------------------------------------
     | Authentication Language Lines
     |--------------------------------------------------------------------------
     |
     | The following language lines are used during authentication for various
     | messages that we need to display to the user. You are free to modify
     | these language lines according to your application's requirements.
     |
     */
    
    'failed'   => 'Ovi podaci ne odgovaraju našima.',
    'throttle' => 'Previše pokušaja prijave. Molim Vas pokušajte ponovno za :seconds sekundi.',
    'logout' => 'Ispis',
    'login' => 'Upis',
    'register' => 'Registracija',
    'resetpass' => 'Kliknite ovdje da biste resitirali lozinku',
    'reset' => 'Resetiraj lozinku',
    'email' => 'E-mail adresa',
    'sendlink' => 'Pošalji link za resetiranje lozinke',
    'pass' => 'Lozinka',
    'passconf' => 'Potvrdi lozinku',
    'username' => 'Korisničko ime',
    'remember' => 'Zapamti me',
    'forgot' => 'Zaboravili ste lozinku?',
    'title' => 'Naslov',
    'address' => 'Adresa',
    'logo' => 'Logo',
    'telephone' => 'Telefon',
    'cellphone' => 'Mobilni telefon',
    'www' => 'Web stranica',
    'emoruser' => 'E-mail ili korisničko ime',
    'logged' => 'Prijavljeni ste!',
    'successreg' => 'Uspješno ste kreirali novi račun. Provjerite svoj e-mail i aktivirajte svoj račun.',
    'nousercreated' => 'Nije moguće kreirati novog korisnika.',
    'notverified' => 'E-mail nije potvrđen. Molimo Vas, potvrdite svoj e-mail prije pokušaja prijave.',
    'notenabled' => 'Ovaj korisnik je onemogućen. Za dodatne informacije kontaktirajte administratora.',
    'usertype' => 'Tip korisnika',
    'agency' => 'Touristička agencija',
    'provider' => 'Ponuditelj usluga',
    'group' => 'Grupa',
    'filenotfound' => 'Pogreška pri prijenosu datoteke.',
    'unauthorized' => 'Neovlašteno djelovanje',
    'logindata' => 'Prijavite se sa svojim podacima',
    'fillindata' => 'Molimo, ispunite sva polja kako biste se registrirali.',
    'passchanged' => 'Vaša je lozinka uspješno promijenjena.',
    'passnotchanged' => 'Vaša lozinka nije promijenjena. Molimo, pokušajte ponovo.',
    
];