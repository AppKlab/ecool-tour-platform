<?php
return [
    /*
     |--------------------------------------------------------------------------
     | Authentication Language Lines
     |--------------------------------------------------------------------------
     |
     | The following language lines are used during authentication for various
     | messages that we need to display to the user. You are free to modify
     | these language lines according to your application's requirements.
     |
     */
    'failed'   => 'Ti podatki se ne ujemajo z našimi.',
    'throttle' => 'Preveč poskusov prijave. Prosimo, poskusite ponovno čez :seconds sekund.',
    'logout' => 'Odjava',
    'login' => 'Prijava',
    'register' => 'Registracija',
    'resetpass' => 'Kliknite tukaj, da ponastavite svoje geslo',
    'reset' => 'Ponastavi geslo',
    'email' => 'E-poštni naslov',
    'sendlink' => 'Pošlji povezavo do ponastavitve gesla',
    'pass' => 'Geslo',
    'passconf' => 'Potrditev gesla',
    'username' => 'Uporabniško ime',
    'remember' => 'Zapomni si me',
    'forgot' => 'Ste pozabili svoje geslo?',
    'title' => 'Naziv',
    'address' => 'Naslov',
    'logo' => 'Logotip',
    'telephone' => 'Telefonska številka',
    'cellphone' => 'Mobitel številka',
    'www' => 'Spletna stran',
    'emoruser' => 'E-poštni naslov ali uporabniško ime',
    'logged' => 'Prijavljeni ste.',
    'successreg' => 'Uspešno ustvarjen nov uporabniški račun. Prosim preverite vaš e-poštni naslov in aktivirajte svoj račun.',
    'nousercreated' => 'Ni mogoče ustvariti novega uporabniškega računa.',
    'notverified' => 'E-pošta ni potrjena. Preverite svoj e-poštni naslov preden se poskušate ponovno prijaviti.',
    'notenabled' => 'Ta uporabnik je bil onemogočen. Prosimo kontaktirajte administratorja za dodatne informacije.',
    'usertype' => 'Tip uporabnika',
    'agency' => 'Turistična agencija',
    'provider' => 'Ponudnik',
    'group' => 'Skupina',
    'filenotfound' => 'Napaka pri nalaganju mape.',
    'unauthorized' => 'Nepooblaščeno dejanje',
    'logindata' => 'Prosim prijavite se s svojimi podatki.',
    'fillindata' => 'Prosim izpolnite vsa polja, da se registrirate.',
    'passchanged' => 'Vaše geslo je bilo uspešno spremenjeno.',
    'passnotchanged' => 'Vaše geslo ni bilo spremenjeno. Prosimo, poskusite znova.',
    
];