@extends('layouts.app')

@section('content')

@include('layouts.header-auth')
<div class="wrapper">
	<div class="betitle">
		<div class="row">
    		<h3 class="betitle">
    			@lang('forms.messages')
    		</h3>
    		<img src="{{ asset('images/pixel.png') }}" alt="@lang('forms.messages')" />
    	</div>
	</div>
</div>

<div class="wrapper">
    <!-- Current modules in itinerary -->
    <table class="table table-striped itinerarymodule-table">
        <thead>
            <th>@lang('forms.itineraryname'): {{ $itineraryname }}</th>
            <th>@lang('forms.messages')</th>
        </thead>
        <tbody>
            @if (count($itinerarymodules) > 0)
        
                <form action="{{url('modulesitidate/' . $itineraryid)}}" method="POST">
                    {{ csrf_field() }}
                    {{ method_field('POST') }}
                            
                    @foreach ($itinerarymodules as $itinerarymodule)
				        @php
                    		$parentModuleId = $itinerarymodule->idModule;
                    	@endphp                    	
                        <tr>
                            <td class="table-text col-md-4">
                                <div>
                                    <i class="fas fa-circle status1-{{ $itinerarymodule->status }}"></i> &nbsp;
                                    @if ($userlanguage == 'en')
                                    	{{ $moduleParent[$parentModuleId]->modulename }}
                                    @else
                                    	{{ $moduleChild[$parentModuleId]->modulename }}
                                    @endif
									<br />
									{{ $itinerarymodule->dateFrom }} - {{ $itinerarymodule->dateTo }}
                                </div>
                            </td>
                            
                        	@if ($moduleParent[$parentModuleId]->enabled == '0')
                            	<td>@lang('forms.disabledadmin')</td>
                       		@else 

                       		@endif
                                <!-- Itinerarymodule Messages -->
                                <td class="table-text col-md-8">
                                	@php
                                    		$itiModuleId = $itinerarymodule->id;
                                	@endphp

                                    @if (isset($messages[$itiModuleId]))
                                        @php
                                    		$messagesitimods = $messages[$itiModuleId];
                                    	@endphp
      									
                                    	@foreach ($messagesitimods as $messagesitimod)
                                    		- {{ $messagesitimod->message }}<br />
                                    		@php
                                    			$date = new DateTime($messagesitimod->created_at);
                                    			$date = $date->format('d. m. Y H:i');
                                    		@endphp
                                    		<p class="notes">&nbsp;&nbsp;@lang('texts.sent'): {{ $date }}</p>
                                    		
                                    	@endforeach
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </form>    
            @else
                <tr>
                    <td colspan='2'>
                       @lang('forms.noitinerarymodules')
                    </td>
                </tr>
            
            @endif
        
		</tbody>
    </table>
</div>

@endsection


