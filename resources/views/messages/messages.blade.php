@extends('layouts.app')

@section('content')

@include('layouts.header-small')

<div class="wrapper">
	<div class="pagetitle">
		<div class="row">
    		<h4 class="pagetitle">&nbsp;</h3>
    		<h3 class="pagetitle">@lang('texts.providerresponse2')</h3>
    		<img src="{{ asset('images/pixel.png') }}" alt="@lang('forms.providerresponse2')" />
    		<h5 class="pagetitle">&nbsp;</h3>
    	</div>
	</div>
</div>

<div class="container">
    <div class="row">
        @if (isset($messages) AND $messages <> '')
        
                <div class="col-md-8 col-md-offset-2">
                    <div class="panel-group">
                        <div class="alert alert-success" role="alert">
                            @lang('texts.responsesent')
                        </div>
                    </div>
                </div>
        
        @elseif (isset($errors) AND $errors  <> '')
        
                <div class="col-md-8 col-md-offset-2">
                    <div class="panel-group">
                        <div class="alert alert-danger" role="alert">
                            {{ $errors }}
                        </div>
                    </div>
                </div>
        
        @else
                    <div class="col-md-3">&nbsp;</div>
                    <div class="col-md-6">
                   		<p><img src="{{ asset('images/alert.png') }}" alt="@lang('forms.providerresponse2')" style="vertical-align: text-bottom" />&nbsp;
            			@if($answer == 'confirm')
                              @lang('texts.abouttoconfirm'):
                		@elseif($answer == 'reject')
                              @lang('texts.abouttoreject'):
                		@endif
                		</p>
            			<div class="panel panel-default">
              				<div class="panel-body">
                                <b>@lang('texts.agencyname'):</b> {{ $agency['title'] }}<br/>
                                <b>@lang('forms.modulename'):</b> {{ $module['modulename'] }}<br/>
                                <b>@lang('forms.description'):</b> {{ $module['description'] }}<br/>
                                <b>@lang('forms.daterange'):</b> {{ $dateFrom }} - {{ $dateTo }}<br/>
                            </div>
            			</div>
                		
                		<form action="{{url('msg/' . $answer . '/' . $token)}}" method="POST">
                            {{ csrf_field() }}
                            {{ method_field('POST') }}
                            
                            <!-- Long Description -->
                            <p>@lang('forms.msgtoagency')</p>
                            <p>
                               <textarea class="form-control" rows="5" name="message"></textarea>
                            </p>
                            @if($answer == 'confirm')
                                <button type="submit" id="get-itinerarymodule" class="btn btn-info" value="confirm" name="button">
                                    <i class="fas fa-check"></i>&nbsp;@lang('texts.confirm')  
                                </button>
                    		@elseif($answer == 'reject')
                                <button type="submit" id="get-itinerarymodule" class="btn btn-danger" value="reject" name="button">
                                    <i class="fa fa-times"></i>&nbsp;@lang('texts.reject')  
                                </button>
                    		@endif
                            
                        </form>
                        
                		@if($answer != 'confirm' AND $answer != 'reject')
                		    <div class="alert alert-danger">
                              @lang('texts.miskakeinresponse')
                            </div>
                		@endif
            		</div>
            		<div class="col-md-3">&nbsp;</div>
        @endif
    </div>
</div>
<br /><br />
@endsection