@extends('layouts.app')

@section('content')

@include('layouts.header-auth')
<div class="wrapper">
	<div class="betitle">
		<div class="row">
    		<h3 class="betitle">
    			@lang('forms.addmodule')
    		</h3>
    		<img src="{{ asset('images/pixel.png') }}" alt="@lang('forms.addmodule')" />
    	</div>
	</div>
</div>

<div class="wrapper">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
        <b>@lang('forms.itinerary') : {{ $itineraryname }}</b>

        <table class="table table-striped module-table">
            <thead>
            </thead>
            <tbody>
    			@foreach ($modules as $module)
			        @if ($userlanguage == 'en')
                    	@php
                    		$image = $module->mainimage;
                    	@endphp
        			 @else
                		 @foreach ($mainimages as $key => $value)
                            @if (($value->idModule ==  $module->idModule) and ($value->user_id ==  $module->user_id) )                    	
        						@php
                            		$image = $value->mainimage;
                            		$parentid = $value->id;
                            	@endphp
                    		@endif
        				@endforeach
        			 @endif	                    	
                    <tr>
                        <td class="table-text">
                        	<div><b>{{ $module->modulename }}</b>
                            	<div class="tags">
        							@if (isset($categories[$module->id]) or isset($parentid))
        
        								@if (isset($categories[$module->id]))
        									@php 
        										$categories1 = $categories[$module->id];
        									@endphp
        								@elseif(isset($parentid))
        									@php 
        										$categories1 = $categories[$parentid];
        									@endphp
        								@endif
        									
        								@foreach ($categories1 as $category)
        									<span class="tag tag0{{ $category->idCategory }}"></span>
        									
        							
        								@endforeach
        							@endif
        						</div>
                        	</div>
                        	<p><br />{{ $module->description }}</p>
                        	<!-- More info on module Button -->
                        	<div><a href="{{ url('offer/'.$module->id) }}" target="_blank"><i class="fas fa-info-circle"></i></a></div>
                        </td>

                        <td>
                            <img src='{{ asset('upload_media/modules/'.$image) }}' height="100px" alt="{{ $itineraryname }}" />
                            
                        </td>
                                                                
                        <!-- Module Add to itinerary Button -->                                        
                        <td>
                            <form action="{{ url('modulesiti') }}" enctype="multipart/form-data"  method="POST" class="form-horizontal" role="form">
                                {!! csrf_field() !!}
               	
                            	<input type="hidden" name="itineraryid" value="{{$itineraryid}}">
                            	<input type="hidden" name="itiparentid" value="{{ $itiparentid}}">
                            	<input type="hidden" name="moduleid" value="{{ $module->id}}">
                            	@if(!isset($parentid))
									@php 
										$parentid = $module->id;
									@endphp
								@endif
                            	<input type="hidden" name="parentid" value="{{ $parentid}}">
                                        
                                <!-- Add Image Button -->
                                    	<button type="submit" class="btn btn-primary">
                                            <i class="fa fa-btn fa-plus"></i>&nbsp;@lang('forms.add')
                                        </button>
                            </form>
                        </td>
                     </tr>
                     @php
						unset($parentid);
					@endphp
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
