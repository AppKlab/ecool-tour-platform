@extends('layouts.app')

@section('content')

@include('layouts.header-auth')
<div class="wrapper">
	<div class="betitle">
		<div class="row">
    		<h3 class="betitle">
    			@lang('forms.itinerarymodules')
    		</h3>
    		<img src="{{ asset('images/pixel.png') }}" alt="@lang('forms.itinerarymodules')" />
    	</div>
	</div>
</div>

<div class="wrapper">
        <!-- Current modules in itinerary -->
        <table class="table table-striped itinerarymodule-table">
                <thead>
                    <th>@lang('forms.itineraryname'): {{ $itineraryname }}</th>
                    <th>&nbsp;</th>
                    <th>@lang('forms.daterange')</th>
                    <th>@lang('forms.status')</th>
                    <th>@lang('forms.delete')</th>
                </thead>
                <tbody>
                    @php
                		$confirmflag = 'disabled';
                	@endphp
                    @if (count($itinerarymodules) > 0)
                
	                    <form action="{{url('modulesitidate/' . $itineraryid)}}" method="POST">
                            {{ csrf_field() }}
                            {{ method_field('POST') }}
		                            
                            @foreach ($itinerarymodules as $itinerarymodule)
						        @php
                            		$parentModuleId = $itinerarymodule->idModule;
                            	@endphp                    	
                                <tr>
                                    <td class="table-text">
                                        <div>
                                            @if ($userlanguage == 'en')
                                            	{{ $moduleParent[$parentModuleId]->modulename }}
                                            @else
                                            	{{ $moduleChild[$parentModuleId]->modulename }}
                                            @endif
                                            <div class="tags">
                								@if (isset($categories[$parentModuleId]))
                    								@foreach ($categories[$parentModuleId] as $category)
                    									<span class="tag tag0{{ $category->idCategory }}"></span>
                    								@endforeach
                    							@endif
                    						</div>
                                        </div>
                                    </td>
                                    
                                	@if ($moduleParent[$parentModuleId]->enabled == '0')
                                    	<td>@lang('forms.disabledadmin')</td>
                               		@else 
                                   		<td class="table-text">
                                                <img src='{{ asset('upload_media/modules/'.$moduleParent[$parentModuleId]->mainimage) }}' height="100px" width="133px" alt="{{ $itineraryname }}" />
                                        </td>
                               		@endif
                                    	<!-- Itinerarymodule Date/time -->
                                        <td>
    										@if ($itinerarymodule->status > 1 AND $itinerarymodule->status <> 3)
        										@php
                                            		$addflag = 'disabled';
                                            	@endphp
                                        	@endif
                                        	
    										@if ($itinerarymodule->status == '0' or $itinerarymodule->status == '1' or $itinerarymodule->status == '3') 
                                                    <input type="text" class="form-control" name="daterange-{{ $itinerarymodule->id }}" value="{{ null !==old('daterange') ? old('daterange') : ''  }}"/>
                        							@if ($itinerarymodule->dateFrom != 'none' AND $itinerarymodule->dateTo != 'none')
                        								<script>
                                                            $(function() {
                                                              $('input[name="daterange-{{ $itinerarymodule->id }}"]').daterangepicker();
                                                              $('input[name="daterange-{{ $itinerarymodule->id }}"]').daterangepicker({
                                                            	    timePicker: true,
                                                            	    startDate: '{{ $itinerarymodule->dateFrom }}',
                                                       	    		endDate: '{{ $itinerarymodule->dateTo }}',
                                                       	    		minDate: '{{ $itineraryFrom }}',
                                                       	    	    maxDate: '{{ $itineraryTo }}',
                                                            	    locale: {
                                                            	      format: 'DD/MM/YYYY H:mm'
                                                            	    }
                                                            	});
                                                            });
                                                            
                                                        </script>
                        							@else
                        								<div class="notes">*@lang('forms.noteondate')</div>
                        								<script>
                                                            $(function() {
                                                              $('input[name="daterange-{{ $itinerarymodule->id }}"]').daterangepicker();
                                                              $('input[name="daterange-{{ $itinerarymodule->id }}"]').daterangepicker({
                                                            	    timePicker: true,
                                                            	    startDate: '{{ $itineraryFrom }}',
                                                       	    		endDate: '{{ $itineraryTo }}',
                                                       	    		minDate: '{{ $itineraryFrom }}',
                                                       	    	    maxDate: '{{ $itineraryTo }}',
                                                            	    locale: {
                                                            	      format: 'DD/MM/YYYY H:mm'
                                                            	    }
                                                            	});
                                                            });
                                                            
                                                        </script>
                                                         
                        							@endif
                    							
                    							
                                                    @if ($errors->has('day1'))
                                                        <span class="help-block">
                                                            <strong>{{ $errors->first('day1') }}</strong>
                                                        </span>
                                                    @endif
                                                    @if ($errors->has('time1'))
                                                        <span class="help-block">
                                                            <strong>{{ $errors->first('time1') }}</strong>
                                                        </span>
                                                    @endif
                                     		@else
                                     			{{ $itinerarymodule->dateFrom }} - {{ $itinerarymodule->dateTo }}
                                     		@endif
                                            
                                        </td>
                                        <!-- Status -->
                                        <td>
                                        <div>
                                        <a href="#" data-toggle="tooltip" title="@lang('texts.status'.$itinerarymodule->status.'m')"><i class="fas fa-circle status1-{{ $itinerarymodule->status }}"></i></a></div>
                                        </td>
                                        <!-- Itinerarymodule Delete Button -->
                                        <td>
                                            @if($itinerarymodule->status  == '0' OR $itinerarymodule->status  == '1' OR  $itinerarymodule->status  == 3)
                                  				<a href="{{url('modulesitid/' . $itinerarymodule->id)}}" onClick="return confirm('@lang('forms.delitinerarymoduleconfirmation')');"><i class="fa fa-btn fa-trash"></i></a>
                                  			@else
                                  				<i class="fas fa-ban"></i>
                                  			@endif
                                  			@if($itinerarymodule->status  <= 3)
                                  				@php
                                            		$confirmflag = '';
                                            	@endphp
                                            @endif
                                        </td>
                                       	
										
											
                                        </tr>
                                    @endforeach
                                    <tr><td colspan="5" align="right">
							   		@if($itinerarystatus < 5)
    							   		
            							    <button type="submit" id="get-itinerarymodule-{{ $itineraryid }}" class="btn btn-info" value="Draft" name="button">
                                                <i class="fab fa-firstdraft"></i>&nbsp;@lang('forms.savedraft')  
                                            </button>
                                      	
                                  	@endif
                                  	@if($itinerarystatus < 5)
    										@php 
    											if (!isset($confirmflag)) $confirmflag = '';
                                     		@endphp
                                            <button type="submit" id="get-itinerarymodule-{{ $itineraryid }}" class="btn btn-info" value="Itinerary" name="button" {{ $confirmflag }}>
                                                <i class="fas fa-check"></i>&nbsp;@lang('forms.itiok')  
                                            </button>
                                  	@endif	
                                  	</td></tr>
                            </form>    
            @else
                <tr>
                    <td colspan='5'>
                       @lang('forms.noitinerarymodules')
                    </td>
                </tr>
            
            @endif
            
				</tbody>
            </table>


                    @if ($itinerarystatus < 5)
                    
                        <form action="{{url('modulesitinew/' . $itineraryid . '')}}" method="POST">
                            {{ csrf_field() }}
                            {{ method_field('GET') }}
    
                            <button type="submit" id="get-itinerarymodule-{{ $itineraryid }}" class="btn btn-info" >  
                                <i class="fa fa-btn fa-plus"></i>&nbsp;@lang('forms.addmodule')  
                            </button>
                        </form>
                        <br/><br/>
                    @endif
                    
<table style="width:100%;">
        <tr class="smalltext2">
            <td colspan="17">@lang('texts.statusmodule')</td>
        </tr>
        <tr>
            <td align="center" width="6.66%"><a href="#" data-toggle="tooltip" title="@lang('texts.status1m')"><i class="fas fa-circle status1-1"></i></a></td>
            <td align="center" width="6.66%"><img src="{{ asset('images/arrow.png') }}" alt="arrow" width="50px" /></td>
            <td align="center" width="6.66%"><a href="#" data-toggle="tooltip" title="@lang('texts.status2m')"><i class="fas fa-circle status1-2"></i></a></td>
            <td align="center" width="6.66%"><img src="{{ asset('images/arrow.png') }}" alt="arrow" width="50px" /></td>
            <td align="center" width="6.66%"><a href="#" data-toggle="tooltip" title="@lang('texts.status3m')"><i class="fas fa-circle status1-3"></i></a></td>
            <td align="center" width="6.66%"><img src="{{ asset('images/arrow.png') }}" alt="arrow" width="50px" /></td>
            <td align="center" width="6.66%"><a href="#" data-toggle="tooltip" title="@lang('texts.status4m')"><i class="fas fa-circle status1-4"></i></a></td>
            <td align="center" width="6.66%"><img src="{{ asset('images/arrow.png') }}" alt="arrow" width="50px" /></td>
            <td align="center" width="6.66%"><a href="#" data-toggle="tooltip" title="@lang('texts.status5m')"><i class="fas fa-circle status1-5"></i></a></td>
            <td align="center" width="6.66%"><img src="{{ asset('images/arrow.png') }}" alt="arrow" width="50px" /></td>
            <td align="center" width="6.66%"><a href="#" data-toggle="tooltip" title="@lang('texts.status6m')"><i class="fas fa-circle status1-6"></i></a></td>
            <td align="center" width="6.66%"><img src="{{ asset('images/arrow.png') }}" alt="arrow" width="50px" /></td>
            <td align="center" width="6.66%"><a href="#" data-toggle="tooltip" title="@lang('texts.status7m')"><i class="fas fa-circle status1-7"></i></a></td>
            <td align="center" width="6.66%"><img src="{{ asset('images/arrow.png') }}" alt="arrow" width="50px" /></td>
            <td align="center" width="6.66%"><a href="#" data-toggle="tooltip" title="@lang('texts.status8m')"><i class="fas fa-circle status1-8"></i></a></td>
        </tr>
    </table>
</div>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  
  <script>
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();   
});
</script>

@endsection


