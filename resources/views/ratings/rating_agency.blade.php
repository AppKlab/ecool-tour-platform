@extends('layouts.app')

@section('content')

@include('layouts.header-auth')

<div class="wrapper">
	<div class="betitle">
		<div class="row">
    		<h3 class="betitle">
    			@lang('texts.ratingform')
    		</h3>
    		<img src="{{ asset('images/pixel.png') }}" alt="@lang('texts.ratingform')" />
    	</div>
	</div>
</div>

<div class="wrapper">
    <br/>
    @if (isset($rating) AND $rating == 'ok')
    	<p class="pagetext"><b>@lang('texts.ratingsent')</b></p>
    	<p class="pagetext">@lang('texts.pleaseevaluate')</p>
    @elseif (isset($ratingexists) AND $ratingexists == 'exists')
    	<p class="pagetext"><b>@lang('texts.ratingexistsa')</b></p>
    @else
        <p class="pagetext"><b>@lang('forms.itineraryname'):</b>
        @if ($userlanguage == 'en')
        	{{ $itinerary->itineraryname }}
        @else
        	{{ $itinerarylang->itineraryname }}
        @endif
        </p>
        <p class="pagetext"><b>@lang('texts.ratingforma'):</b></p>
        <form action="{{ url( url()->current() ) }}" method="POST" class="form-horizontal" role="form">
			{{ csrf_field() }}
			<input id="agencyId" name="agencyId" type="hidden" value="{{ $agency->id }}">
        	<input id="itineraryId" name="itineraryId" type="hidden" value="{{ $itinerary->id }}">
        	<input id="token" name="token" type="hidden" value="{{ $token }}">
        	<hr />
        	@foreach ($itimodules as $itimodule)
                <p>
                    <b>@lang('texts.providername'):</b> {{ $providers[$itimodule->id]->title }}<br/>
                    <b>@lang('forms.modulename'):</b> {{ $modules[$itimodule->id]->modulename }}<br/>
                    <b>@lang('forms.daterange'):</b> {{ $dateFrom[$itimodule->id] }} - {{ $dateTo[$itimodule->id] }}
                </p>
            	<br />
                <div class="row">
                	<div class="col-md-8">
                		<br/>@lang('texts.ratingquestiona1')
                	</div>
                	<div class="col-md-4 form-group{{ $errors->has('question1['.$itimodule->id.']') ? ' has-error' : '' }}">
                        <input class="star star-5" id="star-5-1-{{ $itimodule->id }}" type="radio" name="question1[{{ $itimodule->id }}]" value="5" required/>
                        <label class="star star-5 " for="star-5-1-{{ $itimodule->id }}"></label>
                        <input class="star star-4" id="star-4-1-{{ $itimodule->id }}" type="radio" name="question1[{{ $itimodule->id }}]" value="4" />
                        <label class="star star-4" for="star-4-1-{{ $itimodule->id }}"></label>
                        <input class="star star-3" id="star-3-1-{{ $itimodule->id }}" type="radio" name="question1[{{ $itimodule->id }}]" value="3" />
                        <label class="star star-3" for="star-3-1-{{ $itimodule->id }}"></label>
                        <input class="star star-2" id="star-2-1-{{ $itimodule->id }}" type="radio" name="question1[{{ $itimodule->id }}]" value="2" />
                        <label class="star star-2" for="star-2-1-{{ $itimodule->id }}"></label>
                        <input class="star star-1" id="star-1-1-{{ $itimodule->id }}" type="radio" name="question1[{{ $itimodule->id }}]" value="1" />
                        <label class="star star-1" for="star-1-1-{{ $itimodule->id }}"></label>
                        @if ($errors->has('question1['.$itimodule->id.']'))
                            <span class="help-block">
                                <strong>{{ $errors->first('question1['.$itimodule->id.']') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="row">
                	<div class="col-md-8">
                		<br/>@lang('texts.ratingquestiona2')
                	</div>
                	<div class="col-md-4 form-group{{ $errors->has('question2['.$itimodule->id.']') ? ' has-error' : '' }}">
                		<input class="star star-5" id="star-5-2-{{ $itimodule->id }}" type="radio" name="question2[{{ $itimodule->id }}]" value="5" required/>
                        <label class="star star-5 " for="star-5-2-{{ $itimodule->id }}"></label>
                        <input class="star star-4" id="star-4-2-{{ $itimodule->id }}" type="radio" name="question2[{{ $itimodule->id }}]" value="4" />
                        <label class="star star-4" for="star-4-2-{{ $itimodule->id }}"></label>
                        <input class="star star-3" id="star-3-2-{{ $itimodule->id }}" type="radio" name="question2[{{ $itimodule->id }}]" value="3" />
                        <label class="star star-3" for="star-3-2-{{ $itimodule->id }}"></label>
                        <input class="star star-2" id="star-2-2-{{ $itimodule->id }}" type="radio" name="question2[{{ $itimodule->id }}]" value="2" />
                        <label class="star star-2" for="star-2-2-{{ $itimodule->id }}"></label>
                        <input class="star star-1" id="star-1-2-{{ $itimodule->id }}" type="radio" name="question2[{{ $itimodule->id }}]" value="1" />
                        <label class="star star-1" for="star-1-2-{{ $itimodule->id }}"></label>
                        @if ($errors->has('question2['.$itimodule->id.']'))
                            <span class="help-block">
                                <strong>{{ $errors->first('question2['.$itimodule->id.']') }}</strong>
                            </span>
                        @endif
                	</div>
                </div>
                <div class="row">
                	<div class="col-md-8">
                		<br/>	@lang('texts.ratingquestiona3')
                	</div>
                	<div class="col-md-4 form-group{{ $errors->has('question3['.$itimodule->id.']') ? ' has-error' : '' }}">
                	    <input class="star star-5" id="star-5-3-{{ $itimodule->id }}" type="radio" name="question3[{{ $itimodule->id }}]" value="5" required/>
                        <label class="star star-5 " for="star-5-3-{{ $itimodule->id }}"></label>
                        <input class="star star-4" id="star-4-3-{{ $itimodule->id }}" type="radio" name="question3[{{ $itimodule->id }}]" value="4" />
                        <label class="star star-4" for="star-4-3-{{ $itimodule->id }}"></label>
                        <input class="star star-3" id="star-3-3-{{ $itimodule->id }}" type="radio" name="question3[{{ $itimodule->id }}]" value="3" />
                        <label class="star star-3" for="star-3-3-{{ $itimodule->id }}"></label>
                        <input class="star star-2" id="star-2-3-{{ $itimodule->id }}" type="radio" name="question3[{{ $itimodule->id }}]" value="2" />
                        <label class="star star-2" for="star-2-3-{{ $itimodule->id }}"></label>
                        <input class="star star-1" id="star-1-3-{{ $itimodule->id }}" type="radio" name="question3[{{ $itimodule->id }}]" value="1" />
                        <label class="star star-1" for="star-1-3-{{ $itimodule->id }}"></label>
                        @if ($errors->has('question3['.$itimodule->id.']'))
                            <span class="help-block">
                                <strong>{{ $errors->first('question3['.$itimodule->id.']') }}</strong>
                            </span>
                        @endif
                	</div>
                </div>
                <hr />
            @endforeach
            <div class="row">
            	<div class="col-md-6">              
             	</div>
             	<div class="col-md-3">              
             	</div>
            	<div class="col-md-3">
            		<br/>
            		<div style="float: right">
            	    <button type="submit" class="btn btn-primary">
                    <i class="far fa-chart-bar"></i>&nbsp;
                        @lang('texts.saverating')   
                     </button>
                     <p class="smalltext2">@lang('texts.alltosaverating')</p>
                     </div>
            	</div>
            </div>
        </form>
    @endif
	<br />
	<br />	
</div>
@endsection