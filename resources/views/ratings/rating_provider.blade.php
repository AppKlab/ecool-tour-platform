@extends('layouts.app')

@section('content')

@include('layouts.header-auth')

<div class="wrapper">
	<div class="betitle">
		<div class="row">
    		<h3 class="betitle">
    			@lang('texts.ratingform')
    		</h3>
    		<img src="{{ asset('images/pixel.png') }}" alt="@lang('texts.ratingform')" />
    	</div>
	</div>
</div>

<div class="wrapper">
    <br/>
    @if (isset($rating) AND $rating == 'ok')
    	<p class="pagetext"><b>@lang('texts.ratingsent')</b></p>
    	<p class="pagetext">@lang('texts.pleaseevaluate')</p>
    @elseif (isset($ratingexists) AND $ratingexists == 'exists')
    	<p class="pagetext"><b>@lang('texts.ratingexistsp')</b></p>
    @else
        <p class="pagetext"><b>@lang('texts.ratingformp'):</b></p>
        <p class="pagetext">@lang('texts.agencyname'): {{ $agency->title }}<br/>
        @lang('forms.modulename'): {{ $module->modulename }}<br/>
        @lang('forms.description'): {{ $module->description }}<br/>
        @lang('forms.daterange'): {{ $itimodule->dateFrom }} - {{ $itimodule->dateTo }}</p>
        	<br />
        <p class="pagetext"><b>@lang('texts.howwouldyou'):</b></p>
        
        
        <form action="{{ url( url()->current() ) }}" method="POST" class="form-horizontal" role="form">
        	{{ csrf_field() }}
        	<input id="agencyId" name="agencyId" type="hidden" value="{{ $agency->id }}">
        	<input id="providerId" name="providerId" type="hidden" value="{{ $provider->id }}">
        	<input id="itimoduleId" name="itimoduleId" type="hidden" value="{{ $itimodule->id }}">
        	<input id="itineraryId" name="itineraryId" type="hidden" value="{{ $itinerary->id }}">
        	<input id="token" name="token" type="hidden" value="{{ $token }}">
            <div class="row">
            	<div class="col-md-8">
            		<br/>@lang('texts.ratingquestion1')
            	</div>
            	<div class="col-md-4 form-group{{ $errors->has('question1') ? ' has-error' : '' }}">
                    <input class="star star-5" id="star-5-1" type="radio" name="question1" value="5" />
                    <label class="star star-5 " for="star-5-1"></label>
                    <input class="star star-4" id="star-4-1" type="radio" name="question1" value="4" />
                    <label class="star star-4" for="star-4-1"></label>
                    <input class="star star-3" id="star-3-1" type="radio" name="question1" value="3" />
                    <label class="star star-3" for="star-3-1"></label>
                    <input class="star star-2" id="star-2-1" type="radio" name="question1" value="2" />
                    <label class="star star-2" for="star-2-1"></label>
                    <input class="star star-1" id="star-1-1" type="radio" name="question1" value="1" />
                    <label class="star star-1" for="star-1-1"></label>
                    @if ($errors->has('question1'))
                        <span class="help-block">
                            <strong>{{ $errors->first('question1') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            <div class="row">
            	<div class="col-md-8">
            		<br/>@lang('texts.ratingquestion2')
            	</div>
            	<div class="col-md-4 form-group{{ $errors->has('question2') ? ' has-error' : '' }}">
            		<input class="star star-5" id="star-5-2" type="radio" name="question2" value="5" />
                    <label class="star star-5 " for="star-5-2"></label>
                    <input class="star star-4" id="star-4-2" type="radio" name="question2" value="4" />
                    <label class="star star-4" for="star-4-2"></label>
                    <input class="star star-3" id="star-3-2" type="radio" name="question2" value="3" />
                    <label class="star star-3" for="star-3-2"></label>
                    <input class="star star-2" id="star-2-2" type="radio" name="question2" value="2" />
                    <label class="star star-2" for="star-2-2"></label>
                    <input class="star star-1" id="star-1-2" type="radio" name="question2" value="1" />
                    <label class="star star-1" for="star-1-2"></label>
                    @if ($errors->has('question2'))
                        <span class="help-block">
                            <strong>{{ $errors->first('question2') }}</strong>
                        </span>
                    @endif
            	</div>
            </div>
            <div class="row">
            	<div class="col-md-8">
            		<br/>	@lang('texts.ratingquestion3')
            	</div>
            	<div class="col-md-4 form-group{{ $errors->has('question3') ? ' has-error' : '' }}">
            	    <input class="star star-5" id="star-5-3" type="radio" name="question3" value="5" />
                    <label class="star star-5 " for="star-5-3"></label>
                    <input class="star star-4" id="star-4-3" type="radio" name="question3" value="4" />
                    <label class="star star-4" for="star-4-3"></label>
                    <input class="star star-3" id="star-3-3" type="radio" name="question3" value="3" />
                    <label class="star star-3" for="star-3-3"></label>
                    <input class="star star-2" id="star-2-3" type="radio" name="question3" value="2" />
                    <label class="star star-2" for="star-2-3"></label>
                    <input class="star star-1" id="star-1-3" type="radio" name="question3" value="1" />
                    <label class="star star-1" for="star-1-3"></label>
                    @if ($errors->has('question3'))
                        <span class="help-block">
                            <strong>{{ $errors->first('question3') }}</strong>
                        </span>
                    @endif
            	</div>
            </div>
            <div class="row">
            	<div class="col-md-6">              
             	</div>
             	<div class="col-md-3">              
             	</div>
            	<div class="col-md-3">
            		<br/>
            		<div style="float: right">
            	    <button type="submit" class="btn btn-primary">
                    <i class="far fa-chart-bar"></i>&nbsp;
                        @lang('texts.saverating')   
                     </button>
                     </div>
            	</div>
            </div>
        </form>
    @endif
	<br />
	<br />	
</div>
@endsection
