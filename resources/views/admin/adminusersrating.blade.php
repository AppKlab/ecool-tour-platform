@extends('layouts.app')

@section('content')

@include('layouts.header-auth')
<div class="wrapper">
	<div class="betitle">
		<div class="row">
    		<h3 class="betitle">
    			@lang('texts.userratings')
    		</h3>
    		<img src="{{ asset('images/pixel.png') }}" alt="@lang('texts.userratings')" />
    	</div>
	</div>
</div>

<div class="wrapper">
        <p><b>@lang('texts.showingratings'):</b> {{ $user->title }}</p>
        <!-- Current Users-->
        @if ($rating1 > 0)
                    <p><b>@lang('texts.ratingsnumber')</b>: {{ $ratings }}</p>
                                
                    <table class="table table-striped user-table">
                        <thead>
                            <th>@lang('texts.question')</th>
                            <th>@lang('texts.rating')</th>
                        </thead>
                        <tbody>
                			@if ($user->userType == 2 OR $user->userType == 4)
                            	 @php
                            	 	$question = 'ratingquestiona';
                            	 @endphp
                            @elseif ($user->userType == 3)
                            	 @php
                            	 	$question = 'ratingquestion';
                            	 @endphp
                            @endif
                            <tr>
                                <td class="table-text"><div>@lang('texts.'.$question.'1')</div></td>
                                <td class="table-text"><div>{{ number_format($rating1, 2) }}</div></td>
                            </tr>
                            <tr>
                                <td class="table-text"><div>@lang('texts.'.$question.'2')</div></td>
                                <td class="table-text"><div>{{ number_format($rating2, 2) }}</div></td>
                            </tr>
                            <tr>
                                <td class="table-text"><div>@lang('texts.'.$question.'3')</div></td>
                                <td class="table-text"><div>{{ number_format($rating3, 2) }}</div></td>
                            </tr>
                            <tr>
                                <td class="table-text"><div><b>@lang('texts.average')</b></div></td>
                                <td class="table-text"><div><b>{{ number_format($avg, 2) }}</b></div></td>
                            </tr>
     	               </tbody>
	                </table>
            @else
            <div class="panel panel-default">
                    <div class="panel-heading">
                        @lang('texts.noratings')
                    </div>
            </div>
            @endif

    </div>
    
@endsection
