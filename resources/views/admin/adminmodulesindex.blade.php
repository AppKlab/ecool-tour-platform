@extends('layouts.app')

@section('content')

@include('layouts.header-auth')
<div class="wrapper">
	<div class="betitle">
		<div class="row">
    		<h3 class="betitle">
    			@lang('forms.allmodules')
    		</h3>
    		<img src="{{ asset('images/pixel.png') }}" alt="@lang('forms.allmodules')" />
    	</div>
	</div>
</div>

<!-- <div class="wrapper container">
    <div class="indexheader" style="float:left">
        <b>@lang('forms.filter'):</b>&nbsp;&nbsp;&nbsp;
        <a href="{{url('Admin/modules/1')}}">@lang('forms.cat1')</a>&nbsp;&nbsp;&nbsp;
        <a href="{{url('Admin/modules/2')}}">@lang('forms.cat2')</a>&nbsp;&nbsp;&nbsp;
        <a href="{{url('Admin/modules/3')}}">@lang('forms.cat3')</a>&nbsp;&nbsp;&nbsp;
        <a href="{{url('Admin/modules')}}">@lang('forms.allmodules')</a>&nbsp;&nbsp;&nbsp;
    </div>
</div>
 -->
<div class="wrapper">
<!-- Current Modules -->
        @if (count($modules) > 0)
                <div class="panel-body">
                    <table class="table table-striped module-table">
                        <thead>
                            <th>@lang('forms.module')</th>
                            <th>@lang('forms.user')</th>
                            <th>@lang('forms.enabled')</th>
                            <th>@lang('forms.mainimage')</th>
                            <th>@lang('forms.delete')</th>
                            <!-- <th>&nbsp;</th>
                            <th>&nbsp;</th>
                            <th>&nbsp;</th>
                            <th>&nbsp;</th>-->
                        </thead>
                        <tbody>
                            @foreach ($modules as $module)
                            	
                                <tr>
                                    <td class="table-text"><div>{{ $module->modulename }}</div></td>
    								@foreach ($users as $usermod)
                                            
                                        @if ($usermod['id'] == $module['user_id'])
                                        
                                        <td>{{ $usermod->title }}</td>
                                        
                                        @endif
                                    @endforeach
    								<!-- Module Enable Button -->
                                    <td>
        									<form action="{{url('Admin/moduleenable/' . $module->id)}}" method="POST">
                                                        {{ csrf_field() }}
                                                        {{ method_field('PATCH') }}
            											@if ($module->enabled)
        													<input type="hidden" name="actionenable" value="disable">
        													<button type="submit" id="get-module-{{ $module->idModule }}-sl" class="btn btn-light">
                                                                <i class="fas fa-toggle-on fa-2x"></i>
                                                            </button>
            											@else
        													<input type="hidden" name="actionenable" value="enable">
        													<button type="submit" id="get-module-{{ $module->idModule }}-sl" class="btn btn-light">
                                                                <i class="fas fa-toggle-off fa-2x"></i>
                                                            </button>
            											@endif
                                                    </form>
                                    </td>
                                    
                                    <!-- Module change mainimage Button -->
                                    <td>
        								<a href="{{url('Admin/moduleimg/' . $module->id)}}"><i class="fa fa-btn fa-edit"></i>&nbsp;@lang('forms.edit')</a>
                                    </td>
                                    
                                    <!-- Module Delete Button -->
                                    <td>
                                        <form onsubmit="return confirm('@lang('forms.deleteconfirmation')');" action="{{url('Admin/module/' . $module->id)}}" method="POST">
                                            {{ csrf_field() }}
                                            {{ method_field('DELETE') }}
    
                                            <button type="submit" id="delete-module-{{ $module->id }}" class="btn btn-danger">
                                                <i class="fa fa-btn fa-trash"></i>
                                            </button>
                                        </form>
                                        
                                    </td>
                                    
                                    
                                    
                                    <!-- Module Edit languages 
                                    		
                                            @foreach ($modulestranslated as $moduletranslated)
                                            
                                                @if ($moduletranslated['idModule'] == $module['idModule'])
													                                                  
                                                  @php 
                                                  $languagesinmodule[] = $moduletranslated['idLang'] 
                                                  @endphp
                                                  
                                                <td>
                                                    <form action="{{url('Admin/module/' . $moduletranslated->id)}}" method="POST">
                                                        {{ csrf_field() }}
                                                        {{ method_field('GET') }}
            
                                                        <button type="submit" id="get-module-{{ $moduletranslated->id }}" class="btn btn-primary">
                                                            <i class="fa fa-btn fa-edit"></i>&nbsp;@lang('forms.edit') ({{ $moduletranslated->idLang }})
                                                        </button>
                                                    </form>
                                                </td>
                                                
                                                @endif
                                                 
                                            @endforeach
                                            
                                           <!-- Module Add languages 
                                            @foreach ($languages as $language)

                                            	@if(!in_array($language['language'], $languagesinmodule))
                                                	<td>
                                                        <form action="{{url('modulenew/' . $module->idModule . '/' . $language['language']. '')}}" method="POST">
                                                            {{ csrf_field() }}
                                                            {{ method_field('GET') }}
                
                                                            <button type="submit" id="get-module-{{ $module->idModule }}-{{ $language['language']}}" class="btn btn-info">
                                                                <i class="fa fa-btn fa-plus"></i>&nbsp;@lang('forms.add') ({{ $language['language'] }})
                                                            </button>
                                                        </form>
                                                    </td>
                                                  @endif
                                            @endforeach

        									@php $languagesinmodule=empty($languagesinmodule) @endphp
        							     -->		
                                        </tr>
                                    @endforeach
                                    
									
                                </tbody>
                            </table>
            @else
            <div class="panel panel-default">
                    <div class="panel-heading">
                        @lang('forms.nomodules')
                    </div>
            </div>
            @endif
			
                                     
</div>
@endsection
