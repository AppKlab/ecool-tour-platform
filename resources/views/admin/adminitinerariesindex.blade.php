@extends('layouts.app')

@section('content')

@include('layouts.header-auth')
<div class="wrapper">
	<div class="betitle">
		<div class="row">
    		<h3 class="betitle">
    			@lang('texts.itineraries')
    		</h3>
    		<img src="{{ asset('images/pixel.png') }}" alt="@lang('texts.itineraries')" />
    	</div>
	</div>
</div>

<div class="wrapper">
        <!-- Current Itineraries -->
        @if (count($itineraries) > 0)
                    <table class="table table-striped itinerary-table">
                        <thead>
                            <th>Status</th>
                            <th>@lang('forms.itinerary')</th>
                           	<th>@lang('forms.public')</th>
                           	<th>@lang('forms.enable')</th>
                            <th><img alt="@lang('forms.english')" src="{{ asset('images/en.svg') }}" height="15px" /></th>
                            <th><img alt="@lang('forms.slovenian')" src="{{ asset('images/sl.svg') }}" height="15px" /></th>
                            <th><img alt="@lang('forms.croatian')" src="{{ asset('images/hr.svg') }}" height="15px" /></th>
                            <th><img alt="@lang('forms.german')" src="{{ asset('images/de.svg') }}" height="15px" /></th>
                            <th>@lang('forms.modules')</th>
                        </thead>
                        <tbody>
                            @foreach ($itineraries as $itinerary)
						        @php
    						        $date1 = new DateTime($itinerary->dateFrom);
    						        $date1 = $date1->format('d. m. Y H:i');
                                    $date2 = new DateTime($itinerary->dateTo);
                                	$date2 = $date2->format('d. m. Y H:i');
						        @endphp                    	
                                <tr>
                                    <td class="table-text">
                                    	<div><a href="#" data-toggle="tooltip" title="@lang('texts.status'.$itinerary->status.'i')"><i class="fas fa-circle status-{{ $itinerary->status }}"></i></a></div>
                                    </td>
                                    <td class="table-text">
                                    	<div>{{ $itinerary->itineraryname }}</div>
                                    	<div class="notes">{{ $date1 }} - {{ $date2 }}</div>
                                    </td>
    
                                            <!-- Itinerary Public Button -->
                                            <td>
                									<form action="{{url('Admin/itinerarypublic/' . $itinerary->id)}}" method="POST">
                                                                
                                                                {{ csrf_field() }}
                                                                {{ method_field('PATCH') }}
                    											@if ($itinerary->public)
                													<input type="hidden" name="actionenable" value="disable">
                													<button type="submit" id="get-itinerary-{{ $itinerary->idItinerary }}-sl" class="btn btn-light">
                                                                        <i class="fas fa-toggle-on fa-2x"></i>
                                                                    </button>
                    											@else
                													<input type="hidden" name="actionenable" value="enable">
                													<button type="submit" id="get-itinerary-{{ $itinerary->idItinerary }}-sl" class="btn btn-light">
                                                                        <i class="fas fa-toggle-off fa-2x"></i>
                                                                    </button>
                    											@endif
                                                            </form>
                                            </td>
                                            
                                            <!-- Itinerary Enable Button -->
                                            <td>
                									<form action="{{url('Admin/itineraryenable/' . $itinerary->id)}}" method="POST">
                                                                
                                                                {{ csrf_field() }}
                                                                {{ method_field('PATCH') }}
                    											@if ($itinerary->enabled)
                													<input type="hidden" name="actionenable" value="disable">
                													<button type="submit" id="get-itinerary-{{ $itinerary->idItinerary }}-sl" class="btn btn-light">
                                                                        <i class="fas fa-toggle-on fa-2x"></i>
                                                                    </button>
                    											@else
                													<input type="hidden" name="actionenable" value="enable">
                													<button type="submit" id="get-itinerary-{{ $itinerary->idItinerary }}-sl" class="btn btn-light">
                                                                        <i class="fas fa-toggle-off fa-2x"></i>
                                                                    </button>
                    											@endif
                                                            </form>
                                            </td>
                                        @foreach ($itinerariestranslated as $itinerarytranslated)

                                            @if ($itinerarytranslated['idItinerary'] == $itinerary['idItinerary'])
												                                                  
                                              @php 

                                                  $languagesinitinerary[] = $itinerarytranslated['idLang'];
                                                  $iditinerarylang[] = $itinerarytranslated['id'];

                                              @endphp
                                              
                                            @endif
                                        @endforeach
                                       @foreach ($languages as $key => $language)
                                        	
                                        	  @if(!in_array($language['language'], $languagesinitinerary))
                                                	<td>
                                                        <i class="fas fa-ban"></i>
                                                    </td>
                                              @else
                                              		@foreach ($itinerariestranslated as $itinerarytranslated)

                                            			@if ($itinerarytranslated['idItinerary'] == $itinerary['idItinerary'] and $itinerarytranslated['idLang'] == $language['language'])
                                                          	<td>
                                                                <a href="{{url('Admin/itinerary/' . $itinerarytranslated->id)}}"><i class="fas fa-eye"></i></i>&nbsp;@lang('forms.view')</a><br />
                                                            </td>
                                                         @endif
                                                    @endforeach
                                              @endif
                                        
                                       	@endforeach
                                       	
                                       	@php $languagesinitinerary=empty($languagesinitinerary) @endphp
                                       	@php $iditinerarylang=empty($iditinerarylang) @endphp
										<!-- Itinerary Modules Button -->
										<td>
                                            <a href="{{url('Admin/modulesiti/' . $itinerary->id)}}"><i class="fas fa-project-diagram"></i></a>
                                        </td>
                                        
											
                                        </tr>
                                    @endforeach
                                    
									
                                </tbody>
                            </table>
            @else
            <div class="panel panel-default">
                <div class="panel-heading">
                    @lang('forms.noitineraries')
                </div>
            </div>
            @endif
            
            
	<table style="width:100%;">
        <tr class="smalltext2">
            <td colspan="17">@lang('texts.statusitinerary')</td>
        </tr>
        <tr>
            <td align="center" width="5.88%"><a href="#" data-toggle="tooltip" title="@lang('texts.status0i')"><i class="fas fa-circle status-0"></i></a></td>
            <td align="center" width="5.88%"><img src="{{ asset('images/arrow.png') }}" alt="arrow" width="50px" width="50px"/></td>
            <td align="center" width="5.88%"><a href="#" data-toggle="tooltip" title="@lang('texts.status1i')"><i class="fas fa-circle status-1"></i></a></td>
            <td align="center" width="5.88%"><img src="{{ asset('images/arrow.png') }}" alt="arrow" width="50px" /></td>
            <td align="center" width="5.88%"><a href="#" data-toggle="tooltip" title="@lang('texts.status2i')"><i class="fas fa-circle status-2"></i></a></td>
            <td align="center" width="5.88%"><img src="{{ asset('images/arrow.png') }}" alt="arrow" width="50px" /></td>
            <td align="center" width="5.88%"><a href="#" data-toggle="tooltip" title="@lang('texts.status3i')"><i class="fas fa-circle status-3"></i></a></td>
            <td align="center" width="5.88%"><img src="{{ asset('images/arrow.png') }}" alt="arrow" width="50px" /></td>
            <td align="center" width="5.88%"><a href="#" data-toggle="tooltip" title="@lang('texts.status4i')"><i class="fas fa-circle status-4"></i></a></td>
            <td align="center" width="5.88%"><img src="{{ asset('images/arrow.png') }}" alt="arrow" width="50px" /></td>
            <td align="center" width="5.88%"><a href="#" data-toggle="tooltip" title="@lang('texts.status5i')"><i class="fas fa-circle status-5"></i></a></td>
            <td align="center" width="5.88%"><img src="{{ asset('images/arrow.png') }}" alt="arrow" width="50px" /></td>
            <td align="center" width="5.88%"><a href="#" data-toggle="tooltip" title="@lang('texts.status6i')"><i class="fas fa-circle status-6"></i></a></td>
            <td align="center" width="5.88%"><img src="{{ asset('images/arrow.png') }}" alt="arrow" width="50px" /></td>
            <td align="center" width="5.88%"><a href="#" data-toggle="tooltip" title="@lang('texts.status7i')"><i class="fas fa-circle status-7"></i></a></td>
            <td align="center" width="5.88%"><img src="{{ asset('images/arrow.png') }}" alt="arrow" width="50px" /></td>
            <td align="center" width="5.88%"><a href="#" data-toggle="tooltip" title="@lang('texts.status8i')"><i class="fas fa-circle status-8"></i></a></td>
        </tr>
    </table>
</div>

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  
  <script>
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();   
});
</script>
    
   
@endsection
