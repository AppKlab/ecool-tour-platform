@extends('layouts.app')

@section('content')

@include('layouts.header-auth')
<div class="wrapper">
	<div class="betitle">
		<div class="row">
    		<h3 class="betitle">
              @lang('forms.itinerary') <img alt="Header" src="{{ asset('images/'.$itinerary->idLang.'.svg') }}" height="15px"  alt="@lang('forms.itinerary')" />    	
    		</h3>
    	</div>
	</div>
</div>

<div class="wrapper">
    	 <form action="#" enctype="multipart/form-data" method="POST" class="form-horizontal" role="form">
        {!! csrf_field() !!}
        <!-- Itinerary Name -->
        <div class="form-group{{ $errors->has('itineraryname') ? ' has-error' : '' }}">
            <label class="col-md-4 control-label">@lang('forms.itineraryname')</label>
    
            <div class="col-sm-6">
                {{ isset($itineraryparent->itineraryname) ? $itineraryparent->itineraryname :  $itinerary->itineraryname }}
            </div>
        </div>
        
        
        <!-- Description -->
        <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
            <label class="col-md-4 control-label">@lang('forms.description')</label>
    
            <div class="col-sm-6">
                {{ isset($itineraryparent->description) ? $itineraryparent->description :  $itinerary->description }}
            </div>
        </div>
        
        <!-- Long Description -->
        <div class="form-group{{ $errors->has('longdescription') ? ' has-error' : '' }}">
            <label class="col-md-4 control-label">@lang('forms.longdescription')</label>
    
            <div class="col-sm-6">
            	{{ isset($itineraryparent->longdescription) ? $itineraryparent->longdescription :  $itinerary->longdescription }}
            </div>
        </div>
        
        <!-- Price Description -->
        <div class="form-group{{ $errors->has('pricedescription') ? ' has-error' : '' }}">
            <label class="col-md-4 control-label">@lang('forms.pricedescription')</label>
    
            <div class="col-sm-6">
                {{ isset($itineraryparent->pricedescription) ? $itineraryparent->pricedescription :  $itinerary->pricedescription }}
            </div>
        </div>
                <!-- Main image -->
                <div class="form-group">
                    <label class="col-md-4 control-label">@lang('forms.mainimage')</label>
    
                    <div class="col-sm-6">
                        @if (isset($itineraryparent->mainimage))
                        	@php $itinerarymainimage = $itineraryparent->mainimage; @endphp
                        @else
                        	@php $itinerarymainimage = $itinerary->mainimage; @endphp
                        @endif
                        <img src='{{ asset('upload_media/itineraries/'.$itinerarymainimage) }}' height="100px" alt="@lang('forms.itinerary')" />
                    </div>
                </div>
                
                <!-- googlemapsUrl 
                <div class="form-group">
                    <label class="col-md-4 control-label">@lang('forms.googlemapsUrl')</label>
    
                    <div class="col-sm-6">
                        {{ isset($itineraryparent->googlemapsUrl) ? $itineraryparent->googlemapsUrl :  $itinerary->googlemapsUrl }}
                    </div>
                </div>-->
                
                <!-- Persons Minimum -->
                <div class="form-group">
                    <label class="col-md-4 control-label">@lang('forms.personsMin')</label>
    
                    <div class="col-sm-6">
                        {{ isset($itineraryparent->personsMin) ? $itineraryparent->personsMin :  $itinerary->personsMin }}
                    </div>
                </div>
                
                <!-- Persons Maximum -->
                <div class="form-group">
                    <label class="col-md-4 control-label">@lang('forms.personsMax')</label>
    
                    <div class="col-sm-6">
                        {{ isset($itineraryparent->personsMax) ? $itineraryparent->personsMax :  $itinerary->personsMax }}
                    </div>
                </div>
                
                <!-- Stay Minimum -->
                <div class="form-group">
                    <label class="col-md-4 control-label">@lang('forms.dateFrom') <i class="far fa-calendar-alt"></i></label>
    
                    <div class="col-sm-6">
                        {{ isset($itineraryparent->dateFrom) ? $itineraryparent->dateFrom :  $itinerary->dateFrom }}
                    </div>
                </div>
                
                <!-- Stay Maximum -->
                <div class="form-group">
                    <label class="col-md-4 control-label">@lang('forms.dateTo') <i class="far fa-calendar-alt"></label></i>
    
                    <div class="col-sm-6">
                        {{ isset($itineraryparent->dateTo) ? $itineraryparent->dateTo :  $itinerary->dateTo }}
                    </div>
                </div>
                
                <!-- Price -->
                <div class="form-group">
                    <label class="col-md-4 control-label">@lang('forms.price') (EUR)</label>
    
                    <div class="col-sm-6">
                        {{ isset($itineraryparent->price) ? $itineraryparent->price :  $itinerary->price }}
                    </div>
                </div>
                
    
        <!-- Add Itinerary Button -->
        
    </form>
    <br/><br/>
</div>    
@endsection
