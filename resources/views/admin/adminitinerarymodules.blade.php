@extends('layouts.app')

@section('content')

@include('layouts.header-auth')
<div class="wrapper">
	<div class="betitle">
		<div class="row">
    		<h3 class="betitle">
    			@lang('forms.itinerarymodules')
    		</h3>
    		<img src="{{ asset('images/pixel.png') }}" alt="@lang('forms.itinerarymodules')" />
    	</div>
	</div>
</div>

<div class="wrapper">
        <!-- Current modules in itinerary -->
        <table class="table table-striped itinerarymodule-table">
                <thead>
                    <th>@lang('forms.itineraryname'): {{ $itineraryname }}</th>
                    <th>&nbsp;</th>
                    <th>@lang('forms.daterange')</th>
                    <th>@lang('forms.status')</th>
                </thead>
                <tbody>
                    @if (count($itinerarymodules) > 0)
                
	                    <form action="{{url('modulesitidate/' . $itineraryid)}}" method="POST">
                            {{ csrf_field() }}
                            {{ method_field('POST') }}
		                            
                            @foreach ($itinerarymodules as $itinerarymodule)
						        @php
                            		$parentModuleId = $itinerarymodule->idModule;
                            	@endphp                    	
                                <tr>
                                    <td class="table-text">
                                        <div>
                                            @if ($userlanguage == 'en')
                                            	{{ $moduleParent[$parentModuleId]->modulename }}
                                            @else
                                            	{{ $moduleChild[$parentModuleId]->modulename }}
                                            @endif
                                            <div class="tags">
                								@if (isset($categories[$parentModuleId]))
                    								@foreach ($categories[$parentModuleId] as $category)
                    									<span class="tag tag0{{ $category->idCategory }}"></span>
                    								@endforeach
                    							@endif
                    						</div>
                                        </div>
                                    </td>
                                    
                                	@if ($moduleParent[$parentModuleId]->enabled == '0')
                                    	<td>@lang('forms.disabledadmin')</td>
                               		@else 
                                   		<td class="table-text">
                                                <img src='{{ asset('upload_media/modules/'.$moduleParent[$parentModuleId]->mainimage) }}' height="100px" width="133px" alt="{{ $itineraryname }}" />
                                        </td>
                               		@endif
                                    	<!-- Itinerarymodule Date/time -->
                                        <td>
                                     			{{ $itinerarymodule->dateFrom }} - {{ $itinerarymodule->dateTo }}
                                        </td>
                                        <!-- Status -->
                                        <td>
                                        <div>
                                        <a href="#" data-toggle="tooltip" title="@lang('texts.status'.$itinerarymodule->status.'m')"><i class="fas fa-circle status1-{{ $itinerarymodule->status }}"></i></a></div>
                                        </td>
                                        </tr>
                                    @endforeach
                            </form>    
            @else
                <tr>
                    <td colspan='5'>
                       @lang('forms.noitinerarymodules')
                    </td>
                </tr>
            
            @endif
            
				</tbody>
            </table>
</div>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  
  <script>
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();   
});
</script>

@endsection


