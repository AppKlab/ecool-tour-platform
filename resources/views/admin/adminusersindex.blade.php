@extends('layouts.app')

@section('content')

@include('layouts.header-auth')
<div class="wrapper">
	<div class="betitle">
		<div class="row">
    		<h3 class="betitle">
    			@lang('forms.users')
    		</h3>
    		<img src="{{ asset('images/pixel.png') }}" alt="@lang('forms.users')" />
    	</div>
	</div>
</div>

<!-- <div class="wrapper container">
    <div class="indexheader" style="float:left">
        <b>@lang('forms.filter'):</b>&nbsp;&nbsp;&nbsp;
        <a href="{{url('Admin/users/2')}}">@lang('auth.agency')</a>&nbsp;&nbsp;&nbsp;
        <a href="{{url('Admin/users/3')}}">@lang('auth.provider')</a>&nbsp;&nbsp;&nbsp;
        <a href="{{url('Admin/users/4')}}">@lang('auth.group')</a>&nbsp;&nbsp;&nbsp;
        <a href="{{url('Admin/users')}}">@lang('forms.allusers')</a>&nbsp;&nbsp;&nbsp;
    </div>
</div>
 -->
<div class="wrapper">
        <!-- Current Users-->
        @if (count($users) > 0)
                    <table class="table table-striped user-table">
                        <thead>
                            <th>@lang('forms.usertype')</th>
                            <th>@lang('forms.user')</th>
                            <th>@lang('forms.title')</th>
                            <th>@lang('forms.enabled')</th>
                            <th>@lang('forms.verified')</th>
                            <th>@lang('forms.ratings')</th>
                            <th>@lang('forms.delete')</th>
                        </thead>
                        <tbody>
                            @foreach ($users as $user)
                            	
                                <tr>
                                    <td class="table-text"><div>
                                    @if ($user->userType == 2)
                                    	 @lang('forms.agency')
                                    @elseif ($user->userType == 3)
                                    	 @lang('forms.provider')
                                	@elseif ($user->userType == 4)
                                	 	@lang('forms.group')
                                    @endif
                                    </div></td>
                                    <td class="table-text"><div>{{ $user->username }}</div></td>
                                    <td class="table-text"><div>{{ $user->title }}</div></td>
    								
    								<!-- User Enable Button -->
                                    <td>
        									<form action="{{url('Admin/userenable/' . $user->id)}}" method="POST">
                                                        {{ csrf_field() }}
                                                        {{ method_field('PATCH') }}
            											@if ($user->enabled)
        													<input type="hidden" name="actionenable" value="disable">
        													<button type="submit" id="get-user-{{ $user->id}}-sl" class="btn btn-light">
                                                                <i class="fas fa-toggle-on fa-2x"></i>
                                                            </button>
            											@else
        													<input type="hidden" name="actionenable" value="enable">
        													<button type="submit" id="get-user-{{ $user->id}}-sl" class="btn btn-light">
                                                                <i class="fas fa-toggle-off fa-2x"></i>
                                                            </button>
            											@endif
                                                    </form>
                                    </td>
                                    
                                    <!-- User Verify Button -->
                                    <td>
        									<form action="{{url('Admin/userverify/' . $user->id)}}" method="POST">
                                                {{ csrf_field() }}
                                                {{ method_field('PATCH') }}
    											@if ($user->status)
													<input type="hidden" name="actionenable" value="disable">
													<button type="submit" id="get-user-{{ $user->id}}-sl" class="btn btn-light">
                                                        <i class="fas fa-toggle-on fa-2x"></i>
                                                    </button>
    											@else
													<input type="hidden" name="actionenable" value="enable">
													<button type="submit" id="get-user-{{ $user->id}}-sl" class="btn btn-light">
                                                        <i class="fas fa-toggle-off fa-2x"></i>
                                                    </button>
    											@endif
                                            </form>
                                    </td>
                                    
                                    <!-- User Ratings -->
                                    <td>
                                        <form action="{{url('Admin/ratings/' . $user->id)}}" method="POST">
                                            {{ csrf_field() }}
												<input type="hidden" name="actionenable" value="enable">
												<button type="submit" id="get-user-{{ $user->id}}-sl" class="btn btn-primary">
                                                    <i class="far fa-chart-bar"></i>
                                                </button>
                                        </form>
                                    </td>
                                    
                                    <!-- User Delete Button -->
                                    <td>
                                        <form onsubmit="return confirm('@lang('forms.deluserconfirmation')');" action="{{url('Admin/user/' . $user->id)}}" method="POST">
                                            {{ csrf_field() }}
                                            {{ method_field('DELETE') }}
    
                                            <button type="submit" id="delete-user-{{ $user->id }}" class="btn btn-danger">
                                                 <i class="fa fa-trash"></i></button>
                                        </form>
                                        
                                    </td>
                                    
                                  </tr>
                                @endforeach
									
                                </tbody>
                            </table>
            @else
            <div class="panel panel-default">
                    <div class="panel-heading">
                        @lang('forms.nousers')
                    </div>
            </div>
            @endif

    </div>
    
@endsection
