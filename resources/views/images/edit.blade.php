@extends('layouts.app')

@section('content')

@include('layouts.header-auth')

<div class="wrapper">
	<div class="betitle">
		<div class="row">
    		<h3 class="betitle">
    			@lang('forms.moduleimages')
    		</h3>
    		<img src="{{ asset('images/pixel.png') }}" alt="@lang('forms.moduleimages')" />
    	</div>
	</div>
</div>

<div class="wrapper">
    	<table class="table table-striped image-table">
            <thead>
                <th>@lang('forms.modulename'): {{ $modulename }}</th>
            </thead>
            <tbody>
            	<tr>
            		<td>
                            <!-- New Image Form -->
                           	<form action="{{ url('image') }}" enctype="multipart/form-data"  method="POST" class="form-horizontal" role="form">
                                {!! csrf_field() !!}
               	
                            	<input type="hidden" name="moduleid" value="{{$moduleid}}">
        
                                <!-- Image -->
                                <div class="form-group{{ $errors->has('image') ? ' has-error' : '' }}">
                                    <label class="col-md-4 control-label">@lang('forms.addimage')</label>
        
                                    <div class="col-sm-6">
                                        <input type="file" name="image" value="{{ old('image') }}">
                                        
                                        @if ($errors->has('image'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('image') }}</strong>
                                            </span>
                                        @endif
                                        
                                    </div>
                                </div>
                                        
                                <!-- Add Image Button -->
                                <div class="form-group">
                                    <div class="col-md-4">
                                    </div>
                                    <div class="col-sm-6">
                                    	<button type="submit" class="btn btn-primary">
                                            <i class="fa fa-btn fa-save"></i>&nbsp;
                                                	@lang('forms.saveform')
                                        </button>
                                    </div>
                                </div>
                            </form>
            		</td>
            	</tr>
        	</tbody>
        </table>
</div>
@endsection
