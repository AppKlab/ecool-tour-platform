@extends('layouts.app')

@section('content')

@include('layouts.header-auth')

<div class="wrapper">
	<div class="betitle">
		<div class="row">
    		<h3 class="betitle">
    			@lang('forms.moduleimages')
    		</h3>
    		<img src="{{ asset('images/pixel.png') }}" alt="@lang('forms.moduleimages')" />
    	</div>
	</div>
</div>


<div class="wrapper">
        <!-- Current Images -->
        <table class="table table-striped image-table">
            <thead>
                <th>@lang('forms.modulename'): {{ $modulename }}</th>
                <th>@lang('forms.delete')</th>
            </thead>
            <tbody>
                @if (count($images) > 0)
            
                    @foreach ($images as $image)
				                            	
                        <tr>
                            <td class="table-text">
                                <div>
                                    <img src='{{ asset('upload_media/images_thumbs/'.$image->imagename) }}' alt="{{ $modulename }}" />
                                </div>
                            </td>

                        	@if ($image->enabled == '0')
                            	<td>@lang('forms.disabledadmin')</td>
                       		@else 
                            
                                <!-- Image Delete Button -->
                                <td>
                                    <a href="{{url('imaged/' . $image->id)}}" onClick="return confirm('@lang('forms.delimageconfirmation')');"><i class="fa fa-btn fa-trash"></i></a>
                                </td>
                               	
								@endif
									
                                </tr>
                            @endforeach
                            
							
                        
            @else
                <tr>
                    <td colspan='2'>
                       @lang('forms.noimages')
                    </td>
                </tr>

            @endif
            </tbody>
          </table>
            @if (count($images) < 5)
                <form action="{{url('imagenew/' . $moduleid . '')}}" method="POST">
                    {{ csrf_field() }}
                    {{ method_field('GET') }}

                    <button type="submit" id="get-image-{{ $moduleid }}" class="btn btn-info">
                        <i class="fa fa-btn fa-plus"></i>&nbsp;@lang('forms.add')  
                    </button>
                </form>
            @else
            	@lang('forms.fiveimages')
            @endif
            <br/><br/>
    </div>
    
@endsection
