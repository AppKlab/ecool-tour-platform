@extends('layouts.app')

@section('content')

@include('layouts.header-auth')

<div class="wrapper">
	<div class="betitle">
		<div class="row">
    		<h3 class="betitle">
    			@lang('texts.welcomemsg') 
	    		<img src="{{ asset('images/pixel.png') }}" alt="@lang('texts.welcomemsg')" />    	
    		</h3>
    	</div>
	</div>
</div>

<div class="wrapper">
    <div class="panel-group">
        <br />
        @if (session('status'))
            <div class="alert alert-success" role="alert">
                {{ session('status') }}
            </div>
        @endif
			
        @if ($userType == 1)
        	@lang('texts.welcomeadmin')
        @elseif ($userType == 2)
        	@lang('texts.welcomeagency')
        @elseif ($userType == 3)
        	@lang('texts.welcomeprovider')
        @elseif ($userType == 4)
        	@lang('texts.welcomegroup')
        @endif
   </div>
</div>
@endsection
