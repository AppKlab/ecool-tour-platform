<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    
    <meta name="revisit" content="7 Days" />
    <meta name="robots" content="index,follow" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no"/>
    
    <title>Site title</title>
    
    <!-- Include Favicon -->	
	<link rel="shortcut icon" type="image/ico" href="{{ asset('images/favicon.ico') }}">
	

	<!-- Fonts & styles -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700" rel='stylesheet' type='text/css'>
		
	<!-- Bootstrap -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.css" rel="stylesheet">
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
        

    <!-- Material Design Bootstrap -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/mdb.min.css" rel="stylesheet">
    
	<!-- Custom css -->
	<link rel="stylesheet" href="/css/styles.css" />
	
</head>
<body>
 	@include('layouts.topmenu')
 	@include('layouts.header-error')

    <div class="wrapper">
    	<div class="pagetitle">
    		<div class="row">
        		<h4 class="pagetitle">
    		      &nbsp;
        		</h4>
        		<h3 class="pagetitle">@lang('errors.whoops')</h3>
        		<img src="{{ asset('images/pixel.png') }}" alt="@lang('errors.whoops')" />
        	</div>
    	</div>
    </div>
    <br />
	<br />
	<div class="wrapper">
    	<div class="pagetext">
    		{{ $error }}
    	</div>
    </div>
	<br />
	<br />
	@include('layouts.footer')

</body>
</html>