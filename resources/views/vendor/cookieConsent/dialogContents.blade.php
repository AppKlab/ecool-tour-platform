<div class="js-cookie-consent cookie-consent cookie-bar">

    <span class="cookie-consent__message">
        {!! trans('cookieConsent::texts.message') !!}
    </span>

    <button class="js-cookie-consent-agree cookie-consent__agree btn btn-success btn-cookie">
        {{ trans('cookieConsent::texts.agree') }}
    </button>

</div>
<div class="nomargin"><hr/></div>
