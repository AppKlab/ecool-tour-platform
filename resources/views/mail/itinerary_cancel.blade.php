@extends('layouts.app-mail')

@section('content')

@include('layouts.header-mail')
<div class="bodytext">
    <h3>@lang('texts.agencycancel')</h3>
    @lang('texts.agencyname'): {{ $agency }}<br/>
    @lang('forms.modulename'): {{ $modulename }}<br/>
    @lang('forms.description'): {{ $description }}<br/>
    @lang('forms.daterange'): {{ $daterange }}<br/>
    <br/>
    @lang('texts.moreinfocontact')
</div>
@endsection