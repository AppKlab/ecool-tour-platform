@extends('layouts.app-mail')

@section('content')

@include('layouts.header-mail')
<div class="bodytext">
	@if ($usertype == 'provider')
		<h3>@lang('texts.ratingmailp')</h3>
		@lang('texts.register3'):
	@else
		<h3>@lang('texts.ratingmaila')</h3>
		@lang('texts.register2'):
	@endif
     {{ $user}}<br/>
    @lang('forms.modulename'): {{ $modulename }}<br/>
    @lang('forms.description'): {{ $description }}<br/>
    @lang('forms.daterange'): {{ $daterange }}<br/>
    <br/>
    
    <table>
        <tr>
            <td><b>@lang('texts.question')</b></td>
            <td><b>@lang('texts.rating')</b></td>
        </tr>
        <tbody>
            <tr>
                <td class="table-text"><div>
                	@if ($usertype == 'provider')
                		@lang('texts.ratingquestion1')
                	@else
                		@lang('texts.ratingquestiona1')
                	@endif
                	</div></td>
                <td class="table-text"><div>{{ number_format($answer1, 2) }}</div></td>
            </tr>
            <tr>
                <td class="table-text"><div>
					@if ($usertype == 'provider')
                		@lang('texts.ratingquestion2')
                	@else
                		@lang('texts.ratingquestiona2')
                	@endif
					</div></td>
                <td class="table-text"><div>{{ number_format($answer2, 2) }}</div></td>
            </tr>
            <tr>
                <td class="table-text"><div>
					@if ($usertype == 'provider')
                		@lang('texts.ratingquestion3')
                	@else
                		@lang('texts.ratingquestiona3')
                	@endif
                	</div></td>
                <td class="table-text"><div>{{ number_format($answer3, 2) }}</div></td>
            </tr>
            <tr>
                <td class="table-text"><div><b>@lang('texts.average')</b></div></td>
                <td class="table-text"><div><b>{{ number_format($avg, 2) }}</b></div></td>
            </tr>
       </tbody>
    </table>
</div>
@endsection