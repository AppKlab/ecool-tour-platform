@extends('layouts.app-mail')

@section('content')

@include('layouts.header-mail')
<div class="bodytext">
    <h3>@lang('texts.haveregistered')</h3>
    @lang('texts.linkverify'): <a href="{{ url('/verify/' . $activation_code) }}">{{ url('/verify/' . $activation_code) }}</a>
</div>
@endsection