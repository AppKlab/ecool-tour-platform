@extends('layouts.app-mail')

@section('content')

@include('layouts.header-mail')
<div class="bodytext">
    <h3>@lang('texts.pleaseevaluate')</h3>
    <p>@lang('texts.pleaseevaluateprovider')</p>
    @lang('forms.itineraryname'): {{ $itineraryname }}<br/>
    @lang('forms.description'): {{ $description }}<br/>
    @lang('forms.daterange'): {{ $daterange }}<br/>
    <br/>
    <a href="{{ url('/ratinga/' . $token) }}">{{ url('/ratinga/' . $token) }}</a><br/>
</div>
@endsection