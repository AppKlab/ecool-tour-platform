@extends('layouts.app-mail')

@section('content')

@include('layouts.header-mail')
<div class="bodytext">
    <h3>@lang('texts.pleaseevaluate')</h3>
    <p>@lang('texts.pleaseevaluateagency')</p>
    @lang('texts.agencyname'): {{ $agency }}<br/>
    @lang('forms.modulename'): {{ $modulename }}<br/>
    @lang('forms.description'): {{ $description }}<br/>
    @lang('forms.daterange'): {{ $daterange }}<br/>
    <br/>
    <a href="{{ url('/rating/' . $token) }}">{{ url('/rating/' . $token) }}</a><br/>
</div>
@endsection