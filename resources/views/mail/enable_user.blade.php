@extends('layouts.app-mail')

@section('content')

@include('layouts.header-mail')
<div class="bodytext">
    <h3>@lang('texts.newuser')</h3>
    @lang('forms.title'): {{ $username }}<br/>
    @lang('forms.usertype'): @lang('texts.register'.$userType)<br/>
    @lang('forms.email'): {{ $email }}<br/>
    @lang('forms.address'): {{ $address }}<br/>
    @lang('texts.linkenable'): <a href="{{ url('/enable/' . $enabling_code) }}">{{ url('/enable/' . $enabling_code) }}</a>
</div>
@endsection