@extends('layouts.app')

@section('content')

@include('layouts.header-small')

<div class="wrapper">
	<div class="pagetitle">
		<div class="row">
    		<h4 class="pagetitle">&nbsp;</h3>
    		<h3 class="pagetitle">@lang('forms.confirmmail')</h3>
    		<img src="{{ asset('images/pixel.png') }}" alt="@lang('forms.confirmmail')" />
    		<h5 class="pagetitle">&nbsp;</h3>
    	</div>
	</div>
</div>

<div class="container">
       <div class="panel-body">
			@if(isset($message))
                <div class="alert alert-success">
                  {{ $message }}
                </div>
    		@endif
    		
    		@if(isset($error))
                <div class="alert alert-danger">
                  {{ $error }}
                </div>
    		@endif

    </div>
</div>
@endsection