@extends('layouts.app-mail')

@section('content')

@include('layouts.header-mail')
<div class="bodytext">
    <h3>@lang('texts.msg'.$msg)</h3>
    <br/>
    @if ($msg < 3)
        @lang('texts.agencyname'): {{ $agency->title }} - {{ $agency->address }}<br/>
        @lang('auth.telephone'): {{ $agency->telephone }}<br/>
        @lang('auth.cellphone'): {{ $agency->cellphone }}<br/>
        @lang('auth.email'): {{ $agency->email }}<br/>
        @lang('auth.www'): {{ $agency->www }}<br/>
    @else
        @lang('texts.register3'): {{ $provider->title }} - {{ $provider->address }}<br/>
        @lang('auth.telephone'): {{ $provider->telephone }}<br/>
        @lang('auth.cellphone'): {{ $provider->cellphone }}<br/>
        @lang('auth.email'): {{ $provider->email }}<br/>
        @lang('auth.www'): {{ $provider->www }}<br/>
    	<br/>
    	@lang('forms.itineraryname'): {{ $itinerary->itineraryname }}<br/>
    @endif
    <br/>
    @lang('forms.modulename'): {{ $modulename }}<br/>
    @lang('forms.description'): {{ $description }}<br/>
    @lang('forms.daterange'): {{ $datefrom }} - {{ $dateto }}<br/>
    @if ($msg == 1)
        <br/>
        @lang('texts.requestconfirm'): <a href="{{ url('/msg/confirm/' . $token) }}">{{ url('/msg/confirm/' . $token) }}</a><br/>
        @lang('texts.requestreject'): <a href="{{ url('/msg/reject/' . $token) }}">{{ url('/msg/reject/' . $token) }}</a>
    @endif
</div>
@endsection