@extends('layouts.app-mail')

@section('content')

@include('layouts.header-mail')
<div class="bodytext">
	<h3>@lang('texts.modulerequest')</h3>
    @lang('texts.agencyname'): {{ $agency->title }} - {{ $agency->address }}<br/>
    @lang('auth.telephone'): {{ $agency->telephone }}<br/>
    @lang('auth.cellphone'): {{ $agency->cellphone }}<br/>
    @lang('auth.email'):{{ $agency->email }}<br/>
    @lang('auth.www'): {{ $agency->www }}<br/>
    <br/>
    @lang('forms.modulename'): {{ $modulename }}<br/>
    @lang('forms.description'): {{ $description }}<br/>
    @lang('forms.daterange'): {{ $daterange }}<br/>
    <br/>
    @lang('texts.requestconfirm'): <a href="{{ url('/msg/confirm/' . $token) }}">{{ url('/msg/confirm/' . $token) }}</a><br/>
    @lang('texts.requestreject'): <a href="{{ url('/msg/reject/' . $token) }}">{{ url('/msg/reject/' . $token) }}</a>
</div>
@endsection