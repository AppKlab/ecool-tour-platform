@extends('layouts.app-mail')

@section('content')

@include('layouts.header-mail')
<div class="bodytext">
    <h3>@lang('texts.providerresponse')</h3>
    @lang('texts.providerresponse2'): {{ $answer }}<br />
    <br />
    @lang('texts.providername'): {{ $provider }}<br/>
    @lang('forms.modulename'): {{ $modulename }}<br/>
    @lang('forms.description'): {{ $description }}<br/>
    @lang('forms.daterange'): {{ $daterange }}<br/>
    <br/>
    @if ($messageprovider  <> 'none')
    	@lang('texts.providermsg'): {{ $messageprovider }}<br/>
    @endif
</div>
@endsection