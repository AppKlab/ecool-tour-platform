@extends('layouts.app-mail')

@section('content')

@include('layouts.header-mail')
<div class="bodytext">
    <h3>@lang('texts.msggroup'.$msg)</h3>
    <br/>
    @if ($msg == 1)
        @lang('texts.msggroup1login'): <a href="{{ url('/login') }}">{{ url('/login') }}</a><br/>
    @endif
</div>
@endsection