@extends('layouts.app')

@section('content')

@include('layouts.header-auth')
<div class="wrapper">
	<div class="betitle">
		<div class="row">
    		<h3 class="betitle">
    			@lang('forms.mymodules')
    		</h3>
    		<img src="{{ asset('images/pixel.png') }}" alt="@lang('forms.mymodules')" />
    	</div>
	</div>
</div>

<div class="wrapper">
        <!-- Current Modules -->
        @if (count($modules) > 0)
                    <table class="table table-striped module-table">
                        <thead>
                            <th>@lang('forms.module')</th>
                            <th>@lang('forms.public')</th>
                            <th><img alt="@lang('forms.english')" src="{{ asset('images/en.svg') }}" height="15px" /></th>
                            <th><img alt="@lang('forms.slovenian')" src="{{ asset('images/sl.svg') }}" height="15px" /></th>
                            <th><img alt="@lang('forms.croatian')" src="{{ asset('images/hr.svg') }}" height="15px" /></th>
                            <th><img alt="@lang('forms.german')" src="{{ asset('images/de.svg') }}" height="15px" /></th>
                            <th>@lang('forms.gallery')</th>
                            <th>@lang('forms.schedule')</th>
                            <th>@lang('forms.delete')</th>
                        </thead>
                        <tbody>
                            @foreach ($modules as $module)
						                            	
                                <tr>
                                    <td class="table-text"><div>{{ $module->modulename }}</div></td>
    
                                	@if ($module->enabled == '0')
                                    	<td>@lang('forms.disabledadmin')</td>
                                        <td colspan="7">&nbsp;</td>
                               		@else 
                                    
                                        <!-- Module Public Button -->
                                        <td>
            									<form action="{{url('modulepublic/' . $module->id)}}" method="POST">
                                                            
                                                            {{ csrf_field() }}
                                                            {{ method_field('PATCH') }}
                											@if ($module->public)
            													<input type="hidden" name="actionenable" value="disable">
            													<button type="submit" id="get-module-{{ $module->idModule }}-sl" class="btn btn-light">
                                                                    <i class="fas fa-toggle-on fa-2x"></i>
                                                                </button>
                											@else
            													<input type="hidden" name="actionenable" value="enable">
            													<button type="submit" id="get-module-{{ $module->idModule }}-sl" class="btn btn-light">
                                                                    <i class="fas fa-toggle-off fa-2x"></i>
                                                                </button>
                											@endif
                                                        </form>
                                        </td>
                                        
                                        @foreach ($modulestranslated as $moduletranslated)

                                            @if ($moduletranslated['idModule'] == $module['idModule'])
												                                                  
                                              @php 

                                                  $languagesinmodule[] = $moduletranslated['idLang'];
                                                  $idmodulelang[] = $moduletranslated['id'];

                                              @endphp
                                              
                                            @endif
                                        @endforeach
                                       @foreach ($languages as $key => $language)
                                        	
                                        	@if(!in_array($language['language'], $languagesinmodule))
                                            	<td>
                                                    <a href="{{url('modulenew/' . $module->idModule . '/' . $module->id . '/' . $language['language']. '')}}"><i class="fa fa-btn fa-plus"></i>&nbsp;@lang('forms.add')</a>
                                                </td>
                                              @else
                                              		@foreach ($modulestranslated as $moduletranslated)

                                            			@if ($moduletranslated['idModule'] == $module['idModule'] and $moduletranslated['idLang'] == $language['language'])
                                                          	<td>
                                                                <a href="{{url('module/' . $moduletranslated->id)}}"><i class="fa fa-btn fa-edit"></i>&nbsp;@lang('forms.edit')</a>
                                                            </td>
                                                         @endif
                                                    @endforeach
                                              @endif
                                        
                                       	@endforeach
                                       	
                                       	@php $languagesinmodule=empty($languagesinmodule) @endphp
                                       	@php $idmodulelang=empty($idmodulelang) @endphp
                                       		<td>
                                                <a href="{{url('images/' . $module->id)}}"><i class="far fa-image"></i></a>
                                            </td>
                                            
                                            <td>
                                                <a href="{{url('schedules/' . $module->id)}}"><i class="fas fa-calendar-alt"></i></a>
                                                
                                            </td>
                                            <!-- Module Delete Button -->
                                            <td>
                                                <a href="{{url('moduled/' . $module->id)}}" onClick="return confirm('@lang('forms.delmodconfirmation')');"><i class="fa fa-btn fa-trash"></i></a>
                                            </td>
										@endif
                                        </tr>
                                    @endforeach
                                    
									
                                </tbody>
                            </table>
            @else
            <div class="panel panel-default">
                <div class="panel-heading">
                    @lang('forms.nomodules')
                </div>
            </div>
            @endif
    </div>
    
@endsection
