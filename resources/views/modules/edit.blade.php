@extends('layouts.app')

@section('content')

@include('layouts.header-auth')

@php
	$tagNames = array ( 
                "1" => "Kulinarika",
                "2" => "Prenocisca",
                "3" => "Izobrazevalne vsebine",
                "4" => "Vodeni ogledi",
                "5" => "Dostop za invalide",
                "6" => "Kultura",
                "7" => "Zivali"
            ); 
                                
	$categoryNames = array ( 
                "1" => "Kulinarika",
                "2" => "Dozivetja",
                "3" => "Dogodki"
            ); 
@endphp
<div class="wrapper">
	<div class="betitle">
		<div class="row">
    		<h3 class="betitle">
    			@if ($id > 0)
                    @lang('forms.editmodule') <img alt="Header" src="{{ asset('images/'.$module->idLang.'.svg') }}" height="15px" alt="@lang('forms.editmodule')" />    	
                @elseif ($idparent > 0 && $newlang)
                	@lang('forms.addnewlangmod') <img alt="Header" src="{{ asset('images/'.$newlang.'.svg') }}" height="15px" alt="@lang('forms.addnewlangmod')" />
                @else
                	@lang('forms.newmodule') <img alt="Header" src="{{ asset('images/en.svg') }}" height="15px" alt="@lang('forms.newmodule')" />
                @endif
    		</h3>
    	</div>
	</div>
</div>

<div class="wrapper">

                
                    <!-- New Module Form -->
                    @if ($id > 0)
                    	
                         @if ($module->idLang == 'en')
                         	<form action="{{ action('Backend\ModuleController@update', $id) }}" enctype="multipart/form-data"  method="POST" class="form-horizontal" role="form">
                         @else
                         	<form action="{{ action('Backend\ModuleController@updatelang', $id) }}" enctype="multipart/form-data"  method="POST" class="form-horizontal" role="form">
                         @endif
                        {!! csrf_field() !!}
                        {{ method_field('PATCH') }}        	
                    @elseif ($idparent > 0 && $newlang)
                    	
                    	<form action="{{ action('Backend\ModuleController@addnewlang') }}" method="POST" class="form-horizontal" role="form">
                    	<input type="hidden" name="idparent" value="{{$idparent}}">
                    	<input type="hidden" name="realidparent" value="{{$moduleparent->id}}">
                    	<input type="hidden" name="newlang" value="{{$newlang}}">
                        {!! csrf_field() !!}
                        {{ method_field('PATCH') }} 
                    @else
                    	
                    	 <form action="{{ url('module') }}" enctype="multipart/form-data" method="POST" class="form-horizontal" role="form">
                        {!! csrf_field() !!}
                    @endif
                                
                     	

                        <!-- Module Name -->
                        <div class="form-group{{ $errors->has('modulename') ? ' has-error' : '' }}">
                            <label class="col-md-4 control-label">@lang('forms.modulename')</label>

                            <div class="col-sm-6">
                                <input type="text" class="form-control" name="modulename" value="{{ null !==old('modulename') ? old('modulename') : $module->modulename }}">
                                
                                @if ($errors->has('modulename'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('modulename') }}</strong>
                                    </span>
                                @endif
                                
                            </div>
                        </div>
                        
                        
                        <!-- Description -->
                        <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                            <label class="col-md-4 control-label">@lang('forms.description')</label>

                            <div class="col-sm-6">
                                <input type="text" class="form-control" name="description" value="{{ null !==old('description') ? old('description') : $module->description  }}">
                                
                                @if ($errors->has('description'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                @endif
                                
                            </div>
                        </div>
                        
                        <!-- Long Description -->
                        <div class="form-group{{ $errors->has('longdescription') ? ' has-error' : '' }}">
                            <label class="col-md-4 control-label">@lang('forms.longdescription')</label>

                            <div class="col-sm-6">
                                <textarea class="form-control" rows="5" name="longdescription">{{ null !==old('longdescription') ? old('longdescription') : $module->longdescription }}</textarea>
                                
                                @if ($errors->has('longdescription'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('longdescription') }}</strong>
                                    </span>
                                @endif
                                
                            </div>
                        </div>
                        
                        
                        <!-- Price Description -->
                        <div class="form-group{{ $errors->has('pricedescription') ? ' has-error' : '' }}">
                            <label class="col-md-4 control-label">@lang('forms.pricedescription')</label>

                            <div class="col-sm-6">
                                <textarea class="form-control" rows="5" name="pricedescription">{{ null !==old('pricedescription') ? old('pricedescription') : $module->pricedescription }}</textarea>
                                
                                @if ($errors->has('pricedescription'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('pricedescription') }}</strong>
                                    </span>
                                @endif
                                
                            </div>
                        </div>
                        
                        <!-- Video Url -->
                        <div class="form-group{{ $errors->has('videoUrl') ? ' has-error' : '' }}">
                            <label class="col-md-4 control-label">@lang('forms.videoUrl') (youtube video)</label>

                            <div class="col-sm-6">
                                <input type="text" class="form-control" name="videoUrl" value="{{ null !==old('videoUrl') ? old('videoUrl') : $module->videoUrl }}">
                                
                                @if ($errors->has('videoUrl'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('videoUrl') }}</strong>
                                    </span>
                                @endif
                                
                            </div>
                        </div>
       
                        
                        <div class="form-group" align="center">
                        	<br /><h6>@lang('forms.commonattr')</h6><br />
                        </div>
                        
                
                            @if (($id > 0 and $module->idLang <> 'en') or ($id == 0 and $newlang))
								
                                <!-- Address -->
                                <div class="form-group">
                                    <label class="col-md-4 control-label">@lang('forms.address')</label>
        
                                    <div class="col-sm-6">
                                        {{ isset($moduleparent->address) ? $moduleparent->address :  $module->address }}
                                    </div>
                                </div>
                                
                                <!-- Zip code -->
                                <div class="form-group">
                                    <label class="col-md-4 control-label">@lang('forms.zip')</label>
        
                                    <div class="col-sm-6">
                                        {{ isset($moduleparent->zip) ? $moduleparent->zip :  $module->zip }}
                                    </div>
                                </div>
        
                				<!-- City -->
                                <div class="form-group">
                                    <label class="col-md-4 control-label">@lang('forms.city')</label>
        
                                    <div class="col-sm-6">
                                        {{ isset($moduleparent->city) ? $moduleparent->city :  $module->city }}
                                    </div>
                                </div>
        
                                        
                                <!-- Main image -->
                                <div class="form-group">
                                    <label class="col-md-4 control-label">@lang('forms.mainimage')</label>
        
                                    <div class="col-sm-6">
                                        @if (isset($moduleparent->mainimage))
                                        	@php $modulemainimage = $moduleparent->mainimage; @endphp
                                        @else
                                        	@php $modulemainimage = $module->mainimage; @endphp
                                        @endif
                                        <img src='{{ asset('upload_media/modules_thumbs/'.$modulemainimage) }}' height="100px" alt="@lang('forms.module')" />
                                    </div>
                                </div>
                                
                                <!-- Geolocation 
                                <div class="form-group">
                                    <label class="col-md-4 control-label">@lang('forms.geolocation')</label>
        
                                    <div class="col-sm-6">
                                        {{ isset($moduleparent->geolocation) ? $moduleparent->geolocation :  $module->geolocation }}
                                    </div>
                                </div>-->
                                
                                <!-- Persons Minimum -->
                                <div class="form-group">
                                    <label class="col-md-4 control-label">@lang('forms.personsMin')</label>
        
                                    <div class="col-sm-6">
                                        {{ isset($moduleparent->personsMin) ? $moduleparent->personsMin :  $module->personsMin }}
                                    </div>
                                </div>
                                
                                <!-- Persons Maximum -->
                                <div class="form-group">
                                    <label class="col-md-4 control-label">@lang('forms.personsMax')</label>
        
                                    <div class="col-sm-6">
                                        {{ isset($moduleparent->personsMax) ? $moduleparent->personsMax :  $module->personsMax }}
                                    </div>
                                </div>
                                
                                <!-- Price -->
                                <div class="form-group">
                                    <label class="col-md-4 control-label">@lang('forms.price') (EUR)</label>
        
                                    <div class="col-sm-6">
                                        {{ isset($moduleparent->price) ? $moduleparent->price :  $module->price }}
                                    </div>
                                </div>
                                
                                <!-- Categories -->
                                <div class="form-group">
                                    <label class="col-md-4 control-label">@lang('forms.categories')</label>
                                    <div class="col-sm-6">
                                        @if(empty($categories))
    										@lang('forms.notchosen')
                                        @else
                                            @foreach ($categoryNames as $key => $value)
                                            	
                                                @if(in_array($key, $categories))
    													{{ $value }}<br />
                                                @endif
                                   			
            								@endforeach
            							@endif
                                    </div>
                                </div>
                                
                                <!-- Tags -->
                                <div class="form-group">
                                    <label class="col-md-4 control-label">@lang('forms.tags')</label>
                                    <div class="col-sm-6">
                                        @if(empty($tags))
    										@lang('forms.notchosen')
                                        @else
                                            @foreach ($tagNames as $key => $value)
                                            	
                                                @if(in_array($key, $tags))
    													{{ $value }}<br />
                                                @endif
                                                    
            								@endforeach
            							@endif
                                    </div>
                                </div>
                                
                            
                            @else
                                <!-- Address -->
                                <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                                    <label class="col-md-4 control-label">@lang('forms.address')</label>
        
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control" name="address" value="{{ null !==old('address') ? old('address') : $module->address }}">
                                        
                                        @if ($errors->has('address'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('address') }}</strong>
                                            </span>
                                        @endif
                                        
                                    </div>
                                </div>
                                
                                <!-- zip code -->
                                <div class="form-group{{ $errors->has('zip') ? ' has-error' : '' }}">
                                    <label class="col-md-4 control-label">@lang('forms.zip')</label>
        
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control" name="zip" value="{{ null !==old('zip') ? old('zip') : $module->zip }}">
                                        
                                        @if ($errors->has('zip'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('zip') }}</strong>
                                            </span>
                                        @endif
                                        
                                    </div>
                                </div>
                                
                                <!-- City -->
                                <div class="form-group{{ $errors->has('city') ? ' has-error' : '' }}">
                                    <label class="col-md-4 control-label">@lang('forms.city')</label>
        
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control" name="city" value="{{ null !==old('city') ? old('city') : $module->city }}">
                                        
                                        @if ($errors->has('city'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('city') }}</strong>
                                            </span>
                                        @endif
                                        
                                    </div>
                                </div>
                                
                                <!-- Main Image -->
                                <div class="form-group{{ $errors->has('mainimage') ? ' has-error' : '' }}">
                                    <label class="col-md-4 control-label">@lang('forms.mainimage')</label>
        
                                    <div class="col-sm-6">
                                        @if ($id > 0)
                                        	<img src='{{ asset('upload_media/modules_thumbs/'.$module->mainimage) }}' height="100px" alt="@lang('forms.module')" />
                                        	<br />@lang('forms.change'):<br />
                                        @endif
                                        <input type="file" name="mainimage" value="{{ isset($module->mainimage) ? $module->mainimage : old('mainimage') }}">
                                        
                                        @if ($errors->has('mainimage'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('mainimage') }}</strong>
                                            </span>
                                        @endif
                                        
                                    </div>
                                </div>
                                
                                <!-- Geolocation
                                <div class="form-group{{ $errors->has('geolocation') ? ' has-error' : '' }}">
                                    <label class="col-md-4 control-label">@lang('forms.geolocation')</label>
        
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control" name="geolocation" value="{{ null !==old('geolocation') ? old('geolocation') : $module->geolocation  }}">
                                        
                                        @if ($errors->has('geolocation'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('geolocation') }}</strong>
                                            </span>
                                        @endif
                                        
                                    </div>
                                </div> -->
                                
                                <!-- Persons Minimum -->
                                <div class="form-group{{ $errors->has('personsMin') ? ' has-error' : '' }}">
                                    <label class="col-md-4 control-label">@lang('forms.personsMin')</label>
        
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control" name="personsMin" value="{{ null !==old('personsMin') ? old('personsMin') : $module->personsMin  }}">
                                        
                                        @if ($errors->has('personsMin'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('personsMin') }}</strong>
                                            </span>
                                        @endif
                                        
                                    </div>
                                </div>
                                
                                <!-- Persons Maximum -->
                                <div class="form-group{{ $errors->has('personsMax') ? ' has-error' : '' }}">
                                    <label class="col-md-4 control-label">@lang('forms.personsMax')</label>
        
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control" name="personsMax" value="{{ null !==old('personsMax') ? old('personsMax') : $module->personsMax  }}">
                                        
                                        @if ($errors->has('personsMax'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('personsMax') }}</strong>
                                            </span>
                                        @endif
                                        
                                    </div>
                                </div>
                                
                               
                                <!-- Price -->
                                <div class="form-group{{ $errors->has('price') ? ' has-error' : '' }}">
                                    <label class="col-md-4 control-label">@lang('forms.price') (EUR)</label>
        
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control" name="price" value="{{  null !==old('price') ? old('price') : $module->price }}">
                                        
                                        @if ($errors->has('price'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('price') }}</strong>
                                            </span>
                                        @endif
                                        
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-4"></div>
                    
                                    <div class="col-md-6 smalltext">@lang('forms.allmandatory')
                                    </div>
                                </div>

                                <!-- Categories -->
                                <div class="form-group{{ $errors->has('categories') ? ' has-error' : '' }}">
                                    <label class="col-md-4 control-label">@lang('forms.categories')</label>
                                    <div class="col-sm-6">
                                    	@php $i=1; @endphp
                                        @foreach ($categoryNames as $key => $value)
                                        	
                                            @if(in_array($key, $categories))
                                            	<div class="checkbox">
                                            		<label>
    												<input data-toggle="toggle" type="checkbox" data-size="mini" name="category[{{ $key }}]" data-on=" " data-off=" " checked>
													@lang('forms.cat'.$i)
													</label>
												</div>
                                            @else
                                            	<div class="checkbox">
                                            		<label>
    												<input data-toggle="toggle" type="checkbox" data-size="mini" name="category[{{ $key }}]"  data-on=" " data-off=" ">
													@lang('forms.cat'.$i)
													</label>
												</div>
                                            @endif
                                            @php $i = $i+1; @endphp   
                                			<br />
        								@endforeach
                                        
                                        @if ($errors->has('categories'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('categories') }}</strong>
                                            </span>
                                        @endif
                                        
                                    </div>
                                </div>
                                
                                <!-- Tags -->
                                <div class="form-group">
                                    &nbsp;
                                </div>
                                
                                <div class="form-group{{ $errors->has('tags') ? ' has-error' : '' }}">
                                    <label class="col-md-4 control-label">@lang('forms.tags')</label>
                                    <div class="col-sm-6">
                                    	@php $i=1; @endphp
                                        @foreach ($tagNames as $key => $value)
                                        	
                                            @if(in_array($key, $tags))
                                            	<div class="checkbox">
                                            		<label>
    												<input data-toggle="toggle" type="checkbox"  data-size="mini" name="tag[{{ $key }}]" data-on=" " data-off=" " checked>
													@lang('forms.tag'.$i)
													</label>
												</div>
                                            @else
                                            	<div class="checkbox">
                                            		<label>
    												<input data-toggle="toggle" type="checkbox" data-size="mini" name="tag[{{ $key }}]"  data-on=" " data-off=" ">
													@lang('forms.tag'.$i)
													</label>
												</div>
                                            @endif
                                            @php $i = $i+1 @endphp       
                                			<br />
        								@endforeach
                                        
                                        @if ($errors->has('tags'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('tags') }}</strong>
                                            </span>
                                        @endif
                                        
                                    </div>
                                </div>
                        @endif        
                        <!-- Add Module Button -->
                        <div class="form-group">
                            <div class="col-md-4"></div>
                            <div class="col-sm-6">
                            	<button type="submit" class="btn btn-primary">
                                    <i class="fa fa-btn fa-save"></i>&nbsp;
                                        @if ($id > 0)
                                        	@lang('forms.savechanges')
                                        	
                                        @elseif ($idparent > 0 && $newlang)
                                            @lang('forms.addnewlangmod') ({{ $newlang }})   
                                        @else
                                        	@lang('forms.saveform')
                                        @endif
                                     </button>
                            </div>
                        </div>
                    </form>
</div>    
@endsection
