@extends('layouts.app')

@section('content')

@include('layouts.header-auth')
<div class="wrapper">
	<div class="betitle">
		<div class="row">
    		<h3 class="betitle">
    			@lang('forms.profile')
    		</h3>
    		<img src="{{ asset('images/pixel.png') }}" alt="@lang('forms.profile')" />
    	</div>
	</div>
</div>

<div class="container">
    <div class="row">
       		@if(session()->has('message'))
                <div class="alert alert-success">
                  {{ session()->get('message') }}
                </div>
    		@else

          <form class="form-horizontal" enctype="multipart/form-data" method="POST" action="{{ url('profile/' . $user->id) }}">
            {!! csrf_field() !!}
            {{ method_field('PATCH') }}     

           <div class="form-group">
                <label class="col-md-4 control-label">@lang('auth.username')</label>

                <div class="col-md-6">
                    {{ $user->username }}
                </div>
            </div>
            
            <div class="form-group">
                <label class="col-md-4 control-label">@lang('auth.email')</label>

                <div class="col-md-6">
                    {{ $user->email }}
                </div>
            </div>
            @if ($user->userType < 4)
                <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                    <label class="col-md-4 control-label">@lang('auth.title')</label>
    
                    <div class="col-md-6">
                        <input type="text" class="form-control" name="title" value="{{ isset($user->title) ? $user->title : old('title') }}">
    
                        @if ($errors->has('title'))
                            <span class="help-block">
                                <strong>{{ $errors->first('title') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                
                <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                    <label class="col-md-4 control-label">@lang('auth.address')</label>
    
                    <div class="col-md-6">
                        <input type="text" class="form-control" name="address" value="{{ isset($user->address) ? $user->address : old('address') }}">
    
                        @if ($errors->has('address'))
                            <span class="help-block">
                                <strong>{{ $errors->first('address') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                
                <div class="form-group{{ $errors->has('logo') ? ' has-error' : '' }}">
                    <label class="col-md-4 control-label">@lang('auth.logo')</label>
    
                    <div class="col-md-6">
                        <img src='{{ asset('upload_media/logos_thumbs/'.$user->logo) }}' height="100px" alt="@lang('forms.profile')" />
                        <br />@lang('forms.change'):<br />
                        <input type="file" name="logo" value="{{ isset($user->logo) ? $user->logo : old('logo') }}">
                        
                        
    
                        @if ($errors->has('logo'))
                            <span class="help-block">
                                <strong>{{ $errors->first('logo') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                
                <div class="form-group{{ $errors->has('www') ? ' has-error' : '' }}">
                    <label class="col-md-4 control-label">@lang('auth.www')</label>
    
                    <div class="col-md-6">
                        <input type="text" class="form-control" name="www" value="{{ isset($user->www) ? $user->www : old('www') }}">
    
                        @if ($errors->has('www'))
                            <span class="help-block">
                                <strong>{{ $errors->first('www') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

            	<div class="form-group{{ $errors->has('telephone') ? ' has-error' : '' }}">
                    <label class="col-md-4 control-label">@lang('auth.telephone')</label>
    
                    <div class="col-md-6">
                        <input type="text" class="form-control" name="telephone" value="{{ isset($user->telephone) ? $user->telephone : old('telephone') }}">
    
                        @if ($errors->has('telephone'))
                            <span class="help-block">
                                <strong>{{ $errors->first('telephone') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
            @endif
            
            <div class="form-group{{ $errors->has('cellphone') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">@lang('auth.cellphone')</label>

                <div class="col-md-6">
                    <input type="text" class="form-control" name="cellphone" value="{{ isset($user->cellphone) ? $user->cellphone : old('cellphone') }}">

                    @if ($errors->has('cellphone'))
                        <span class="help-block">
                            <strong>{{ $errors->first('cellphone') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            
            

			<div class="form-group">
                <div class="col-md-4"></div>

                <div class="col-md-6 smalltext">@lang('forms.allmandatory')
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-6 col-md-offset-4">
                    <button type="submit" class="btn btn-primary">
                        <i class="fa fa-btn fa-save"></i>&nbsp;@lang('forms.savechanges')
                    </button>
                </div>
            </div>
        </form>
    	@endif
    </div>
</div>
@endsection