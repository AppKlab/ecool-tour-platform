@extends('layouts.app')

@section('content')

@include('layouts.header-auth')
<div class="wrapper">
	<div class="betitle">
		<div class="row">
    		<h3 class="betitle">
    			@lang('forms.profile')
    		</h3>
    		<img src="{{ asset('images/pixel.png') }}" alt="@lang('forms.profile')" />
    	</div>
	</div>
</div>

<div class="wrapper">
    <div class="row">
                @if(session()->has('message'))
                            <div class="alert alert-success">
                              {{ session()->get('message') }}
                            </div>
                @endif
				<form class="form-horizontal" action="{{url('profile/' . $user->id)}}" method="POST">
                      <div class="form-group">
                            <label class="col-md-4 control-label">@lang('auth.username')</label>

                            <div class="col-md-6">
                                {{ $user->username }}
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="col-md-4 control-label">@lang('auth.email')</label>

                            <div class="col-md-6">
                                {{ $user->email }}
                            </div>
                        </div>
                        
                        @if ($user->userType < 4)
                            <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">@lang('auth.title')</label>
    
                                <div class="col-md-6">
                                    {{ $user->title }}
                                </div>
                            </div>
                            
                            <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">@lang('auth.address')</label>
    
                                <div class="col-md-6">
                                    {{ $user->address }}
                                </div>
                            </div>
                            
                            <div class="form-group{{ $errors->has('logo') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">@lang('auth.logo')</label>
    
                                <div class="col-md-6">
                                    <img src='{{ asset('upload_media/logos_thumbs/'.$user->logo) }}' height="100px" alt="@lang('forms.profile')" />
                                </div>
                            </div>
                            
                            <div class="form-group{{ $errors->has('www') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">@lang('auth.www')</label>
    
                                <div class="col-md-6">
                                    {{ $user->www }}
                                </div>
                            </div>
                        
                        	<div class="form-group{{ $errors->has('telephone') ? ' has-error' : '' }}">
                                <label class="col-md-4 control-label">@lang('auth.telephone')</label>
    
                                <div class="col-md-6">
                                    {{ $user->telephone }}
                                </div>
                            </div>
						@endif                        
                        <div class="form-group{{ $errors->has('cellphone') ? ' has-error' : '' }}">
                            <label class="col-md-4 control-label">@lang('auth.cellphone')</label>

                            <div class="col-md-6">
                                {{ $user->cellphone }}
                            </div>
                        </div>
                        
                        


                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                
                                            {{ csrf_field() }}
                                            {{ method_field('GET') }}

                                            <button type="submit" id="get-user-{{ $user->id }}" class="btn btn-primary">
                                                <i class="fa fa-btn fa-edit"></i>&nbsp;@lang('forms.edit')
                                            </button>
                                        
                            </div>
                        </div>
                     </form>
         </div>
</div>

@endsection