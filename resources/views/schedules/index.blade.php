@extends('layouts.app')

@section('content')

@include('layouts.header-auth')
<div class="wrapper">
	<div class="betitle">
		<div class="row">
    		<h3 class="betitle">
    			@lang('forms.moduleschedules')
    		</h3>
    		<img src="{{ asset('images/pixel.png') }}" alt="@lang('forms.moduleschedules')" />
    	</div>
	</div>
</div>

<div class="wrapper">
        <!-- Current Images -->
        <table class="table table-striped image-table">
            <thead>
                <th>@lang('forms.module'): {{ $modulename }}</th>
                <th>@lang('forms.delete')</th>
            </thead>
            <tbody>
            <!-- Current Schedules -->
        @if (count($schedules) > 0)
                            @foreach ($schedules as $schedule)
						                            	
                                <tr>
                                    <td class="table-text">
                                        <div>
                                            <!-- //Dates format must be changed from Y-m-d H:m:s to d. m. Y H:i for the ui -->
                                            @php
                                                $dateFrom = new DateTime($schedule['dateFrom']);
                                                $dateFrom1 = $dateFrom->format('d. m. Y H:i');
                                                $dateTo = new DateTime($schedule['dateTo']);
                                                $dateTo1 = $dateTo->format('d. m. Y H:i');
                                                $schedule['dateFrom'] = $dateFrom1;
                                                $schedule['dateTo'] = $dateTo1;
        									@endphp
        
        									{{ $schedule->dateFrom }} - {{ $schedule->dateTo }}
                                        </div>
                                    </td>
    
                                	@if ($schedule->enabled == '0')
                                    	<td>@lang('forms.disabledadmin')</td>
                               		@else 
                                    
                                        <!-- Schedule Delete Button -->
                                        <td>
                                            <a href="{{url('scheduled/' . $schedule->id)}}" onClick="return confirm('@lang('forms.delscheduleconfirmation')');"><i class="fa fa-btn fa-trash"></i></a>
                                        </td>
                                       	
										@endif
											
                                        </tr>
                                    @endforeach
                                    
									

            @else
                <tr>
                    <td colspan='2'>
                       @lang('forms.noschedules')
                    </td>
                </tr>
            @endif
                                            </tbody>
                            </table>
                    <form action="{{url('schedulenew/' . $moduleid . '')}}" method="POST">
                        {{ csrf_field() }}
                        {{ method_field('GET') }}

                        <button type="submit" id="get-schedule-{{ $moduleid }}" class="btn btn-info">
                            <i class="fa fa-btn fa-plus"></i>&nbsp;@lang('forms.add')  
                        </button>
                    </form>
           <br /><br />
    </div>
    
@endsection
