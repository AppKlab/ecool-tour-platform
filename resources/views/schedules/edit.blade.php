@extends('layouts.app')

@section('content')

@include('layouts.header-auth')
<div class="wrapper">
	<div class="betitle">
		<div class="row">
    		<h3 class="betitle">
    			@lang('forms.moduleschedules')
    		</h3>
    		<img src="{{ asset('images/pixel.png') }}" alt="@lang('forms.moduleschedules')" />
    	</div>
	</div>
</div>

<div class="wrapper">
    	<table class="table table-striped image-table">
            <thead>
                <th>@lang('forms.modulename'): {{ $modulename }}</th>
            </thead>
            <tbody>
            	<tr>
            		<td>
                    <!-- New Schedule Form -->
                   	<form action="{{ url('schedule') }}" method="POST" class="form-horizontal" role="form">
                        {!! csrf_field() !!}
       	
                    	<input type="hidden" name="moduleid" value="{{$moduleid}}">

                        <!-- Date Range (From/to) -->
                        <div class="form-group{{ $errors->has('daterange') ? ' has-error' : '' }}">
                            <label class="col-md-4 control-label">@lang('forms.daterange')<br/></label>

                            <div class="col-sm-6">
                                <input type="text" class="form-control" name="daterange" value="" />
                                <script>
                                    $(function() {
                                      $('input[name="daterange"]').daterangepicker();
                                      $('input[name="daterange"]').daterangepicker({
                                    	    timePicker: true,
                                    	    startDate: moment().startOf('hour'),
                                    	    endDate: moment().startOf('hour').add(32, 'hour'),
                                    	    locale: {
                                    	      format: 'DD/MM/YYYY H:mm'
                                    	    }
                                    	});
                                    });
                                    
                                </script>
                                @if ($errors->has('day1'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('day1') }}</strong>
                                    </span>
                                @endif
                                @if ($errors->has('time1'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('time1') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                                
                        <!-- Add Image Button -->
                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-6">
                            	<button type="submit" class="btn btn-primary">
                                    <i class="fa fa-btn fa-save"></i>&nbsp;
                                        	@lang('forms.saveform')
                                </button>
                            </div>
                        </div>
                    </form>
            		</td>
            	</tr>
        	</tbody>
        </table>
</div>
@endsection
