<section id="main" class="imageheader">
		<nav class="main">
			<div class="wrapper">
				<div class="topnav" id="myTopnav" style="width:100%">
					 <a class="logostyle" href="{{ url('home') }}"><img src="{{ asset('images/logo.png') }}" alt="Logo" height="60px"></a>
					 <a class="menuitemblank" href=""></a>
					 <a class="menuitem" href="{{ url('how-to-register') }}">@lang('texts.designpackages')</a>
					 <a class="menuitem" href="{{ url('how-to-register') }}">@lang('texts.madepackages')</a>
					 <a class="menuitem" href="{{ url('how-to-register') }}">@lang('texts.beaprovider')</a>
					 <a href="javascript:void(0);" class="icon" onclick="myFunction()">
					    <i class="fa fa-bars"></i>
					 </a>
				</div>
			</div>
		</nav>
	<script>
	function myFunction() {
	    var x = document.getElementById("myTopnav");
	    if (x.className === "topnav") {
	        x.className += " responsive";
	    } else {
	        x.className = "topnav";
	    }
	}
	</script>
	<article>
		<div class="wrapper main-content">
			<h3 class="banner">@lang('texts.banner1')</h3>
		</div>
	</article>
</section>