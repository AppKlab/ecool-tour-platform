<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    
    <meta name="revisit" content="7 Days" />
    <meta name="robots" content="index,follow" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no"/>
    
    <title>Site title</title>
    
    <!-- Include Favicon -->	
	<link rel="icon" href="{{ asset('images/favicon.ico') }}" type="image/x-icon">
	<link rel="shortcut icon" type="image/ico" href="{{ asset('images/favicon.ico') }}">
	
	<!-- Bootstrap -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.css" rel="stylesheet">
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
        
    <!-- Elixir -->
    {{-- <link href="{{ elixir('css/app.css') }}" rel="stylesheet"> --}}

	<!-- Custom css -->
	<style>
    body { font-family: DejaVu Sans, sans-serif; }
    .wrapper { 
    	width: 100%;
    	height: 100%; 
    	margin: auto; 
    }
    .row { 
    	width: 100%; 
    	margin: auto; 
    }
    
    .agencytitle {
    	text-align: left;
    	font-size: 20px;
    	font-weight: bold;
    	color: #383838;	
    	letter-spacing: 2px;
    	line-height: 1;
    	text-transform: none;
    }
    .agencytext {
    	text-align: left;
    	font-size: 12px;
    	font-weight: normal;
    	color: #383838;	
    	letter-spacing: 2px;
    	line-height: 1,1;
    	text-transform: none;
    }
    .pagetitle {
    	text-align: left;
    	font-size: 30px;
    	font-weight: bold;
    	color: #383838;	
    	letter-spacing: 2px;
    	line-height: 1;
    	text-transform: uppercase;
    }
    .pagetext {
    	text-align: left;
    	font-size: 17px;
    	font-weight: normal;
    	color: #383838;	
    	letter-spacing: 0px;
    	text-transform: none;
    	line-height: 1,1;
    }
    .pdf-image {
    	float:right;
    	align: right;
    	display: block;
    }
    .smalltext 
    {
        margin: 0px;
        padding: 0px;
    	text-align: left;
    	font-size: 11px;
    	font-weight: normal;
    	color: #383838;	
    	letter-spacing: 0px;
    	line-height: 1,1;
    	text-transform: none;
    }
        
    </style>

    <!-- Daterangepicker -->
	<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />	
	  
</head>
<body id="app-layout">
    @yield('content')
    <!-- JavaScripts
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>-->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
    {{-- <script src="{{ elixir('js/app.js') }}"></script> --}}
</body>
</html>
