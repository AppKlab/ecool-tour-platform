<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    
    <meta name="revisit" content="7 Days" />
    <meta name="robots" content="index,follow" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no"/>
    
    <title>Site title</title>
    
    <!-- Include Favicon -->	
	<link rel="icon" href="{{ asset('images/favicon.ico') }}" type="image/x-icon">
	<link rel="shortcut icon" type="image/ico" href="{{ asset('images/favicon.ico') }}">
	
	<!-- Fonts & styles -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700" rel='stylesheet' type='text/css'>
		
	<style>
    	body {
        	margin: 0px;
        	padding: 0px;
            font-family: 'Poppins';
        }
        p.pf {
        	text-align: center;
        	font-size: 16px;
        	color: #383838;	
        	letter-spacing: 2px;
        	text-transform: uppercase;
        	margin-bottom: 30px;
        }
        p.credits {
        	text-align: center;
        	font-size: 13px;
        	color: #c9c9c9;
        	margin-top: 40px;
        	margin-bottom: 20px;
        }
        .wrapper { 
        	width: 100%; 
        	height: 100%;
        }
        
        ul{
            list-style-type: none;
            height: 100px;
            margin:0;
            padding:0;
        }
        .logostyle {
          width:200px;
          padding-top:15px;
          padding-left:15px;
          float:left;
        }
        hr {
        	height: 1px;
        	border: none;
        	background: linear-gradient(to right, #ce5776, #5ba76a, #289ed4);
        	width:100%;
        	margin-top:1px;
        }
        .bodytext{
          margin:15px;
        }
    </style>  
</head>
<body id="app-layout">
    @yield('content')
<br />
	@include('layouts.footer')
</body>
</html>
