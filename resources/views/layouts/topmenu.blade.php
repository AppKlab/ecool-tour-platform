@php
    $apploc = App::getLocale();
    //echo ("app locale:" . $apploc . '<br/>');
    $session_language = Session::get('locale');
    if($apploc != '' and $session_language != ''){
       //echo ("/session locale set:" . $session_language  . '<br/>');   
    }elseif($apploc == ''){
     	//echo ("session locale NOT set. Setting to: 'en' <br/>");   
     	Session::put('locale', 'en');
     	$session_language = Session::get('locale');
     	//print ('set language in session: '.$session_language . '<br/>');
     	App::setLocale($session_language);
     	$apploc1 = App::getLocale();
    	//echo ("app locale1:" . $apploc1 . '<br/>');
     	
    }

@endphp

<nav class="top">
	<div class="wrapper">
		<ul>
			<li class="top{{ 'sl' == $session_language ? ' topactive' : '' }}"><a href="{{ url('lang/sl') }}">SI</a></li>
			<li class="top{{ 'hr' == $session_language ? ' topactive' : '' }}"><a href="{{ url('lang/hr') }}">HR</a></li>
			<li class="top{{ 'en' == $session_language ? ' topactive' : '' }}"><a href="{{ url('lang/en') }}">EN</a></li>
			<li class="top{{ 'de' == $session_language ? ' topactive' : '' }}"><a href="{{ url('lang/de') }}">DE</a></li>
			<li><a href="{{ url('home#about') }}">@lang('texts.contact')</a></li>

            <!-- Authentication Links -->
            @if (Auth::guest())
                <li class="top"><a href="{{ url('/login') }}">@lang('auth.login')</a></li>
                <li class="top"><a href="{{ url('/how-to-register') }}">@lang('auth.register')</a></li>
            @else
            	<li class="top"><a href="{{ url('logout') }}">@lang('auth.logout')</a></li>
            	<li class="top"><a href="{{ url('authhome') }}">{{ Auth::user()->username }}</a></li>
            @endif
		</ul>                
    </div>
</nav>