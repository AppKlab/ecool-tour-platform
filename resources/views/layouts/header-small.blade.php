<nav class="main-small">
		<div class="wrapper">
			<div class="topnav2" id="myTopnav2" style="width:100%">
				 <a class="logostyle2" href="{{ url('home') }}"><img src="{{ asset('images/logo.png') }}" alt="Logo" height="60px"></a>
				 <a class="menuitemblank2" href=""></a>
				 <a class="menuitem2" href="{{ url('how-to-register') }}">@lang('texts.designpackages')</a>
				 <a class="menuitem2" href="{{ url('how-to-register') }}">@lang('texts.madepackages')</a>
				 <a class="menuitem2" href="{{ url('how-to-register') }}">@lang('texts.beaprovider')</a>
				 <a href="javascript:void(0);" class="icon2" onclick="myFunction()">
				    <i class="fa fa-bars"></i>
				 </a>
			</div>
		</div>
	</nav>

<nav class="main-small2" id="myTopnav3">
	<div class="wrapper">
		<ul class="home">
			<li class="lighter categorieshide tag0"><p>@lang('texts.sklopi'):</p></li>
			<li class="main-small2 tag1"><a href="{{ url('food') }}" id="@lang('forms.cat1')">@lang('forms.cat1')</a></li>
			<li class="main-small2 tag2"><a href="{{ url('experiences') }}" id="@lang('forms.cat2')">@lang('forms.cat2')</a></li>
			<li class="main-small2 tag3"><a href="{{ url('events') }}" id="@lang('forms.cat3')">@lang('forms.cat3')</a></li>
			<li><a href="{{ url('packages') }}">@lang('texts.sklop4')</a></li>
			<li class="lihide">&nbsp</li>
			<li class="searchhide"><a href="{{ url('search') }}">@lang('texts.search')</a></li>
			<li class="search"><a href="{{ url('search') }}"><img src="{{ asset('images/icon-search-black-20px.png') }}" alt="@lang('texts.search')"></a></li>
		</ul>
	</div>
</nav>	
<script>		
function myFunction() {
    var x = document.getElementById("myTopnav2");
    if (x.className === "topnav2") {
        x.className += " responsive";
    } else {
        x.className = "topnav2";
    }

    var x = document.getElementById("myTopnav3");
    if (x.className === "main-small2") {
        x.className += " responsive";
    } else {
        x.className = "main-small2";
    }
}
</script>			

