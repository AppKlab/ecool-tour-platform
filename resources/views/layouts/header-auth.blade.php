@if (Auth::guest())

@else
<nav class="main-small">
		<div class="wrapper">
			<div class="topnav2" id="myTopnav2" style="width:100%">
    			<a class="logostyle2" href="{{ url('home') }}"><img src="{{ asset('images/logo.png') }}" alt="Logo" height="60px"></a>
    			<a class="menuitemblank2" href=""></a>
    			@if (Session::get('isAdmin'))
            		<a class="menuitem2" href="{{ url('/Admin/itineraries') }}">@lang('texts.itineraries')</a>
            		<a class="menuitem2" href="{{ url('/Admin/modules') }}">@lang('forms.modules')</a>
            		<a class="menuitem2" href="{{ url('/Admin/users') }}">@lang('forms.users')</a>
            	@else
            		@php 
            			$userid = Auth::user()->id; 
            			$userType = Auth::user()->userType;
            		@endphp
            		
            		<!-- User = Agency -->
            		@if ($userType == 2 or $userType == 4)
                		<a class="menuitem2" href="{{ url('/itineraries') }}">@lang('forms.myitineraries')</a>
                		<a class="menuitem2" href="{{ url('/newitinerary') }}">@lang('forms.newitinerary')</a>
                    <!-- User = Provider -->
                    @elseif($userType == 3)
                   		<a class="menuitem2" href="{{ url('/modules') }}">@lang('forms.mymodules')</a>
                		<a class="menuitem2" href="{{ url('/newmodule') }}">@lang('forms.newmodule')</a>
                    <!-- User = Group -->
                    @endif
            	@endif
    			@if (Session::get('isAdmin'))
                    
            	@else
            		<a class="menuitem2" href="{{ url("/myprofile/$userid") }}">@lang('forms.profile')</a>
            	@endif
				 <a href="javascript:void(0);" class="icon2" onclick="myFunction()">
				    <i class="fa fa-bars"></i>
				 </a>
			</div>
		</div>
	</nav>	
<script>		
function myFunction() {
    var x = document.getElementById("myTopnav2");
    if (x.className === "topnav2") {
        x.className += " responsive";
    } else {
        x.className = "topnav2";
    }

}
</script>			



@endif