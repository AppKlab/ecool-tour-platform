@extends('layouts.app')

@section('content')

@include('layouts.header-small')

<div class="wrapper">
	<div class="pagetitle">
		<div class="row">
    		<h4 class="pagetitle">&nbsp;</h3>
    		<h3 class="pagetitle">@lang('forms.cat'.$cat)</h3>
    		<img src="{{ asset('images/pixel.png') }}" alt="@lang('forms.cat'.$cat)" />
    		<h5 class="pagetitle">&nbsp;</h3>
    	</div>
	</div>
</div>

<div class="wrapper">
    <div class="modulecat-row">
    	@php
    		$i = 0;
    	@endphp
        
        @foreach ($modules as $module)
            
				@if ($i == 3)
					</div>
					<div class="modulecat-row">
					@php
                		$i = 0;
                	@endphp
        		@endif
        		
            <div class="modulecat-row-item">
            <a href="{{url('offer/' . $module->id)}}">
			 @if ($userlanguage == 'en')
            	<img src='{{ asset('upload_media/modules/'.$module->mainimage) }}' class="img-rounded modcatimg" alt="{{ $module->modulename }}" />
			 @else
        		 @foreach ($mainimages as $key => $value)

                     @if (($value->idModule ==  $module->idModule) and ($value->user_id ==  $module->user_id) )                    	
        						<img src='{{ asset('upload_media/modules/'.$value->mainimage) }}' class="img-rounded modcatimg" alt="{{ $module->modulename }}" />
        			@endif
				@endforeach
			 @endif	
            	<br />
                <h6>{{ $module->modulename }}</h6><br />
                <h5 class="pagetext">{{ $module->description }}</h5><br /><br /></a>
			</div>
			@php
				$i = $i+1;
			@endphp
        @endforeach
        @if ($i >0 and $i < 3)
			@for ($x = $i; $x < 3; $x++)
   				<div class="col-md-4">&nbsp;</div>
			@endfor
   		@endif
    </div>
</div>
<br/>

	<div id="gmap"></div>
      <script src="https://maps.googleapis.com/maps/api/js?key=YourKeyHere&callback=initMap" async defer></script>
      <script>
      function initMap() {
    	var gmap = new google.maps.Map(document.getElementById('gmap'), {
    		center: {lat: 46.490000 , lng: 16.039000 },
    		zoom: 9.5,
    		/*** blocks or allows control bars ***/
    		zoomControl: true,			
		    mapTypeControl: true,
		    scaleControl: false,
		    streetViewControl: true,
		    rotateControl: false,
		    fullscreenControl: false,
		    /*** blocks ctrl+scroll option for zoom ***/
		    //gestureHandling: 'greedy',  
		    /*** blocks hand drag option ***/
		    //draggable: false, 
		    		
    	});

    	var image = '{{ asset('images/map_marker.png') }}';
    	
    	/*** Markers ***/
    	@foreach ($modules as $module)
        
            @php
           		$mapimage = '';
    			$geolocationLat = '';
				$geolocationLon = '';
				$maplocation = '';
           	@endphp
            	
			 @if ($userlanguage == 'en')
            	@php
            		$mapimage = $module->mainimage;
            		$geolocationLat = $module->geolocationLat;
            		$geolocationLon = $module->geolocationLon;
            		$maplocation = $module->address.','.$module->zip.' '.$module->city;
            	@endphp
			 @else
        		 @foreach ($mainimages as $key => $value)
                    @if (($value->idModule ==  $module->idModule) AND ($value->user_id ==  $module->user_id) )                    	
						@php
                    		$mapimage = $value->mainimage;
                    		$geolocationLat = $value->geolocationLat;
                    		$geolocationLon = $value->geolocationLon;
                    		$maplocation = $value->address.','.$value->zip.' '.$value->city;
                    	@endphp
            		@endif
				@endforeach
			 @endif	

			
	    	
	    	var marker{{ $module->id }} = new google.maps.Marker({
	    	  position: {lat: {{ $geolocationLat }} , lng: {{ $geolocationLon }} },
	    	  map: gmap,
	          icon: image,
	    	  title: '{{ $module->modulename }}',
	    	});	
	    	var content{{ $module->id }} = '<div class="googlemaps"><a href="{{url('offer/' . $module->id)}}"><img src="{{ asset("upload_media/modules/".$mapimage) }}" alt="{{ $module->modulename }}"/><h3 class="googlemaps">{{ $maplocation }}</h3><h4 class="googlemaps">{{ $module->modulename }}</h4><p class="googlemaps">{{ $module->description }}</p></a></div>';
	    	var infoWindow{{ $module->id }} = new google.maps.InfoWindow({
	    	  content: content{{ $module->id }},
	    	  maxWidth: 350,
	    	  maxHeight:470,
	    	  
	    	});


	    	marker{{ $module->id }}.addListener('click', function() {
	    	  infoWindow{{ $module->id }}.open(gmap, marker{{ $module->id }});
	    	});

	    	gmap.addListener('click', function() {
	      	  infoWindow{{ $module->id }}.close(gmap, marker{{ $module->id }});
	      	});


        	google.maps.event.addListener(infoWindow{{ $module->id }}, 'domready', function() {

     		   // Reference to the DIV which receives the contents of the infowindow using jQuery
     		   var iwOuter = $('.gm-style-iw');

     		   /* The DIV we want to change is above the .gm-style-iw DIV.
     		    * So, we use jQuery and create a iwBackground variable,
     		    * and took advantage of the existing reference to .gm-style-iw for the previous DIV with .prev().
     		    */
     		   var iwBackground = iwOuter.prev();

     		   // Remove the background shadow DIV
     		   iwBackground.children(':nth-child(2)').css({'display' : 'none'});

     		   // Remove the white background DIV
     		   iwBackground.children(':nth-child(4)').css({'display' : 'none'});

     		   // Remove the white arrow down DIV
     		   iwBackground.children(':nth-child(3)').css({'display' : 'none'});
     		   iwBackground.children(':nth-child(1)').css({'display' : 'none'});

     		});

        @endforeach
        
        var imagem = '{{ asset('images/map_marker_monuments.png') }}';

        /*** Markers for monuments ***/
    	@foreach ($monuments as $monument)
        
	    	var markerm{{ $monument->id }} = new google.maps.Marker({
	    	  position: {lat: {{ $monument->geolocationLat }} , lng: {{ $monument->geolocationLon }} },
	    	  map: gmap,
	          icon: imagem,
	    	  title: '{{ $monument->monumentName }}',
	    	});	
	    	var contentm{{ $monument->id }} = '<div class="googlemaps"><h3 class="googlemaps">{{ $monument->monumentAddress }}</h3><h4 class="googlemaps">{{ $monument->monumentName }}</h4></div>';
	    	var infoWindowm{{ $monument->id }} = new google.maps.InfoWindow({
	    	  content: contentm{{ $monument->id }},
	    	  maxWidth: 350,
	    	  maxHeight:470,
	    	  
	    	});


	    	markerm{{ $monument->id }}.addListener('click', function() {
	    	  infoWindowm{{ $monument->id }}.open(gmap, markerm{{ $monument->id }});
	    	});

	    	gmap.addListener('click', function() {
	      	  infoWindowm{{ $monument->id }}.close(gmap, markerm{{ $monument->id }});
	      	});


        	google.maps.event.addListener(infoWindowm{{ $monument->id }}, 'domready', function() {

     		   // Reference to the DIV which receives the contents of the infowindow using jQuery
     		   var iwOuter = $('.gm-style-iw');

     		   /* The DIV we want to change is above the .gm-style-iw DIV.
     		    * So, we use jQuery and create a iwBackground variable,
     		    * and took advantage of the existing reference to .gm-style-iw for the previous DIV with .prev().
     		    */
     		   var iwBackground = iwOuter.prev();

     		   // Remove the background shadow DIV
     		   iwBackground.children(':nth-child(2)').css({'display' : 'none'});

     		   // Remove the white background DIV
     		   iwBackground.children(':nth-child(4)').css({'display' : 'none'});

     		   // Remove the white arrow down DIV
     		   iwBackground.children(':nth-child(3)').css({'display' : 'none'});
     		   iwBackground.children(':nth-child(1)').css({'display' : 'none'});

     		});

        @endforeach
    
    }
    </script>
@endsection
