@extends('layouts.app')

@section('content')

@include('layouts.header-small')
<div class="wrapper">
	<div class="pagetitle">
		<div class="row">
    		<h4 class="pagetitle">&nbsp;</h3>
    		<h3 class="pagetitle">{{ $itinerary->itineraryname }}</h3>
    		<img src="{{ asset('images/pixel.png') }}" alt="{{ $itinerary->itineraryname }}" />
    		<h5 class="pagetitle">{{ $itineraryFrom }} - {{ $itineraryTo }}</h3>
    	</div>
	</div>
</div>
<p>&nbsp;</p>
<p>&nbsp;</p>
<div class="row">
    <div class="pagetags">
    	<div class="wrapper">
            <table>
            	<tr>
            		<td style="padding-right:20px;">
            			<p><b>{{ $agency->title }}</b></p>
                		<h5 class="pagetext">
                			{{ $agency->address }}<br/>
                			{{ $agency->telephone }} / {{ $agency->cellphone }} / {{ $agency->email }}<br/>
                			{{ $agency->www }}<br/>
                		</h5>
                	</td>
            		<td>
            			<img class="pdf-image" src='{{ asset('upload_media/logos/'.$agency->logo) }}' width="200px" alt="{{ $agency->title }}" />
            		</td>
            	</tr>
            </table>
         </div>
	</div>
</div>
<p>&nbsp;</p>
<p>&nbsp;</p>
<div class="wrapper">
    <table width="100%">
    	<tr>
    		<td class="itinerary">
                @if (isset($itineraryparent->mainimage))
                	@php $itinerarymainimage = $itineraryparent->mainimage; @endphp
                @else
                	@php $itinerarymainimage = $itinerary->mainimage; @endphp
                @endif
                <img src='{{ asset('upload_media/itineraries/'.$itinerarymainimage) }}' height="300px" alt="{{ $itinerary->itineraryname }}" />
        	</td>
        	<td style="padding-left:20px;"  class="itinerary pagetext">
				<p>{!! nl2br(e($itinerary->description)) !!}</p>
				<p><b>@lang('forms.personsMin'):</b> {{ isset($itineraryparent->personsMin) ? $itineraryparent->personsMin :  $itinerary->personsMin }}</p>
                <p><b>@lang('forms.personsMax'):</b> {{ isset($itineraryparent->personsMax) ? $itineraryparent->personsMax :  $itinerary->personsMax }}</p>
                <p><b>@lang('forms.price'):</b> {{ isset($itineraryparent->price) ? $itineraryparent->price :  $itinerary->price }} EUR</p>
        	</td>
        </tr>
        <tr>
        	<td colspan="2">
    			<p>&nbsp;</p>
                <h6>@lang('texts.descriptionitinerary')</h6><br/>
        	</td>
        </tr>
    	<tr>
    		<td colspan="2"  class="pagetext">
    			<p>{!! nl2br(e($itinerary->longdescription)) !!}</p>
                <br/>
            </td>
        </tr>
   	 </table>

    <table width="100%" class="pdf-table">
    	<tr>
    		<td colspan="2">
                <h6>@lang('texts.descriptionmodules')</h6>
        	</td>
    	</tr>
   	</table>

	<table>
        @if (count($itinerarymodules) > 0)
    
            @foreach ($itinerarymodules as $itinerarymodule)
    	        @php
            		$parentModuleId = $itinerarymodule->idModule;
            	@endphp                    	
                <tr>
                  	<td colspan="2"  class="itinerary itinerarymod pagetext">
            			<div>
                            @if ($userlanguage == 'en')
                            	<b>{{ $moduleParent[$parentModuleId]->modulename }}</b><br/>
                            	{{ $itinerarymodule->dateFrom }} - {{ $itinerarymodule->dateTo }}<br/>
                            	{!! nl2br(e($moduleParent[$parentModuleId]->description)) !!}
                            @else
                            	<b>{{ $moduleChild[$parentModuleId]->modulename }}</b><br/>
                            	{!! nl2br(e($moduleChild[$parentModuleId]->description)) !!}
                            @endif
                        </div>
		        	</td>
		        </tr>
                <tr>
                    <td class="itinerary itinerarymod pagetext">
                        <img src='{{ asset('upload_media/modules/'.$moduleParent[$parentModuleId]->mainimage) }}' width="300px" alt="{{ $itinerary->itineraryname }}" />
                    </td>
                    
                	@if ($moduleParent[$parentModuleId]->enabled == '0')
                    	<td>@lang('forms.disabledadmin')</td>
               		@else 
                   		<td class="itinerary itinerarymod pagetext">
                            @if ($userlanguage == 'en')
                            	{!! nl2br(e($moduleParent[$parentModuleId]->longdescription)) !!}
                            @else
                            	{!! nl2br(e($moduleChild[$parentModuleId]->longdescription)) !!}
                            @endif
                        </td>
               		@endif
                  </tr>
                  <tr>
                  	<td colspan="2">
            			<br/>
		        	</td>
		          </tr>
            @endforeach
    
        @else
            <tr>
                <td colspan='3'>
                    @lang('forms.noitinerarymodules')
                </td>
            </tr>
        
        @endif
    </table>
	<table width="100%">
    	<tr>
            <td colspan="2">
                <h6>@lang('forms.pricedescription')</h6>
                <p>{!! nl2br(e($itinerary->pricedescription)) !!}</p>
                <br/>
        	</td>
    	</tr>
   	 </table>
</div>

@endsection


