@extends('layouts.app')

@section('content')

@include('layouts.header-small')

<div class="wrapper">
	<div class="pagetitle">
		<div class="row">
    		<h4 class="pagetitle">
    		@if ($module->idLang == 'en')
    			{{ $module->city }}
    		@else
    			{{ $parentmodule->city }}
    		@endif
    		</h4>
    		<h3 class="pagetitle">{{ $module->modulename }}</h3>
    		<img src="{{ asset('images/pixel.png') }}" alt="{{ $module->modulename }}" />
    		<h5 class="pagetitle">{{ $module->description }}</h3>
    	</div>
	</div>
</div>
<div class="row">
    <div class="pagetags">
       <nav class="tags">
			<ul>
            	@foreach ($tags as $tag)
            		<li>@lang('forms.tag'.$tag->idTag)</li>
            		<li>&nbsp;</li>
            	@endforeach
			</ul>
		</nav>
	</div>
</div>

<div class="wrapper">
    <div class="row">
    	 @if (count($gallery) > 0)
    	   <!-- Carousel -->
            <div id="GalleryCarousel" class="carousel slide">
                <div class="carousel-inner" role="listbox">
                    <div class="item active module-carousel">
                      	 @if ($module->idLang == 'en')
                        	<img src='{{ asset('upload_media/modules/'.$module->mainimage) }}'  alt="{{ $module->modulename }}" />
                		 @else
                			<img src='{{ asset('upload_media/modules/'.$parentmodule->mainimage) }}' alt="{{ $module->modulename }}" />
                		 @endif
                    </div>
                    @foreach ($gallery as $image)
                		<div class="item module-carousel">
                			<img src='{{ asset('upload_media/images/'.$image->imagename) }}' alt="{{ $module->modulename }}" />
                		</div>
                	@endforeach
                </div>
            	<a class="left carousel-control" data-slide="prev" href="#GalleryCarousel"><span class="icon-prev"></span></a>
  			<a class="right carousel-control" data-slide="next" href="#GalleryCarousel"><span class="icon-next"></span></a>
            </div>
    	 @else
        	<div class="item active module-carousel">
            	 @if ($module->idLang == 'en')
                	<img src='{{ asset('upload_media/modules/'.$module->mainimage) }}' alt="{{ $module->modulename }}" />
        		 @else
        			<img src='{{ asset('upload_media/modules/'.$parentmodule->mainimage) }}' alt="{{ $module->modulename }}" />
        		 @endif
    		 </div>
        @endif
       	<hr/>
    </div>
</div>	

<div class="wrapper">
	<div class="row pagetextcontainer">
        <div class="col-md-8 pagetext">
            <p>{!! nl2br(e($module->longdescription)) !!}</p>
            <br/>
        </div>
        <div class="col-md-4 pagetext">    
             @if ($module->idLang == 'en')
            	<p><b>@lang('texts.capacity')</b></p>
            	<p>@lang('texts.from') {{ $module->personsMin }}
                	@lang('texts.to') {{ $module->personsMax }}</p>
                <p><b>@lang('forms.price')</b></p>
                <p>{{ $module->price }} EUR</p>
                @php
					$geolocationLat = $module->geolocationLat;
					$geolocationLon = $module->geolocationLon;
					$mapimage = $module->mainimage;
					$maplocation = $module->address.','.$module->zip.' '.$module->city;
                @endphp
    		 @else
    			<p><b>@lang('texts.capacity')</b></p>
            	<p>@lang('texts.from') {{ $parentmodule->personsMin }}
                	@lang('texts.to') {{ $parentmodule->personsMax }}</p>
                <p><b>@lang('forms.price')</b></p>
                <p>{{ $parentmodule->price }} EUR</p>
                @php
					$geolocationLat = $parentmodule->geolocationLat;
					$geolocationLon = $parentmodule->geolocationLon;
					$mapimage = $parentmodule->mainimage;
					$maplocation = $parentmodule->address.','.$parentmodule->zip.' '.$parentmodule->city;
                @endphp
    		 @endif
    		 <p><b>@lang('forms.pricedescription')</b></p>
             <p>{!! nl2br(e($module->pricedescription)) !!}</p>
    		 @if (count($schedules) > 0)
        		 <p><b>@lang('forms.schedule')</b></p>
        		 @foreach ($schedules as $schedule)
            		<p>{{ $schedule->dateFrom }} - {{ $schedule->dateTo }}</p>
            	 @endforeach
             @endif	
             <p><b>@lang('texts.contact')</b></p>
             <p>{{ $provider->telephone }}</p>
             <p>{{ $provider->cellphone }}</p>
             <p>{{ $provider->email }}</p>
             <br/>
            

            @if (!empty($agencies))
				@php
					$i = 0;
				@endphp
				@foreach ($agencies as $agency)
					@if (!empty($agency->title))
						@if ($i == 0)
    						<p><b>@lang('texts.offeredby')</b></p>
    						@php
                				$i = 1;
                			@endphp
    					@endif
						<p><img src='{{ asset('upload_media/logos_thumbs/'. $agency->logo ) }}' alt="{{ $agency->title }}" width = "100px" /><br />{{ $agency->title }}</p>
					@endif
				@endforeach
    		@endif	
		</div>	
    </div>
</div>

<div class="row">
	<div class="wrapper">
	   <div class="videourl">
       		<iframe width="306" height="200" class="frame" src="https://www.youtube.com/embed/{{ $module->videoUrl1 }}?rel=0&showinfo=0&color=white&iv_load_policy=3" frameborder="0" allowfullscreen></iframe>
       </div>
    </div>
</div>

<div class="row">
      <div id="gmap"></div>
      <script src="https://maps.googleapis.com/maps/api/js?key=YourKeyHere&callback=initMap" async defer></script>
      <script>
      function initMap() {
    	var gmap = new google.maps.Map(document.getElementById('gmap'), {
    		center: {lat: {{ $geolocationLat }} , lng: {{ $geolocationLon }} },
    		zoom: 16,
    		/*** blocks or allows control bars ***/
    		zoomControl: true,			
		    mapTypeControl: true,
		    scaleControl: false,
		    streetViewControl: true,
		    rotateControl: false,
		    fullscreenControl: false,
    	});
    	
    	/*** Marker ***/
    	var image = '{{ asset('images/map_marker.png') }}';
    	var marker = new google.maps.Marker({
    	  position: {lat: {{ $geolocationLat }} , lng: {{ $geolocationLon }} },
    	  map: gmap,
          icon: image,
    	  title: '{{ $module->modulename }}',
    	});	
    	var content = '<div class="googlemaps"><img src="{{ asset("upload_media/modules/".$mapimage) }}" alt="{{ $module->modulename }}"/><h3 class="googlemaps">{{ $maplocation }}</h3><h4 class="googlemaps">{{ $module->modulename }}</h4><p class="googlemaps">{{ $module->description }}</p></div>';
    	var infoWindow = new google.maps.InfoWindow({
    	  content: content,
    	  maxWidth: 350,
    	  maxHeight:470,
    	  
    	});

    	marker.addListener('click', function() {
    	  infoWindow.open(gmap, marker);
    	});

    	gmap.addListener('click', function() {
      	  infoWindow.close(gmap, marker);
      	});

    	google.maps.event.addListener(infoWindow, 'domready', function() {

    		   // Reference to the DIV which receives the contents of the infowindow using jQuery
    		   var iwOuter = $('.gm-style-iw');

    		   /* The DIV we want to change is above the .gm-style-iw DIV.
    		    * So, we use jQuery and create a iwBackground variable,
    		    * and took advantage of the existing reference to .gm-style-iw for the previous DIV with .prev().
    		    */
    		   var iwBackground = iwOuter.prev();

    		   // Remove the background shadow DIV
    		   //iwBackground.children(':nth-child(2)').css({'display' : 'none'});

    		   // Remove the white background DIV
    		   iwBackground.children(':nth-child(4)').css({'display' : 'none'});

    		   // Remove the white arrow down DIV
    		   iwBackground.children(':nth-child(3)').css({'display' : 'none'});
    		   iwBackground.children(':nth-child(1)').css({'display' : 'none'});

    		});

    	var imagem = '{{ asset('images/map_marker_monuments.png') }}';

        /*** Markers for monuments ***/
    	@foreach ($monuments as $monument)
        
	    	var markerm{{ $monument->id }} = new google.maps.Marker({
	    	  position: {lat: {{ $monument->geolocationLat }} , lng: {{ $monument->geolocationLon }} },
	    	  map: gmap,
	          icon: imagem,
	    	  title: '{{ $monument->monumentName }}',
	    	});	
	    	var contentm{{ $monument->id }} = '<div class="googlemaps"><h3 class="googlemaps">{{ $monument->monumentAddress }}</h3><h4 class="googlemaps">{{ $monument->monumentName }}</h4></div>';
	    	var infoWindowm{{ $monument->id }} = new google.maps.InfoWindow({
	    	  content: contentm{{ $monument->id }},
	    	  maxWidth: 350,
	    	  maxHeight:470,
	    	  
	    	});


	    	markerm{{ $monument->id }}.addListener('click', function() {
	    	  infoWindowm{{ $monument->id }}.open(gmap, markerm{{ $monument->id }});
	    	});

	    	gmap.addListener('click', function() {
	      	  infoWindowm{{ $monument->id }}.close(gmap, markerm{{ $monument->id }});
	      	});


        	google.maps.event.addListener(infoWindowm{{ $monument->id }}, 'domready', function() {

     		   // Reference to the DIV which receives the contents of the infowindow using jQuery
     		   var iwOuter = $('.gm-style-iw');

     		   /* The DIV we want to change is above the .gm-style-iw DIV.
     		    * So, we use jQuery and create a iwBackground variable,
     		    * and took advantage of the existing reference to .gm-style-iw for the previous DIV with .prev().
     		    */
     		   var iwBackground = iwOuter.prev();

     		   // Remove the background shadow DIV
     		   iwBackground.children(':nth-child(2)').css({'display' : 'none'});

     		   // Remove the white background DIV
     		   iwBackground.children(':nth-child(4)').css({'display' : 'none'});

     		   // Remove the white arrow down DIV
     		   iwBackground.children(':nth-child(3)').css({'display' : 'none'});
     		   iwBackground.children(':nth-child(1)').css({'display' : 'none'});

     		});

        @endforeach
    
    }
    </script>
</div>
<!-- JavaScripts -->
<script src="{{ asset('js/jquery-2-14.min.js') }}"></script>
@endsection
