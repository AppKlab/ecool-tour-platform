@extends('layouts.app')

@section('content')

@include('layouts.header-small')

<div class="wrapper">
	<div class="pagetitle">
		<div class="row">
    		<h4 class="pagetitle">&nbsp;</h3>
    		<h3 class="pagetitle">@lang('texts.sklop4')</h3>
    		<img src="{{ asset('images/pixel.png') }}" alt="@lang('texts.sklop4')" />
    		<h5 class="pagetitle">&nbsp;</h3>
    	</div>
	</div>
</div>

<div class="wrapper">
    <div class="modulecat-row">
    	@php
    		$i = 0;
    	@endphp
        
        @if (count($itineraries) > 0)
        
            @foreach ($itineraries as $itinerary)
    
    				@if ($i == 3)
    					</div>
    					<div class="modulecat-row">
    					@php
                    		$i = 0;
                    	@endphp
            		@endif
                <div class="modulecat-row-item">
                    <a href="{{url('package/' . $itinerary->id)}}">
            			 @if ($userlanguage == 'en')
                        	<img src="{{ asset('upload_media/itineraries/'.$itinerary->mainimage) }}" class="img-rounded modcatimg" alt="{{ $itinerary->itineraryname }}" />
            			 @else
                    		 @foreach ($mainimages as $key => $value)
                                 @if (($value->idItinerary ==  $itinerary->idItinerary) and ($value->user_id ==  $itinerary->user_id) )                    	
                    				<img src="{{ asset('upload_media/itineraries/'.$value->mainimage) }}" class="img-rounded modcatimg" alt="{{ $itinerary->itineraryname }}" />
                    			@endif
            				@endforeach
            			 @endif	
                    	 <br />
                         <h6>{{ $itinerary->itineraryname }}</h6><br />
                         <h5 class="pagetext">{{ $itinerary->description }}</h5><br /><br />
                     </a>
    			</div>
    			@php
    				$i = $i+1;
    			@endphp
            @endforeach
        @endif
        @if ($i >0 and $i < 3)
			@for ($x = $i; $x < 3; $x++)
   				<div class="col-md-4">&nbsp;</div>
			@endfor
   		@endif
    </div>
<br/>
</div>
    
@endsection
