@extends('layouts.app')

@section('content')

@include('layouts.header-small')

<div class="wrapper">
	<div class="pagetitle">
		<div class="row">
    		<h3 class="pagetitle">@lang('forms.search')</h3>
    		<img src="{{ asset('images/pixel.png') }}" alt="@lang('forms.search')" />
    	</div>
	</div>
</div>
@php
	$tagNames = array ( 
                "1" => "Kulinarika",
                "2" => "Prenocisca",
                "3" => "Izobrazevalne vsebine",
                "4" => "Vodeni ogledi",
                "5" => "Dostop za invalide",
                "6" => "Kultura",
                "7" => "Zivali"
            ); 
                                
	$categoryNames = array ( 
                "1" => "Kulinarika",
                "2" => "Dozivetja",
                "3" => "Dogodki"
            ); 
@endphp
<div class="wrapper">
<br />
	<form action="{{ url('search') }}" method="POST" class="form-horizontal" role="form">
    {!! csrf_field() !!}
       	
    	<div class="row search-top"><br />
        </div>
        
        <div class="row">
              <div class="col-md-3" style="text-align:right;">
                    <input type="text" class="form-control" name="search" style="width:265px"  value="{{ isset($request['search']) ? $request['search'] : '' }}"/>
                        <br/>
                </div>
				<div class="col-md-3" style="text-align:left;">
                    <button type="submit" class="btn btn-primary">
                            <i class="fas fa-search"></i>&nbsp;
                                	@lang('forms.search')
                        </button>
                </div>
				<div class="col-md-6" style="text-align:right;">
                    <a href="#" onclick="myFunction()">@lang('forms.filter')</a>
                </div>
                
        </div>
        <div class="row" id="myDIV" style="display: none">
        
        	<div class="col-md-4">
                @lang('forms.location')<br/>
    			@if(isset($request['location']))
                	@php
            			$checkedlocs = $request['location'];
            		@endphp
            	@endif
    			@foreach ($locations as $location)
                    	<div class="checkbox">
                    		<label>
							@if(isset($checkedlocs[$location->city]) AND $checkedlocs[$location->city] == 'on')
                            		<input data-toggle="toggle" type="checkbox" data-size="mini" name="location[{{ $location->city }}]"  data-on=" " data-off=" "  checked>
                            @else
									<input data-toggle="toggle" type="checkbox" data-size="mini" name="location[{{ $location->city }}]"  data-on=" " data-off=" ">
                            @endif
							{{ $location->city }}
							</label>
						</div>
        			<br />
				@endforeach
				
				<br />
			</div>
			<div class="col-md-4">
				<!-- Categories -->
                @lang('forms.categories')<br />

            	@php $i=1; @endphp
                @if(isset($request['category']))
                	@php
            			$checkedcats = $request['category'];
            		@endphp
            	@endif
                @foreach ($categoryNames as $key => $value)
                    	<div class="checkbox">
                    		<label>
                    		@if(isset($checkedcats[$key]) AND $checkedcats[$key] == 'on')
                            		<input data-toggle="toggle" type="checkbox" data-size="mini" name="category[{{ $key }}]" data-on=" " data-off=" " checked />
                            @else
									<input data-toggle="toggle" type="checkbox" data-size="mini" name="category[{{ $key }}]"  data-on=" " data-off=" " />
                            @endif
							@lang('forms.cat'.$i)
							</label>
						</div>
                    @php $i = $i+1; @endphp   
        			<br />
				@endforeach
		
			</div>
			<div class="col-md-4">
				
				<!-- Tags -->
				@lang('forms.tags')<br/>
            	@php $i=1; @endphp
                @if(isset($request['tag']))
                	@php
            			$checkedtags = $request['tag'];
            		@endphp
            	@endif
                @foreach ($tagNames as $key => $value)
                    	<div class="checkbox">
                    		<label>
							@if(isset($checkedtags[$key]) AND $checkedtags[$key] == 'on')
                            		<input data-toggle="toggle" type="checkbox" data-size="mini" name="tag[{{ $key }}]"  data-on=" " data-off=" " checked />
                            @else
									<input data-toggle="toggle" type="checkbox" data-size="mini" name="tag[{{ $key }}]"  data-on=" " data-off=" " />
                            @endif
							@lang('forms.tag'.$i)
							</label>
						</div>
                    @php $i = $i+1 @endphp       
        			<br />
				@endforeach
			</div>
		</div>	
	    <div class="row search-bottom">
        </div>
	</form>
	<br />
	@if(isset($results))
    	<div class="row">
    		<p><b>@lang('texts.searchresults'):</b> {{ $results }}</p>
        </div>
        <br />
        @if ($results > 0)
            <div class="row">
        		@php
            		$i = 0;
            	@endphp
            	@foreach ($modulesmap as $module)
        			@if ($i == 3)
        				</div>
        				<div class="row">
        				@php
                    		$i = 0;
                    	@endphp
            		@endif
        			@if ($userlanguage == 'en')
                    	@php
                    		$mapimage = $module->mainimage;
                    		$maplocation = $module->address.','.$module->zip.' '.$module->city;
                    		$city = $module->city;
                    	@endphp
        			 @else
                		 @foreach ($mainimagesmap as $key => $value)
                            @if (($value->idModule ==  $module->idModule) AND ($value->user_id ==  $module->user_id) )                    	
        						@php
                            		$mapimage = $value->mainimage;
                            		$maplocation = $value->address.','.$value->zip.' '.$value->city;
                            		$city = $value->city;
                            	@endphp
                    		@endif
        				@endforeach
        			 @endif	
        			<div class="col-md-4 text-center"><a href="{{url('offer/' . $module->id)}}"><img src="{{ asset("upload_media/modules/".$mapimage) }}" alt="{{ $module->modulename }}" width='100px'/>
        			<h5 class="pagetitle"><b>{{ $module->modulename }}</b></h5>
        			<h4 class="pagetitle">{{ $city }}</h4>
        			<!-- <p class="pagetext">{{ $module->description }}</p> --></a></div>
        			@php
        				$i = $i+1;
        			@endphp
        		@endforeach
        		@if ($i >0 and $i < 3)
        			@for ($x = $i; $x < 3; $x++)
           				<div class="col-md-4">&nbsp;</div>
        			@endfor
           		@endif
            </div>
            <br />
         @else
            <div class="row">@lang('texts.showingall')</div>
            <br />
         @endif
    @endif
    <div class="row">      	
		<div id="gmap"></div>
          <script src="https://maps.googleapis.com/maps/api/js?key=YourKeyHere&callback=initMap" async defer></script>
          <script>
          function initMap() {
        	var gmap = new google.maps.Map(document.getElementById('gmap'), {
        		center: {lat: 46.490000 , lng: 15.639000 },
        		zoom: 9.5,
        		/*** blocks or allows control bars ***/
        		zoomControl: true,			
    		    mapTypeControl: true,
    		    scaleControl: false,
    		    streetViewControl: true,
    		    rotateControl: false,
    		    fullscreenControl: false,
    		    /*** blocks ctrl+scroll option for zoom ***/
    		    //gestureHandling: 'greedy',  
    		    /*** blocks hand drag option ***/
    		    //draggable: false, 
    		    		
        	});
    
        	var image = '{{ asset('images/map_marker.png') }}';
        	
        	/*** Markers ***/
        	@foreach ($modulesmap as $module)
            
                @php
               		$mapimage = '';
        			$geolocationLat = '';
    				$geolocationLon = '';
    				$maplocation = '';
               	@endphp
                	
    			 @if ($userlanguage == 'en')
                	@php
                		$mapimage = $module->mainimage;
                		$geolocationLat = $module->geolocationLat;
                		$geolocationLon = $module->geolocationLon;
                		$maplocation = $module->address.','.$module->zip.' '.$module->city;
                	@endphp
    			 @else
            		 @foreach ($mainimagesmap as $key => $value)
                        @if (($value->idModule ==  $module->idModule) AND ($value->user_id ==  $module->user_id) )                    	
    						@php
                        		$mapimage = $value->mainimage;
                        		$geolocationLat = $value->geolocationLat;
                        		$geolocationLon = $value->geolocationLon;
                        		$maplocation = $value->address.','.$value->zip.' '.$value->city;
                        	@endphp
                		@endif
    				@endforeach
    			 @endif	
    
    			
    	    	
    	    	var marker{{ $module->id }} = new google.maps.Marker({
    	    	  position: {lat: {{ $geolocationLat }} , lng: {{ $geolocationLon }} },
    	    	  map: gmap,
    	          icon: image,
    	    	  title: '{{ $module->modulename }}',
    	    	});	
    	    	var content{{ $module->id }} = '<div class="googlemaps"><a href="{{url('offer/' . $module->id)}}"><img src="{{ asset("upload_media/modules/".$mapimage) }}" alt="{{ $module->modulename }}"/><h3 class="googlemaps">{{ $maplocation }}</h3><h4 class="googlemaps">{{ $module->modulename }}</h4><p class="googlemaps">{{ $module->description }}</p></a></div>';
    	    	var infoWindow{{ $module->id }} = new google.maps.InfoWindow({
    	    	  content: content{{ $module->id }},
    	    	  maxWidth: 350,
    	    	  maxHeight:470,
    	    	  
    	    	});
    
    
    	    	marker{{ $module->id }}.addListener('click', function() {
    	    	  infoWindow{{ $module->id }}.open(gmap, marker{{ $module->id }});
    	    	});
    
    	    	gmap.addListener('click', function() {
    	      	  infoWindow{{ $module->id }}.close(gmap, marker{{ $module->id }});
    	      	});
    
    
            	google.maps.event.addListener(infoWindow{{ $module->id }}, 'domready', function() {
    
         		   // Reference to the DIV which receives the contents of the infowindow using jQuery
         		   var iwOuter = $('.gm-style-iw');
    
         		   /* The DIV we want to change is above the .gm-style-iw DIV.
         		    * So, we use jQuery and create a iwBackground variable,
         		    * and took advantage of the existing reference to .gm-style-iw for the previous DIV with .prev().
         		    */
         		   var iwBackground = iwOuter.prev();
    
         		   // Remove the background shadow DIV
         		   iwBackground.children(':nth-child(2)').css({'display' : 'none'});
    
         		   // Remove the white background DIV
         		   iwBackground.children(':nth-child(4)').css({'display' : 'none'});
    
         		   // Remove the white arrow down DIV
         		   iwBackground.children(':nth-child(3)').css({'display' : 'none'});
         		   iwBackground.children(':nth-child(1)').css({'display' : 'none'});
    
         		});
    
            @endforeach

            var imagem = '{{ asset('images/map_marker_monuments.png') }}';

            /*** Markers for monuments ***/
        	@foreach ($monuments as $monument)
            
    	    	var markerm{{ $monument->id }} = new google.maps.Marker({
    	    	  position: {lat: {{ $monument->geolocationLat }} , lng: {{ $monument->geolocationLon }} },
    	    	  map: gmap,
    	          icon: imagem,
    	    	  title: '{{ $monument->monumentName }}',
    	    	});	
    	    	var contentm{{ $monument->id }} = '<div class="googlemaps"><h3 class="googlemaps">{{ $monument->monumentAddress }}</h3><h4 class="googlemaps">{{ $monument->monumentName }}</h4></div>';
    	    	var infoWindowm{{ $monument->id }} = new google.maps.InfoWindow({
    	    	  content: contentm{{ $monument->id }},
    	    	  maxWidth: 350,
    	    	  maxHeight:470,
    	    	  
    	    	});


    	    	markerm{{ $monument->id }}.addListener('click', function() {
    	    	  infoWindowm{{ $monument->id }}.open(gmap, markerm{{ $monument->id }});
    	    	});

    	    	gmap.addListener('click', function() {
    	      	  infoWindowm{{ $monument->id }}.close(gmap, markerm{{ $monument->id }});
    	      	});


            	google.maps.event.addListener(infoWindowm{{ $monument->id }}, 'domready', function() {

         		   // Reference to the DIV which receives the contents of the infowindow using jQuery
         		   var iwOuter = $('.gm-style-iw');

         		   /* The DIV we want to change is above the .gm-style-iw DIV.
         		    * So, we use jQuery and create a iwBackground variable,
         		    * and took advantage of the existing reference to .gm-style-iw for the previous DIV with .prev().
         		    */
         		   var iwBackground = iwOuter.prev();

         		   // Remove the background shadow DIV
         		   iwBackground.children(':nth-child(2)').css({'display' : 'none'});

         		   // Remove the white background DIV
         		   iwBackground.children(':nth-child(4)').css({'display' : 'none'});

         		   // Remove the white arrow down DIV
         		   iwBackground.children(':nth-child(3)').css({'display' : 'none'});
         		   iwBackground.children(':nth-child(1)').css({'display' : 'none'});

         		});

            @endforeach
        
        }
        </script>
     </div>
</div>
<br />
<br />

<script>
    function myFunction() {
        var x = document.getElementById("myDIV");
        if (x.style.display === "none") {
            x.style.display = "block";
        } else {
            x.style.display = "none";
        }
    } 
</script>
@endsection
