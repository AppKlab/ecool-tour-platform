@extends('layouts.app')

@section('content')

@include('layouts.header')

@php
 //Order for the modules flex-row value
 $row[] = array ('6','3','3','3','3','6'); 
@endphp
		<section id="modules">
				<header>
					<nav class="modules">
						<div class="wrapper">
							<ul>
								<li class="categorieshide"><p>@lang('texts.sklopi'):</p></li>
								<li id="tag01"><a href="{{ url('food') }}" id="@lang('forms.cat1')">@lang('forms.cat1')</a></li>
								<li id="tag02"><a href="{{ url('experiences') }}" id="@lang('forms.cat2')">@lang('forms.cat2')</a></li>
								<li id="tag03"><a href="{{ url('events') }}" id="@lang('forms.cat3')">@lang('forms.cat3')</a></li>
								<li><a href="{{ url('packages') }}">@lang('texts.sklop4')</a></li>
								<li class="lihide">&nbsp</li>
								<li class="searchhide"><a href="{{ url('search') }}">@lang('texts.search')</a></li>
								<li class="search"><a href="{{ url('search') }}"><img src="{{ asset('images/icon-search-20px.svg') }}" alt="@lang('texts.search')"></a></li>
							</ul>
						</div>
					</nav>
				</header>
				<article>
					<div class="modules-content">
						<div class="module-row">
    						 	@php
                            		$i = 0;
                            	@endphp
                                @foreach ($modules as $module)
                                
									@if ($i == 3)
										</div>
										<div class="module-row">
                            		@endif
                                        
                                        @php
                							$flexrow = $row[0][$i];
                                       		$image = '';
                                       	@endphp
                                        	
                            			 @if ($userlanguage == 'en')
                                        	@php
                                        		$image = $module->mainimage;
                                        	@endphp
                            			 @else
                                    		 @foreach ($mainimages as $key => $value)
                                                @if (($value->idModule ==  $module->idModule) and ($value->user_id ==  $module->user_id) )                    	
                            						@php
                                                		$image = $value->mainimage;
                                                		$parentid = $value->id;
                                                	@endphp
            	                        		@endif
                            				@endforeach
                            			 @endif	
                            			 <div class="module-gallery col-md-{{ $flexrow }}" style="background: white url('{{ asset("upload_media/modules/".$image) }}') center center/cover no-repeat">
            								
            								<a href="{{url('offer/' . $module->id)}}"><h4>{{ $module->modulename }}</h4></a>
            								<div class="tags">
            									@if (isset($categories[$module->id]) or isset($parentid))

            										@if (isset($categories[$module->id]))
            											@php 
															$categories1 = $categories[$module->id];
														@endphp
            										@elseif(isset($parentid))
            											@php 
															$categories1 = $categories[$parentid];
														@endphp
            										@endif
            											
													@foreach ($categories1 as $category)
														<span class="tag tag0{{ $category->idCategory }}"></span>
														
												
													@endforeach
            									@endif
            								</div>
            							</div>
            							@php
                							unset($parentid);
                							$i = $i+1;
                						@endphp
                                @endforeach

                        </div>
                    </div>
                </article>
		</section>
		<section id="map">
				<header>
					<h2 class="map">@lang('texts.locations')</h2>
				</header>
				<div id="gmap"></div>
      <script src="https://maps.googleapis.com/maps/api/js?key=YourKeyHere&callback=initMap" async defer></script>
      <script>
      function initMap() {
    	var gmap = new google.maps.Map(document.getElementById('gmap'), {
    		center: {lat: 46.490000 , lng: 16.039000 },
    		zoom: 9.5,
    		/*** blocks or allows control bars ***/
    		zoomControl: true,			
		    mapTypeControl: true,
		    scaleControl: false,
		    streetViewControl: true,
		    rotateControl: false,
		    fullscreenControl: false,
    	});

    	var image = '{{ asset('images/map_marker.png') }}';
    	
    	/*** Markers ***/
    	@foreach ($modulesmap as $module)
        
            @php
           		$mapimage = '';
    			$geolocationLat = '';
				$geolocationLon = '';
				$maplocation = '';
           	@endphp
            	
			 @if ($userlanguage == 'en')
            	@php
            		$mapimage = $module->mainimage;
            		$geolocationLat = $module->geolocationLat;
            		$geolocationLon = $module->geolocationLon;
            		$maplocation = $module->address.','.$module->zip.' '.$module->city;
            	@endphp
			 @else
        		 @foreach ($mainimagesmap as $key => $value)
                    @if (($value->idModule ==  $module->idModule) AND ($value->user_id ==  $module->user_id) )                    	
						@php
                    		$mapimage = $value->mainimage;
                    		$geolocationLat = $value->geolocationLat;
                    		$geolocationLon = $value->geolocationLon;
                    		$maplocation = $value->address.','.$value->zip.' '.$value->city;
                    	@endphp
            		@endif
				@endforeach
			 @endif	

			
	    	
	    	var marker{{ $module->id }} = new google.maps.Marker({
	    	  position: {lat: {{ $geolocationLat }} , lng: {{ $geolocationLon }} },
	    	  map: gmap,
	          icon: image,
	    	  title: '{{ $module->modulename }}',
	    	});	
	    	var content{{ $module->id }} = '<div class="googlemaps"><a href="{{url('offer/' . $module->id)}}"><img src="{{ asset("upload_media/modules/".$mapimage) }}" alt="{{ $module->modulename }}"/><h3 class="googlemaps">{{ $maplocation }}</h3><h4 class="googlemaps">{{ $module->modulename }}</h4><p class="googlemaps">{{ $module->description }}</p></a></div>';
	    	var infoWindow{{ $module->id }} = new google.maps.InfoWindow({
	    	  content: content{{ $module->id }},
	    	  maxWidth: 350,
	    	  maxHeight:470,
	    	  
	    	});


	    	marker{{ $module->id }}.addListener('click', function() {
	    	  infoWindow{{ $module->id }}.open(gmap, marker{{ $module->id }});
	    	});

	    	gmap.addListener('click', function() {
	      	  infoWindow{{ $module->id }}.close(gmap, marker{{ $module->id }});
	      	});


        	google.maps.event.addListener(infoWindow{{ $module->id }}, 'domready', function() {

     		   // Reference to the DIV which receives the contents of the infowindow using jQuery
     		   var iwOuter = $('.gm-style-iw');

     		   /* The DIV we want to change is above the .gm-style-iw DIV.
     		    * So, we use jQuery and create a iwBackground variable,
     		    * and took advantage of the existing reference to .gm-style-iw for the previous DIV with .prev().
     		    */
     		   var iwBackground = iwOuter.prev();

     		   // Remove the background shadow DIV
     		   iwBackground.children(':nth-child(2)').css({'display' : 'none'});

     		   // Remove the white background DIV
     		   iwBackground.children(':nth-child(4)').css({'display' : 'none'});

     		   // Remove the white arrow down DIV
     		   iwBackground.children(':nth-child(3)').css({'display' : 'none'});
     		   iwBackground.children(':nth-child(1)').css({'display' : 'none'});

     		});

        @endforeach
        
		var imagem = '{{ asset('images/map_marker_monuments.png') }}';

        /*** Markers for monuments ***/
    	@foreach ($monuments as $monument)
        
	    	var markerm{{ $monument->id }} = new google.maps.Marker({
	    	  position: {lat: {{ $monument->geolocationLat }} , lng: {{ $monument->geolocationLon }} },
	    	  map: gmap,
	          icon: imagem,
	    	  title: '{{ $monument->monumentName }}',
	    	});	
	    	var contentm{{ $monument->id }} = '<div class="googlemaps"><h3 class="googlemaps">{{ $monument->monumentAddress }}</h3><h4 class="googlemaps">{{ $monument->monumentName }}</h4></div>';
	    	var infoWindowm{{ $monument->id }} = new google.maps.InfoWindow({
	    	  content: contentm{{ $monument->id }},
	    	  maxWidth: 350,
	    	  maxHeight:470,
	    	  
	    	});


	    	markerm{{ $monument->id }}.addListener('click', function() {
	    	  infoWindowm{{ $monument->id }}.open(gmap, markerm{{ $monument->id }});
	    	});

	    	gmap.addListener('click', function() {
	      	  infoWindowm{{ $monument->id }}.close(gmap, markerm{{ $monument->id }});
	      	});


        	google.maps.event.addListener(infoWindowm{{ $monument->id }}, 'domready', function() {

     		   // Reference to the DIV which receives the contents of the infowindow using jQuery
     		   var iwOuter = $('.gm-style-iw');

     		   /* The DIV we want to change is above the .gm-style-iw DIV.
     		    * So, we use jQuery and create a iwBackground variable,
     		    * and took advantage of the existing reference to .gm-style-iw for the previous DIV with .prev().
     		    */
     		   var iwBackground = iwOuter.prev();

     		   // Remove the background shadow DIV
     		   iwBackground.children(':nth-child(2)').css({'display' : 'none'});

     		   // Remove the white background DIV
     		   iwBackground.children(':nth-child(4)').css({'display' : 'none'});

     		   // Remove the white arrow down DIV
     		   iwBackground.children(':nth-child(3)').css({'display' : 'none'});
     		   iwBackground.children(':nth-child(1)').css({'display' : 'none'});

     		});

        @endforeach
    	
    	
    
    }
    </script>
		</section>
		<hr/>
		<section id="about">
			<div class="wrapper">
					<h2 class="dark">@lang('texts.aboutus')</h2>
					<br />
					<p class="hometext">
						@lang('texts.aboutustext')
					</p>
					<p>&nbsp;</p>
					<p class="hometext">    
                        <a href="{{url('how-to-register')}}" class="btn btn-success btn-about">@lang('texts.registernow')</a>
            		</p>
            		<p>&nbsp;</p>
            		<p>&nbsp;</p>
			</div>
		</section>

@endsection
