@extends('layouts.app')

@section('content')

@include('layouts.header-small')

<div class="wrapper">
	<div class="pagetitle">
		<div class="row">
    		<h4 class="pagetitle">&nbsp;</h3>
    		<h3 class="pagetitle">@lang('auth.register')</h3>
    		<img src="{{ asset('images/pixel.png') }}" alt="@lang('auth.register')" />
    		<h5 class="pagetitle">&nbsp;</h3>
    	</div>
	</div>
</div>

<div class="container pagetext">
    <div class="row">
        	<h7>@lang('forms.regagency')</h7>
        	<p>@lang('texts.regagency')</p>
        	<p class="hometext">    
                <a href="{{url('register/2')}}" class="btn btn-success btn-registration">@lang('texts.registernow')</a>
    		</p>
   	</div>
   	<hr class="gray" />
   	<div class="row">
        	<h7>@lang('forms.reggroup')</h7>
        	<p>@lang('texts.reggroup')</p>
        	<p class="hometext">    
                <a href="{{url('register/4')}}" class="btn btn-success btn-registration">@lang('texts.registernow')</a>
    		</p>
   	</div>
   	<hr class="gray" />
   	<div class="row">
        	<h7>@lang('texts.individuals1')</h7>
        	<p>@lang('texts.individuals2')</p>
   	</div>
   	<hr class="gray" />
   	<div class="row">
        	<h7>@lang('forms.regprovider')</h7>
        	<p>@lang('texts.regprovider')</p>
        	<p class="hometext">    
                <a href="{{url('register/3')}}" class="btn btn-success btn-registration">@lang('texts.registernow')</a>
    		</p>
   	</div>
</div>
<br />
@endsection
