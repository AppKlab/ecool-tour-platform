@extends('layouts.pdf')

@section('content')
<div class="wrapper">
    <table width="100%">
    	<tr>
    		<td width="75%">
    			<p class="agencytitle">
        			{{ $agency->title }}
        		</p>
        		<p class="agencytext">
        			{{ $agency->address }}<br/>
        			{{ $agency->telephone }} / {{ $agency->cellphone }} / {{ $agency->email }}<br/>
        			{{ $agency->www }}<br/>
        		</p>
        	</td>
    		<td width="25%">
    			<img class="pdf-image" src='{{ asset('upload_media/logos/'.$agency->logo) }}' width="200px" alt="{{ $agency->title }}" />
    		</td>
    	</tr>
    	<tr>
    		<td colspan="2">
    			<p>&nbsp;</p>
    			<hr />
    			<p>&nbsp;</p>
        	</td>
    	</tr>
    </table>

    <table width="100%" class="pdf-table">
    	<tr>
    		<td colspan="2">
    			<p class="pagetitle">
    				{{ $itinerary->itineraryname }}
    			<p>
        	</td>
    	</tr>
    	<tr>
    		<td colspan="2">
    				<p>{{ isset($itineraryparent->dateFrom) ? $itineraryparent->dateFrom :  $itinerary->dateFrom }} - {{ isset($itineraryparent->dateTo) ? $itineraryparent->dateTo :  $itinerary->dateTo }}</p>
        	</td>
    	</tr>
    	<tr>
    		<td>
                @if (isset($itineraryparent->mainimage))
                	@php $itinerarymainimage = $itineraryparent->mainimage; @endphp
                @else
                	@php $itinerarymainimage = $itinerary->mainimage; @endphp
                @endif
                <img src='{{ asset('upload_media/itineraries/'.$itinerarymainimage) }}' height="300px" alt="{{ $itinerary->itineraryname }}" />
        	</td>
        	<td>
				<p>{!! nl2br(e($itinerary->description)) !!}</p>
        	</td>
        </tr>
        <tr>
        	<td colspan="2">
    			<p>&nbsp;</p>
                <hr/>
                <p class="agencytitle">@lang('texts.descriptionitinerary')</p>
        	</td>
        </tr>
        <tr>
        	<td colspan="2">
    			<p>@lang('forms.personsMin'): {{ isset($itineraryparent->personsMin) ? $itineraryparent->personsMin :  $itinerary->personsMin }}</p>
                <p>@lang('forms.personsMax'): {{ isset($itineraryparent->personsMax) ? $itineraryparent->personsMax :  $itinerary->personsMax }}</p>
                <p>@lang('forms.price'): {{ isset($itineraryparent->price) ? $itineraryparent->price :  $itinerary->price }} EUR</p>
            </td>
        </tr>
        <tr>
            <td colspan="2">
    			<p>&nbsp;</p>
    			<hr/>
            </td>
    	</tr>
   </table>

   <table width="100%" class="pdf-table">
    	<tr>
    		<td colspan="2">
    			<p>{!! nl2br(e($itinerary->longdescription)) !!}</p>
                <hr/>
            </td>
        </tr>
    	<tr>
            <td colspan="2">
                <p class="agencytitle">@lang('forms.pricedescription')</p>
                <p>{!! nl2br(e($itinerary->pricedescription)) !!}</p>
                <hr/>
        	</td>
    	</tr>
   	 </table>

    <table width="100%" class="pdf-table">
    	<tr>
    		<td colspan="2">
    			<p>&nbsp;</p>
                <p class="agencytitle">@lang('texts.descriptionmodules')</p>
                <hr/>
        	</td>
    	</tr>
   	</table>

    <table>
        @if (count($itinerarymodules) > 0)
    
            @foreach ($itinerarymodules as $itinerarymodule)
    	        @php
            		$parentModuleId = $itinerarymodule->idModule;
            	@endphp                    	
                <tr>
                    <td>
                        <div>
                            @if ($userlanguage == 'en')
                            	<b>{{ $moduleParent[$parentModuleId]->modulename }}</b><br/>
                            	<span class="smalltext">{{ $itinerarymodule->dateFrom }} - {{ $itinerarymodule->dateTo }}</span><br/>
                            	{!! nl2br(e($moduleParent[$parentModuleId]->description)) !!}
                            @else
                            	<b>{{ $moduleChild[$parentModuleId]->modulename }}</b><br/>
                            	{!! nl2br(e($moduleChild[$parentModuleId]->description)) !!}
                            @endif
                        </div>
                    </td>
                    
                	@if ($moduleParent[$parentModuleId]->enabled == '0')
                    	<td>@lang('forms.disabledadmin')</td>
               		@else 
                   		<td width="133px">
                            <img src='{{ asset('upload_media/modules/'.$moduleParent[$parentModuleId]->mainimage) }}' height="100px" width="133px" alt="{{ $itineraryname }}" />
                        </td>
               		@endif
                  </tr>
                  <tr>
                  	<td colspan="2">
                        <br/>
		        	</td>
		          </tr>
                  <tr>
                      <td colspan='2'  style="background-color: #f7f7f7; padding:10px;" class="smalltext">
                            @if ($userlanguage == 'en')
                            	{!! nl2br(e($moduleParent[$parentModuleId]->longdescription)) !!}
                            @else
                            	{!! nl2br(e($moduleChild[$parentModuleId]->longdescription)) !!}
                            @endif
                        </td>
                  </tr>
                  <tr>
                  	<td colspan="2">
            			<p>&nbsp;</p>
                        <hr/>
		        	</td>
		          </tr>
            @endforeach
    
        @else
            <tr>
                <td colspan='3'>
                    @lang('forms.noitinerarymodules')
                </td>
            </tr>
        
        @endif
    </table>

    <table width="50%" class="pdf-table">
    	<tr>
    		<td>
				Powered by:
        	</td>
    		<td width="250px">
    			<img src="{{ asset('images/logo.png') }}" width="100px" alt="Logo" />
    		</td>
    	</tr>
    </table>
</div>
@endsection
