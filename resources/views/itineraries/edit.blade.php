@extends('layouts.app')

@section('content')

@include('layouts.header-auth')
<div class="wrapper">
	<div class="betitle">
		<div class="row">
    		<h3 class="betitle">
    			@if ($id > 0)
                    @lang('forms.edititinerary') <img alt="Header" src="{{ asset('images/'.$itinerary->idLang.'.svg') }}" height="15px"  alt="@lang('forms.edititinerary')" />    	
                @elseif ($idparent > 0 && $newlang)
                	@lang('forms.addnewlangiti') <img alt="Header" src="{{ asset('images/'.$newlang.'.svg') }}" height="15px"  alt="@lang('forms.addnewlangiti')" />
                @else
                	@lang('forms.newitinerary') <img alt="Header" src="{{ asset('images/en.svg') }}" height="15px"  alt="@lang('forms.newitinerary')" />
                @endif
    		</h3>
    	</div>
	</div>
</div>

<div class="wrapper">
                    <!-- New Itinerary Form -->
                    @if ($id > 0)
                    	
                         @if ($itinerary->idLang == 'en')
                         	<form action="{{ action('Backend\ItineraryController@update', $id) }}" enctype="multipart/form-data"  method="POST" class="form-horizontal" role="form">
                         @else
                         	<form action="{{ action('Backend\ItineraryController@updatelang', $id) }}" enctype="multipart/form-data"  method="POST" class="form-horizontal" role="form">
                         @endif
                        {!! csrf_field() !!}
                        {{ method_field('PATCH') }}        	
                    @elseif ($idparent > 0 && $newlang)
                    	
                    	<form action="{{ action('Backend\ItineraryController@addnewlang') }}" method="POST" class="form-horizontal" role="form">
                    	<input type="hidden" name="idparent" value="{{$idparent}}">
                    	<input type="hidden" name="realidparent" value="{{$itineraryparent->id}}">
                    	<input type="hidden" name="newlang" value="{{$newlang}}">
                        {!! csrf_field() !!}
                        {{ method_field('PATCH') }} 
                    @else
                    	
                    	 <form action="{{ url('itinerary') }}" enctype="multipart/form-data" method="POST" class="form-horizontal" role="form">
                        {!! csrf_field() !!}
                    @endif
                                
                     	

                        <!-- Itinerary Name -->
                        <div class="form-group{{ $errors->has('itineraryname') ? ' has-error' : '' }}">
                            <label class="col-md-4 control-label">@lang('forms.itineraryname')</label>

                            <div class="col-sm-6">
                                <input type="text" class="form-control" name="itineraryname" value="{{ null !==old('itineraryname') ? old('itineraryname') : $itinerary->itineraryname }}">
                                
                                @if ($errors->has('itineraryname'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('itineraryname') }}</strong>
                                    </span>
                                @endif
                                
                            </div>
                        </div>
                        
                        
                        <!-- Description -->
                        <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                            <label class="col-md-4 control-label">@lang('forms.description')</label>

                            <div class="col-sm-6">
                                <input type="text" class="form-control" name="description" value="{{ null !==old('description') ? old('description') : $itinerary->description  }}">
                                
                                @if ($errors->has('description'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                @endif
                                
                            </div>
                        </div>
                        
                        <!-- Long Description -->
                        <div class="form-group{{ $errors->has('longdescription') ? ' has-error' : '' }}">
                            <label class="col-md-4 control-label">@lang('forms.longdescription')</label>

                            <div class="col-sm-6">
                                <textarea class="form-control" rows="5" name="longdescription">{{ null !==old('longdescription') ? old('longdescription') : $itinerary->longdescription }}</textarea>
                                @if ($errors->has('longdescription'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('longdescription') }}</strong>
                                    </span>
                                @endif
                                
                            </div>
                        </div>
                        
                        <!-- Price Description -->
                        <div class="form-group{{ $errors->has('pricedescription') ? ' has-error' : '' }}">
                            <label class="col-md-4 control-label">@lang('forms.pricedescription')</label>

                            <div class="col-sm-6">
                                <textarea class="form-control" rows="5" name="pricedescription">{{ null !==old('pricedescription') ? old('pricedescription') : $itinerary->pricedescription }}</textarea>
                                
                                @if ($errors->has('pricedescription'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('pricedescription') }}</strong>
                                    </span>
                                @endif
                                
                            </div>
                        </div>
                        
                        <div class="form-group" align="center">
                        	<br /><h6>@lang('forms.commonattr')</h6><br />
                        </div>
                        
                
                            @if (($id > 0 and $itinerary->idLang <> 'en') or ($id == 0 and $newlang))
								
                                <!-- Main image -->
                                <div class="form-group">
                                    <label class="col-md-4 control-label">@lang('forms.mainimage')</label>
        
                                    <div class="col-sm-6">
                                        @if (isset($itineraryparent->mainimage))
                                        	@php $itinerarymainimage = $itineraryparent->mainimage; @endphp
                                        @else
                                        	@php $itinerarymainimage = $itinerary->mainimage; @endphp
                                        @endif
                                        <img src='{{ asset('upload_media/itineraries/'.$itinerarymainimage) }}' height="100px" alt="@lang('forms.itinerary')" />
                                    </div>
                                </div>
                                
                                <!-- googlemapsUrl 
                                <div class="form-group">
                                    <label class="col-md-4 control-label">@lang('forms.googlemapsUrl')</label>
        
                                    <div class="col-sm-6">
                                        {{ isset($itineraryparent->googlemapsUrl) ? $itineraryparent->googlemapsUrl :  $itinerary->googlemapsUrl }}
                                    </div>
                                </div>-->
                                
                                <!-- Persons Minimum -->
                                <div class="form-group">
                                    <label class="col-md-4 control-label">@lang('forms.personsMin')</label>
        
                                    <div class="col-sm-6">
                                        {{ isset($itineraryparent->personsMin) ? $itineraryparent->personsMin :  $itinerary->personsMin }}
                                    </div>
                                </div>
                                
                                <!-- Persons Maximum -->
                                <div class="form-group">
                                    <label class="col-md-4 control-label">@lang('forms.personsMax')</label>
        
                                    <div class="col-sm-6">
                                        {{ isset($itineraryparent->personsMax) ? $itineraryparent->personsMax :  $itinerary->personsMax }}
                                    </div>
                                </div>
                                
                                <!-- Stay Minimum -->
                                <div class="form-group">
                                    <label class="col-md-4 control-label">@lang('forms.dateFrom') <i class="far fa-calendar-alt"></i></label>
        
                                    <div class="col-sm-6">
                                        {{ isset($itineraryparent->dateFrom) ? $itineraryparent->dateFrom :  $itinerary->dateFrom }}
                                    </div>
                                </div>
                                
                                <!-- Stay Maximum -->
                                <div class="form-group">
                                    <label class="col-md-4 control-label">@lang('forms.dateTo') <i class="far fa-calendar-alt"></label></i>
        
                                    <div class="col-sm-6">
                                        {{ isset($itineraryparent->dateTo) ? $itineraryparent->dateTo :  $itinerary->dateTo }}
                                    </div>
                                </div>
                                
                                <!-- Price -->
                                <div class="form-group">
                                    <label class="col-md-4 control-label">@lang('forms.price') (EUR)</label>
        
                                    <div class="col-sm-6">
                                        {{ isset($itineraryparent->price) ? $itineraryparent->price :  $itinerary->price }}
                                    </div>
                                </div>
                                
                            @else
                                <!-- Main Image -->
                                <div class="form-group{{ $errors->has('mainimage') ? ' has-error' : '' }}">
                                    <label class="col-md-4 control-label">@lang('forms.mainimage')</label>
        
                                    <div class="col-sm-6">
                                        @if ($id > 0)
                                        	<img src='{{ asset('upload_media/itineraries/'.$itinerary->mainimage) }}' height="100px"  alt="@lang('forms.itinerary')" />
                                        	<br />@lang('forms.change'):<br />
                                        @endif
                                        <input type="file" name="mainimage" value="{{ isset($user->mainimage) ? $itinerary->mainimage : old('mainimage') }}">
                                        
                                        @if ($errors->has('mainimage'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('mainimage') }}</strong>
                                            </span>
                                        @endif
                                        
                                    </div>
                                </div>
                                
                                <!-- googlemapsUrl
                                <div class="form-group{{ $errors->has('googlemapsUrl') ? ' has-error' : '' }}">
                                    <label class="col-md-4 control-label">@lang('forms.googlemapsUrl')</label>
        
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control" name="googlemapsUrl" value="{{ null !==old('googlemapsUrl') ? old('googlemapsUrl') : $itinerary->googlemapsUrl  }}">
                                        
                                        @if ($errors->has('googlemapsUrl'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('googlemapsUrl') }}</strong>
                                            </span>
                                        @endif
                                        
                                    </div>
                                </div> -->
                                
                                <!-- Persons Minimum -->
                                <div class="form-group{{ $errors->has('personsMin') ? ' has-error' : '' }}">
                                    <label class="col-md-4 control-label">@lang('forms.personsMin')</label>
        
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control" name="personsMin" value="{{ null !==old('personsMin') ? old('personsMin') : $itinerary->personsMin  }}">
                                        
                                        @if ($errors->has('personsMin'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('personsMin') }}</strong>
                                            </span>
                                        @endif
                                        
                                    </div>
                                </div>
                                
                                <!-- Persons Maximum -->
                                <div class="form-group{{ $errors->has('personsMax') ? ' has-error' : '' }}">
                                    <label class="col-md-4 control-label">@lang('forms.personsMax')</label>
        
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control" name="personsMax" value="{{ null !==old('personsMax') ? old('personsMax') : $itinerary->personsMax  }}">
                                        
                                        @if ($errors->has('personsMax'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('personsMax') }}</strong>
                                            </span>
                                        @endif
                                        
                                    </div>
                                </div>
                                
                                <!-- Date Range (From/to) -->
                                <div class="form-group{{ $errors->has('day1') ? ' has-error' : '' }}">
                                    <label class="col-md-4 control-label">@lang('forms.daterange')</label>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control" name="daterange" value="{{ null !==old('daterange') ? old('daterange') : ''  }}"/>
            							@if (old('daterange'))
            								@php
            									$dates = explode(' - ', old('daterange'));
            								@endphp
            								<script>
                                                $(function() {
                                                  $('input[name="daterange"]').daterangepicker();
                                                  $('input[name="daterange"]').daterangepicker({
                                                	    timePicker: true,
                                                	    startDate: '{{ $dates[0] }}',
                                           	    		endDate: '{{ $dates[1] }}',
                                                	    locale: {
                                                	      format: 'DD/MM/YYYY H:mm'
                                                	    }
                                                	});
                                                });
                                                
                                            </script>
            							@elseif ($itinerary->dateFrom && $itinerary->dateTo)
            								<script>
                                                $(function() {
                                                  $('input[name="daterange"]').daterangepicker();
                                                  $('input[name="daterange"]').daterangepicker({
                                                	    timePicker: true,
                                           	    		startDate: '{{ $itinerary->dateFrom }}',
                                           	    		endDate: '{{ $itinerary->dateTo }}',
                                                	    locale: {
                                                	      format: 'DD/MM/YYYY H:mm'
                                                	    }
                                                	});
                                                });
                                                
                                            </script>
            							@else
            								<script>
                                                $(function() {
                                                  $('input[name="daterange"]').daterangepicker();
                                                  $('input[name="daterange"]').daterangepicker({
                                                	    timePicker: true,
                                           	    		startDate: moment().startOf('hour'),
                                           	    		endDate: moment().startOf('hour').add(32, 'hour'),
                                                	    locale: {
                                                	      format: 'DD/MM/YYYY H:mm'
                                                	    }
                                                	});
                                                });
                                                
                                            </script>
            							@endif
        							
        							
                                        @if ($errors->has('day1'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('day1') }}</strong>
                                            </span>
                                        @endif
                                        @if ($errors->has('time1'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('time1') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                
                                <!-- Price -->
                                <div class="form-group{{ $errors->has('price') ? ' has-error' : '' }}">
                                    <label class="col-md-4 control-label">@lang('forms.price') (EUR)</label>
        
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control" name="price" value="{{  null !==old('price') ? old('price') : $itinerary->price }}">
                                        
                                        @if ($errors->has('price'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('price') }}</strong>
                                            </span>
                                        @endif
                                        
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <div class="col-md-4"></div>
                    
                                    <div class="col-md-6 smalltext">@lang('forms.allmandatory')
                                    </div>
                                </div>
                                
                        @endif        
                        <!-- Add Itinerary Button -->
                        <div class="form-group">
                            <div class="col-md-4"></div>
                            <div class="col-sm-6">
                            	<button type="submit" class="btn btn-primary">
                                    <i class="fa fa-btn fa-save"></i>&nbsp;
                                        @if ($id > 0)
                                        	@lang('forms.savechanges')
                                        	
                                        @elseif ($idparent > 0 && $newlang)
                                            @lang('forms.addnewlangiti') ({{ $newlang }})   
                                        @else
                                        	@lang('forms.saveform')
                                        @endif
                                     </button>
                            </div>
                        </div>
                    </form>
</div>    
@endsection
