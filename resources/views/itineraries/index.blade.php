@extends('layouts.app')

@section('content')

@include('layouts.header-auth')
<div class="wrapper">
	<div class="betitle">
		<div class="row">
    		<h3 class="betitle">
    			@lang('forms.myitineraries')
    		</h3>
    		<img src="{{ asset('images/pixel.png') }}" alt="@lang('forms.myitineraries')" />
    	</div>
	</div>
</div>

<div class="wrapper">
        <!-- Current Itineraries -->
        @if (count($itineraries) > 0)
                    <table class="table table-striped itinerary-table">
                        <thead>
                            <th>Status</th>
                            <th>@lang('forms.itinerary')</th>
                            @if ($userType == 2)
                            	<th>@lang('forms.public')</th>
                            @endif
                            <th><img alt="@lang('forms.english')" src="{{ asset('images/en.svg') }}" height="15px" /></th>
                            <th><img alt="@lang('forms.slovenian')" src="{{ asset('images/sl.svg') }}" height="15px" /></th>
                            <th><img alt="@lang('forms.croatian')" src="{{ asset('images/hr.svg') }}" height="15px" /></th>
                            <th><img alt="@lang('forms.german')" src="{{ asset('images/de.svg') }}" height="15px" /></th>
                            <th>@lang('forms.modules')</th>
                            <th>@lang('texts.actions')</th>
                            <th>@lang('forms.msgs')</th>
                        </thead>
                        <tbody>
                            @foreach ($itineraries as $itinerary)
						        @php
    						        $date1 = new DateTime($itinerary->dateFrom);
    						        $date1 = $date1->format('d. m. Y H:i');
                                    $date2 = new DateTime($itinerary->dateTo);
                                	$date2 = $date2->format('d. m. Y H:i');
						        @endphp                    	
                                <tr>
                                    <td class="table-text">
                                    	<div><a href="#" data-toggle="tooltip" title="@lang('texts.status'.$itinerary->status.'i')"><i class="fas fa-circle status-{{ $itinerary->status }}"></i></a></div>
                                    </td>
                                    <td class="table-text">
                                    	<div>{{ $itinerary->itineraryname }}</div>
                                    	<div class="notes">{{ $date1 }} - {{ $date2 }}</div>
                                    </td>
    
                                	@if ($itinerary->enabled == '0')
                                    	<td colspan = "7">@lang('forms.disabledadmin')</td>
                                        @if ($userType == 2)
                                        	<td></td>
                                        @endif
                               		@else 
                                    	@if ($userType == 2)
                                            <!-- Itinerary Public Button -->
                                            <td>
                									<form action="{{url('itinerarypublic/' . $itinerary->id)}}" method="POST">
                                                                
                                                                {{ csrf_field() }}
                                                                {{ method_field('PATCH') }}
                    											@if ($itinerary->public)
                													<input type="hidden" name="actionenable" value="disable">
                													<button type="submit" id="get-itinerary-{{ $itinerary->idItinerary }}-sl" class="btn btn-light">
                                                                        <i class="fas fa-toggle-on fa-2x"></i>
                                                                    </button>
                    											@else
                													<input type="hidden" name="actionenable" value="enable">
                													<button type="submit" id="get-itinerary-{{ $itinerary->idItinerary }}-sl" class="btn btn-light">
                                                                        <i class="fas fa-toggle-off fa-2x"></i>
                                                                    </button>
                    											@endif
                                                            </form>
                                            </td>
                                        @endif
                                        @foreach ($itinerariestranslated as $itinerarytranslated)

                                            @if ($itinerarytranslated['idItinerary'] == $itinerary['idItinerary'])
												                                                  
                                              @php 

                                                  $languagesinitinerary[] = $itinerarytranslated['idLang'];
                                                  $iditinerarylang[] = $itinerarytranslated['id'];

                                              @endphp
                                              
                                            @endif
                                        @endforeach
                                       @foreach ($languages as $key => $language)
                                        	
                                        	@if(!in_array($language['language'], $languagesinitinerary))
                                            	<td>
                                                    <a href="{{url('itinerarynew/' . $itinerary->idItinerary . '/' . $itinerary->id . '/' . $language['language']. '')}}"><i class="fa fa-btn fa-plus"></i>&nbsp;@lang('forms.add')</a>
                                                </td>
                                              @else
                                              		@foreach ($itinerariestranslated as $itinerarytranslated)

                                            			@if ($itinerarytranslated['idItinerary'] == $itinerary['idItinerary'] and $itinerarytranslated['idLang'] == $language['language'])
                                                          	<td>
                                                                <a href="{{url('itinerary/' . $itinerarytranslated->id)}}"><i class="fa fa-btn fa-edit"></i>&nbsp;@lang('forms.edit')</a><br />
                                                                @if ($itinerary['status'] >= 4 && $itinerary['status'] <> 8)
                                                                	<a href="{{url('itinerarypdf/'.$language['language'].'/' . $itinerarytranslated->id)}}" target="_blank"><i class="fas fa-print"></i>&nbsp;@lang('forms.print')</a>
                                                                @else
                                                                	&nbsp;
                                                                @endif
                                                            </td>
                                                         @endif
                                                    @endforeach
                                              @endif
                                        
                                       	@endforeach
                                       	
                                       	@php $languagesinitinerary=empty($languagesinitinerary) @endphp
                                       	@php $iditinerarylang=empty($iditinerarylang) @endphp
										<!-- Itinerary Modules Button -->
										<td>
                                            <a href="{{url('modulesiti/' . $itinerary->id)}}"><i class="fas fa-project-diagram"></i></a>
                                        </td>
                                        
                                        <td>
                                            @if ($itinerary['status'] == 0)
                                            	<!-- Itinerary Delete Button -->
                                            	<a href="{{url('itineraryd/' . $itinerary->id)}}" onClick="return confirm('@lang('forms.deliticonfirmation')');"><i class="fa fa-btn fa-trash"></i></a>
                                            @elseif ($itinerary['status'] == 4)
                                            	<!-- If all providers confirmed then agency chooses whether it will be realized or cancelled (if not enough interested people) -->
            									<a href="{{url('confirm/' . $itinerary->id)}}" onClick="return confirm('@lang('forms.confirmconfirmation')');"><i class="fas fa-check"></i></a><br />
            									<a href="{{url('cancel/' . $itinerary->id)}}" onClick="return confirm('@lang('forms.cancelconfirmation')');"><i class="fas fa-times"></i></a>
                                            @elseif ($itinerary['status'] == 5)
                                            	<!-- Itinerary completed  -->
            									<a href="{{url('completed/' . $itinerary->id)}}" onClick="return confirm('@lang('forms.realizedconfirmation')');"><i class="fas fa-flag-checkered"></i></a>
                                            @elseif ($itinerary['status'] == 6)
                                            	<!-- Itinerary Questionary Button -->
            									<a href="{{url('ratea/' . $itinerary->token)}}"><i class="fas fa-question"></i></a>
            								@elseif ($itinerary['status'] == 7 OR $itinerary['status'] == 8)
                                            	<!-- Itinerary is in archive  -->
            									<i class="fas fa-archive"></i>
                                            @else
                                            	<i class="fas fa-tasks"></i>
                                            @endif
                                        </td>
                                        
                                        <!-- Itinerary Messages Button -->
                                        <td>
                                            <a href="{{url('messages/' . $itinerary->id)}}"><i class="fas fa-envelope"></i></a>
                                        </td>    
										@endif
											
                                        </tr>
                                    @endforeach
                                    
									
                                </tbody>
                            </table>
            @else
            <div class="panel panel-default">
                <div class="panel-heading">
                    @lang('forms.noitineraries')
                </div>
            </div>
            @endif
            
            <table style="width:100%;">
        <tr class="smalltext2">
            <td colspan="17">@lang('texts.statusitinerary')</td>
        </tr>
        <tr>
            <td align="center" width="5.88%"><a href="#" data-toggle="tooltip" title="@lang('texts.status0i')"><i class="fas fa-circle status-0"></i></a></td>
            <td align="center" width="5.88%"><img src="{{ asset('images/arrow.png') }}" alt="arrow" width="50px" width="50px"/></td>
            <td align="center" width="5.88%"><a href="#" data-toggle="tooltip" title="@lang('texts.status1i')"><i class="fas fa-circle status-1"></i></a></td>
            <td align="center" width="5.88%"><img src="{{ asset('images/arrow.png') }}" alt="arrow" width="50px" /></td>
            <td align="center" width="5.88%"><a href="#" data-toggle="tooltip" title="@lang('texts.status2i')"><i class="fas fa-circle status-2"></i></a></td>
            <td align="center" width="5.88%"><img src="{{ asset('images/arrow.png') }}" alt="arrow" width="50px" /></td>
            <td align="center" width="5.88%"><a href="#" data-toggle="tooltip" title="@lang('texts.status3i')"><i class="fas fa-circle status-3"></i></a></td>
            <td align="center" width="5.88%"><img src="{{ asset('images/arrow.png') }}" alt="arrow" width="50px" /></td>
            <td align="center" width="5.88%"><a href="#" data-toggle="tooltip" title="@lang('texts.status4i')"><i class="fas fa-circle status-4"></i></a></td>
            <td align="center" width="5.88%"><img src="{{ asset('images/arrow.png') }}" alt="arrow" width="50px" /></td>
            <td align="center" width="5.88%"><a href="#" data-toggle="tooltip" title="@lang('texts.status5i')"><i class="fas fa-circle status-5"></i></a></td>
            <td align="center" width="5.88%"><img src="{{ asset('images/arrow.png') }}" alt="arrow" width="50px" /></td>
            <td align="center" width="5.88%"><a href="#" data-toggle="tooltip" title="@lang('texts.status6i')"><i class="fas fa-circle status-6"></i></a></td>
            <td align="center" width="5.88%"><img src="{{ asset('images/arrow.png') }}" alt="arrow" width="50px" /></td>
            <td align="center" width="5.88%"><a href="#" data-toggle="tooltip" title="@lang('texts.status7i')"><i class="fas fa-circle status-7"></i></a></td>
            <td align="center" width="5.88%">&nbsp;</td>
            <td align="center" width="5.88%"><a href="#" data-toggle="tooltip" title="@lang('texts.status8i')"><i class="fas fa-circle status-8"></i></a></td>
        </tr>
    </table>
    </div>

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  
  <script>
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();   
});
</script>
    
   
@endsection
