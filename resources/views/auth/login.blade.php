@extends('layouts.app')

@section('content')

@include('layouts.header-small')

<div class="wrapper">
	<div class="pagetitle">
		<div class="row">
    		<h4 class="pagetitle">&nbsp;</h3>
    		<h3 class="pagetitle">@lang('auth.login')</h3>
    		<img src="{{ asset('images/pixel.png') }}" alt="@lang('auth.login')" />
    		<h5 class="pagetitle">&nbsp;</h3>
    	</div>
	</div>
</div>

<div class="container">
    <div class="col-md-8 col-md-offset-2">
        <form class="form-horizontal" role="form" method="POST" action="{{ url('login') }}">
            {!! csrf_field() !!}
            @if(session()->has('login_error'))
                <div class="alert alert-danger">
                  {{ session()->get('login_error') }}
                  {{ session()->forget('login_error') }}
                  
                </div>
			@endif
			
            <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">@lang('auth.username')</label>

                <div class="col-md-6">
                    <input type="text" class="form-control" name="username" value="{{ old('username') }}">

                    @if ($errors->has('username'))
                        <span class="help-block">
                            <strong>{{ $errors->first('username') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">@lang('auth.pass')</label>

                <div class="col-md-6">
                    <input type="password" class="form-control" name="password">

                    @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-6 col-md-offset-4">
                    <button type="submit" class="btn btn-primary">
                        <i class="fas fa-sign-in-alt"></i>&nbsp;@lang('auth.login')
                    </button>

                    <a class="btn btn-link" href="{{ url('password/forgotpassword') }}">@lang('auth.forgot')?</a>
                </div>
            </div>
        </form>
		<br/>
    </div>
</div>
@endsection


