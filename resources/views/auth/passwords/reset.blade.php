@extends('layouts.app')

@section('content')

@include('layouts.header-small')

<div class="wrapper">
	<div class="pagetitle">
		<div class="row">
    		<h4 class="pagetitle">&nbsp;</h3>
    		<h3 class="pagetitle">@lang('forms.resetpass')</h3>
    		<img src="{{ asset('images/pixel.png') }}" alt="@lang('forms.resetpass')" />
    		<h5 class="pagetitle">&nbsp;</h3>
    	</div>
	</div>
</div>

<div class="container">
    <div class="col-md-8 col-md-offset-2">
        @if (isset($error))
            <div class="alert alert-danger" role="alert">
                {{ $error }}
            </div>
        @elseif ($message == 'ok')

            <form method="POST" action="{{ url('password/reset') }}" aria-label="{{ __('Reset Password') }}">
                @csrf

                <input type="hidden" name="token" value="{{ $token }}">

                <div class="form-group row">
                    <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                    <div class="col-md-6">
                        <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ $email ?? old('email') }}" required autofocus>

                        @if ($errors->has('email'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                    <div class="col-md-6">
                        <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                        @if ($errors->has('password'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                    <div class="col-md-6">
                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4"">
                        <button type="submit" class="btn btn-primary">
                            {{ __('Reset Password') }}
                        </button>
                    </div>
                </div>
            </form>

		@elseif($message == 'success')
			<div class="alert alert-success" role="alert">
                @lang('auth.passchanged')
            </div>
		@endif
		
    </div>
</div>
<br/><br/>
@endsection
