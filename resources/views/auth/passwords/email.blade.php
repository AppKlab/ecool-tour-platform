@extends('layouts.app')

@section('content')

@include('layouts.header-small')

<div class="wrapper">
	<div class="pagetitle">
		<div class="row">
    		<h4 class="pagetitle">&nbsp;</h3>
    		<h3 class="pagetitle">@lang('forms.resetpass')</h3>
    		<img src="{{ asset('images/pixel.png') }}" alt="@lang('forms.resetpass')" />
    		<h5 class="pagetitle">&nbsp;</h3>
    	</div>
	</div>
</div>

<div class="container">
    <div class="col-md-8 col-md-offset-2">
        @if (session('status'))
            <div class="alert alert-success" role="alert">
                {{ session('status') }}
            </div>
        @endif
        
        @if (isset($error))
            <div class="alert alert-danger" role="alert">
                {{ $error }}
            </div>
        @endif

        <form class="form-horizontal" method="POST" action="{{ url('password/email') }}">
            @csrf

            <div class="form-group">
                <label for="email" class="col-md-4 control-label">@lang('forms.email')</label>

                <div class="col-md-6">
                    <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                    @if ($errors->has('email'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-6 col-md-offset-4">
                    <button type="submit" class="btn btn-primary">
                        @lang('forms.sendreset')
                    </button>
                </div>
            </div>
        </form>
        <br/><br/>
    </div>
</div>
@endsection
