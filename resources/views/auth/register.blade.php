@extends('layouts.app')

@section('content')

@include('layouts.header-small')


@if (isset($usertype) and ($usertype > 1 and $usertype < 5))
    
@else
	 @php $usertype = 3; @endphp

@endif

<div class="wrapper">
	<div class="pagetitle">
		<div class="row">
    		<h4 class="pagetitle">&nbsp;</h3>
    		<h3 class="pagetitle">
				@switch($usertype)
                
                        @case('2')
                            @lang('forms.regagency')
                        @break;
                    
                        @case('3')
                            @lang('forms.regprovider')
                        @break;
                    
                        @case('4')
                            @lang('forms.reggroup')
                        @break;
                    
                    @endswitch
			</h3>
    		<img src="{{ asset('images/pixel.png') }}" alt="@lang('auth.register')" />
    		<h5 class="pagetitle">&nbsp;</h3>
    	</div>
	</div>
</div>

<div class="wrapper">
   		@if(session()->has('message'))
            <div class="alert alert-success">
              {{ session()->get('message') }}
            </div>
		@else
      <form class="form-horizontal" enctype="multipart/form-data" method="POST" action="{{ url('register') }}">
        {!! csrf_field() !!}
			<input type="hidden" name="usertype" value="{{ $usertype }}">
           <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">@lang('auth.username')</label>

                <div class="col-md-6">
                    <input type="text" class="form-control" name="username" value="{{ old('username') }}">

                    @if ($errors->has('username'))
                        <span class="help-block">
                            <strong>{{ $errors->first('username') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            
            <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">@lang('auth.title')</label>

                <div class="col-md-6">
                    <input type="text" class="form-control" name="title" value="{{ old('title') }}">

                    @if ($errors->has('title'))
                        <span class="help-block">
                            <strong>{{ $errors->first('title') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            @if ($usertype < 4)
                <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                    <label class="col-md-4 control-label">@lang('auth.address')</label>
    
                    <div class="col-md-6">
                        <input type="text" class="form-control" name="address" value="{{ old('address') }}">
    
                        @if ($errors->has('address'))
                            <span class="help-block">
                                <strong>{{ $errors->first('address') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
               
                <div class="form-group{{ $errors->has('logo') ? ' has-error' : '' }}">
                    <label class="col-md-4 control-label">@lang('auth.logo')</label>
    
                    <div class="col-md-6">
                        <input type="file" name="logo" value="{{ old('logo') }}">
    
                        @if ($errors->has('logo'))
                            <span class="help-block">
                                <strong>{{ $errors->first('logo') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
            
                <div class="form-group{{ $errors->has('www') ? ' has-error' : '' }}">
                    <label class="col-md-4 control-label">@lang('auth.www')</label>
    
                    <div class="col-md-6">
                        <input type="text" class="form-control" name="www" value="{{ old('www') }}">
    
                        @if ($errors->has('www'))
                            <span class="help-block">
                                <strong>{{ $errors->first('www') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
            
            	<div class="form-group{{ $errors->has('telephone') ? ' has-error' : '' }}">
                    <label class="col-md-4 control-label">@lang('auth.telephone')</label>
    
                    <div class="col-md-6">
                        <input type="text" class="form-control" name="telephone" value="{{ old('telephone') }}">
    
                        @if ($errors->has('telephone'))
                            <span class="help-block">
                                <strong>{{ $errors->first('telephone') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
            @endif
            
            <div class="form-group{{ $errors->has('cellphone') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">@lang('auth.cellphone')</label>

                <div class="col-md-6">
                    <input type="text" class="form-control" name="cellphone" value="{{ old('cellphone') }}">

                    @if ($errors->has('cellphone'))
                        <span class="help-block">
                            <strong>{{ $errors->first('cellphone') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            
            
            
            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">@lang('auth.email')</label>

                <div class="col-md-6">
                    <input type="text" class="form-control" name="email" value="{{ old('email') }}">

                    @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">@lang('auth.pass')</label>

                <div class="col-md-6">
                    <input type="password" class="form-control" name="password">

                    @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">@lang('auth.passconf')</label>

                <div class="col-md-6">
                    <input type="password" class="form-control" name="password_confirmation">

                    @if ($errors->has('password_confirmation'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password_confirmation') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
			
			<div class="form-group">
                <div class="col-md-4"></div>

                <div class="col-md-6 smalltext">@lang('forms.allmandatory')
                </div>
            </div>
            
            
            <div class="form-group">
                <div class="col-md-6 col-md-offset-4">
                    <button type="submit" class="btn btn-primary">
                        <i class="fa fa-btn fa-user"></i>&nbsp;@lang('auth.register')
                    </button>
                </div>
            </div>
        </form>
    	@endif
</div>
@endsection