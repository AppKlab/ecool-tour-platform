<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGalleriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('galleries', function (Blueprint $table) {
            $table->increments('id')->index();
            $table->integer('idModule');
            $table->string('imagename');
            $table->integer('status')->default('0');
            $table->integer('enabled')->default('1');
            $table->softDeletes();
            $table->timestamps();
        });
        
        Schema::dropIfExists('images');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('galleries');
        
        Schema::create('images', function (Blueprint $table) {
            $table->increments('id')->index();
            $table->integer('idModule');
            $table->string('imagename');
            $table->integer('status')->default('0');
            $table->integer('enabled')->default('1');
            $table->softDeletes();
            $table->timestamps();
        });
    }
}
