<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMonumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('monuments', function (Blueprint $table) {
            $table->increments('id')->index();
            $table->string('idLang')->default('en');
            $table->string('monumentType')->default('0');
            $table->string('monumentName');
            $table->string('monumentAddress');
            $table->string('monumentPicture');
            $table->string('monumentUrl')->default('0');
            $table->string('geolocationLat');
            $table->string('geolocationLon');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('monuments');
    }
}
