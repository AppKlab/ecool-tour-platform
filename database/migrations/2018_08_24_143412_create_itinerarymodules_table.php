<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItinerarymodulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('itinerarymodules', function (Blueprint $table) {
            $table->increments('id')->index();
            $table->integer('idItinerary');
            $table->integer('idModule');
            $table->dateTime('dateFrom')->nullable();
            $table->dateTime('dateTo')->nullable();
            $table->string('timestampFrom')->nullable();
            $table->string('timestampTo')->nullable();
            $table->string('token_messages');
            $table->integer('enabled')->default('1');
            $table->integer('status')->default('0');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('itinerarymodules');
    }
}
