<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('modules', function (Blueprint $table) {
            $table->increments('id')->index();
            $table->integer('idModule');
            $table->integer('user_id');
            $table->string('idLang')->default('en');
            $table->string('modulename');
            $table->string('description');
            $table->text('longdescription');
            $table->string('mainimage');
            $table->string('address');
            $table->string('zip');
            $table->string('city');
            $table->string('geolocationLat');
            $table->string('geolocationLon');
            $table->string('videoUrl');
            $table->integer('personsMin');
            $table->integer('personsMax');
            $table->time('timeMin');
            $table->time('timeMax');
            $table->integer('price');
            $table->text('pricedescription');
            $table->integer('public')->default('0');
            $table->integer('status')->default('0');
            $table->integer('enabled')->default('1');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('modules');
    }
    

        
}
