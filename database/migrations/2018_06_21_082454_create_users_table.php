<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id')->index();
            $table->string('idLang');
            $table->integer('userType');
            $table->string('username')->unique();
            $table->string('password', 60);
            $table->string('title');
            $table->string('address');
            $table->string('logo');
            $table->string('telephone');
            $table->string('cellphone');
            $table->string('email')->unique();
            $table->string('www');
            $table->integer('enabled')->default(0);
            $table->string('enable_code');
            $table->boolean('status')->default(0);
            $table->string('activation_code', 100)->nullable();
            $table->string('resetmail_token', 100)->nullable();
            $table->datetime('lastLogin');
            $table->rememberToken();
            $table->softDeletes();
            $table->timestamps();
        });
            
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
