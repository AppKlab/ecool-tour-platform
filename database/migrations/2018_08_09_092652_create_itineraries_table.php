<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItinerariesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
            Schema::create('itineraries', function (Blueprint $table) {
                $table->increments('id')->index();
                $table->integer('idItinerary');
                $table->integer('user_id');
                $table->string('idLang')->default('en');
                $table->string('itineraryname');
                $table->string('description');
                $table->text('longdescription');
                $table->string('googlemapsUrl');
                $table->string('mainimage');
                $table->integer('personsMin');
                $table->integer('personsMax');
                $table->datetime('dateFrom');
                $table->datetime('dateTo');
                $table->string('timestampFrom');
                $table->string('timestampTo');
                $table->integer('price');
                $table->text('pricedescription');
                $table->integer('public')->default('0');
                $table->integer('status')->default('0');
                $table->integer('performed')->default('0');
                $table->string('token');
                $table->integer('enabled')->default('1');
                $table->softDeletes();
                $table->timestamps();
                
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('itineraries');
    }
}
