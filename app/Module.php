<?php

namespace App;

use App\User;
use Illuminate\Database\Eloquent\Model;


/** Soft delete added to this model 
 *  The record is not actually removed from table. Instead, a deleted_at timestamp is set on the record.
*/
use Illuminate\Database\Eloquent\SoftDeletes; 

class Module extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'modules';
    
    use SoftDeletes;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id','idModule','idLang','modulename','description','longdescription','mainimage','address','zip','city','geolocationLat','geolocationLon','videoUrl','personsMin','personsMax','timeMin',
        'timeMax','price','pricedescription','public','status','enabled',
    ];
    
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        //'user_id' => 'int',
    ];
    
    protected $dates = ['deleted_at',];
    

}
