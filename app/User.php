<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Module;
use Illuminate\Foundation\Auth\User as Authenticatable;

use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Auth\Passwords\CanResetPassword;

use Illuminate\Notifications\Notifiable;


/** Soft delete added to this model 
 *  The record is not actually removed from table. Instead, a deleted_at timestamp is set on the record.
 */
use Illuminate\Database\Eloquent\SoftDeletes; 

class User extends Authenticatable 
//class User extends Model implements Authenticatable, CanResetPasswordContract
{
    
    use Notifiable;
    use SoftDeletes;
    use CanResetPassword;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $primaryKey = 'id';
    protected $username = 'username';
    
    
    protected $fillable = [
        'username', 'idLang', 'usertype', 'email', 'password', 'title', 'address', 'logo', 
        'telephone', 'cellphone', 'www', 'status', 'activation_code', 'enable_code',
        'resetmail_token', 'lastLogin'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    
    protected $casts = [
        'id' => 'int',
    ];
    
    protected $dates = ['deleted_at',];
    
}
