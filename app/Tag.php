<?php

namespace App;

use App\User;
use App\Tag;
use Illuminate\Database\Eloquent\Model;


class Tag extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'tags';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'idModule','idTag',
    ];
    
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'int',
    ];
    
    
}
