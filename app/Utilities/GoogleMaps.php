<?php

namespace App\Utilities;

use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;


class GoogleMaps{
    /*
     Geocodes an addres so we can get the latitude and longitude
     */
    //public static function geocodeAddress( $address, $city, $state, $zip ){
    public static function geocodeAddress( $address, $city, $zip ){
        /*
         Builds the URL and request to the Google Maps API
         */
        $url = 'https://maps.googleapis.com/maps/api/geocode/json?address='.urlencode( $address.', '.$city.', '.$zip ).'&key=YourKeyHere&sensor=true';

        /*
         Creates a Guzzle Client to make the Google Maps request.
         */
        $client = new Client;
        
        /*
         Send a GET request to the Google Maps API and get the body of the
         response.
         */
        $geocodeResponse = $client->get( $url )->getBody();
        
        /*
         JSON decodes the response
         */
        $geocodeData = json_decode( $geocodeResponse );

        /*
         Initializes the response for the GeoCode Location
         */
        $coordinates['lat'] = '';
        $coordinates['lon'] = '';
        
        /*
         If the response is not empty (something returned),
         we extract the latitude and longitude from the
         data.
         */
        if( !empty( $geocodeData )
            && $geocodeData->status != 'ZERO_RESULTS'
            && isset( $geocodeData->results )
            && isset( $geocodeData->results[0] ) ){
                $coordinates['lat'] = $geocodeData->results[0]->geometry->location->lat;
                $coordinates['lon'] = $geocodeData->results[0]->geometry->location->lng;
        }
        
        /*
         Return the found coordinates.
         */
        
        return $coordinates;
    }
}