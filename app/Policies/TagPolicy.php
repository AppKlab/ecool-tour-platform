<?php

namespace App\Policies;

use App\User;
use App\Module;
use App\Tag;
use Illuminate\Auth\Access\HandlesAuthorization;

class TagPolicy
{
    use HandlesAuthorization;

    /**
     * Determine if the given module can delete the given category.
     *
     * @param  User  $user
     * @param  Module  $module
     * @return bool
     */
    public function destroy(Module $module, Tag $tag)
    {
        
        return $module->idModule === $tag->idModule;
    }
}
