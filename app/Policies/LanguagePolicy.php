<?php

namespace App\Policies;

use App\User;
use App\Language;
use Illuminate\Auth\Access\HandlesAuthorization;

class LanguagePolicy
{
    use HandlesAuthorization;

    /**
     * Determine if the given user can delete the given language.
     *
     * @param  User  $user
     * @param  Language  $language
     * @return bool
     */
    public function destroy()
    {
        
        //return $user->id === $module->user_id;
    }
}
