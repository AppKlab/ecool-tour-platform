<?php

namespace App\Policies;

use App\User;
use App\Module;
use App\Category;
use Illuminate\Auth\Access\HandlesAuthorization;

class CategoryPolicy
{
    use HandlesAuthorization;

    /**
     * Determine if the given module can delete the given category.
     *
     * @param  User  $user
     * @param  Module  $module
     * @return bool
     */
    public function destroy(Module $module, Category $category)
    {
        
        return $module->idModule === $category->idModule;
    }
}
