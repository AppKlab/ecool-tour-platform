<?php

namespace App\Policies;

use App\User;
use App\Itinerary;
use App\Message;
use Illuminate\Auth\Access\HandlesAuthorization;

class MessagePolicy
{
    use HandlesAuthorization;
    
    /**
     * Determine if the given user can delete the given message.
     *
     * @param  User  $user
     * @param  Itinerary  $itinerary
     * @return bool
     */
    public function destroy($user, $itinerary)
    {
        
        if ($user->id === $itinerary->user_id) {
            
            return true;
            
        } else {
            
            return abort(505, 'Unauthorized action.');
        }
    }
    
    
    /**
     * Determine whether the user has a specific ability for projects.
     *
     * @param  \App\User  $user
     * @param  \App\Project  $project
     * @return mixed
     */
    public function before($user, $ability)
    {
        //echo('Before policy');
        //die();
        //return true;
    }
    
    /**
     * Determine whether the user can view the message.
     *
     * @param  \App\User  $user
     * @param  \App\Itinerary  $itinerary
     * @return mixed
     */
    public function view($user, $itinerary)
    {
        
        if ($user->id === $itinerary->user_id) {
            
            return true;
            
        } else {
            
            return abort(505, 'Unauthorized action.');
        }
        
    }
    
    /**
     * Determine whether the user can create messages in a module
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create($user, $itinerary)
    {
        if ($user->id === $itinerary->user_id) {
            
            return true;
            
        } else {
            
            return abort(505, 'Unauthorized action.');
        }
    }
    
    /**
     * Determine whether the user can update the message.
     *
     * @param  \App\User  $user
     * @param  \App\Itinerary  $itinerary
     * @return mixed
     */
    public function update($user, $itinerary)
    {
        if ($user->id === $itinerary->user_id) {
            
            return true;
            
        } else {
            
            return abort(505, 'Unauthorized action.');
        }
    }
    
    /**
     * Determine whether the user can delete the message.
     *
     * @param  \App\User  $user
     * @param  \App\Itinerary $itinerary
     * @return mixed
     */
    public function delete($user, $itinerary)
    {
        if ($user->id === $itinerary->user_id) {
            
            return true;
            
        } else {
            
            return abort(505, 'Unauthorized action.');
        }
    }
}