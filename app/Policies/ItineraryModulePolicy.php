<?php

namespace App\Policies;

use App\User;
use App\Module;
use App\ItineraryModule;
use Illuminate\Auth\Access\HandlesAuthorization;

class ItineraryModulePolicy
{
    use HandlesAuthorization;
    
    /**
     * Determine if the given user can delete the given itinerarymodule.
     *
     * @param  User  $user
     * @param  Module  $module
     * @return bool
     */
    public function destroy($user, $itinerary)
    {
        
        if ($user->id === $itinerary->user_id) {
            
            return true;
            
        } else {
            
            return abort(505, 'Unauthorized action.');
        }
    }
    
    
    /**
     * Determine whether the user has a specific ability for projects.
     *
     * @param  \App\User  $user
     * @param  \App\Project  $project
     * @return mixed
     */
    public function before($user, $ability)
    {
        //echo('Before policy');
        //die();
        //return true;
    }
    
    /**
     * Determine whether the user can view the itinerarymodule.
     *
     * @param  \App\User  $user
     * @param  \App\Module  $module
     * @return mixed
     */
    public function view($user, $itinerary)
    {
        
        if ($user->id === $itinerary->user_id) {
            
            return true;
            
        } else {
            
            return abort(505, 'Unauthorized action.');
        }
        
    }
    
    /**
     * Determine whether the user can create itinerarymodules in a module
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create($user, $itinerary)
    {
        if ($user->id === $itinerary->user_id) {
            
            return true;
            
        } else {
            
            return abort(505, 'Unauthorized action.');
        }
    }
    
    /**
     * Determine whether the user can update the itinerarymodule.
     *
     * @param  \App\User  $user
     * @param  \App\Module  $module
     * @return mixed
     */
    public function update($user, $itinerary)
    {
        if ($user->id === $itinerary->user_id) {
            
            return true;
            
        } else {
            
            return abort(505, 'Unauthorized action.');
        }
    }
    
    /**
     * Determine whether the user can delete the itinerarymodule.
     *
     * @param  \App\User  $user
     * @param  \App\Module $module
     * @return mixed
     */
    public function delete($user, $itinerary)
    {
        if ($user->id === $itinerary->user_id) {
            
            return true;
            
        } else {
            
            return abort(505, 'Unauthorized action.');
        }
    }
}