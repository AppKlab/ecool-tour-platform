<?php

namespace App\Policies;

use App\User;
use App\Module;
use Illuminate\Auth\Access\HandlesAuthorization;

class ModulePolicy
{
    use HandlesAuthorization;

    /**
     * Determine if the given user can delete the given module.
     *
     * @param  User  $user
     * @param  Module  $module
     * @return bool
     */
    public function destroy($user, $module)
    {
        
        if ($user->id === $module->user_id) {
            
            return true;
            
        } else {
            
            return abort(505, 'Unauthorized action.');
        }
    }
    
    
    /**
     * Determine whether the user has a specific ability for projects.
     *
     * @param  \App\User  $user
     * @param  \App\Project  $project
     * @return mixed
     */
    public function before($user, $ability)
    {
        //echo('Before policy');
        //die();
        //return true;
    }
    
    /**
     * Determine whether the user can view the module.
     *
     * @param  \App\User  $user
     * @param  \App\Module  $module
     * @return mixed
     */
    public function view($user, $module)
    {
        
        if ($user->id === $module->user_id) {

            return true;
            
        } else {

            return abort(505, 'Unauthorized action.');
        }

    }
    
    /**
     * Determine whether the user can create modules in an additional language.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create($user, $module)
    {
        if ($user->id === $module->user_id) {
            
            return true;
            
        } else {
            
            return abort(505, 'Unauthorized action.');
        }
    }
    
    /**
     * Determine whether the user can update the module.
     *
     * @param  \App\User  $user
     * @param  \App\Module  $module
     * @return mixed
     */
    public function update($user, $module)
    {
        if ($user->id === $module->user_id) {
            
            return true;
            
        } else {
            
            return abort(505, 'Unauthorized action.');
        }
    }
    
    /**
     * Determine whether the user can delete the module.
     *
     * @param  \App\User  $user
     * @param  \App\Module $module
     * @return mixed
     */
    public function delete($user, $module)
    {
        if ($user->id === $module->user_id) {
            
            return true;
            
        } else {
            
            return abort(505, 'Unauthorized action.');
        }
    }
}