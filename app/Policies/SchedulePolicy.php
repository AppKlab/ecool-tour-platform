<?php

namespace App\Policies;

use App\User;
use App\Module;
use App\Schedule;
use Illuminate\Auth\Access\HandlesAuthorization;

class SchedulePolicy
{
    use HandlesAuthorization;
    
    /**
     * Determine if the given user can delete the given schedule.
     *
     * @param  User  $user
     * @param  Module  $module
     * @return bool
     */
    public function destroy($user, $module)
    {
        
        if ($user->id === $module->user_id) {
            
            return true;
            
        } else {
            
            return abort(505, 'Unauthorized action.');
        }
    }
    
    
    /**
     * Determine whether the user has a specific ability for projects.
     *
     * @param  \App\User  $user
     * @param  \App\Project  $project
     * @return mixed
     */
    public function before($user, $ability)
    {
        //echo('Before policy');
        //die();
        //return true;
    }
    
    /**
     * Determine whether the user can view the schedule.
     *
     * @param  \App\User  $user
     * @param  \App\Module  $module
     * @return mixed
     */
    public function view($user, $module)
    {
        
        if ($user->id === $module->user_id) {
            
            return true;
            
        } else {
            
            return abort(505, 'Unauthorized action.');
        }
        
    }
    
    /**
     * Determine whether the user can create schedules in a module
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create($user, $module)
    {
        if ($user->id === $module->user_id) {
            
            return true;
            
        } else {
            
            return abort(505, 'Unauthorized action.');
        }
    }
    
    /**
     * Determine whether the user can update the schedule.
     *
     * @param  \App\User  $user
     * @param  \App\Module  $module
     * @return mixed
     */
    public function update($user, $module)
    {
        if ($user->id === $module->user_id) {
            
            return true;
            
        } else {
            
            return abort(505, 'Unauthorized action.');
        }
    }
    
    /**
     * Determine whether the user can delete the schedule.
     *
     * @param  \App\User  $user
     * @param  \App\Module $module
     * @return mixed
     */
    public function delete($user, $module)
    {
        if ($user->id === $module->user_id) {
            
            return true;
            
        } else {
            
            return abort(505, 'Unauthorized action.');
        }
    }
}