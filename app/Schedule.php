<?php

namespace App;

use App\User;
use App\Module;
use Illuminate\Database\Eloquent\Model;


/** Soft delete added to this model 
 *  The record is not actually removed from table. Instead, a deleted_at timestamp is set on the record.
 */
use Illuminate\Database\Eloquent\SoftDeletes;

class Schedule extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'schedules';
    
    use SoftDeletes;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'idModule','dateFrom','dateTo','timestampFrom','timestampTo','status','enabled',
    ];
    
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'user_id' => 'int',
    ];
    
    protected $dates = ['deleted_at',];
    
}
