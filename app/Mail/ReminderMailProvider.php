<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ReminderMailProvider extends Mailable
{

    use Queueable, SerializesModels;
    public $agency;
    public $provider;
    public $itinerary;
    public $modulename;
    public $description;
    public $datefrom;
    public $dateto;
    public $token;
    public $msg;
    
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($agency, $provider, $itinerary, $modulename, $description, $datefrom, $dateto, $token, $msg)
    {
        $this->agency = $agency;
        $this->provider = $provider;
        $this->itinerary = $itinerary;
        $this->modulename = $modulename;
        $this->description = $description;
        $this->datefrom = $datefrom;
        $this->dateto = $dateto;
        $this->token = $token;
        $this->msg = $msg;
    }
    
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $msg = $this->msg;
        
        return $this->subject(trans('texts.msg'.$msg.'subject'))->view('mail.reminder_mail_provider');
        
    }
    
    
    
}
