<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendRating extends Mailable
{

    use Queueable, SerializesModels;
    public $usertype;
    public $user;
    public $modulename;
    public $description;
    public $daterange;
    public $token;
    public $answer1;
    public $answer2;
    public $answer3;
    public $avg;
    
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($usertype,$user, $modulename, $description, $daterange, $token, $answer1, $answer2, $answer3, $avg)
    {
        $this->usertype = $usertype;
        $this->user = $user;
        $this->modulename = $modulename;
        $this->description = $description;
        $this->daterange = $daterange;
        $this->token = $token;
        $this->answer1 = $answer1;
        $this->answer2 = $answer2;
        $this->answer3 = $answer3;
        $this->avg = $avg;
        
    }
    
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        return $this->subject(trans('texts.ratingmail'))->view('mail.send_rating');
        
    }
    
    
    
}
