<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class UserVerification extends Mailable
{

    use Queueable, SerializesModels;
    public $enabling_code;
    public $userType;
    public $username;
    public $email;
    public $address;
    
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($enabling_code, $userType, $username, $email, $address)
    {
        $this->enabling_code = $enabling_code;
        $this->userType = $userType;
        $this->username = $username;
        $this->email = $email;
        $this->address = $address;
    }
    
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mail.enable_user');
        
    }
    
    
    
}
