<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendResetPassword extends Mailable
{

    use Queueable, SerializesModels;
    public $resetmail_token;
    public $email;
    
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($resetmail_token, $email)
    {
        $this->resetmail_token = $resetmail_token;
        $this->email = $email;
    }
    
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject(trans('auth.reset'))->view('auth.emails.password');
        
    }
    
    
    
}
