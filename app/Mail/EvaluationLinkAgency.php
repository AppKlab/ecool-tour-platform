<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class EvaluationLinkAgency extends Mailable
{

    use Queueable, SerializesModels;
    public $itineraryname;
    public $description;
    public $daterange;
    public $token;
    
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($itineraryname, $description, $daterange, $token)
    {
        $this->itineraryname = $itineraryname;
        $this->description = $description;
        $this->daterange = $daterange;
        $this->token = $token;
    }
    
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        return $this->subject(trans('texts.evaluationlink'))->view('mail.evaluation_link_agency');
        
    }
    
    
    
}
