<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class DeleteGroupsMail extends Mailable
{

    use Queueable, SerializesModels;
    public $msg;
    
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($msg)
    {
        $this->msg = $msg;
    }
    
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $msg = $this->msg;
        
        return $this->subject(trans('texts.msggroup'.$msg.'subject'))->view('mail.delete_groups');
        
    }
    
    
    
}
