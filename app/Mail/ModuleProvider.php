<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ModuleProvider extends Mailable
{

    use Queueable, SerializesModels;
    public $agency;
    public $modulename;
    public $description;
    public $daterange;
    public $token;
    
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($agency, $modulename, $description, $daterange, $token)
    {
        $this->agency = $agency;
        $this->modulename = $modulename;
        $this->description = $description;
        $this->daterange = $daterange;
        $this->token = $token;
    }
    
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        return $this->subject(trans('texts.modulerequest'))->view('mail.module_provider');
        
    }
    
    
    
}
