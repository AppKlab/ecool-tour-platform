<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ModuleProviderResponse extends Mailable
{

    use Queueable, SerializesModels;
    public $provider;
    public $modulename;
    public $description;
    public $daterange;
    public $token;
    public $answer;
    public $messageprovider;
    
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($provider, $modulename, $description, $daterange, $token, $answer, $messageprovider)
    {
        $this->provider = $provider;
        $this->modulename = $modulename;
        $this->description = $description;
        $this->daterange = $daterange;
        $this->token = $token;
        $this->answer = $answer;
        $this->messageprovider = $messageprovider;
        
    }
    
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        
        return $this->subject(trans('texts.providerresponse'))->view('mail.module_provider_response');
    }
    
    
    
}
