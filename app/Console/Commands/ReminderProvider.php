<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\User;
use App\Module;
use App\Itinerary;
use App\ItineraryModule;

use App\Repositories\ItineraryModuleRepository;

use DateTime;
use Mail;
use App\Mail\ReminderMailProvider;

class ReminderProvider extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'reminder:provider';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send a mail to the providers if they have not answered to the agency';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //A cronjob will send mails to providers who have not
        //answered (confirmed or cancelled a module) to an agency (status = 2)
        //If 72 hours have past the module is cancelled (status = 3) and a mail
        //sent to the agency and the provider with this info
        
        //Step 1: get itimodules with status 2 
        $itimodules = ItineraryModule::where ('status', '2')
                        ->where ('enabled','1')
                        ->get();
        
        foreach ( $itimodules as $itimodule)
        {
            
            //We have the itimodule's data
            $datetocheck = $itimodule['updated_at'];
            $itineraryid = $itimodule['idItinerary'];
            $moduleid = $itimodule['idModule'];
            $token = $itimodule['token_messages'];
            
            $dateFrom = $itimodule['dateFrom'];
            $dateTo = $itimodule['dateTo'];
            $dateFrom = new DateTime($dateFrom);
            $dateFrom = $dateFrom->format('d. m. Y H:i');
            $dateTo = new DateTime($dateTo);
            $dateTo = $dateTo->format('d. m. Y H:i');

            //get module's data
            $module = Module::where('id',$moduleid)
            ->first();
            
            //get provider's data
            $providerid = $module['user_id'];
            $provider =  User::where('id',$providerid)
            ->first();
            $providerlanguage = $provider['idLang'];
            
            //get module's data in providers language
            $modulelang = Module::where('idModule',$module['idModule'])
            ->where('user_id',$module['user_id'])
            ->where('idLang',$providerlanguage)
            ->first();
            
            //get itinerary's data 
            $itinerary = Itinerary::where('id',$itineraryid)
            ->first();
            
            //get agency's data
            $agencyid = $itinerary['user_id'];
            $agency =  User::where('id',$agencyid)
            ->first();
            $agencylanguage = $agency['idLang'];

            
            //Step 2: Compare the updated_at field of this itimodule with the current date-time
            $daystosum = '3';
            $datesum = date('d-m-Y H:i', strtotime($datetocheck.' + '.$daystosum.' days'));
            $datetime = new DateTime($datesum);
            $timestamp = $datetime->getTimestamp();
            
            $currenttime = date('Y-m-d H:i:s');
            $datetimenow = new DateTime($currenttime);
            $timestampnow = $datetimenow->getTimestamp();
            
            //If less than 72 hours: send mail to provider
            if ($timestampnow < $timestamp) {
                //$msg = 'Mail za provider - reminder';
                $msg = 1;
                Mail::to($provider['email'])
                ->send(new ReminderMailProvider($agency,$provider,$itinerary, $modulelang['modulename'],$modulelang['description'],$dateFrom,$dateTo,$token,$msg));
            }
            //If more than 72 hours have past: mark module with status = 3 (cancelled) and
            //mail the agency and the provider with this info
            else {
                //$msg = 'Mail za provider - module cancelled';
                $msg = 2;
                Mail::to($provider['email'])
                ->send(new ReminderMailProvider($agency, $provider,$itinerary,$modulelang['modulename'],$modulelang['description'],$dateFrom,$dateTo,$token,$msg));
                
                //change status module in itinerary to 3 (cancelled)
                $itimodule->status   = '3';
                $itimodule->save();
                
                //$msg = 'Mail za agency - 72 hours have passed, module cancelled';
                $msg = 3;
                Mail::to($agency['email'])
                ->send(new ReminderMailProvider($agency, $provider,$itinerary,$modulelang['modulename'],$modulelang['description'],$dateFrom,$dateTo,$token,$msg));
                
                //change status itinerary for this itinerary to 2 (there is a rejection)
                $itinerary->status   = '2';
                $itinerary->save();
            }
        }
    }
}
