<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\User;

use DateTime;
use Mail;
use App\Itinerary;
use App\Mail\DeleteGroupsMail;

class DeleteGroups extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'groups:delete';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Delete users with userType=4 after a year of inactivity (GDPR)';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //A cronjob will send a mail to users with userType = 4 AND/OR delete them after a year of inactivity
        
        //Step 1: get users with userType 4
        $groups = User::where ('userType', '4')
        //->where ('enabled','1')
        //->where ('status','1')
        //->where ('lastLogin','>',strtotime('1 year ago'))
        ->get();
        
        foreach ( $groups as $group)
        {
            
            //We have the group user's lastLogin data
            $datetocheck = $group['lastLogin'];
            
            //Step 2: Compare the lastLogin field with the current date-time
            //Date to compare: 1 year from last login
            $daystosum1 = '365';
            $datesum1 = date('d-m-Y H:i', strtotime($datetocheck.' + '.$daystosum1.' days'));
            $datetime = new DateTime($datesum1);
            $timestamp = $datetime->getTimestamp();
            
            //Date to compare: 1 year - 1 week from last login
            $daystosum2 = '358';
            $datesum2 = date('d-m-Y H:i', strtotime($datetocheck.' + '.$daystosum2.' days'));
            $datetimereminder = new DateTime($datesum2);
            $timestampreminder = $datetimereminder->getTimestamp();
            
            $currenttime = date('Y-m-d H:i:s');
            $datetimenow = new DateTime($currenttime);
            $timestampnow = $datetimenow->getTimestamp();
            
            //If is more than 1 year: send mail to user saying that his account will be deleted and change user status
            if ($timestampnow >= $timestamp) {
                $msg = 2;
                Mail::to($group['email'])
                ->send(new DeleteGroupsMail($msg));
                //$group->status   = '0';
                //$group->save();
                
                //Delete all itineraries for this user as well (soft delete)
                Itinerary::where('user_id',$group['id'])->delete();
                
                //Delete the group (user) permanently (forcedelete)
                $group->forcedelete();
                
            }
            //Send a reminder mail one week before it is a year of the lastLogin
            elseif ($timestampnow > $timestampreminder AND $timestampnow < $timestamp){
                $msg = 1;
                Mail::to($group['email'])
                ->send(new DeleteGroupsMail($msg));
            }
        }
    }
}
