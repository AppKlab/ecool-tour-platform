<?php

namespace App;

use App\User;
use Illuminate\Database\Eloquent\Model;


/** Soft delete added to this model 
 *  The record is not actually removed from table. Instead, a deleted_at timestamp is set on the record.
*/
use Illuminate\Database\Eloquent\SoftDeletes; 

class Itinerary extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'itineraries';
    
    use SoftDeletes;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id','idItinerary','idLang','itineraryname','description','longdescription','googlemapsUrl',
        'personsMin','personsMax','dateFrom','dateTo','price','pricedescription',
        'public','status','performed','enabled',
    ];
    
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        //'user_id' => 'int',
    ];
    
    protected $dates = ['deleted_at',];
    
 
}
