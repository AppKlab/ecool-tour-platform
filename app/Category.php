<?php

namespace App;

use App\User;
use App\Module;
use Illuminate\Database\Eloquent\Model;


class Category extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'categories';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'idModule','idCategory',
    ];
    
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'int',
    ];
    
   
 
}
