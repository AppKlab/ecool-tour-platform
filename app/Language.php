<?php

namespace App;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Language extends Model
{
    protected $primaryKey = 'idLang';
    protected $table = 'languages';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'language','enabled',
    ];
    
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'int',
    ];
    

}
