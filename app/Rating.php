<?php

namespace App;

use App\User;
use App\Module;
use App\ItineraryModule;
use App\Itinerary;
use Illuminate\Database\Eloquent\Model;


/** Soft delete added to this model 
 *  The record is not actually removed from table. Instead, a deleted_at timestamp is set on the record.
 */
use Illuminate\Database\Eloquent\SoftDeletes;

class Rating extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'ratings';
    
    use SoftDeletes;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'idItineraryModule','fromUser','toUser','answers','stars','token_messages',
    ];
    
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'int',
    ];
    
    protected $dates = ['deleted_at',];
    
    
}
