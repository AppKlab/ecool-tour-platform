<?php

namespace App\Repositories;

use App\User;
use Illuminate\Http\Request;
use App\Http\Requests;

use Illuminate\Support\Facades\Validator;

class UserRepository
{
    /**
     * Get all of the users (for admin only).
     *
     * @param  User  $user
     * @return Collection
     */
    
    
   public function forAdmin()
    {
        return User::where('userType','<>','1')
        ->orderBy('userType', 'asc')
        ->orderBy('username', 'asc')
        ->get();
    }
    
    public function forAdminFiltered()
    {
        return User::where('idLang','en')
        ->where('userType','<>','3')
        ->orderBy('userType', 'asc')
        ->orderBy('username', 'asc')
        ->get();
    }
    
    public function forAdminFilter($filter)
    {
        return User::where('userType',$filter)
        ->orderBy('userType', 'asc')
        ->orderBy('username', 'asc')
        ->get();
    }
    
}
