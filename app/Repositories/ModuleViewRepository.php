<?php

namespace App\Repositories;

use App\User;
use App\Module;
use App\ModCategory;
use App\Language;
use App\Category;
use App\Tag;
use App\Gallery;
use App\Schedule;
use App\Itinerary;
use App\ItineraryModule;
use Illuminate\Http\Request;
use App\Http\Requests;

use App;
use Session;
use DateTime;

use Illuminate\Support\Facades\Validator;

class ModuleViewRepository
{
    /**
     * Get all of the modules for a given user.
     *
     * @param  User  $user
     * @return Collection
     */
    
    public function getLang()
    {
        
        if($session_language = Session::get('locale')){
            return $language = $session_language;
        }else{
            return App::getLocale();
            
        }
    }
    
    
    public function getParentModule($idModule,$userid)
    {
        
        return Module::where('idModule', $idModule)
        ->where('user_id', $userid)
        ->where('idLang', 'en')
        ->first();
    }
    
    
    public function getCategories($moduleid)
    {
        
        return Category::where('idModule', $moduleid)
        ->orderBy('idCategory', 'asc')
        ->get();
    }
    
    public function getTags($moduleid)
    {
        
        return Tag::where('idModule', $moduleid)
        ->orderBy('idTag', 'asc')
        ->get();
    }
    
    public function getImages($moduleid)
    {
        
        return Gallery::select('id','imagename')
        ->where('idModule', $moduleid)
        ->orderBy('created_at', 'asc')
        ->get();
    }
    
    public function getSchedules($moduleid)
    {
        
        return Schedule::where('idModule', $moduleid)
        ->orderBy('timestampFrom', 'asc')
        ->get();
    }
    
    public function getProvider($userid)
    {
        
        return User::select('title','address','telephone','cellphone','email')
        ->where('id', $userid)
        ->first();
    }
    
    public function getModuleInAnotherLang($idModule,$userid,$idLang)
    {
        
        return Module::where('idModule', $idModule)
        ->where('user_id', $userid)
        ->where('idLang', $idLang)
        ->first();
    }
    
    public function YoutubeID($url)
    {
        if(strlen($url) > 11)
        {
            if (preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $url, $match))
            {
                return $match[1];
            }
            else
                return false;
        }
        
        return $url;
    }
    
    
    public function getAgencies($moduleid){
        
        //Must get agencies that CURRENTLY offer this module in their itineraries
        //module must be in itinerarymodules table for an itinerary
        //AND itinerary status from 4 to 7 AND public = 1
        $now = new DateTime();
        
        //moduleid could not be from parent module, so get parent module first
        $module = Module::findOrFail($moduleid);
        
        $moduleparent = $this->getParentModule($module['idModule'],$module['user_id']);
        
        //with parent module get the records from the itinerarymodules table
        //status of the itimodule must be 4 or 5
        $itineraryModules =  ItineraryModule::where('idModule', $moduleparent['id'])
                                ->where('status', '>', '3')
                                ->where('status', '<', '6')
                                ->where('dateFrom','>',$now)
                                ->where('dateTo','>',$now)
                                ->inRandomOrder()
                                ->get();

        //get the itineraries for the itimodules and check the itinerary status
        $itineraries = array();
        foreach ( $itineraryModules as $itineraryModule )
        {
        
            $itineraries[] = Itinerary::where('id', $itineraryModule['idItinerary'])
                                    ->where('status', '>', '3')
                                    ->where('status', '<', '6')
                                    ->where('public', 1)
                                    ->where('dateFrom','>',$now)
                                    ->where('dateTo','>',$now)
                                    ->first();
        }
        
        $agencies = array();
        //get the agency that owns each itinerary
        foreach ( $itineraries as $itinerary )
        {
            
            $agencies[] = User::where('id', $itinerary['user_id'])
                        ->first();
        }
        
        //return only records that fulfill all the conditions
        
        
        return $agencies;
    }
    
    
}
