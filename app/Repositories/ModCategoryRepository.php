<?php

namespace App\Repositories;

use App\User;
use App\Module;
use App\ModCategory;
use App\Language;
use App\Category;
use App\Tag;
use App\Gallery;
use Illuminate\Http\Request;
use App\Http\Requests;

use App;
use Session;

use Illuminate\Support\Facades\Validator;

class ModCategoryRepository
{
    /**
     * Get all of the modules for a given user.
     *
     * @param  User  $user
     * @return Collection
     */
    
    
    
    public function getCategoryModules($cat)
    {
        
        return Module::where('idLang', 'en')
        ->where('enabled', '1')
        ->where('public', '1')
        ->inRandomOrder()
        ->get();
    }
    
    public function getRealModuleId($moduleid)
    {
        
        //get the idModule of the parent module
        
        return $parentmodule = Module::select('idModule','user_id','mainimage','enabled','public')
        ->where('id', $moduleid)
        ->orderBy('idModule', 'asc')
        ->get();
    }
    
    public function getLangInModules($moduleid,$userid)
    {
        
        return Module::select('id','idModule')
        ->where('idModule', $moduleid)
        ->where('user_id', $userid)
        ->get();
    }
    
    public function getModule($moduleid,$userid,$userlanguage)
    {
            
        return Module::select('id','idModule','user_id','modulename','description','mainimage','geolocationLon','geolocationLat')
        ->where('idModule', $moduleid)
        ->where('user_id', $userid)
        ->where('idLang', $userlanguage)
        ->first();
    }
    
    public function getLang()
    {
        
        if($session_language = Session::get('locale')){
            return $language = $session_language;
        }else{
            return App::getLocale();
            
        }
    }

    
    public function getModulesInCategory($cat)
    {

        return Category::select ('idModule')
        ->where('idCategory', $cat)
        ->inRandomOrder()
        ->get();
    }
    
    public function getMainimage($moduleid,$userid)
    {
        
        return Module::select('mainimage','idModule','user_id','geolocationLon','geolocationLat')
        ->where('idModule', $moduleid)
        ->where('user_id', $userid)
        ->where('idLang', 'en')
        ->first();
    }
}
