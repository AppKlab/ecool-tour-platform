<?php

namespace App\Repositories;

use App\User;
use App\Rating;
use App\Module;
use App\ItineraryModule;
use App\Itinerary;
use Illuminate\Http\Request;
use App\Http\Requests;
use DateTime;

use Illuminate\Support\Facades\Validator;

class RatingRepository
{
    /**
     * Get all of the itineraries for a given user.
     *
     * @param  User  $user
     * @return Collection
     */
    
    
    public function getModule($idModule, $user_id, $userlanguage)
    {
        return Module::where('idModule', $idModule)
                ->where('user_id',$user_id)
                ->where('idLang',$userlanguage)
                ->first();
    }
    
    public function getRating($token)
    {
        return Rating::where('token',$token)
                ->first();
    }
    
    public function getItinerary($token)
    {
        return Itinerary::where('token',$token)
                ->first();
    }

    public function getItiModules($itineraryid)
    {
        return ItineraryModule::where('idItinerary',$itineraryid)
                ->get();
    }
    
    public function getItineraryLang($idItinerary,$user_id,$userlanguage)
    {
        return Itinerary::select('itineraryname')
                ->where('idItinerary',$idItinerary)
                ->where('user_id',$user_id)
                ->where('idLang',$userlanguage)
                ->first();
    }
    
    public function getModuleLang($idModule,$user_id,$userlanguage)
    {
        return Module::where('idModule',$idModule)
                ->where('user_id',$user_id)
                ->where('idLang',$userlanguage)
                ->first();
    }
    
    
    
}
