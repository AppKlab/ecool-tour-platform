<?php

namespace App\Repositories;

use App\User;
use App\Module;
use App\Gallery;
use Illuminate\Http\Request;
use App\Http\Requests;

use Illuminate\Support\Facades\Validator;

class GalleryRepository
{
    /**
     * Get all of the images for a given module.
     *
     * @param  Gallery  $gallery
     * @return Collection
     */
    
    
    public function getImages($moduleid)
    {
        return Gallery::where('idModule', $moduleid)
                    ->orderBy('created_at', 'asc')
                    ->get();
    }
    
    
}
