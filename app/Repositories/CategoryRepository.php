<?php

namespace App\Repositories;

use App\User;
use App\Module;
use Illuminate\Http\Request;
use App\Http\Requests;

use Illuminate\Support\Facades\Validator;

class CategoryRepository
{
    /**
     * Get all of the categories for a given module.
     *
     * @param  Category  $category
     * @return Collection
     */
    
    
    public function forCategory(Module $module)
    {
        return Category::where('idModule', $module->idModule)
                    ->orderBy('created_at', 'asc')
                    ->get();
    }
    
    
}
