<?php

namespace App\Repositories;

use App\User;
use App\Module;
use App\Category;
use App\Tag;
use App\Gallery;
use App\Itinerary;
use App\ItineraryModule;
use Illuminate\Http\Request;
use App\Http\Requests;

use Illuminate\Support\Facades\Validator;
use DateTime;


class ItineraryModuleRepository
{
    /**
     * Get all of the images for a given module.
     *
     * @param  Gallery  $gallery
     * @return Collection
     */
    
    
    public function getItineraryModules($itineraryid)
    {
        $itineraryModules = ItineraryModule::where('idItinerary', $itineraryid)
                            ->orderBy('dateFrom', 'asc')
                            ->orderBy('dateTo', 'asc')
                            ->get();
        
        //change dates format
        foreach ( $itineraryModules as $itineraryModule )
        {

            if ($itineraryModule['dateFrom'] != null and $itineraryModule['dateTo'] != null){

                $dateFrom = new DateTime($itineraryModule['dateFrom']);
                $dateFrom1 = $dateFrom->format('d. m. Y H:i');
                $dateTo = new DateTime($itineraryModule['dateTo']);
                $dateTo1 = $dateTo->format('d. m. Y H:i');
                $itineraryModule['dateFrom'] = $dateFrom1;
                $itineraryModule['dateTo'] = $dateTo1;
            }else{
                
                $itineraryModule['dateFrom'] = 'none';
                $itineraryModule['dateTo'] = 'none';
            }
                
        }
        return $itineraryModules;
    }
    
    public function getAllModules($userlanguage)
    {
        
        return Module::where('idLang', $userlanguage)
        ->where('enabled', '1')
        ->orderBy('created_at', 'asc')
        ->get();
    }
    
    public function getModules()
    {
        
        return Module::select ('id','idModule','user_id','public','enabled')
        ->where('enabled', '1')
        ->where('idLang', 'en')
        ->distinct()
        ->inRandomOrder()
        ->get();
    }
    
    public function getLangInModules($moduleid,$userid)
    {
        
        return Module::select('id','idModule')
        ->where('idModule', $moduleid)
        ->where('user_id', $userid)
        ->get();
    }
    
    public function getMainimage($moduleid,$userid)
    {
        
        return Module::select('id','mainimage','idModule','user_id')
        ->where('idModule', $moduleid)
        ->where('user_id', $userid)
        ->where('idLang', 'en')
        ->first();
    }
    
    public function getModule($moduleid,$userid,$userlanguage)
    {
        
        return Module::select('id','idModule','user_id','modulename','description','mainimage','longdescription')
        ->where('idModule', $moduleid)
        ->where('user_id', $userid)
        ->where('idLang', $userlanguage)
        ->first();
    }
    
    public function getModuleCategories($moduleid)
    {
        
        return Category::select ('idModule','idCategory')
        ->where('idModule', $moduleid)
        ->orderBy('idCategory', 'asc')
        ->get();
    }
    
    public function getParentItineraryId($idItinerary,$user_id){
        
        return Itinerary::select ('id')
        ->where('idItinerary', $idItinerary)
        ->where('user_id', $user_id)
        ->where('idLang', 'en')
        ->get();
    }
    
    public function getModuleOwner($moduleId){
        
        $module = Module::select('idModule','user_id','modulename')
        ->where('id', $moduleId)
        ->first();
        
        return User::select('id','title','email','idLang')
        ->where('id', $module['user_id'])
        ->first();
        
    }
    
    public function getModuleForMail($moduleid,$userlanguage)
    {
        
        if ($userlanguage == 'en'){
            
            //if userLang is english then just get the module with the module id
            
            return Module::select('modulename','description')
            ->where('id', $moduleid)
            ->first();
        }else{
            
            //if userLang is NOT english then first get the module with the module id
            //then the module in the userlang
            $moduleEn = Module::select('idModule','user_id')
            ->where('id', $moduleid)
            ->first();
            
            
            return Module::select('modulename','description')
            ->where('idModule', $moduleEn['idModule'])
            ->where('user_id', $moduleEn['user_id'])
            ->where('idLang', $userlanguage)
            ->first();
        }
        
        
    }
    
    public function validateDates($daterange, Request $request){
        
        //parses the date range input of the itinerary form
        //daterange format: 17/08/2018 11:00 - 18/08/2018 20:00
        
        //places the date range input into an array
        parse_str($daterange, $output1);
        //places the dates (with the hours) in an array
        //Array ( [0] => 17/08/2018 11:00 [1] => 18/08/2018 20:00 )
        $dates = explode('-', $daterange);
        //places the date and the hours of each date in an array
        //First date: Array ( [0] => 17/08/2018 [1] => 11:00 [2] => )
        //Second date: Array ( [0] => [1] => 18/08/2018 [2] => 20:00)
        $dayhour1 = explode(' ', $dates[0]);
        $dayhour2 = explode(' ', $dates[1]);
        //places the date of each date in an array
        //First date: Array ( [0] => 17 [1] => 08 [2] => 2018 )
        //Second date: Array ( [0] => 18 [1] => 08 [2] => 2018 )
        $day1 = explode('/', $dayhour1[0]);
        $day2 = explode('/', $dayhour2[1]);
        //places the dates in a string
        //First date: 2018-08-17
        //Second date: 2018-08-18
        $day1_validation = $day1[2].'-'.$day1[1].'-'.$day1[0];
        $day2_validation = $day2[2].'-'.$day2[1].'-'.$day2[0];
        //places the hours in a string and the AM or PM
        //First hour: 11:00
        //Second hour: 20:00
        $hour1 = $dayhour1[1];
        $hour2 = $dayhour2[2];
        //New initial date and timestamp for validation using the values we've aquired before
        $date1 = new DateTime($day1_validation . ' ' . $hour1 );
        $timestamp1 = $date1->getTimestamp();
        //New end date and timestamp for validation using the values we've aquired before
        $date2 = new DateTime($day2_validation . ' ' . $hour2 );
        $timestamp2 = $date2->getTimestamp();
        //place values into $request variable
        $request['day1']= $day1_validation . ' ' . $hour1;
        $request['day2']= $day2_validation . ' ' . $hour2;
        $request['time1']= $timestamp1;
        $request['time2']= $timestamp2;
        
        return true;
    }
    
    public function getItiModuleFromToken($token){
    
        return ItineraryModule::where('token_messages', $token)
        ->first();
    
    }
    
    public function getItiModuleFromStatus($status,$itineraryid){
        
        return ItineraryModule::where('idItinerary', $itineraryid)
        ->where('status', $status)
        ->get();
        
    }
}
