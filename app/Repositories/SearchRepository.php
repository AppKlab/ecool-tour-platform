<?php

namespace App\Repositories;

use App\User;
use App\Module;
use App\ModCategory;
use App\Language;
use App\Category;
use App\Tag;
use App\Gallery;
use Illuminate\Http\Request;
use App\Http\Requests;

use App;
use Session;

use Illuminate\Support\Facades\Validator;

class SearchRepository
{
    /**
     * Get all of the modules for a given user.
     *
     * @param  User  $user
     * @return Collection
     */
    
    
    
    public function getLocations()
    {
        
        return Module::select('city')
        ->orderBy('city', 'asc')
        ->where('idLang', 'en')
        ->where('public', '1')
        ->where('enabled', '1')
        ->distinct()
        ->get();
    }
    
    public function getCategoryModules($cat)
    {
        
        return Module::where('idLang', 'en')
        ->where('enabled', '1')
        ->where('public', '1')
        ->inRandomOrder()
        ->get();
    }
    
    public function getRealModuleId($moduleid)
    {
        
        //get the idModule of the parent module
        
        return $parentmodule = Module::select('idModule','user_id','mainimage','enabled','public')
        ->where('id', $moduleid)
        ->orderBy('idModule', 'asc')
        ->get();
    }
    
    public function getLangInModules($moduleid,$userid)
    {
        
        return Module::select('id','idModule')
        ->where('idModule', $moduleid)
        ->where('user_id', $userid)
        ->get();
    }
    
    public function getModuleOLD($moduleid,$userid,$userlanguage)
    {
            
        return Module::select('id','idModule','user_id','modulename','description','mainimage')
        ->where('idModule', $moduleid)
        ->where('user_id', $userid)
        ->where('idLang', $userlanguage)
        ->first();
    }
    
    public function getModule($moduleid,$userlanguage)
    {
        if ($userlanguage == 'en'){
        
            return Module::where('id', $moduleid)
                ->first();
            
        }else{
            
            $moduleparent = Module::select('idModule','user_id')
            ->where('id', $moduleid)
            ->first();
            
            return Module::where('idModule', $moduleparent['idModule'])
            ->where ('user_id', $moduleparent['user_id'])
            ->where ('idLang', $userlanguage)
            ->first();
        }
    }
    
    public function getLang()
    {
        
        if($session_language = Session::get('locale')){
            return $language = $session_language;
        }else{
            return App::getLocale();
            
        }
    }

    
    public function getModulesInCategory($cat)
    {

        return Category::select ('idModule')
        ->where('idCategory', $cat)
        ->inRandomOrder()
        ->get();
    }
    
    public function getMainimage($moduleid,$userid)
    {
        
        return Module::select('mainimage','idModule','user_id')
        ->where('idModule', $moduleid)
        ->where('user_id', $userid)
        ->where('idLang', 'en')
        ->first();
    }
    
    public function getParentId($moduleid,$userid)
    {
        
        return Module::select('id')
        ->where('idModule', $moduleid)
        ->where('user_id', $userid)
        ->where('idLang', 'en')
        ->first();
    }
}
