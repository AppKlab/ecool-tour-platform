<?php

namespace App\Repositories;

use App\User;
use App\Module;
use App\ModCategory;
use App\Language;
use App\Category;
use App\Tag;
use App\Gallery;
use App\Monument;
use Illuminate\Http\Request;
use App\Http\Requests;

use App;
use Session;

use Illuminate\Support\Facades\Validator;

class FrontpageRepository
{
    /**
     * Get all of the modules for a given user.
     *
     * @param  User  $user
     * @return Collection
     */
    
    public function getModules()
    {
        
        return Module::select ('id','idModule','user_id')
        ->where('public', '1')
        ->where('enabled', '1')
        ->where('idLang', 'en')
        ->distinct()
        ->inRandomOrder()
        ->get();
    }
    
    
    public function getModuleCategories($moduleid)
    {
        
        return Category::select ('idModule','idCategory')
        ->where('idModule', $moduleid)
        ->orderBy('idCategory', 'asc')
        ->get();
    }
    
    public function getMainimage($moduleid,$userid)
    {
        
        return Module::withTrashed()
        ->where('idModule', $moduleid)
        ->where('user_id', $userid)
        ->where('idLang', 'en')
        ->first();
    }
    
    public function getLangInModules($moduleid,$userid)
    {
        
        return Module::withTrashed()
        ->select('id','idModule')
        ->where('idModule', $moduleid)
        ->where('user_id', $userid)
        ->get();
    }
    
    public function getModule($moduleid,$userid,$userlanguage)
    {
        
        return Module::withTrashed()
        ->where('idModule', $moduleid)
        ->where('user_id', $userid)
        ->where('idLang', $userlanguage)
        ->first();
    }
    
    public function getMonuments()
    {
        
        return Monument::where('idLang', 'en')
        ->get();
    }
    
    
    public function getModulesInCategory()
    {
        
        return Category::select ('idModule')
        ->distinct()
        ->inRandomOrder()
        ->limit(6)
        ->get();
    }
    

    public function getRealModuleId($moduleid)
    {
        
        //get the idModule of the parent module
        
        return $parentmodule = Module::withTrashed()
        ->select('idModule','user_id','mainimage')
        ->where('id', $moduleid)
        ->orderBy('idModule', 'asc')
        ->get();
    }
    
    
}
