<?php

namespace App\Repositories;

use App\Language;
use Illuminate\Http\Request;
use App\Http\Requests;

use Illuminate\Support\Facades\Validator;

use App\Repositories\LanguageRepository;

use App\Policies\LanguagePolicy;

use Lang;
use App;
use Session;
use DB;

class LanguageRepository
{
    /**
     * Get all of the languages 
     *
     * @param  Language  $language
     * @return Collection
     */
    
    
   public function getLanguages()
    {
        return Language::select('language')
        ->where('enabled','=','1')
        ->orderBy('idLang', 'asc')
        ->get();
    }
    
    
}
