<?php

namespace App\Repositories;

use App\User;
use App\Module;
use App\Schedule;
use Illuminate\Http\Request;
use App\Http\Requests;
use DateTime;

use Illuminate\Support\Facades\Validator;

class ScheduleRepository
{
    /**
     * Get all of the schedules for a given module.
     *
     * @param  Schedule  $schedule
     * @return Collection
     */
    
    
    public function getSchedules($moduleid)
    {
        return Schedule::where('idModule', $moduleid)
                    ->orderBy('created_at', 'asc')
                    ->get();
    }
    
    public function validateDates(Request $request){
        
        //parses the date range input of the itinerary form
        //daterange format: 17/08/2018 11:00 - 18/08/2018 20:00
        
        //places the date range input into an array
        parse_str($request->daterange, $output1);
        //places the dates (with the hours) in an array
        //Array ( [0] => 17/08/2018 11:00 [1] => 18/08/2018 20:00 )
        $dates = explode('-', $request->daterange);
        //places the date and the hours of each date in an array
        //First date: Array ( [0] => 17/08/2018 [1] => 11:00 [2] => )
        //Second date: Array ( [0] => [1] => 18/08/2018 [2] => 20:00)
        $dayhour1 = explode(' ', $dates[0]);
        $dayhour2 = explode(' ', $dates[1]);
        //places the date of each date in an array
        //First date: Array ( [0] => 17 [1] => 08 [2] => 2018 )
        //Second date: Array ( [0] => 18 [1] => 08 [2] => 2018 )
        $day1 = explode('/', $dayhour1[0]);
        $day2 = explode('/', $dayhour2[1]);
        //places the dates in a string
        //First date: 2018-08-17
        //Second date: 2018-08-18
        $day1_validation = $day1[2].'-'.$day1[1].'-'.$day1[0];
        $day2_validation = $day2[2].'-'.$day2[1].'-'.$day2[0];
        //places the hours in a string and the AM or PM
        //First hour: 11:00
        //Second hour: 20:00
        $hour1 = $dayhour1[1];
        $hour2 = $dayhour2[2];
        //New initial date and timestamp for validation using the values we've aquired before
        $date1 = new DateTime($day1_validation . ' ' . $hour1 );
        $timestamp1 = $date1->getTimestamp();
        //New end date and timestamp for validation using the values we've aquired before
        $date2 = new DateTime($day2_validation . ' ' . $hour2 );
        $timestamp2 = $date2->getTimestamp();
        //place values into $request variable
        $request['day1']= $day1_validation;
        $request['day2']= $day2_validation;
        $request['dayhour1']= $hour1;
        $request['dayhour2']= $hour2;
        $request['time1']= $timestamp1;
        $request['time2']= $timestamp2;
        
        return true;
    }
    
}
