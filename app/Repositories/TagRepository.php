<?php

namespace App\Repositories;

use App\User;
use App\Module;
use Illuminate\Http\Request;
use App\Http\Requests;

use Illuminate\Support\Facades\Validator;

class TagRepository
{
    /**
     * Get all of the tags for a given module.
     *
     * @param  Tag  $tag
     * @return Collection
     */
    
    
    public function forTag(Module $module)
    {
        return Tag::where('idModule', $module->idModule)
                    ->orderBy('created_at', 'asc')
                    ->get();
    }
    
    
}
