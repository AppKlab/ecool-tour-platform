<?php

namespace App\Repositories;

use App\User;
use App\Module;
use App\Language;
use App\Category;
use App\Tag;
use App\Gallery;
use Illuminate\Http\Request;
use App\Http\Requests;

use Illuminate\Support\Facades\Validator;

class ModuleRepository
{
    /**
     * Get all of the modules for a given user.
     *
     * @param  User  $user
     * @return Collection
     */
    
    
    public function forUser($user)
    {
        return Module::where('user_id', $user->id)
                    ->orderBy('idModule', 'asc')
                    ->get();
    }
    
    public function forUserLastId($user)
    {
        return Module::withTrashed()
        ->where('user_id', $user->id)
        ->where('idLang', 'en')
        ->orderBy('idModule', 'des')
        ->first();
    }
    
    public function forUserFiltered($user)
    {
        return Module::where('user_id', $user->id)
        ->where('idLang','en')
        ->orderBy('idModule', 'asc')
        ->get();
    }
    
    public function forAdmin()
    {
        return Module::orderBy('created_at', 'asc')
        ->get();
    }
    
    public function forAdminFiltered()
    {
        return Module::where('idLang','en')
        ->orderBy('created_at', 'asc')
        ->get();
    }
    
    public function forAdminFilteredByCategory($catid)
    {
        
        $modsincat = Category::where('idCategory',$catid)
        ->orderBy('idModule', 'asc')
        ->get();
        
        foreach ($modsincat as $modincat){
            
            $modules[] = Module::where('id',$modincat['idModule'])
            ->first();
        }
        
        return $modules;
    }
    
    public function forAdminUsers()
    {
        return User::orderBy('created_at', 'asc')
        ->get();
    }
    
    public function getLanguages()
    {
        return Language::select('language')
        ->orderBy('idLang', 'asc')
        ->get();
        
    }
    
    public function getCategories($idModule)
    {
        //Must get categories for parent module (module in english)

        
        return Category::select('idCategory')
        ->where('idModule',$idModule)
        ->orderBy('idCategory', 'asc')
        ->get();
        
    }
    
    public function getTags($idModule)
    {
        //Must get tags for parent module (module in english)
            
        return Tag::select('idTag')
        ->where('idModule',$idModule)
        ->orderBy('idTag', 'asc')
        ->get();
        
    }
    
    
}
