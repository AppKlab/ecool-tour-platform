<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


/** Soft delete added to this model 
 *  The record is not actually removed from table. Instead, a deleted_at timestamp is set on the record.
 */
use Illuminate\Database\Eloquent\SoftDeletes;

class Monument extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'monuments';
    
    use SoftDeletes;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'idLang','monumentName','monumentPicture','monumentUrl','geolocationLat','geolocationLon',
    ];
    
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'int',
    ];
    
    protected $dates = ['deleted_at',];
    
}
