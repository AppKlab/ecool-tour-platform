<?php

namespace App\Providers;

use Illuminate\Contracts\Auth\Access\Gate as GateContract;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

use Illuminate\Support\Facades\Gate;

use App\Policies\ModulePolicy;
use App\Policies\CategoryPolicy;
use App\Policies\LanguagePolicy;
use App\Policies\TagPolicy;
use App\Policies\GalleryPolicy;
use App\Policies\ItineraryPolicy;
use App\Policies\ItineraryModulePolicy;
use App\Policies\SchedulePolicy;
use App\Policies\UserPolicy;
use App\Policies\MessagePolicy;

use App\Module;
use App\Category;
use App\Language;
use App\Tag;
use App\Gallery;
use App\Itinerary;
use App\ItineraryModule;
use App\Schedule;
use App\User;
use App\Message;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        Module::class => ModulePolicy::class,
        Category::class => CategoryPolicy::class,
        Language::class => LanguagePolicy::class,
        Tag::class => TagPolicy::class,
        Gallery::class => GalleryPolicy::class,
        Itinerary::class => ItineraryPolicy::class,
        ItineraryModule::class => ItineraryModulePolicy::class,
        Schedule::class => SchedulePolicy::class,
        User::class => UserPolicy::class,
        Message::class => MessagePolicy::class,

    ];

    /**
     * Register any application authentication / authorization services.
     *
     * @param  \Illuminate\Contracts\Auth\Access\Gate  $gate
     * @return void
     */
    //public function boot(GateContract $gate)
    public function boot()
    {
        //$this->registerPolicies($gate);
        $this->registerPolicies();
        
    }
}
