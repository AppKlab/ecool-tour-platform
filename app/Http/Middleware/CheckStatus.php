<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class CheckStatus
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = $next($request);
        
        
        //If the status is not verified or user has been disabled by admin redirects to login with message
        if(Auth::check() && Auth::user()->status != '1'){
            Auth::logout();
            return redirect('/login')->with('login_error', trans('auth.notverified'));
        }elseif(Auth::check() && Auth::user()->enabled != '1'){
            Auth::logout();
            return redirect('/login')->with('login_error', trans('auth.notenabled'));
        }elseif(Auth::check()){
            return redirect('/authhome');
        }
        return $response;
    }
}
