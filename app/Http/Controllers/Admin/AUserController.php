<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Repositories\UserRepository;

use App\Policies\UserPolicy;

use Lang;
use App;
use Session;
use DB;
use App\User;
use App\Module;
use App\Rating;
use App\Itinerary;
use Auth;


class AUserController extends Controller
{
   
    /**
     * The user repository instance.
     *
     * @var UserRepository
     */
    protected $users;
    protected $primaryKey = 'id';
    protected $table = 'users';

    /**
     * Create a new controller instance.
     * 
     * Add the language to the session 
     *
     * @param  UserRepository  $users
     * @return void
     */
    public function __construct(UserRepository $users)
    {
        $this->middleware('auth');
        
        $this->users = $users;
        
        if($apploc = App::getLocale()){ 

            Session::put('locale', $apploc);
            $session_language = Session::get('locale');
            
        }else{ 
            
            Session::put('locale', 'sl');
            $session_language = Session::get('locale');
        }
    }
    
    /**
     * Display a list of all of the users.
     *
     * @param  Request  $request
     * @return Response
     */
    public function index(Request $request)
    {
       
        //Only admin can view user index
        if (Session::get('isAdmin')){
            
            return view('admin.adminusersindex', [
                'users' => $this->users->forAdmin(),
            ]);
            
        }else{
            
            return abort(505, 'Unauthorized action.');
            
        }
        
    }
    
    public function indexFiltered(Request $request,$filter)
    {
        
        //Only admin can view user index
        if (Session::get('isAdmin')){
            
            return view('admin.adminusersindex', [
                'users' => $this->users->forAdminFilter($filter),
            ]);
            
        }else{
            
            return abort(505, 'Unauthorized action.');
            
        }
        
    }
    
    /**
     * Update the enable field of a user.
     *
     * @param  Request  $request
     * @return Response
     */
    public function enable($id, Request $request)
    {
        
        //Only admin can enable users
        if (Session::get('isAdmin')){
            
            $user = User::find($id);
            
            if ($request->get('actionenable') == 'enable'){
                $action = '1';
                $user-> enable_code = null; 
            }else{
                $action = '0';
                $user-> enable_code = str_random(30).time(); 
            }
            
            $user-> enabled = $action;
            
            
            $user->save();
            
            return redirect('Admin/users');
            
        }else{
            
            return abort(505, 'Unauthorized action.');
            
        }
        
        
    }
    
    /**
     * Update the status field of a user (VERIFIED field).
     *
     * @param  Request  $request
     * @return Response
     */
    public function verify($id, Request $request)
    {
        
        //Only admin can verify users
        if (Session::get('isAdmin')){
            
            $user = User::find($id);
            
            if ($request->get('actionenable') == 'enable'){
                $action = '1';
                $user-> activation_code = null;
            }else{
                $action = '0';
                $user-> activation_code = str_random(30).time();
            }
            
            $user-> status = $action;
             
            
            $user->save();
            
            return redirect('Admin/users');
            
        }else{
            
            return abort(505, 'Unauthorized action.');
            
        }
        
        
        
    }
    
  
    
    /**
     * Destroy the given user.
     *
     * @param  Request  $request
     * @param  User  $user
     * @return Response
     */
    public function destroy(User $user, $userid)
    {
        
        //Only admin can verify users
        if (Session::get('isAdmin')){
            
            //Using soft deletes (deleted_at)
            User::destroy($userid);
            
            Module::where('user_id','=',$userid)
            ->delete();
            
            Itinerary::where('user_id','=',$userid)
            ->delete();
            
            return redirect('Admin/users');
            
        }else{
            
            return abort(505, 'Unauthorized action.');
            
        }
        
        
        
    }
    
    /**
     * Display the rating for the given user.
     *
     * @param  Request  $request
     * @return Response
     */
    public function rating(Request $request, $userid)
    {
        
        //Only admin can view user's ratings
        if (Session::get('isAdmin')){
            
            $user = User::findOrFail($userid);
            
            $ratings = count(Rating::where('toUser',$userid)->get()); 
            
            $rating1 = Rating::where('toUser',$userid)->avg('answer1');
            $rating2 = Rating::where('toUser',$userid)->avg('answer2');
            $rating3 = Rating::where('toUser',$userid)->avg('answer3');
            
            $avg = ($rating1 + $rating2 + $rating3)/3;
            
            return view('admin.adminusersrating', [
                'user' => $user,
                'ratings' => $ratings,
                'rating1' => $rating1,
                'rating2' => $rating2,
                'rating3' => $rating3,
                'avg' => $avg,
            ]);
            
        }else{
            
            return abort(505, 'Unauthorized action.');
            
        }
        
    }
    
    
}
