<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Validator;
use DateTime;

use App\Itinerary;
use App\Repositories\ItineraryRepository;
use App\Repositories\ItineraryModuleRepository;

use App\Policies\ItineraryPolicy;

use Lang;
use App;
use Session;
use DB;
use Auth;
use Image;
use PDF;

use App\User;
use App\Module;
use App\Category;
use App\Tag;
use App\Gallery;
Use App\ItineraryModule;

use Mail;
use App\Mail\ItineraryConfirmation;
use App\Jobs\ItineraryConfirmationEmail;
use App\Mail\ItineraryCancel;
use App\Jobs\ItineraryCancelEmail;
use App\Mail\EvaluationLink;
use App\Jobs\EvaluationLinkEmail;
use App\Mail\EvaluationLinkAgency;
use App\Jobs\EvaluationLinkAgencyEmail;



class AItineraryController extends Controller
{
   
    /**
     * The itinerary repository instance.
     *
     * @var ItineraryRepository
     */
    protected $itineraries;
    protected $primaryKey = 'id';
    protected $table = 'itineraries';

    /**
     * Create a new controller instance.
     * 
     * @param  ItineraryRepository  $itineraries
     * @return void
     */
    public function __construct(ItineraryRepository $itineraries, ItineraryModuleRepository $itinerarymodules)
    {
        $this->middleware('auth');
        
        $this->itineraries = $itineraries;
        $this->itinerarymodules = $itinerarymodules;
        
    }
    
    /**
     * Display a list of all of the user's itineraries.
     *
     * @param  Request  $request
     * @return Response
     */
    public function index(Request $request)
    {
        //Only admin can see index
        if (Session::get('isAdmin')){
            
            return view('admin.adminitinerariesindex', [
                'itineraries' => $this->itineraries->forAdminFiltered(),
                'itinerariestranslated' => $this->itineraries->forAdmin(),
                'languages' => $this->itineraries->getLanguages(),
                'userType' => Auth::user()->userType,
            ]);
            
        }else{
            
            return abort(505, 'Unauthorized action.');
            
        }
    }
    
    
    
    /**
     * Update the public field of a itinerary.
     *
     * @param  Request  $request
     * @return Response
     */
    public function public($id, Request $request)
    {
        
        //Only admin can see itineraries here
        if (Session::get('isAdmin')){
            $itinerary = Itinerary::find($id);
        
            if ($request->get('actionenable') == 'enable'){
                $action = '1';
            }else{
                $action = '0';
            }
            
            $itinerary-> public  = $action;
            
            $itinerary->save();
            
            
            
            return redirect('Admin/itineraries');
        
        }else{
            
            return abort(505, 'Unauthorized action.');
            
        }
    }
    
    
    /**
     * Update the enabled field of a itinerary.
     *
     * @param  Request  $request
     * @return Response
     */
    public function enable($id, Request $request)
    {
        
        //Only admin can see itineraries here
        if (Session::get('isAdmin')){
            $itinerary = Itinerary::find($id);
            
            if ($request->get('actionenable') == 'enable'){
                $action = '1';
            }else{
                $action = '0';
            }
            
            $itinerary-> enabled  = $action;
            
            $itinerary->save();
            
            
            
            return redirect('Admin/itineraries');
            
        }else{
            
            return abort(505, 'Unauthorized action.');
            
        }
    }
    
    /**
     * Get an existing itinerary .
     *
     * @param  Request  $request
     * @return Response
     */
     
     public function viewItinerary($id)
    {
        
        //Only admin can see itineraries here
        if (Session::get('isAdmin')){
            
            $itinerary=Itinerary::findOrFail($id);
            
            //If itinerary is not in english then is child itinerary
            //Must get parent to get the common attributes
            $itineraryparent = array();
            
            if ($itinerary->idLang <> 'en'){
                $itineraryparentid=Itinerary::where('idItinerary',$itinerary->idItinerary)
                ->where ('user_id',$itinerary->user_id)
                ->where ('idLang','en')
                ->first();
                
                $parentid = $itineraryparentid['id'];

                $itineraryparent=Itinerary::findOrFail($parentid);
                //Dates format must be changed from Y-m-d H:m:s to d. m. Y H:i for the ui
                
                $dateFrom = new DateTime($itineraryparent['dateFrom']);
                $dateFrom1 = $dateFrom->format('d. m. Y H:i');
                $dateTo = new DateTime($itineraryparent['dateTo']);
                $dateTo1 = $dateTo->format('d. m. Y H:i');
                $itineraryparent['dateFrom'] = $dateFrom1;
                $itineraryparent['dateTo'] = $dateTo1;
                
            }else{
                //Dates format must be changed from Y-m-d H:m:s to d. m. Y H:i for the ui
                $dateFrom = new DateTime($itinerary['dateFrom']);
                $dateFrom1 = $dateFrom->format('d. m. Y H:i');
                $dateTo = new DateTime($itinerary['dateTo']);
                $dateTo1 = $dateTo->format('d. m. Y H:i');
                $itinerary['dateFrom'] = $dateFrom1;
                $itinerary['dateTo'] = $dateTo1;
            }
            
            
            
            return view('admin.adminitineraryview',['itinerary' => $itinerary,'id' => $id,'itineraryparent' => $itineraryparent]);
            
        }else{
            
            return abort(505, 'Unauthorized action.');
            
        }
        
        
       
    }
    
    
    /**
     * Display a list of all of an itinerary's modules.
     *
     * @param  Request  $request
     * @return Response
     */
    public function viewModules($itineraryid)
    {
        
        //Only admin can see itineraries here
        if (Session::get('isAdmin')){
            
            $itinerary = Itinerary::findOrFail($itineraryid);
            
            
            $itineraryname = $itinerary['itineraryname'];
            
            if ($itinerary->idLang == 'en'){
                $itineraryFrom = $itinerary['dateFrom'];
                $itineraryTo = $itinerary['dateTo'];
            }else{
                $itinerary = $this->itinerarymodules->getParentItineraryId($itinerary['idItinerary'],$itinerary['user_id']);
                $itineraryFrom = $itinerary['dateFrom'];
                $itineraryTo = $itinerary['dateTo'];
            }
            
            $itineraryFrom = new DateTime($itineraryFrom);
            $itineraryFrom = $itineraryFrom->format('d. m. Y H:i');
            
            $itineraryTo = new DateTime($itineraryTo);
            $itineraryTo = $itineraryTo->format('d. m. Y H:i');
            
            $itinerarymodulesid = new ItineraryModule;
            $moduleParent = new Module;
            $categories = new Category;
            $moduleChild = array();
            
            $itinerarymodulesid = $this->itinerarymodules->getItineraryModules($itineraryid);
            $userlanguage = Session::get('locale');
            
            foreach ( $itinerarymodulesid as $value )
            {
                $idparentmodule = $value['idModule'];
                $moduleParent[$idparentmodule] = Module::findOrFail($idparentmodule);
                //Get all the categories for each module
                $categories[$idparentmodule] = $this->itinerarymodules->getModuleCategories($idparentmodule);
                
                if($userlanguage != 'en'){
                    $pluckedid = $moduleParent[$idparentmodule]['idModule'];
                    $pluckeduser = $moduleParent[$idparentmodule]['user_id'];
                    $moduleChild[$idparentmodule] = $this->itinerarymodules->getModule($pluckedid,$pluckeduser,$userlanguage);
                }
                
            }
            
            return view('admin.adminitinerarymodules', [
                'itinerarymodules' => $itinerarymodulesid, 'itineraryid' => $itineraryid, 'itineraryname' => $itineraryname, 'moduleParent' => $moduleParent,
                'categories' => $categories, 'moduleChild' => $moduleChild, 'userlanguage' => $userlanguage, 'itineraryFrom' => $itineraryFrom,
                'itineraryTo' => $itineraryTo, 'itinerarystatus' => $itinerary['status'],
            ]);
        }else{
            
            return abort(505, 'Unauthorized action.');
            
        }
    }
    
    
    
    
    
    /**
     * Destroy the given itinerary.
     *
     * @param  Request  $request
     * @param  Itinerary  $itinerary
     * @return Response
     */
    public function destroy(User $user, Itinerary $itinerary, $itineraryid)
    {
        
        $itinerary = Itinerary::findOrFail($itineraryid);
        
        //User can delete only his own itineraries
        $this->authorize('destroy', $itinerary);
        
        //Using soft deletes (deleted_at)
        Itinerary::destroy($itineraryid);
        
        return redirect('/itineraries');
    }
    
    /**
     * Get an existing itinerary to edit.
     *
     * @param  Request  $request
     * @return Response
     */
    
        
}
