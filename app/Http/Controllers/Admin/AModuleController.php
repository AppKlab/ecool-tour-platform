<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Module;
use App\Repositories\ModuleRepository;

use App\Policies\ModulePolicy;

use Lang;
use App;
use Session;
use DB;
use App\User;
use Image;


class AModuleController extends Controller
{
   
    /**
     * The module repository instance.
     *
     * @var ModuleRepository
     */
    protected $modules;
    protected $primaryKey = 'id';
    protected $table = 'modules';

    /**
     * Create a new controller instance.
     * 
     * Add the language to the session
     *
     * @param  ModuleRepository  $modules
     * @return void
     */
    public function __construct(ModuleRepository $modules)
    {
        $this->middleware('auth');
        
        $this->modules = $modules;
        
        if($apploc = App::getLocale()){ 

            Session::put('locale', $apploc);
            $session_language = Session::get('locale');
            
        }else{ 
            
            Session::put('locale', 'sl');
            $session_language = Session::get('locale');
        }
    }
    
    /**
     * Display a list of all of the modules.
     *
     * @param  Request  $request
     * @return Response
     */
    public function index(Request $request)
    {
       
        if (Session::get('isAdmin')){
            
            return view('admin.adminmodulesindex', [
                'modules' => $this->modules->forAdminFiltered(),
                'modulestranslated' => $this->modules->forAdmin(),
                'users' => $this->modules->forAdminUsers(),
                'languages' => $this->modules->getLanguages(),
            ]);
            
        }else{
            
            return abort(505, 'Unauthorized action.');
            
        }
        
        
    }
    
    /**
     * Display a list of all of the modules in a category.
     *
     * @param  Request  $request
     * @return Response
     */
    public function indexFiltered(Request $request, $catid)
    {
        
        if (Session::get('isAdmin')){
            
            $modules = $this->modules->forAdminFilteredByCategory($catid);
            
            return view('admin.adminmodulesindex', [
                'modules' => $modules,
                'modulestranslated' => $this->modules->forAdmin(),
                'users' => $this->modules->forAdminUsers(),
                'languages' => $this->modules->getLanguages(),
            ]);
            
        }else{
            
            return abort(505, 'Unauthorized action.');
            
        }
        
        
    }
    
    /**
     * When user wants to create a new module: 
     * Step1: call edit blade with id=0
     *
     * @param  Request  $request
     * @return Response
     */
     
    
    /**
     * Update the enable field of a module.
     *
     * @param  Request  $request
     * @return Response
     */
    public function enable($id, Request $request)
    {
        
        if (Session::get('isAdmin')){
            
            $module = Module::find($id);
            
            if ($request->get('actionenable') == 'enable'){
                $action = '1';
            }else{
                $action = '0';
            }
            
            $module-> enabled = $action;
            
            $module->save();
            
            
            
            return redirect('Admin/modules');
            
        }else{
            
            return abort(505, 'Unauthorized action.');
            
        }
        
    }
    
    /**
     * Update the mainimage of a module.
     *
     * @param  Request  $request
     * @return Response
     */
    public function changeImageForm($moduleid, Request $request)
    {
        
        if (Session::get('isAdmin')){
            
            $module = Module::find($moduleid);
            
            return view('Admin/adminmoduleimg', [
                'module' => $module,
                ]);
            
        }else{
            
            return abort(505, 'Unauthorized action.');
            
        }
        
    }
    
    public function changeImage(Request $request)
    {
        
        if (Session::get('isAdmin')){
            
            $module = Module::find($request['moduleid']);
            
            /*FIRST TRY TO UPLOAD FILE*/
            $file = $request['image'];
            
            if ($request->hasFile('image')) {
                //Move Uploaded File
                $destinationPath = '/storage/app/public/modules';
                $path = public_path();
                $path1 = base_path().'/public/upload_media/modules';
                $timeforname = time();
                $filename = 'module' . '-' . time() . '-' . $file->getClientOriginalName();
                $img = Image::make($request->file('image')->getRealPath());
                // read width of image to see if needs to be resized
                $width = $img->width();
                
                if ($width > 1000){
                    // resize the image to a width of 1000 and constrain aspect ratio (auto height)
                    $img->resize(1000, null, function ($constraint) {
                        $constraint->aspectRatio();
                    });
                }
                $img->save(base_path('/public/upload_media/modules/').$filename);
                // resize the image to a width of 100 and constrain aspect ratio (auto height)
                // for THUMBS
                $img->resize(100, null, function ($constraint) {
                    $constraint->aspectRatio();
                });
                $img->save(base_path('/public/upload_media/modules_thumbs/').$filename);
                
                $module -> mainimage = $filename;
                $module->save();
                    
            }else{
                //echo '</br>No file';

                return redirect()->back()->with('message', trans('auth.filenotfound'));
            }
            
            return view('admin.adminmodulesindex', [
                'modules' => $this->modules->forAdminFiltered(),
                'modulestranslated' => $this->modules->forAdmin(),
                'users' => $this->modules->forAdminUsers(),
                'languages' => $this->modules->getLanguages(),
            ]);
            
        }else{
            
            return abort(505, 'Unauthorized action.');
            
        }
        
    }
    
    /**
     * Destroy the given module.
     *
     * @param  Request  $request
     * @param  Module  $module
     * @return Response
     */
    public function destroy(User $user, Module $module, $moduleid)
    {
        
        if (Session::get('isAdmin')){
            
            //Using soft deletes (deleted_at)
            Module::destroy($moduleid);
            
            return redirect('Admin/modules');
            
        }else{
            
            return abort(505, 'Unauthorized action.');
            
        }
        
        
    }
    
}
