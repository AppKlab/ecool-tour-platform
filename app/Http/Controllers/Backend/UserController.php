<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Repositories\UserRepository;

use App\Policies\UserPolicy;

use Lang;
use App;
use Session;
use DB;
use App\User;
use Auth;
use Image;

class UserController extends Controller
{
    /**
     * The module repository instance.
     *
     * @var ModuleRepository
     */
    protected $users;
    protected $primaryKey = 'id';
    protected $table = 'users';

    /**
     * Create a new controller instance.
     * 
     * Add the language to the session 
     *
     * @param  ModuleRepository  $modules
     * @return void
     */
    public function __construct(UserRepository $user)
    {
        $this->middleware('auth');
        
    }
    
    /**
     * Show the profile for the given user.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function show(Request $request, $id)
    {

        //User can only view his own profile
        $user = User::findOrFail($id);
        $this->authorize('view', $user);
        
        return view('users.profile', ['user' => $user]);
        
    }
    
    public function edit($id)
    {
        
        //User can only edit his own profile
        $user = User::findOrFail($id);
        $this->authorize('update', $user);

        return view('users.editprofile',compact('user','id'));
        
        
    }
    
    public function update(Request $request, $id)
    {
        
        //User can only update his own profile
        $user = User::findOrFail($id);
        $this->authorize('update', $user);
       
        if ($user->userType < 4){
            
                $validatedData = $request->validate([
                'title'     => 'required|max:255',
                'address'   => 'required|max:255',
                'logo'      => 'image',
                'telephone' => 'required|max:15',
                'cellphone' => 'required|max:15',
                'www'       => 'required|url',
            ]);
        }else{
            $validatedData = $request->validate([
                'cellphone' => 'required|max:15',
            ]);
        }
        try {
            
            //If lang must also be changed for user
            /*if($idLang = Session::get ('locale')){
                $validatedData['idLang']        = $idLang;
            }else{
                $idLang = App::getLocale();
                $validatedData['idLang']        = $idLang;
            }*/
            
            /*FIRST TRY TO UPLOAD FILE*/
            $file = $request['logo'];
            
            
            if ($request->hasFile('logo')) {
                //Move Uploaded File
                $destinationPath = '/storage/app/public/logos';
                $path = public_path();
                $path1 = base_path().'/public/upload_media/logos';
                $timeforname = time();
                $filename = 'logo' . '-' . time() . '-' . $file->getClientOriginalName();
                $img = Image::make($request->file('logo')->getRealPath());
                // read width of image to see if needs to be resized
                $width = $img->width();
                
                if ($width > 500){
                    // resize the image to a width of 500 and constrain aspect ratio (auto height)
                    $img->resize(500, null, function ($constraint) {
                        $constraint->aspectRatio();
                    });
                }
                $img->save(base_path('/public/upload_media/logos/').$filename);
                // resize the image to a width of 100 and constrain aspect ratio (auto height)
                // for THUMBS
                $img->resize(100, null, function ($constraint) {
                    $constraint->aspectRatio();
                });
                $img->save(base_path('/public/upload_media/logos_thumbs/').$filename);
                    
                $validatedData['logo']  = $filename;
                
            }else{
                //echo '</br>No file';
                $validatedData['logo']  = 'No file';

            }
            
            /*WHEN FILE IS UPLOADED THEN UPDATE USER*/
            $user-> title        = $request->get('title');
            $user-> address      = $request->get('address');
            $user-> telephone    = $request->get('telephone');
            $user-> cellphone    = $request->get('cellphone');
            $user-> www          = $request->get('www');
            if ($validatedData['logo'] <> 'No file')
                $user-> logo  = $validatedData['logo'];
            
            $user->save();

            
        } catch (\Exception $exception) {
            logger()->error($exception);
            return redirect()->back()->with('message', trans('forms.nouseredited'));
        }
        
        
        
        return redirect()->back()->with('message', trans('forms.successedit'));
        
    }
}