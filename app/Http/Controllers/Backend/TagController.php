<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Module;
use App\Tag;
use App\Repositories\TagRepository;

use App\Policies\TagPolicy;

use Lang;
use App;
use Session;
use DB;
use App\User;

use Illuminate\Support\Facades\Validator;


class TagController extends Controller
{
    
    /**
     * The tag repository instance.
     *
     * @var TagRepository
     */
    protected $tags;
    protected $primaryKey = 'id';
    protected $table = 'tags';
    
    /**
     * Create a new controller instance.
     *
     *
     * @param  TagRepository  $tags
     * @return void
     */
    public function __construct(TagRepository $tags)
    {
        $this->tags = $tags;
        
    }
    
}
