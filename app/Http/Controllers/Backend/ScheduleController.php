<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Module;
use App\Schedule;
use App\Repositories\ScheduleRepository;

use App\Policies\SchedulePolicy;

use Lang;
use App;
use Session;
use DB;
use App\User;
use DateTime;

use Illuminate\Support\Facades\Validator;


class ScheduleController extends Controller
{
    
    /**
     * The schedule repository instance.
     *
     * @var ScheduleRepository
     */
    protected $schedules;
    protected $primaryKey = 'id';
    protected $table = 'schedules';
    
    /**
     * Create a new controller instance.
     *
     *
     * @param  ScheduleRepository  $schedules
     * @return void
     */
    public function __construct(ScheduleRepository $schedules)
    {
        $this->middleware('auth');
        
        $this->schedules = $schedules;
        
    }
    
    /**
     * Display a list of all of the module's schedules.
     *
     * @param  Request  $request
     * @return Response
     */
    public function index($moduleid)
    {
        
        $module = Module::find($moduleid);
        //User can only see schedules of his own modules
        $this->authorize('view', $module);
        
        $modulename = $module['modulename'];
        
        return view('schedules.index', [
            'schedules' => $this->schedules->getSchedules($moduleid), 'moduleid' => $moduleid, 'modulename' => $modulename,
        ]);
    }
    
    
    /**
     * Destroy the given module.
     *
     * @param  Request  $request
     * @param  Module  $module
     * @return Response
     */
    public function destroy(User $user, schedule $image, $scheduleid)
    {
        //User can only delete schedules of his own modules
        $schedule = Schedule::findOrFail($scheduleid);
        $module = Module::findOrFail($schedule['idModule']);
        $this->authorize('destroy', $module);
        
        //Using soft deletes (deleted_at)
        Schedule::destroy($scheduleid);
        
        return redirect('/schedules/'.$module->id);
    }
    
    /**
     * Destroy the given module.
     *
     * @param  Request  $request
     * @param  Module  $module
     * @return Response
     */
    public function create(User $user, $moduleid)
    {
        
        $module = Module::find($moduleid);
        //User can only add schedules to his own modules
        $this->authorize('create', $module);
        
        $modulename = $module['modulename'];
        
        return view('schedules.edit', ['moduleid' => $moduleid, 'modulename' => $modulename]);
        
        
    }
    
    public function store(User $user, Request $request)
    {
        
        //call funtion to change dates into values for the query
        $this->schedules->validateDates($request);
        $dateFrom = $request->day1 .' '. $request->dayhour1.':00';
        $dateTo = $request->day2 .' '. $request->dayhour2 .':00';
        
        $module = Module::find($request['moduleid']);
        //User can only add schedules to his own modules
        $this->authorize('create', $module);
        
        $schedule = new Schedule;
        $schedule -> idModule       = $request['moduleid'];
        $schedule -> dateFrom       = $dateFrom;
        $schedule -> dateTo         = $dateTo;
        $schedule -> timestampFrom  = $request['time1'];
        $schedule -> timestampTo    = $request['time2'];
        
        $schedule->save();

        $modulename = $module['modulename'];
        
        return view('schedules.index', [
            'schedules' => $this->schedules->getSchedules($request['moduleid']), 'moduleid' => $request['moduleid'], 'modulename' => $modulename,
        ]);
        
        
        
    }
    
}
