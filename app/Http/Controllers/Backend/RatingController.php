<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Validator;
use DateTime;

use App\Rating;
use App\Repositories\RatingRepository;

use Lang;
use App;
use Session;
use DB;
use Auth;

use App\User;
use App\Module;
use App\Itinerary;
use App\ItineraryModule;

use Mail;
use App\Mail\SendRating;
use App\Jobs\SendRatingEmail;

class RatingController extends Controller
{
   
    /**
     * The rating repository instance.
     *
     * @var RatingRepository
     */
    protected $ratings;
    protected $primaryKey = 'id';
    protected $table = 'ratings';

    /**
     * Create a new controller instance.
     * 
     * @param  RatingRepository  $ratings
     * @return void
     */
    public function __construct(RatingRepository $ratings)
    {
        $this->ratings = $ratings;
        
    }
    
    protected function validateRating(Request $request)
    {
        return $this->validate($request, [
            'question1'   => 'required',
            'question2'   => 'required',
            'question3'   => 'required',
        ]);

    }
    
    
    /**
     * When user wants to create a new rating: 
     * Step1: call rating blade (for provider or agency)
     *
     * @param  Request  $request
     * @return Response
     */
    public function showRatingFormProvider($token, Request $request)
    {
        //Must first check if the evaluation was already made
        $rating = new Rating;
        
        $rating = $this->ratings->getRating($token);
          
        if (isset($rating->id) AND $rating->id > 0){
            return view('ratings.rating_provider',[
                'ratingexists' => 'exists',
            ]);
        }else{
            
            //Token is itimodule token_messages
            $itimodule = ItineraryModule::where('token_messages',$token)
            ->first();
            
            //Parent module (module in english)
            $module = Module::findOrFail($itimodule['idModule']);
            
            $itinerary = Itinerary::findOrFail($itimodule['idItinerary']);
            
            $agency = User::findOrFail($itinerary['user_id']);
            
            $provider = User::findOrFail($module['user_id']);
            
            $userlanguage = $provider['idLang'];
            Session::put('locale', $userlanguage);
            
            if ($userlanguage != 'en'){
                $module = $this->ratings->getModule($module['idModule'], $module['user_id'], $userlanguage); 
            }
            
            //change date format for itimodule
            $dateFrom = new DateTime($itimodule['dateFrom']);
            $dateFrom1 = $dateFrom->format('d. m. Y H:i');
            $dateTo = new DateTime($itimodule['dateTo']);
            $dateTo1 = $dateTo->format('d. m. Y H:i');
            $itimodule['dateFrom'] = $dateFrom1;
            $itimodule['dateTo'] = $dateTo1;
            
            
            return view('ratings.rating_provider',[
                'itinerary' => $itinerary,
                'agency' => $agency,
                'itimodule' => $itimodule,
                'module' => $module,
                'provider' => $provider,
                'token' => $token,
            ]);
        }
    }
    
    public function showRatingFormAgency($token, Request $request)
    {
        //Must first check if the evaluation was already made
        $rating = new Rating;
        
        $rating = $this->ratings->getRating($token);
        
        if (isset($rating->id) AND $rating->id > 0){
            return view('ratings.rating_agency',[
                'ratingexists' => 'exists',
            ]);
        }else{
            
            $modules = array();
            $moduleslang = array();
            $providers = array();
            
            //Token is itinerary token
            $itinerary = $this->ratings->getItinerary($token);
            
            $agency = User::findOrFail($itinerary['user_id']);
            
            //get itimodules for this itinerary
            $itimodules = $this->ratings->getItiModules($itinerary['id']);
            
            $userlanguage = $agency['idLang'];
            Session::put('locale', $userlanguage);
            
            $itinerarylang = $this->ratings->getItineraryLang($itinerary->idItinerary,$itinerary->user_id,$userlanguage);
            
            //get module info & provider for each itimodule
            foreach ($itimodules as $itimodule){
                //Parent module (module in english)
                $modules[$itimodule['id']] = Module::findOrFail($itimodule['idModule']);
                $moduleslang[$itimodule['id']] = $this->ratings->getModuleLang($modules[$itimodule['id']]->idModule,$modules[$itimodule['id']]->user_id,$userlanguage);
                $providers[$itimodule['id']] = User::findOrFail($modules[$itimodule['id']]->user_id);
                
                //change date format for modules
                $dateFrom = new DateTime($itimodule['dateFrom']);
                $dateFrom1 = $dateFrom->format('d. m. Y H:i');
                $dateTo = new DateTime($itimodule['dateTo']);
                $dateTo1 = $dateTo->format('d. m. Y H:i');
                $dateFrom2[$itimodule['id']] = $dateFrom1;
                $dateTo2[$itimodule['id']] = $dateTo1;
                
            }
            
            return view('ratings.rating_agency',[
                'itinerary' => $itinerary,
                'itinerarylang' => $itinerarylang,
                'agency' => $agency,
                'itimodules' => $itimodules,
                'modules' => $moduleslang,
                'providers' => $providers,
                'token' => $token,
                'userlanguage' => $userlanguage,
                'dateFrom' => $dateFrom2,
                'dateTo' => $dateTo2,
            ]);
        }
        
        
    }
    
    /**
     * Step2: Create a new rating after form is sent.
     *
     * @param  Request  $request
     * @return Response
     */
    public function RatingProvider($token, Request $request)
    {
        
        $this->validateRating($request);
        
        $rating = new Rating;
        
        $rating->idItineraryModule  = $request->itimoduleId;
        $rating->idItinerary  = $request->itineraryId;
        $rating->fromUser   = $request->providerId;
        $rating->toUser     = $request->agencyId;
        $rating->answer1    = $request->question1;
        $rating->answer2    = $request->question2;
        $rating->answer3    = $request->question3;
        //Token is itimodule token_messages
        $rating->token        = $request->token;
        $rating->save();
        
        //The itimodule's state must be changed to 7 (w/rating)
        $itimodule = ItineraryModule::findOrFail($request->itimoduleId);
        $itimodule-> status  = '7';
        $itimodule->save();
        
        //The agency gets a mail with this data
        $agency = User::findOrFail($request->agencyId);
        $provider = User::findOrFail($request->providerId);
        $itimodule = ItineraryModule::findOrFail($request->itimoduleId);
        $module = Module::findOrFail($itimodule->idModule);
        
        $userlanguage = $agency->idLang;
        $moduleLang = $this->ratings->getModuleLang($module->idModule,$module->user_id,$userlanguage);
        
        //change date format 
        $dateFrom = new DateTime($itimodule['dateFrom']);
        $dateFrom1 = $dateFrom->format('d. m. Y H:i');
        $dateTo = new DateTime($itimodule['dateTo']);
        $dateTo1 = $dateTo->format('d. m. Y H:i');
        
        $daterange = $dateFrom1 . ' - ' . $dateTo1;
        
        $avg = ($request->question1 + $request->question2 + $request->question3)/3;
        
        Mail::to($agency->email)
        ->send(new SendRating('provider',$provider['title'], $moduleLang['modulename'],$moduleLang['description'],$daterange,$token,$request->question1,$request->question2,$request->question3,$avg));
        
        return view('ratings.rating_provider',[
            'rating' => 'ok',
        ]);
        
        
    }
    
    public function RatingAgency($token, Request $request)
    {

        $modules = array();
        $moduleslang = array();
        $providers = array();
        
        //Token is itinerary token
        $itinerary = $this->ratings->getItinerary($token);
        
        //get itimodules for this itinerary
        $itimodules = $this->ratings->getItiModules($itinerary['id']);
        
        //validation is made within the form with 'required' attribute
        
        //get module info & provider for each itimodule
        foreach ($itimodules as $itimodule){
            //Parent module (module in english)
            $module = Module::select('user_id')->where('id',$itimodule['idModule'])->first();
            $provider = User::select('id')->where('id',$module['user_id'])->first();
            
            $rating = new Rating;
            
            $rating->idItineraryModule  = $itimodule->id;
            $rating->idItinerary        = $request->itineraryId;
            $rating->fromUser           = $request->agencyId;
            $rating->toUser             = $provider->id;
            $rating->answer1            = $request->question1[$itimodule['id']];
            $rating->answer2            = $request->question2[$itimodule['id']];
            $rating->answer3            = $request->question3[$itimodule['id']];
            //Token is itinerary token
            $rating->token              = $request->token;
            $rating->save();
            
            //The provider gets a mail with this data
            $agency = User::findOrFail($request->agencyId);
            $provider = User::findOrFail($provider->id);
            $itimodule = ItineraryModule::findOrFail($itimodule->id);
            $module = Module::findOrFail($itimodule->idModule);
            
            $userlanguage = $provider->idLang;
            $moduleLang = $this->ratings->getModuleLang($module->idModule,$module->user_id,$userlanguage);
            
            //change date format
            $dateFrom = new DateTime($itimodule['dateFrom']);
            $dateFrom1 = $dateFrom->format('d. m. Y H:i');
            $dateTo = new DateTime($itimodule['dateTo']);
            $dateTo1 = $dateTo->format('d. m. Y H:i');
            
            $daterange = $dateFrom1 . ' - ' . $dateTo1;
            
            $avg = ($request->question1[$itimodule['id']] + $request->question2[$itimodule['id']] + $request->question3[$itimodule['id']])/3;
            
            Mail::to($provider->email)
            ->send(new SendRating('agency',$agency['title'], $moduleLang['modulename'],$moduleLang['description'],
                $daterange,$token,$request->question1[$itimodule['id']],$request->question2[$itimodule['id']],$request->question3[$itimodule['id']],$avg));
            
        }
        
        //The itinerary's state must be changed to 7 (w/rating)
        $itinerary = Itinerary::findOrFail($request->itineraryId);
        $itinerary-> status  = '7';
        $itinerary->save();
        
        return view('ratings.rating_provider',[
            'rating' => 'ok',
        ]);
        
        
    }
    
    
}
