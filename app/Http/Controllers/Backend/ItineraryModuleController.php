<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Module;
use App\Itinerary;
use App\ItineraryModule;
use App\Category;
use App\Tag;
use App\Gallery;
use App\Repositories\ItineraryModuleRepository;

use App\Policies\ItineraryModulePolicy;

use Lang;
use App;
use Session;
use DB;
use App\User;
use DateTime;
use Auth;

use Illuminate\Support\Facades\Validator;

use Mail;
use App\Mail\ModuleProvider;
use App\Jobs\ModuleProviderEmail;

class ItineraryModuleController extends Controller
{
    
    /**
     * The itinerarymodule repository instance.
     *
     * @var ItineraryModuleRepository
     */
    protected $itinerarymodules;
    protected $primaryKey = 'id';
    protected $table = 'itinerarymodules';
    
    /**
     * Create a new controller instance.
     *
     *
     * @param  ItineraryModuleRepository  $itinerarymodules
     * @return void
     */
    public function __construct(ItineraryModuleRepository $itinerarymodules)
    {
        
        $this->itinerarymodules = $itinerarymodules;
        
    }
    
    /**
     * Display a list of all of the itinerary's modules.
     *
     * @param  Request  $request
     * @return Response
     */
    public function index($itineraryid)
    {
        
        $itinerary = Itinerary::findOrFail($itineraryid);
        
        //User can only see modules of his own itineraries
        $this->authorize('view', $itinerary);
        
        $itineraryname = $itinerary['itineraryname'];
        
        if ($itinerary->idLang == 'en'){
            $itineraryFrom = $itinerary['dateFrom'];
            $itineraryTo = $itinerary['dateTo'];
        }else{
            $itinerary = $this->itinerarymodules->getParentItineraryId($itinerary['idItinerary'],$itinerary['user_id']);
            $itineraryFrom = $itinerary['dateFrom'];
            $itineraryTo = $itinerary['dateTo'];
        }
        
        $itineraryFrom = new DateTime($itineraryFrom);
        $itineraryFrom = $itineraryFrom->format('d. m. Y H:i');

        $itineraryTo = new DateTime($itineraryTo);
        $itineraryTo = $itineraryTo->format('d. m. Y H:i');
        
        $itinerarymodulesid = new ItineraryModule;
        $moduleParent = new Module;
        $categories = new Category;
        $moduleChild = array();
        
        $itinerarymodulesid = $this->itinerarymodules->getItineraryModules($itineraryid);
        $userlanguage = Session::get('locale');

        foreach ( $itinerarymodulesid as $value )
        {
            $idparentmodule = $value['idModule'];
            $moduleParent[$idparentmodule] = Module::findOrFail($idparentmodule); 
            //Get all the categories for each module
            $categories[$idparentmodule] = $this->itinerarymodules->getModuleCategories($idparentmodule);
            
            if($userlanguage != 'en'){
                $pluckedid = $moduleParent[$idparentmodule]['idModule'];
                $pluckeduser = $moduleParent[$idparentmodule]['user_id'];
                $moduleChild[$idparentmodule] = $this->itinerarymodules->getModule($pluckedid,$pluckeduser,$userlanguage);

            }
          
        }
        
        return view('itinerarymodules.index', [
            'itinerarymodules' => $itinerarymodulesid, 'itineraryid' => $itineraryid, 'itineraryname' => $itineraryname, 'moduleParent' => $moduleParent,
            'categories' => $categories, 'moduleChild' => $moduleChild, 'userlanguage' => $userlanguage, 'itineraryFrom' => $itineraryFrom,
            'itineraryTo' => $itineraryTo, 'itinerarystatus' => $itinerary['status'],
        ]);
    }
    
    
    /**
     * Destroy the given itinerary module.
     *
     * @param  Request  $request
     * @param  ItineraryModule  $itinerarymodule
     * @return Response
     */
    public function destroy(User $user, ItineraryModule $itinerarymodule, $moduleid)
    {
        //$moduleid is not the id of the module to delete (modules table)
        //but the id of the itinerarymodules table
        //so, get the idItinerary from it and then find the itinerary
        //to make the authorization
        $itinerarymodule = ItineraryModule::findOrFail($moduleid);
        
        $itinerary = Itinerary::findOrFail($itinerarymodule->idItinerary);
        
        //User can only delete modules from his own itineraries
        $this->authorize('destroy', $itinerary);
        
        //Using soft deletes (deleted_at)
        ItineraryModule::destroy($moduleid);
        
        //Must change itinerary status if there are no more rejected modules (to 1, 3 or 4)
        $itinerarymodules = array();
        $itinerarymodules = ItineraryModule::where('idItinerary', $itinerary->id)->where('status','3')->get();
        //More rejections (itinerary status)
        if (count($itinerarymodules) > 0){
            $itinerary->status = '2';
        }else {
            //check if there are itimodules with status 1 = waiting for response
            $itinerarymodulesall = array();
            $itinerarymodulesall = ItineraryModule::where('idItinerary', $itinerary->id)->get();
            $allmodules = count($itinerarymodulesall);
            
            $itinerarymodules = array();
            $itinerarymodules = ItineraryModule::where('idItinerary', $itinerary->id)->where('status','4')->get();
            $confirmedmodules = count($itinerarymodules);
            if ($confirmedmodules == 0){
                $itinerary->status = '1';
            }elseif ($confirmedmodules == $allmodules){
                $itinerary->status = '4';
            }
            else {
                $itinerary->status = '3';
            }
            
        }
        
        $itinerary->save();
        
        return redirect('/modulesiti/'.$itinerarymodule->idItinerary);
    }
    
    /**
     * Destroy the given module.
     *
     * @param  Request  $request
     * @param  Module  $module
     * @return Response
     */
    public function create(User $user, $itineraryid)
    {
        
        $itinerary = Itinerary::findOrFail($itineraryid);
        
        //User can only add modules to his own itineraries
        $this->authorize('create', $itinerary);
        
        $itineraryname = $itinerary['itineraryname'];
        
        $userlanguage = Session::get('locale');
        
        if (!$userlanguage)
            $userlanguage = 'en';
            
            
        //Get modules from the modules table (Random order)
        $modules = $this->itinerarymodules->getModules();
        
        //For each record check if the content is in all four languages
        $mainimages = array();
        $categories = array();

        foreach ( $modules as $value )
        {
            $realidmodule = $value['id'];
            
            $pluckedid = $value['idModule'];
            $pluckeduser = $value['user_id'];
            $languagesinmodule = $this->itinerarymodules->getLangInModules($pluckedid,$pluckeduser);
            $numberoflanguages = count($languagesinmodule);
            
            //Display only modules that have content in all languages
            if($numberoflanguages == 4){
                //If language is not 'en' then use the parentmodule mainimage
                
                if($userlanguage <> 'en'){
                    $mainimages[] = $this->itinerarymodules->getMainimage($pluckedid,$pluckeduser);
                }
                
                $allmodules[] = $this->itinerarymodules->getModule($pluckedid,$pluckeduser,$userlanguage);
                
                //Get all the categories for each module
                $categories[$realidmodule] = $this->itinerarymodules->getModuleCategories($realidmodule);
                
                    
            }
        }
        
        $itinerarylang = $itinerary['idLang'];
        $parentitineraryid = '0';
        if($itinerarylang != 'en'){
            $parentitineraryid = $this->itinerarymodules->getParentItineraryId($itinerary['idItinerary'],$itinerary['user_id']);
        }
        
        return view('itinerarymodules.edit', [
            'modules' => $allmodules, 'mainimages' => $mainimages, 'userlanguage' => $userlanguage, 'categories' => $categories,
            'itineraryname' => $itineraryname, 'itineraryid' => $itineraryid, 'itiparentid' =>  $parentitineraryid,
        ]);
    }
    
  
    public function store(User $user, Request $request)
    {
        

        //User can only add modules to his own itineraries
        $itineraryCheck = Itinerary::findOrFail($request['itineraryid']);
        $this->authorize('create', $itineraryCheck);
        
        
        $itineraryid = $request['itineraryid'];
        $moduleid = $request['moduleid'];
        $parentid = $request['parentid'];
        
        $itinerarymodule = new ItineraryModule;
        
        if ($request['itiparentid'] == '0'){
            $itinerarymodule->idItinerary   = $request['itineraryid'];
        }else{
            $itinerarymodule->idItinerary   = $request['itiparentid'];
        }
        
        if ($request['moduleid'] == $request['parentid']){
            $itinerarymodule->idModule = $request['moduleid'];
        }else{
            $itinerarymodule->idModule = $request['parentid'];
        }
        
        if ($itineraryCheck->idLang == 'en'){
            $itineraryFrom = $itineraryCheck['dateFrom'];
            $itineraryTo = $itineraryCheck['dateTo'];
        }else{
            $itineraryCheck = $this->itinerarymodules->getParentItineraryId($itineraryCheck['idItinerary'],$itineraryCheck['user_id']);
            $itineraryFrom = $itineraryCheck['dateFrom'];
            $itineraryTo = $itineraryCheck['dateTo'];
        }
        $itineraryFrom = new DateTime($itineraryFrom);
        $itineraryFrom = $itineraryFrom->format('d. m. Y H:i');
        
        $itineraryTo = new DateTime($itineraryTo);
        $itineraryTo = $itineraryTo->format('d. m. Y H:i');
        
        $itinerarymodule->token_messages = str_random(30).time();
        
        $itinerarymodule->save();
        
        $itinerary = Itinerary::findOrFail($itineraryid);
        $itineraryname = $itinerary['itineraryname'];
        
        $itinerarymodulesid = new ItineraryModule;
        $moduleParent = new Module;
        $categories = new Category;
        $moduleChild = array();
        
        $itinerarymodulesid = $this->itinerarymodules->getItineraryModules($itineraryid);
        $userlanguage = Session::get('locale');
        
        foreach ( $itinerarymodulesid as $value )
        {
            $idparentmodule = $value['idModule'];
            $moduleParent[$idparentmodule] = Module::findOrFail($idparentmodule);
            //Get all the categories for each module
            $categories[$idparentmodule] = $this->itinerarymodules->getModuleCategories($idparentmodule);
            
            if($userlanguage != 'en'){
                $pluckedid = $moduleParent[$idparentmodule]['idModule'];
                $pluckeduser = $moduleParent[$idparentmodule]['user_id'];
                $moduleChild[$idparentmodule] = $this->itinerarymodules->getModule($pluckedid,$pluckeduser,$userlanguage);
                
            }
            
        }
        
        return view('itinerarymodules.index', [
            'itinerarymodules' => $itinerarymodulesid, 'itineraryid' => $itineraryid, 'itineraryname' => $itineraryname, 'moduleParent' => $moduleParent,
            'categories' => $categories, 'moduleChild' => $moduleChild, 'userlanguage' => $userlanguage, 'itineraryFrom' => $itineraryFrom,
            'itineraryTo' => $itineraryTo, 'itinerarystatus' => $itineraryCheck['status'],
        ]);
        
        
    }
    
    public function storedates(Request $request, $itineraryid)
    {
        
        
        //User can only change dates to his own itineraries
        $itineraryCheck = Itinerary::findOrFail($itineraryid);
        $this->authorize('update', $itineraryCheck);
        
        
        //get user (agency)
        $agency = Auth::user();

        //Get the daterange pickers for the modules in the itinerary
        //Must save the dates (those where status = 1 or = 3)

        foreach ( $request->except('_token','_method','button') as $key => $value)
        {
            
            //get the id of the itimodule daterange-(id)
            $itimoduleid = explode('-', $key);
            //get the id of the itimodule daterange-(id)
            $itinerarymodule = ItineraryModule::findOrFail($itimoduleid['1']);
            $token = $itinerarymodule['token_messages'];

            //update datarange format
            $this->itinerarymodules->validateDates($value, $request);
            
            $itinerarymodule-> dateFrom         = $request->get('day1');
            $itinerarymodule-> dateTo           = $request->get('day2');
            $itinerarymodule-> timestampFrom    = $request->get('time1');
            $itinerarymodule-> timestampTo      = $request->get('time2');
            
            $itinerarymodule->save();
            
            
            //If the user has confirmed the itinerary then the providers must
            //be informed through mail if status of this itimodule is not 2 (already sent) or 4 (already confirmed)
            if ($request['button'] == 'Itinerary' AND $itinerarymodule['status'] != 2 AND $itinerarymodule['status'] != 4){
                //SEND MAIL TO PROVIDER
                //get user (provider)
                $moduleUser = $this->itinerarymodules->getModuleOwner($itinerarymodule->idModule);
                
                //get module in the user (provider) lang for mail
                $itinerarymoduleLang = $this->itinerarymodules->getModuleForMail($itinerarymodule->idModule, $moduleUser->idLang);
                
                //The language for the mail is the provider's language
                if(Session::get ('locale') <> $moduleUser->idLang){
                    Session::put('locale', $moduleUser->idLang);
                    App::setLocale($moduleUser->idLang);
                }
                
                Mail::to($moduleUser->email)
                ->send(new ModuleProvider($agency, $itinerarymoduleLang['modulename'],$itinerarymoduleLang['description'],$request[$key],$token));
                
                //change status module in itinerary to 2 (sent mail - waiting for response)
                $itinerarymodule->status   = '2';
                $itinerarymodule->save();
                
                //change status itinerary for this itinerary to 1 (sent mail - waiting for response) or 2 (there has been a rejection) or 3 (some have confirmed)
                $itinerary= Itinerary::findOrFail($itineraryid);
                //if some modules for this itinerary are still status 3 (not confirmed) then the itinerary is still status 2
                
                if (count($this->itinerarymodules->getItiModuleFromStatus('3',$itineraryid)) > 0 ){
                    $itinerary->status   = '2';
                }
                //if some modules for this itinerary are status 4 (confirmed) then the itinerary is status 3
                elseif (count($this->itinerarymodules->getItiModuleFromStatus('4',$itineraryid)) > 0){
                    $itinerary->status   = '3';
                }
                //if all modules for this itinerary are status 2 (sent mail - waiting for response) then the itinerary is status 1
                else {
                    $itinerary->status   = '1';
                }
                
                $itinerary->save();
            }
            
        }
        
        if(Session::get ('locale') <> $agency->idLang){
            Session::put('locale', $agency->idLang);
            App::setLocale($agency->idLang);
        }
        
        if ($request['button'] == 'Draft'){
            return redirect('/modulesiti/'.$itineraryid);
        }else{
            return redirect('/itineraries');
        }
        
    }
    
    
}
