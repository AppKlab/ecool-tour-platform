<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Auth;
use App\User;
use Session;
use App;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        $this->middleware('auth');
        
   }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        /*Set the lang for the user
         * idLang in DB
         * */
        $idLang = Auth::user()->idLang;
        $userType = Auth::user()->userType;
        $userId = Auth::user()->id;
        
        Session::put('locale',$idLang);
        
        /*Set the variable IsAdmin for the user
         * idLang in DB
         * */
        
        Session::forget('isAdmin');
        $IsAdmin = 0;
        $userType = Auth::user()->userType;
        
        if ($userType == 1){
            $IsAdmin = 1;
            Session::put('isAdmin',$IsAdmin);
        }
        
        //Login date is recorded because of GDPR (groups must be deleted after a year)
        $user = User::findOrFail($userId);
        $user->lastLogin = date('Y-m-d H:i:s');
        $user->save();
        
        return view('authhome',compact('idLang','userType'));
    }
    
 
}
