<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Module;
use App\Category;
use App\Repositories\CategoryRepository;

use App\Policies\CategoryPolicy;

use Lang;
use App;
use Session;
use DB;
use App\User;

use Illuminate\Support\Facades\Validator;


class CategoryController extends Controller
{
    
    /**
     * The category repository instance.
     *
     * @var CategoryRepository
     */
    protected $categories;
    protected $primaryKey = 'id';
    protected $table = 'categories';
    
    /**
     * Create a new controller instance.
     *
     *
     * @param  CategoryRepository  $categories
     * @return void
     */
    public function __construct(CategoryRepository $categories)
    {
        $this->categories = $categories;
        
    }
    
}
