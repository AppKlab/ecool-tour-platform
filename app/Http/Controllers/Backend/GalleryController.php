<?php

namespace App\Http\Controllers\Backend;

use App\Gallery;
use App\Module;
use App\User;
use App\Http\Controllers\Controller;
use App\Repositories\GalleryRepository;
use Illuminate\Http\Request;


// import the Intervention Image Manager Class
use Image;

class GalleryController extends Controller
{
    
    /**
     * The gallery repository instance.
     *
     * @var GalleryRepository
     */
    protected $galleries;
    protected $primaryKey = 'id';
    protected $table = 'galleries';
    
    /**
     * Create a new controller instance.
     *
     *
     * @param  GalleryRepository  $galleries
     * @return void
     */
    public function __construct(GalleryRepository $galleries)
    {
        $this->middleware('auth');
        
        $this->galleries = $galleries;
        
    }
    
    /**
     * Display a list of all of the module's images.
     *
     * @param  Request  $request
     * @return Response
     */
    public function index($moduleid)
    {
        
        $module = Module::find($moduleid);
        //User can only see images of his own modules
        $this->authorize('view', $module);
        
        $modulename = $module['modulename'];
        
        
        return view('images.index', [
            'images' => $this->galleries->getImages($moduleid), 'moduleid' => $moduleid, 'modulename' => $modulename,
        ]);
    }
    
    
    /**
     * Destroy the given module.
     *
     * @param  Request  $request
     * @param  Module  $module
     * @return Response
     */
    public function destroy(User $user, Gallery $gallery, $imageid)
    {
        //User can only delete images of his own modules
        $image = Gallery::findOrFail($imageid);
        $module = Module::findOrFail($image['idModule']);
        $this->authorize('destroy', $module);
        
        //Using soft deletes (deleted_at)
        Gallery::destroy($imageid);
        
        return redirect('/images/'.$module->id);
    }
    
    /**
     * Destroy the given module.
     *
     * @param  Request  $request
     * @param  Module  $module
     * @return Response
     */
    public function create(User $user, $moduleid)
    {
        
        $module = Module::find($moduleid);
        //User can only add images to his own modules
        $this->authorize('create', $module);
        
        $modulename = $module['modulename'];
        
        return view('images.edit', ['moduleid' => $moduleid, 'modulename' => $modulename]);
        
        
    }
    
    public function store(User $user, Request $request)
    {
        
        $module = Module::find($request['moduleid']);
        //User can only add images to his own modules
        $this->authorize('create', $module);
        
        $image = new Gallery;
        $image -> idModule   = $request['moduleid'];
        
        /*FIRST TRY TO UPLOAD FILE*/
        $file = $request['image'];

        if ($request->hasFile('image')) {
            //Move Uploaded File
            $destinationPath = '/storage/app/public/images';
            $path = public_path();
            $path1 = base_path().'/public/upload_media/images';
            $timeforname = time();
            $filename = 'modul' . '-' . time() . '-' . $file->getClientOriginalName();
            $img = Image::make($request->file('image')->getRealPath());
            // read width of image to see if needs to be resized
            $width = $img->width();
            
            if ($width > 1000){
                // resize the image to a width of 1000 and constrain aspect ratio (auto height)
                $img->resize(1000, null, function ($constraint) {
                    $constraint->aspectRatio();
                });
            }
            $img->save(base_path('/public/upload_media/images/').$filename);
            // resize the image to a width of 100 and constrain aspect ratio (auto height)
            // for THUMBS
            $img->resize(100, null, function ($constraint) {
                $constraint->aspectRatio();
            });
            $img->save(base_path('/public/upload_media/images_thumbs/').$filename);
            
            $image -> imagename  = $filename;
            
        }else{
            //echo '</br>No file';
           
            return redirect()->back()->with('message', trans('auth.filenotfound'));
        }
        
        
        
        $image->save();

        $modulename = $module['modulename'];
        
        return view('images.index', [
            'images' => $this->galleries->getImages($request['moduleid']), 'moduleid' => $request['moduleid'], 
            'modulename' => $modulename, 
        ]);
        
        
        
    }
    
}
