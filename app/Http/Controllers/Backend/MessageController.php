<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Module;
use App\Itinerary;
use App\ItineraryModule;
use App\Message;
use App\Repositories\MessageRepository;

use App\Policies\MessagePolicy;

use Lang;
use App;
use Session;
use DB;
use App\User;
use DateTime;
use Auth;

use Illuminate\Support\Facades\Validator;

use Mail;
use App\Mail\ModuleProviderResponse;
use App\Jobs\ModuleProviderResponseEmail;


class MessageController extends Controller
{
    
    /**
     * The itinerarymodule repository instance.
     *
     * @var ItineraryModuleRepository
     */
    protected $itinerarymodules;
    protected $primaryKey = 'id';
    protected $table = 'itinerarymodules';
    
    /**
     * Create a new controller instance.
     *
     *
     * @param  ItineraryModuleRepository  $itinerarymodules
     * @return void
     */
    public function __construct(MessageRepository $messages)
    {
        
        $this->messages = $messages;
        
    }
    
    /**
     * Display a list of all of the itinerary's modules.
     *
     * @param  Request  $request
     * @return Response
     */
    public function index($itineraryid)
    {
        
        $itinerary = Itinerary::findOrFail($itineraryid);
        
        //User can only see messages of his own itineraries
        $this->authorize('view', $itinerary);
        
        $itineraryname = $itinerary['itineraryname'];
        
        $itineraryFrom = $itinerary['dateFrom'];
        $itineraryTo = $itinerary['dateTo'];
        
        $itineraryFrom = new DateTime($itineraryFrom);
        $itineraryFrom = $itineraryFrom->format('d. m. Y H:i');

        $itineraryTo = new DateTime($itineraryTo);
        $itineraryTo = $itineraryTo->format('d. m. Y H:i');
        
        $moduleChild = new Module;
        $moduleParent = new Module;
        $messages = array();
        
        $iditimodule = 0;
    
        $itinerarymodulesid = $this->messages->getItineraryModules($itineraryid);
        $userlanguage = Session::get('locale');

        foreach ( $itinerarymodulesid as $value )
        {
            $iditimodule = $value['id'];
            $idparentmodule = $value['idModule'];
            $moduleParent[$idparentmodule] = Module::findOrFail($idparentmodule); 
            //Get all the categories for each module
            
            if($userlanguage != 'en'){
                $pluckedid = $moduleParent[$idparentmodule]['idModule'];
                $pluckeduser = $moduleParent[$idparentmodule]['user_id'];
                $moduleChild[$idparentmodule] = $this->messages->getModule($pluckedid,$pluckeduser,$userlanguage);
            }
            
            
            
            $messages[$iditimodule] = $this->messages->getMessages($iditimodule);
          
        }

        return view('messages.index', [
            'itinerarymodules' => $itinerarymodulesid, 'itineraryid' => $itineraryid, 'itineraryname' => $itineraryname, 'moduleParent' => $moduleParent,
            'moduleChild' => $moduleChild, 'userlanguage' => $userlanguage, 'itineraryFrom' => $itineraryFrom,
            'itineraryTo' => $itineraryTo, 'itinerarystatus' => $itinerary['status'], 'iditimodule' => $iditimodule, 'messages' => $messages,
        ]);
    }
    
    
    public function showResponseForm($answer, $token)
    {
        
        $itinerarymodule = $this->messages->getItiModuleFromToken($token);
        
        if ($itinerarymodule['status'] > 2){
            return view('messages.messages')->with('errors', trans('texts.messageexists'));
        }
        
        $itinerary = Itinerary::findOrFail($itinerarymodule['idItinerary']);
        $agency = User::findOrFail($itinerary['user_id']);
        $module = Module::findOrFail($itinerarymodule['idModule']);
        
        //get user (provider)
        $moduleUser = $this->messages->getModuleOwner($itinerarymodule->idModule);
        
        //Dates format
        $dateFrom = new DateTime($itinerarymodule['dateFrom']);
        $dateFrom = $dateFrom->format('d. m. Y H:i');
        $dateTo = new DateTime($itinerarymodule['dateTo']);
        $dateTo = $dateTo->format('d. m. Y H:i');
        
        //The language for the form is the provider's language
        if(Session::get ('locale') <> $moduleUser->idLang){
            Session::put('locale', $moduleUser->idLang);
        }
        
        return view('messages.messages', [
            'token' => $token, 'answer' => $answer, 'agency' => $agency, 'module' => $module,
            'dateFrom' => $dateFrom, 'dateTo' => $dateTo, 'errors' => '', 'messages' => '',
        ]);
    }
    
    public function providerResponse(Request $request, $answer, $token)
    {
       
        //send mail to agency with response
        $itinerarymodule = $this->messages->getItiModuleFromToken($token);
        
        //itinerarymodule status will be changed according to the response
        //3 (rejected) or 4 (confirmed)
        if ($answer == 'confirm'){
            $itinerarymodule->status   = '4';
            $confirmed = '1';
        }elseif ($answer == 'reject'){
            $itinerarymodule->status   = '3';
            $confirmed = '0';
        }else{
            return view('messages.messages')->with('errors', trans('texts.miskakeinresponse'));
        }
        $itinerarymodule->save();
        
        $itinerary = Itinerary::findOrFail($itinerarymodule['idItinerary']);
        $agency = User::findOrFail($itinerary['user_id']);
        
        //Dates format
        $dateFrom = new DateTime($itinerarymodule['dateFrom']);
        $dateFrom = $dateFrom->format('d. m. Y H:i');
        $dateTo = new DateTime($itinerarymodule['dateTo']);
        $dateTo = $dateTo->format('d. m. Y H:i');
        $daterange = $dateFrom . ' - ' . $dateTo;
        
        //get user (provider)
        $moduleUser = $this->messages->getModuleOwner($itinerarymodule->idModule);
        
        //get module in the user (agency) lang for mail
        $itinerarymoduleLang = $this->messages->getModuleForMail($itinerarymodule->idModule, $agency->idLang);
        
        $message = new Message;
        
        $message->idItineraryModule = $itinerarymodule->id;
        $message->fromUser          = $moduleUser->id;
        $message->toUser            = $itinerary->user_id;
        $message->message           = $request->message;
        $message->confirmed         = $confirmed;
        $message->token_messages    = $token;
        
        $message->save();
        
        
        //The language for the mail is the agency's language
        if(Session::get ('locale') <> $moduleUser->idLang){
            Session::put('locale', $moduleUser->idLang);
        }
        
        if ($request->message){
            $messageprovider = $request->message;
        }else{
            $messageprovider = 'none';
        }
        
        Mail::to($agency->email)
        ->send(new ModuleProviderResponse($moduleUser['title'], $itinerarymoduleLang['modulename'],$itinerarymoduleLang['description'],$daterange,$token,$answer,$messageprovider));
        

        
        //If necessary change status itinerary for this itinerary to 2 or 3 (there is a rejection - all confirmed) 
        //get all itimodules with the itinerary id
        $itimodules = $this->messages->getItineraryModules($itinerarymodule['idItinerary']);
        
        foreach ($itimodules as $itimodule)
        {
            $statuses[] = $itimodule['status'];
            
        }
        
        //if any of the modules was rejected then itinerary status is 2 (there is a rejection)
        if (in_array('3', $statuses)) {
            $itinerary->status   = '2';
            
        }elseif (in_array('2', $statuses)){
            //if any of the modules still has no response then itinerary status is 3 (still waiting for responses) 
            $itinerary->status   = '3';
        }else{
            //else means all the modules have replied and confirmed so itinerary status is 4
            $itinerary->status   = '4';
        }
        
        $itinerary->save();
 
        return view('messages.messages')->with('messages', trans('texts.responsesent'));
    }
    
}
