<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Validator;
use DateTime;

use App\Itinerary;
use App\Repositories\ItineraryRepository;
use App\Repositories\ItineraryModuleRepository;

use App\Policies\ItineraryPolicy;

use Lang;
use App;
use Session;
use DB;
use Auth;
use Image;
use PDF;

use App\User;
use App\Module;
use App\Category;
use App\Tag;
use App\Gallery;
Use App\ItineraryModule;

use Mail;
use App\Mail\ItineraryConfirmation;
use App\Jobs\ItineraryConfirmationEmail;
use App\Mail\ItineraryCancel;
use App\Jobs\ItineraryCancelEmail;
use App\Mail\EvaluationLink;
use App\Jobs\EvaluationLinkEmail;
use App\Mail\EvaluationLinkAgency;
use App\Jobs\EvaluationLinkAgencyEmail;



class ItineraryController extends Controller
{
   
    /**
     * The itinerary repository instance.
     *
     * @var ItineraryRepository
     */
    protected $itineraries;
    protected $primaryKey = 'id';
    protected $table = 'itineraries';

    /**
     * Create a new controller instance.
     * 
     * @param  ItineraryRepository  $itineraries
     * @return void
     */
    public function __construct(ItineraryRepository $itineraries, ItineraryModuleRepository $itinerarymodules)
    {
        $this->middleware('auth');
        
        $this->itineraries = $itineraries;
        $this->itinerarymodules = $itinerarymodules;
        
    }
    
    protected function validateItinerary(Request $request, $id)
    {
        if ($id == 0){
            return $this->validate($request, [
                'itineraryname'        => 'required|max:50',
                'description'       => 'required|max:100',
                'longdescription'   => 'required',
                'personsMin'        => 'required|integer',
                'personsMax'        => 'required|integer|gte:personsMin',
                'day1'           => 'required|after:'.$request['today'],
                'price'             => 'required|integer',
                'pricedescription'  => 'required',
                'mainimage'         => 'required',
                
            ]);

        }else {
            //No need to validate mainimage if is an edit
            return $this->validate($request, [
                'itineraryname'     => 'required|max:50',
                'description'       => 'required|max:100',
                'longdescription'   => 'required',
                'personsMin'        => 'required|integer',
                'personsMax'        => 'required|integer|gte:personsMin',
                'day1'           => 'required|after:'.$request['today'],
                'price'             => 'required|integer',
                'pricedescription'  => 'required',
            ]);
        }
            
        
        
    }
    
    protected function validateItinerarylang(Request $request)
    {
        
        return $this->validate($request, [
            'itineraryname'     => 'required|max:50',
            'description'       => 'required|max:100',
            'longdescription'   => 'required',
            'pricedescription'  => 'required',
        ]);
        
    }
    
    
    /**
     * Display a list of all of the user's itineraries.
     *
     * @param  Request  $request
     * @return Response
     */
    public function index(Request $request)
    {
        return view('itineraries.index', [
            'itineraries' => $this->itineraries->forUserFiltered($request->user()), 
            'itinerariestranslated' => $this->itineraries->forUser($request->user()), 
            'languages' => $this->itineraries->getLanguages(),
            'userType' => Auth::user()->userType,
        ]);
    }
    
    /**
     * When user wants to create a new itinerary: 
     * Step1: call edit blade with id=0
     *
     * @param  Request  $request
     * @return Response
     */
     
     public function create(Request $request)
    {
        
        $id=0;
        $idparent=0;
        $newlang=0;
        
        $emptyitinerary= new Itinerary;
        
        return view('itineraries.edit',['id' => $id, 'newlang' => $newlang, 'idparent' => $idparent,'itinerary' => $emptyitinerary]);
        
           
        
    }
    
     
    /**
     * Step2: Create a new itinerary after form is sent.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {

        //call funtion to change dates into values for the query
        $this->itineraries->validateDates($request);
        $dateFrom = $request->day1 .' '. $request->dayhour1.':00';
        $dateTo = $request->day2 .' '. $request->dayhour2 .':00';
        
        
        $id = 0;
        $this->validateItinerary($request, $id);
        
        /*
         * Get the last value of idItinerary so we can increment it by 1
         */
        
        $gotanyitineraries = $this->itineraries->forUserLastId($request->user());
        
        if($gotanyitineraries['idItinerary']){
            $newIdItinerary = $gotanyitineraries['idItinerary'] + 1;
        }else{
            $newIdItinerary = 1;
        }
        
        $userid = Auth::user()->id;
        
        $itinerary = new Itinerary;
        
        $itinerary->user_id           = $userid;
        $itinerary->idItinerary       = $newIdItinerary;
        $itinerary->itineraryname     = $request->itineraryname;
        $itinerary->description       = $request->description;
        $itinerary->longdescription   = $request->longdescription;
        $itinerary->personsMin        = $request->personsMin;
        $itinerary->personsMax        = $request->personsMax;
        $itinerary->dateFrom          = $dateFrom;
        $itinerary->dateTo            = $dateTo;
        $itinerary->timestampFrom     = $request->time1;
        $itinerary->timestampTo       = $request->time2;
        $itinerary->price             = $request->price;
        $itinerary->pricedescription  = $request->pricedescription;
        
        /*FIRST TRY TO UPLOAD FILE*/
        $file = $request['mainimage'];
        
        if ($request->hasFile('mainimage')) {
            //Move Uploaded File
            $destinationPath = '/storage/app/public/itineraries';
            $path = public_path();
            $path1 = base_path().'/public/upload_media/itineraries';
            $timeforname = time();
            $filename = 'itinerary' . '-' . time() . '-' . $file->getClientOriginalName();
            $img = Image::make($request->file('mainimage')->getRealPath());
            // read width of image to see if needs to be resized
            $width = $img->width();
            
            if ($width > 1000){
                // resize the image to a width of 1000 and constrain aspect ratio (auto height)
                $img->resize(1000, null, function ($constraint) {
                    $constraint->aspectRatio();
                });
            }
            $img->save(base_path('/public/upload_media/itineraries/').$filename);
            
            // resize the image to a width of 100 and constrain aspect ratio (auto height)
            // for THUMBS
            $img->resize(100, null, function ($constraint) {
                $constraint->aspectRatio();
            });
            $img->save(base_path('/public/upload_media/itineraries_thumbs/').$filename);
                
            
            $itinerary-> mainimage         = $filename;

        }else{
            //echo '</br>No file';
            return redirect()->back()->with('message', trans('auth.filenotfound'));
        }
        
        $itinerary->save();
        
        $newrecordid = Itinerary::orderBy('id', 'desc')->first();
        
        return redirect('/itineraries');
        
        
    }
    
    
    /**
     * When user wants to create a new version of the itinerary but in another language: 
     * Step1: call edit blade with id=0, the id of the parent itinerary (itinerary in english) 
     * and the value of the language that wants to add 
     *
     * @param  Request  $request
     * @return Response
     */
    
    public function createlang($itineraryid, $itinerary, $itilang, Request $request)
    {
        
        //$itineraryid is idItinerary of parent itinerary
        //$itinerary is id of parent itinerary
        
        //$itinerary is id of parent itinerary so is the id to check for authorization
        $itinerarycheck = Itinerary::find($itinerary);

        //User can create a itinerary in a new lang only for his own itineraries
        $this->authorize('create', $itinerarycheck);
        
        $id = 0;
        
        $emptyitinerary = new Itinerary;
        
        return view('itineraries.edit',['idparent' => $itineraryid,'newlang' => $itilang ,'id' => $id,'itineraryparent' => $itinerarycheck, 'itinerary' => $emptyitinerary]);
        

    }
    
    /**
     * Step2: Create the new itinerary after form is sent.
     *
     * @param  Request  $request
     * @return Response
     */
    public function addnewlang(Request $request)
    {
        
        //$itineraryid is idItinerary of parent itinerary
        //$itinerary is id of parent itinerary
        
        //$itinerary is id of parent itinerary so is the id to check for authorization
        $itinerarycheck = Itinerary::find($request->realidparent);
        
        //User can create a itinerary in a new lang only for his own itineraries
        $this->authorize('create', $itinerarycheck);
        
        
        $this->validateItinerarylang($request);
        
        
        $userid = Auth::user()->id;
        
        $itinerary = new Itinerary;
        
        $itinerary->user_id           = $userid;
        $itinerary->idItinerary          = $request->idparent;
        $itinerary->idLang            = $request->newlang;
        $itinerary->itineraryname        = $request->itineraryname;
        $itinerary->description       = $request->description;
        $itinerary->longdescription   = $request->longdescription;
        $itinerary->pricedescription  = $request->pricedescription;
        
        $itinerary->save();
        
        return redirect('/itineraries');
        
        
    }
    
    
    /**
     * Update an existing itinerary.
     *
     * @param  Request  $request
     * @return Response
     */
    
  
    public function update($id, Request $request)
    {
        
        $itinerary = Itinerary::find($id);
        
        //User can update only his own itineraries
        $this->authorize('update', $itinerary);
        
        //call funtion to change dates into values for the query
        $this->itineraries->validateDates($request);
        $dateFrom = $request->day1 .' '. $request->dayhour1.':00';
        $dateTo = $request->day2 .' '. $request->dayhour2 .':00';
        
        
        $this->validateItinerary($request,$id);
        
        $itinerary-> itineraryname     = $request->get('itineraryname');
        $itinerary-> description       = $request->get('description');
        $itinerary-> longdescription   = $request->get('longdescription');
        $itinerary-> personsMin        = $request->get('personsMin');
        $itinerary-> personsMax        = $request->get('personsMax');
        $itinerary->dateFrom           = $dateFrom;
        $itinerary->dateTo             = $dateTo;
        $itinerary->timestampFrom      = $request->time1;
        $itinerary->timestampTo        = $request->time2;
        $itinerary-> price             = $request->get('price');
        $itinerary-> pricedescription  = $request->get('pricedescription');
        
        /*FIRST TRY TO UPLOAD FILE*/
        $file = $request['mainimage'];
        
        if ($request->hasFile('mainimage')) {
            //Move Uploaded File
            $destinationPath = '/storage/app/public/itineraries';
            $path = public_path();
            $path1 = base_path().'/public/upload_media/itineraries';
            $timeforname = time();
            $filename = 'itinerary' . '-' . time() . '-' . $file->getClientOriginalName();
            
            $img = Image::make($request->file('mainimage')->getRealPath());
            // read width of image to see if needs to be resized
            $width = $img->width();
            
            if ($width > 1000){
                // resize the image to a width of 1000 and constrain aspect ratio (auto height)
                $img->resize(1000, null, function ($constraint) {
                    $constraint->aspectRatio();
                });
            }
            $img->save(base_path('/public/upload_media/itineraries/').$filename);
            
            // resize the image to a width of 100 and constrain aspect ratio (auto height)
            // for THUMBS
            $img->resize(100, null, function ($constraint) {
                $constraint->aspectRatio();
            });
            $img->save(base_path('/public/upload_media/itineraries_thumbs/').$filename);
                
            
            $itinerary-> mainimage         = $filename;
            
        }else{
            //echo '</br>No file';
        }
        
        
        $itinerary->save();
        
        return redirect('/itineraries');
        
        
    }
    
    public function updatelang($id, Request $request)
    {
        
        $itinerary = Itinerary::find($id);
        
        //User can update only his own itineraries
        $this->authorize('update', $itinerary);
        
        $this->validateItinerarylang($request);        
        
        $itinerary-> itineraryname        = $request->get('itineraryname');
        $itinerary-> description       = $request->get('description');
        $itinerary-> longdescription   = $request->get('longdescription');
        $itinerary-> pricedescription  = $request->get('pricedescription');
        
        $itinerary->save();
        
        
        return redirect('/itineraries');
        
        
    }
    
    
    /**
     * Update the public field of a itinerary.
     *
     * @param  Request  $request
     * @return Response
     */
    public function public($id, Request $request)
    {
        
        $itinerary = Itinerary::find($id);
       
        //User can update only his own itineraries
        $this->authorize('update', $itinerary);
        
        
        if ($request->get('actionenable') == 'enable'){
            $action = '1';
        }else{
            $action = '0';
        }
        
        $itinerary-> public  = $action;
        
        $itinerary->save();
        
        
        
        return redirect('/itineraries');
        
        
    }
    
    /**
     * Get an existing itinerary to edit.
     *
     * @param  Request  $request
     * @return Response
     */
     
     public function edit($id)
    {
        
        $itinerary=Itinerary::findOrFail($id);
        
        //User can edit only his own itineraries
        $this->authorize('view', $itinerary);
        
        //If itinerary is not in english then is child itinerary
        //Must get parent to get the common attributes  
        $itineraryparent = array();
        $userid = Auth::user()->id;
        
        if ($itinerary->idLang <> 'en'){
            $itineraryparentid=Itinerary::where('idItinerary',$itinerary->idItinerary)
            ->where ('user_id',$userid)
            ->where ('idLang','en')
            ->first();
            
            $parentid = $itineraryparentid['id'];
            
            $itineraryparent=Itinerary::findOrFail($parentid);
            //Dates format must be changed from Y-m-d H:m:s to d. m. Y H:i for the ui
            $dateFrom = new DateTime($itineraryparent['dateFrom']);
            $dateFrom1 = $dateFrom->format('d. m. Y H:i');
            $dateTo = new DateTime($itineraryparent['dateTo']);
            $dateTo1 = $dateTo->format('d. m. Y H:i');
            $itineraryparent['dateFrom'] = $dateFrom1;
            $itineraryparent['dateTo'] = $dateTo1;
            
        }else{
            //Dates format must be changed from Y-m-d H:m:s to d. m. Y H:i for the ui
            $dateFrom = new DateTime($itinerary['dateFrom']);
            $dateFrom1 = $dateFrom->format('d. m. Y H:i');
            $dateTo = new DateTime($itinerary['dateTo']);
            $dateTo1 = $dateTo->format('d. m. Y H:i');
            $itinerary['dateFrom'] = $dateFrom1;
            $itinerary['dateTo'] = $dateTo1;
        }
        
        
        
        return view('itineraries.edit',['itinerary' => $itinerary,'id' => $id,'itineraryparent' => $itineraryparent]);
        
        
       
    }
    
    /**
     * Update the status field of an itinerary.
     *
     * @param  Request  $request
     * @return Response
     */
    public function confirm($id, Request $request)
    {
        
        $itinerary = Itinerary::find($id);
        
        //User can update only his own itineraries
        $this->authorize('update', $itinerary);
        
        //Change itinerary status to 5 (will be realized)
        $itinerary->status  = '5';
        
        $itinerary->save();
        
        //get user (agency)
        $agency = Auth::user()->title;
        
        //Send mail to all providers with this info
        $itinerarymodules = $this->itineraries->getItineraryModules($id);

        foreach ( $itinerarymodules as $itinerarymodule )
        {
            //get user (provider)
            $moduleUser = $this->itineraries->getModuleOwner($itinerarymodule->idModule);
            
            //get module in the user (provider) lang for mail
            $itinerarymoduleLang = $this->itineraries->getModuleForMail($itinerarymodule->idModule, $moduleUser->idLang);
            
            //The language for the mail is the provider's language
            if(Session::get ('locale') <> $moduleUser->idLang){
                Session::put('locale', $moduleUser->idLang);
                App::setLocale($moduleUser->idLang);
            }
            
            
            $daterange = $itinerarymodule->dateFrom1.' - ' . $itinerarymodule->dateTo1;
            unset($itinerarymodule->dateFrom1);
            unset($itinerarymodule->dateTo1);
            
            Mail::to($moduleUser->email)
            ->send(new ItineraryConfirmation($agency, $itinerarymoduleLang['modulename'],$itinerarymoduleLang['description'],$daterange,$itinerarymodule['token_messages']));
            
            //change status module in itinerary to 5 (will be realized)
            $itinerarymodule->status   = '5';
            $itinerarymodule->save();
            
        }
        
        //Change language back to the agency's
        $agency = Auth::user();
        if(Session::get ('locale') <> $agency->idLang){
            Session::put('locale', $agency->idLang);
            App::setLocale($agency->idLang);
        }
        
        return redirect('/itineraries');
        
        
    }
    
    public function cancel($id, Request $request)
    {
        
        $itinerary = Itinerary::find($id);
        
        //User can update only his own itineraries
        $this->authorize('update', $itinerary);
        
        //Change itinerary status to 8 (cancelled by agency)
        $itinerary-> status  = '8';
        
        $itinerary->save();
        
        //get user (agency)
        $agency = Auth::user()->title;
        
        //Send mail to all providers with this info
        $itinerarymodules = $this->itineraries->getItineraryModules($id);
        
        foreach ( $itinerarymodules as $itinerarymodule )
        {
            //get user (provider)
            $moduleUser = $this->itineraries->getModuleOwner($itinerarymodule->idModule);
            
            //get module in the user (provider) lang for mail
            $itinerarymoduleLang = $this->itineraries->getModuleForMail($itinerarymodule->idModule, $moduleUser->idLang);
            
            //The language for the mail is the provider's language
            if(Session::get ('locale') <> $moduleUser->idLang){
                Session::put('locale', $moduleUser->idLang);
                App::setLocale($moduleUser->idLang);
            }
            
            $daterange = $itinerarymodule->dateFrom1.' - ' . $itinerarymodule->dateTo1;
            unset($itinerarymodule->dateFrom1);
            unset($itinerarymodule->dateTo1);
            
            Mail::to($moduleUser->email)
            ->send(new ItineraryCancel($agency, $itinerarymoduleLang['modulename'],$itinerarymoduleLang['description'],$daterange,$itinerarymodule['token_messages']));
            
            //change status module in itinerary to 8 (has been cancelled)
            $itinerarymodule->status   = '8';
            $itinerarymodule->save();
            
        }
        
        //Change language back to the agency's
        $agency = Auth::user();
        if(Session::get ('locale') <> $agency->idLang){
            Session::put('locale', $agency->idLang);
            App::setLocale($agency->idLang);
        }
        return redirect('/itineraries');
        
        
    }
    
    public function completed($id, Request $request)
    {
        
        $itinerary = Itinerary::find($id);
        
        //User can update only his own itineraries
        $this->authorize('update', $itinerary);
        
        $date1 = new DateTime($itinerary->dateFrom);
        $date1 = $date1->format('d. m. Y H:i');
        $date2 = new DateTime($itinerary->dateTo);
        $date2 = $date2->format('d. m. Y H:i');
        $daterange = $date1.' - ' . $date2;
        
        //Change itinerary status to 6 (itinerary realized)
        $itinerary-> status  = '6';
        
        $token = str_random(30).time();
        $itinerary->token = $token;
        $itinerary->save();
        
        //get user (agency)
        $agency = Auth::user()->title;
        
        //Send mail to all providers AND to the agency with link for evaluation
        $itinerarymodules = $this->itineraries->getItineraryModules($id);
        //Mails to providers 
        foreach ( $itinerarymodules as $itinerarymodule )
        {
            //get user (provider)
            $moduleUser = $this->itineraries->getModuleOwner($itinerarymodule->idModule);
            
            //get module in the user (provider) lang for mail
            $itinerarymoduleLang = $this->itineraries->getModuleForMail($itinerarymodule->idModule, $moduleUser->idLang);
            
            //The language for the mail is the provider's language
            if(Session::get ('locale') <> $moduleUser->idLang){
                Session::put('locale', $moduleUser->idLang);
                App::setLocale($moduleUser->idLang);
            }
            
            $daterange = $itinerarymodule->dateFrom1.' - ' . $itinerarymodule->dateTo1;
            unset($itinerarymodule->dateFrom1);
            unset($itinerarymodule->dateTo1);
            
            //Token for providers is token_messages from itimodule
            //Providers rate the completion of their own module
            Mail::to($moduleUser->email)
            ->send(new EvaluationLink($agency, $itinerarymoduleLang['modulename'],$itinerarymoduleLang['description'],$daterange,$itinerarymodule['token_messages']));
            
            //change status module in itinerary to 6 (has been completed)
            $itinerarymodule->status   = '6';
            $itinerarymodule->save();
            
        }
        
        //Mail to agency
        $agency = Auth::user();
        //The language for the mail is the agency's language
        if(Session::get ('locale') <> $agency->idLang){
            Session::put('locale', $agency->idLang);
            App::setLocale($agency->idLang);
        }
        //get itinerary in the user (agency) lang for mail
        $itineraryLang = $this->itineraries->getItineraryForMail($itinerary->idItinerary,$itinerary->user_id, $agency->idLang);
        
        //Token for agency is token from itinerary
        //Agencies rate the completion of the whole itinerary (for all the providers)
        Mail::to($agency->email)
        ->send(new EvaluationLinkAgency($itinerary['itineraryname'],$itineraryLang['description'],$daterange,$token));
        
        return redirect('/itineraries');
        
        
    }
    
    
    /**
     * Destroy the given itinerary.
     *
     * @param  Request  $request
     * @param  Itinerary  $itinerary
     * @return Response
     */
    public function destroy(User $user, Itinerary $itinerary, $itineraryid)
    {
        
        $itinerary = Itinerary::findOrFail($itineraryid);
        
        //User can delete only his own itineraries
        $this->authorize('destroy', $itinerary);
        
        //Using soft deletes (deleted_at)
        Itinerary::destroy($itineraryid);
        
        return redirect('/itineraries');
    }
    
    /**
     * Get an existing itinerary to edit.
     *
     * @param  Request  $request
     * @return Response
     */
    
    public function pdf($lang, $id)
    {
        
        $itinerary=Itinerary::findOrFail($id);
        
        //User can edit only his own itineraries
        $this->authorize('view', $itinerary);
        
        if($itinerary['status'] < 4 OR $lang != $itinerary['idLang']){
            
            return abort(505, 'Unauthorized action.');
            
        }
        
        //If itinerary is not in english then is child itinerary
        //Must get parent to get the common attributes
        $itineraryparent = array();
        $userid = Auth::user()->id;
        
        $agency = User::findOrFail($userid);
        
        if ($itinerary->idLang <> 'en'){
            $itineraryparentid=Itinerary::where('idItinerary',$itinerary->idItinerary)
            ->where ('user_id',$userid)
            ->where ('idLang','en')
            ->first();
            
            $parentid = $itineraryparentid['id'];
            
            $itineraryparent=Itinerary::findOrFail($parentid);
            //Dates format must be changed from Y-m-d H:m:s to d. m. Y H:i for the ui
            $dateFrom = new DateTime($itineraryparent['dateFrom']);
            $dateFrom1 = $dateFrom->format('d. m. Y H:i');
            $dateTo = new DateTime($itineraryparent['dateTo']);
            $dateTo1 = $dateTo->format('d. m. Y H:i');
            $itineraryparent['dateFrom'] = $dateFrom1;
            $itineraryparent['dateTo'] = $dateTo1;
            
        }else{
            //Dates format must be changed from Y-m-d H:m:s to d. m. Y H:i for the ui
            $dateFrom = new DateTime($itinerary['dateFrom']);
            $dateFrom1 = $dateFrom->format('d. m. Y H:i');
            $dateTo = new DateTime($itinerary['dateTo']);
            $dateTo1 = $dateTo->format('d. m. Y H:i');
            $itinerary['dateFrom'] = $dateFrom1;
            $itinerary['dateTo'] = $dateTo1;
        }
        
        //Now get the modules for this itinerary
        
        $itineraryname = $itinerary['itineraryname'];
        
        if ($itinerary->idLang <> 'en'){
            $itinerary = $this->itinerarymodules->getParentItineraryId($itinerary['idItinerary'],$itinerary['user_id']);
        }
        
        
        $itinerarymodulesid = new ItineraryModule;
        $moduleParent = new Module;
        $categories = new Category;
        $moduleChild = array();
        
        $itinerarymodulesid = $this->itinerarymodules->getItineraryModules($id);
        $userlanguage = Session::get('locale');
        
        foreach ( $itinerarymodulesid as $value )
        {
            $idparentmodule = $value['idModule'];
            $moduleParent[$idparentmodule] = Module::findOrFail($idparentmodule);
            
            //Dates format must be changed from Y-m-d H:m:s to d. m. Y H:i for the ui
            $dateFrom = new DateTime($moduleParent[$idparentmodule]['dateFrom']);
            $dateFrom1 = $dateFrom->format('d. m. Y H:i');
            $dateTo = new DateTime($moduleParent[$idparentmodule]['dateTo']);
            $dateTo1 = $dateTo->format('d. m. Y H:i');
            $moduleParent[$idparentmodule]['dateFrom'] = $dateFrom1;
            $moduleParent[$idparentmodule]['dateTo'] = $dateTo1;
            
            if($userlanguage != 'en'){
                $pluckedid = $moduleParent[$idparentmodule]['idModule'];
                $pluckeduser = $moduleParent[$idparentmodule]['user_id'];
                $moduleChild[$idparentmodule] = $this->itinerarymodules->getModule($pluckedid,$pluckeduser,$userlanguage);
                
                //Dates format must be changed from Y-m-d H:m:s to d. m. Y H:i for the ui
                $dateFrom = new DateTime($moduleChild[$idparentmodule]['dateFrom']);
                $dateFrom1 = $dateFrom->format('d. m. Y H:i');
                $dateTo = new DateTime($moduleChild[$idparentmodule]['dateTo']);
                $dateTo1 = $dateTo->format('d. m. Y H:i');
                $moduleChild[$idparentmodule]['dateFrom'] = $dateFrom1;
                $moduleChild[$idparentmodule]['dateTo'] = $dateTo1;
                
            }
            
        }
        
        $pdf = PDF::loadView('itineraries.pdf-content', [
            'itinerary' => $itinerary,
            'id' => $id,
            'itineraryparent' => $itineraryparent,
            'itinerarymodules' => $itinerarymodulesid,
            'itineraryname' => $itineraryname,
            'moduleParent' => $moduleParent,
            'moduleChild' => $moduleChild,
            'userlanguage' => $userlanguage,
            'agency' => $agency,
        ]);
        
        return $pdf->download('itinerarypdf-'.$itineraryname.'.pdf');
        
        
    }
    
}
