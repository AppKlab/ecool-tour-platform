<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Validator;

use App\Module;
use App\Repositories\ModuleRepository;

use App\Policies\ModulePolicy;

use App\Utilities\GoogleMaps;

use Lang;
use App;
use Session;
use DB;
use Auth;
use Image;

use App\User;
use App\Category;
use App\Tag;
use App\Gallery;


class ModuleController extends Controller
{
   
    /**
     * The module repository instance.
     *
     * @var ModuleRepository
     */
    protected $modules;
    protected $primaryKey = 'id';
    protected $table = 'modules';

    /**
     * Create a new controller instance.
     * 
     * @param  ModuleRepository  $modules
     * @return void
     */
    public function __construct(ModuleRepository $modules)
    {
        $this->middleware('auth');
        
        $this->modules = $modules;
        
    }
    
    protected function validateModule(Request $request, $id)
    {
        
        if ($id == 0){
            return $this->validate($request, [
                'modulename'        => 'required|max:50',
                'description'       => 'required|max:100',
                'longdescription'   => 'required',
                'address'           => 'required',
                'zip'               => 'required',
                'city'              => 'required',
                'videoUrl'          => 'required|url',
                'personsMin'        => 'required|integer',
                'personsMax'        => 'required|integer|gte:personsMin',
                'price'             => 'required|integer',
                'pricedescription'  => 'required',
                'mainimage'         => 'required',
            ]);
        }else {
            //No need to validate mainimage if is an edit
            return $this->validate($request, [
                'modulename'        => 'required|max:50',
                'description'       => 'required|max:100',
                'longdescription'   => 'required',
                'address'           => 'required',
                'zip'               => 'required',
                'city'              => 'required',
                'videoUrl'          => 'required|url',
                'personsMin'        => 'required|integer',
                'personsMax'        => 'required|integer|gte:personsMin',
                'price'             => 'required|integer',
                'pricedescription'  => 'required',
            ]);
        }
            
        
        
    }
    
    protected function validateModulelang(Request $request)
    {
        
        return $this->validate($request, [
            'modulename'        => 'required|max:50',
            'description'       => 'required|max:100',
            'longdescription'   => 'required',
            'pricedescription'  => 'required',
            'videoUrl'          => 'required|url',
        ]);
        
    }
    
    protected function SaveCategories(Request $request, $newrecordid)
    {
        //First delete all categories for this module
        Category::where('idModule', $newrecordid)->delete();
        
        
        //Then save all the chosen categories for this module
        //$request -> checkboxes with name category
        if ($request['category']){
            foreach ($request['category'] as $key => $value){
                
                $category = new Category;
                
                $category->idCategory = $key;
                $category->idModule = $newrecordid;
                
                $category->save();
            }
        }
        
        
        return true;
        
    }
    
    protected function SaveTags(Request $request, $newrecordid)
    {
        
        //First delete all the tags for this module
        Tag::where('idModule', $newrecordid)->delete();
        
        //Then save all the tags for this module
        //$request -> checkboxes with name tag
        if ($request['category']){
            foreach ($request['tag'] as $key => $value){
            
                $tag = new Tag;
                
                $tag->idTag = $key;
                $tag->idModule = $newrecordid;
                
                $tag->save();
            }
            
        }
        
        return true;
        
    }
    
    
    /**
     * Display a list of all of the user's modules.
     *
     * @param  Request  $request
     * @return Response
     */
    public function index(Request $request)
    {
       
        return view('modules.index', [
            'modules' => $this->modules->forUserFiltered($request->user()), 
            'modulestranslated' => $this->modules->forUser($request->user()), 
            'languages' => $this->modules->getLanguages(),
        ]);
    }
    
    /**
     * When user wants to create a new module: 
     * Step1: call edit blade with id=0
     *
     * @param  Request  $request
     * @return Response
     */
     
     public function create(Request $request)
    {
        
        $id=0;
        $idparent=0;
        $newlang=0;
        $categories = array();
        $tags = array();
        
        $emptymodule = new Module;
        
        return view('modules.edit',['id' => $id, 'newlang' => $newlang, 'idparent' => $idparent,'categories'=> $categories,'tags'=> $tags, 'module' => $emptymodule]);
        
           
        
    }
    
     
    /**
     * Step2: Create a new module after form is sent.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        
        $id = 0;
        
        $this->validateModule($request, $id);

        $coordinates = GoogleMaps::geocodeAddress( $request->address, $request->city, $request->zip);
        
        /*
         * Get the last value of idModule so we can increment it by 1
         */
        $gotanymodules = $this->modules->forUserLastId($request->user());
        
        if($gotanymodules['idModule']){
            $newIdModule = $gotanymodules['idModule'] + 1;
        }else{
            $newIdModule = 1;
        }
        
        $userid = Auth::user()->id;
        
        $module = new Module;
        
        $module->user_id           = $userid;
        $module->idModule          = $newIdModule;
        $module->modulename        = $request->modulename;
        $module->description       = $request->description;
        $module->longdescription   = $request->longdescription;
        $module->address           = $request->address;
        $module->zip               = $request->zip;
        $module->city              = $request->city;
        $module->geolocationLat    = $coordinates['lat'];
        $module->geolocationLon    = $coordinates['lon'];
        $module->city              = $request->city;
        $module->videoUrl          = $request->videoUrl;
        $module->personsMin        = $request->personsMin;
        $module->personsMax        = $request->personsMax;
        $module->price             = $request->price;
        $module->pricedescription  = $request->pricedescription;
        
        /*FIRST TRY TO UPLOAD FILE*/
        $file = $request['mainimage'];
        
        if ($request->hasFile('mainimage')) {
            //Move Uploaded File
            $destinationPath = '/storage/app/public/modules';
            $path = public_path();
            $path1 = base_path().'/public/upload_media/modules';
            $timeforname = time();
            $filename = 'module' . '-' . time() . '-' . $file->getClientOriginalName();
            $img = Image::make($request->file('mainimage')->getRealPath());
            // read width of image to see if needs to be resized
            $width = $img->width();
            
            if ($width > 1000){
                // resize the image to a width of 1000 and constrain aspect ratio (auto height)
                $img->resize(1000, null, function ($constraint) {
                    $constraint->aspectRatio();
                });
            }
            $img->save(base_path('/public/upload_media/modules/').$filename);
            
            // resize the image to a width of 100 and constrain aspect ratio (auto height)
            // for THUMBS
            $img->resize(100, null, function ($constraint) {
                $constraint->aspectRatio();
            });
            $img->save(base_path('/public/upload_media/modules_thumbs/').$filename);
            
            $module-> mainimage = $filename;

        }else{
            //echo '</br>No file';
            return redirect()->back()->with('message', trans('auth.filenotfound'));
        }
        
        
        
        $module->save();
        
        $newrecordid = Module::orderBy('id', 'desc')->first();
        /*
         * Save the chosen categories
         */
        
        $this->SaveCategories($request, $newrecordid['id']);
        /*
         * Save the chosen tags
         */
        $this->SaveTags($request,$newrecordid['id']);
        
        return redirect('/modules');
        
        
    }
    
    
    /**
     * When user wants to create a new version of the module but in another language: 
     * Step1: call edit blade with id=0, the id of the parent module (module in english) 
     * and the value of the language that wants to add 
     *
     * @param  Request  $request
     * @return Response
     */
    
    public function createlang($moduleid, $module, $modlang, Request $request)
    {
        //$moduleid is idModule of parent module
        //$module is id of parent module
        
        //$module is id of parent module so is the id to check for authorization
        $modulecheck = Module::find($module);

        //User can create a module in a new lang only for his own modules
        $this->authorize('create', $modulecheck);
        
        $id = 0;
        
        $categories = array();
        $tags = array();
        
        //Must pass the categories for the parent module to the view
        $modulecategories = $this->modules->getCategories($module)->toArray();
        
        foreach ($modulecategories as $key => $value){
            foreach ($value as $key1 => $value1){
                $categories[] = $value1;
            }
        }
        
        //Must pass the tags for the parent module to the view
        $moduletags = $this->modules->getTags($module)->toArray();
        
        foreach ($moduletags as $key => $value){
            foreach ($value as $key1 => $value1){
                $tags[] = $value1;
            }
        }
        
        $emptymodule = new Module;
        
        return view('modules.edit',['idparent' => $moduleid,'newlang' => $modlang ,'id' => $id,'categories'=> $categories,'tags'=> $tags, 'moduleparent' => $modulecheck, 'module' => $emptymodule]);
        

    }
    
    /**
     * Step2: Create the new module after form is sent.
     *
     * @param  Request  $request
     * @return Response
     */
    public function addnewlang(Request $request)
    {
        
        //$moduleid is idModule of parent module
        //$module is id of parent module
        
        //$module is id of parent module so is the id to check for authorization
        $modulecheck = Module::find($request->realidparent);
        
        //User can create a module in a new lang only for his own modules
        $this->authorize('create', $modulecheck);
        
        
        $this->validateModulelang($request);
        
        
        $userid = Auth::user()->id;
        
        $module = new Module;
        
        $module->user_id           = $userid;
        $module->idModule          = $request->idparent;
        $module->idLang            = $request->newlang;
        $module->modulename        = $request->modulename;
        $module->description       = $request->description;
        $module->longdescription   = $request->longdescription;
        $module->videoUrl          = $request->videoUrl;
        $module->pricedescription  = $request->pricedescription;
        
        $module->save();
        
        return redirect('/modules');
        
        
    }
    
    
    /**
     * Update an existing module.
     *
     * @param  Request  $request
     * @return Response
     */
    
  
    public function update($id, Request $request)
    {
        
        $module = Module::find($id);
        
        //User can update only his own modules
        $this->authorize('update', $module);
        
        
        $this->validateModule($request,$id);
        
        //$module = Module::find($id);
        $coordinates = GoogleMaps::geocodeAddress( $request->address, $request->city, $request->zip);
        
        $module-> modulename        = $request->get('modulename');
        $module-> description       = $request->get('description');
        $module-> longdescription   = $request->get('longdescription');
        $module->address            = $request->address;
        $module->zip                = $request->zip;
        $module->city               = $request->city;
        $module->geolocationLat     = $coordinates['lat'];
        $module->geolocationLon     = $coordinates['lon'];
        $module-> videoUrl          = $request->get('videoUrl');
        $module-> personsMin        = $request->get('personsMin');
        $module-> personsMax        = $request->get('personsMax');
        $module-> price             = $request->get('price');
        $module-> pricedescription  = $request->get('pricedescription');
        
        /*FIRST TRY TO UPLOAD FILE*/
        $file = $request['mainimage'];
        
        if ($request->hasFile('mainimage')) {
            
            //Move Uploaded File
            $destinationPath = '/storage/app/public/modules';
            $path = public_path();
            $path1 = base_path().'/public/upload_media/modules';
            $timeforname = time();
            $filename = 'module' . '-' . time() . '-' . $file->getClientOriginalName();
            $img = Image::make($request->file('mainimage')->getRealPath());
            // read width of image to see if needs to be resized
            $width = $img->width();
            
            if ($width > 1000){
                // resize the image to a width of 1000 and constrain aspect ratio (auto height)
                $img->resize(1000, null, function ($constraint) {
                    $constraint->aspectRatio();
                });
            }
            $img->save(base_path('/public/upload_media/modules/').$filename);
            // resize the image to a width of 100 and constrain aspect ratio (auto height)
            // for THUMBS
            $img->resize(100, null, function ($constraint) {
                $constraint->aspectRatio();
            });
            $img->save(base_path('/public/upload_media/modules_thumbs/').$filename);
            
            $module-> mainimage = $filename;
            

        }else{
            //echo '</br>No file';
        }
        
        
        $module->save();
        
        /*
         * Update the categories
         */
        
        $this->SaveCategories($request, $id);
        /*
         * Update the tags
         */
        $this->SaveTags($request,$id);
        
        return redirect('/modules');
        
        
    }
    
    public function updatelang($id, Request $request)
    {
        
        $module = Module::find($id);
        
        //User can update only his own modules
        $this->authorize('update', $module);
        
        $this->validateModulelang($request);        
        
        $module-> modulename        = $request->get('modulename');
        $module-> description       = $request->get('description');
        $module-> longdescription   = $request->get('longdescription');
        $module-> pricedescription  = $request->get('pricedescription');
        $module-> videoUrl  = $request->get('videoUrl');
        
        $module->save();
        
        
        return redirect('/modules');
        
        
    }
    
    
    /**
     * Update the public field of a module.
     *
     * @param  Request  $request
     * @return Response
     */
    public function public($id, Request $request)
    {
        
        $module = Module::find($id);
       
        //User can update only his own modules
        $this->authorize('update', $module);
        
        
        if ($request->get('actionenable') == 'enable'){
            $action = '1';
        }else{
            $action = '0';
        }
        
        $module-> public  = $action;
        
        $module->save();
        
        
        
        return redirect('/modules');
        
        
    }
    /**
     * Get an existing module to edit.
     *
     * @param  Request  $request
     * @return Response
     */
     
     public function edit($id)
    {
        
        $module=Module::findOrFail($id);
        
        //User can edit only his own modules
        $this->authorize('view', $module);
        
        $categories = array();
        $tags = array();
        
        //If module is not in english then is child module
        //Must get parent to get the common attributes and its cats and tags 
        $moduleparent = array();
        $userid = Auth::user()->id;
        
        if ($module->idLang <> 'en'){
            $moduleparentid=Module::where('idModule',$module->idModule)
            ->where ('user_id',$userid)
            ->where ('idLang','en')
            ->first();
            
            $parentid = $moduleparentid['id'];
            
            $moduleparent=Module::findOrFail($parentid);
            $modulecategories = $this->modules->getCategories($parentid)->toArray();
            $moduletags = $this->modules->getTags($parentid)->toArray();
            
        }else{
            $moduletags = $this->modules->getTags($id)->toArray();
            $modulecategories = $this->modules->getCategories($id)->toArray();
        }
        
        //Must pass the categories for this module (or the parent module) to the view
        foreach ($modulecategories as $key => $value){
            foreach ($value as $key1 => $value1){
                $categories[] = $value1;
            }
        }
        
        
        //Must pass the tags for this module (or the parent module) to the view
        foreach ($moduletags as $key => $value){
            foreach ($value as $key1 => $value1){
                $tags[] = $value1;
            }
        }
        
       
        return view('modules.edit',['module' => $module,'id' => $id,'categories'=> $categories,'tags'=> $tags,'moduleparent' => $moduleparent]);
        
        
       
    }
    
    
    /**
     * Destroy the given module.
     *
     * @param  Request  $request
     * @param  Module  $module
     * @return Response
     */
    public function destroy(User $user, Module $module, $moduleid)
    {
        
        $module = Module::findOrFail($moduleid);
        
        //User can delete only his own modules
        $this->authorize('destroy', $module);
        
        //Using soft deletes (deleted_at)
        Module::destroy($moduleid);
        
        return redirect('/modules');
    }
    
}
