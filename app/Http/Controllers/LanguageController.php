<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Repositories\LanguageRepository;
use App\Language;

use Lang;
use App;
use Session;
use DB;

class LanguageController extends Controller
{
    public function __construct()
    {
        //
        
    }
    
    public function update($lang)
    {
        
        empty($locales);

        $languages = Language::get()->toArray();
        
        foreach ($languages as $language1)
        {
            $locales[] = $language1['language'];
            
        }
        
        if(in_array($lang, $locales)) {
            $apploc = App::getLocale();
            App::setLocale($lang);
            $apploc1 = App::getLocale();
            Session::put('locale',$lang);
            
        }
        
        $lang1 = Session::get ('locale');
        
        return redirect()->back();

    }
}