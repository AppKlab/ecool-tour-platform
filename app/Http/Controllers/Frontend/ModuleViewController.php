<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Validator;

use App\Module;
use App\ModCategory;
use App\Repositories\ModuleViewRepository;
use App\Repositories\ModCategoryRepository;
use App\Repositories\FrontpageRepository;

use App\Policies\ModulePolicy;

use Lang;
use App;
use Session;
use DB;
use Auth;

use App\User;
use App\Category;
use App\Tag;
use App\Gallery;
use App\Schedule;


class ModuleViewController extends Controller
{
   
    /**
     * The module repository instance.
     *
     * @var ModuleRepository
     */
    /**
     * Create a new controller instance.
     * 
     * Add the language to the session 
     *
     * @param  ModuleRepository  $modules
     * @return void
     */
    public function __construct(ModuleViewRepository $modules, FrontpageRepository $frontend, ModCategoryRepository $modcats)
    {
        $this->modcats = $modcats;
        $this->modules = $modules;
        $this->frontend = $frontend;
        
    }
    
    
    /**
     * Display a list of all of the user's modules.
     *
     * @param  Request  $request
     * @return Response
     */
    public function showModule($moduleid)
    {

        //Get the language the user is using
        $apploc = App::getLocale();
        if($userlanguage = Session::get('locale')){
            //echo ("/session locale set:" . $userlanguage);
        }else{
            $userlanguage = Session::put('locale', $apploc);
        }
        
        App::setLocale($userlanguage);
        Session::put('locale', $userlanguage);
        
        $userlanguage = $this->modules->getLang();
        
        if (!$userlanguage)
            $userlanguage = 'en';
        
        $module = Module::findOrFail($moduleid);
        
        //If language is changed then must get the content for the same module
        //in a different language
        if($module['idLang'] <> $userlanguage){
            empty ($module);
            $module = $this->modules->getModuleInAnotherLang ($module['idModule'],$module['user_id'],$userlanguage);
        }
        
        //If language is not 'en' then use the parentmodule mainimage
        $parentmodule = new Module;
        if($module['idLang'] <> 'en'){
            $parentmodule = $this->modules->getParentModule($module['idModule'],$module['user_id']);
            $idparent = $parentmodule ['id'];
            $enabled = $parentmodule ['enabled'];
            $public = $parentmodule ['public'];
        }else {
            $idparent = $module ['id'];
            $enabled = $module ['enabled'];
            $public = $module ['public'];
        }
        
        $languagesinmodule = $this->modcats->getLangInModules($module['idModule'],$module['user_id']);
        $numberoflanguages = count($languagesinmodule);
        
        //Display only modules that are enabled, public and have content in all languages
        if ($enabled == '1' && $public == '1' && $numberoflanguages == 4){
            //Get gallery from the images table
            $gallery = $this->modules->getImages($idparent);
            
            //Get categories from the categories table
            $categories = $this->modules->getCategories($idparent);
    
            //Get tags from the tags table
            $tags = $this->modules->getTags($idparent);
            
            //Get schedule from the schedules table
            $schedules = $this->modules->getSchedules($idparent);
            
            $module['videoUrl1'] = $this->modules->YoutubeID($module['videoUrl']);
            
            //Get info about the provider
            $provider = $this->modules->getProvider($module['user_id']);
            
            
            $agencies = $this->modules->getAgencies($moduleid);
            
            
            return view('frontend.module', [
                'module' => $module, 'userlanguage' => $userlanguage, 
                'parentmodule' => $parentmodule, 
                'gallery' => $gallery,
                'categories' => $categories, 
                'tags' => $tags,
                'schedules' => $schedules,
                'provider' => $provider,
                'agencies' => $agencies,
                'monuments' => $this->frontend->getMonuments(),
            ]);
        }else{
            return redirect()->back();
        }
    }
    
}
