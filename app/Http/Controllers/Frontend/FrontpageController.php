<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Auth;
use App\User;
use Session;
use App;

use App\Repositories\FrontpageRepository;

class FrontpageController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(FrontpageRepository $frontend)
    {
        $this->frontend = $frontend;
        
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function frontpage()
    {
        
        //Get the language the user is using
        $apploc = App::getLocale();
        if($userlanguage = Session::get('locale')){
            //echo ("/session locale set:" . $userlanguage);
        }else{
            $userlanguage = Session::put('locale', $apploc);
        }
        
        App::setLocale($userlanguage);
        Session::put('locale', $userlanguage);
        
        if (!$userlanguage)
            $userlanguage = 'en';
        
        
        //Get modules from the modules table (Random order)
        $modules = $this->frontend->getModules();
        
        //For each record check if the content is in all four languages
        $mainimages = array();
        $categories = array();
        $sixmodules = array();
        $mainimagesmap = array();
        $modulesmap = array();
        $i = 0;        
        foreach ( $modules as $value )
        {
            $realidmodule = $value['id'];
            $pluckedid = $value['idModule'];
            $pluckeduser = $value['user_id'];
            $languagesinmodule = $this->frontend->getLangInModules($pluckedid,$pluckeduser);
            $numberoflanguages = count($languagesinmodule);
            
            //Display only modules that have content in all languages
            //The six modules for the frontpage modules section
            if($numberoflanguages == 4 and $i <6){
                
                //If language is not 'en' then use the parentmodule mainimage
                if($userlanguage <> 'en'){
                    $mainimages[] = $this->frontend->getMainimage($pluckedid,$pluckeduser);
                }
                
                $sixmodules[] = $this->frontend->getModule($pluckedid,$pluckeduser,$userlanguage);
                $i = $i+1;
                
                $categories[$realidmodule] = $this->frontend->getModuleCategories($realidmodule);
                
                
            }
            //All the modules for the googlemaps section
            if($numberoflanguages == 4){

                //If language is not 'en' then use the parentmodule mainimage
                if($userlanguage <> 'en'){
                    $mainimagesmap[] = $this->frontend->getMainimage($pluckedid,$pluckeduser);
                }
                
                $modulesmap[] = $this->frontend->getModule($pluckedid,$pluckeduser,$userlanguage);
                
            }
            
        }
        
        $monuments = $this->frontend->getMonuments();
        
        return view('frontend.frontpage', [
            'modules' => $sixmodules, 'mainimages' => $mainimages, 'userlanguage' => $userlanguage, 'categories' => $categories,
            'modulesmap' => $modulesmap, 'mainimagesmap' => $mainimagesmap,  'monuments' => $monuments,
        ]);
        
    }
    
}
