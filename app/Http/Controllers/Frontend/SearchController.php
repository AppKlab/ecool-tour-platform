<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Validator;

use App\Repositories\SearchRepository;
use App\Repositories\FrontpageRepository;

use Lang;
use App;
use Session;
use DB;
use Auth;

use App\User;
use App\Category;
use App\Tag;
use App\Gallery;
use App\Schedule;
use App\Module;
use App\ModCategory;
use App\Monument;


class SearchController extends Controller
{
   
    /**
     * The search repository instance.
     *
     * @var SearchRepository
     */
    /**
     * Create a new controller instance.
     * 
     *
     * @param  SearchRepository  $search
     * @return void
     */
    public function __construct(SearchRepository $search,FrontpageRepository $modules)
    {
        $this->search = $search;
        $this->modules = $modules;
        
    }
    
    
    public function showForm()
    {
        
        //Get the language the user is using
        $apploc = App::getLocale();
        if($userlanguage = Session::get('locale')){
            //echo ("/session locale set:" . $userlanguage);
        }else{
            $userlanguage = Session::put('locale', $apploc);
        }
        
        App::setLocale($userlanguage);
        Session::put('locale', $userlanguage);
        
        if (!$userlanguage)
            $userlanguage = 'en';
        
        //Get modules from the modules table (Random order)
        $modules = $this->modules->getModules();
        
        //For each record check if the content is in all four languages
        $mainimages = array();
        $categories = array();
        $mainimagesmap = array();

        foreach ( $modules as $value )
        {
            $realidmodule = $value['id'];
            $pluckedid = $value['idModule'];
            $pluckeduser = $value['user_id'];
            $languagesinmodule = $this->modules->getLangInModules($pluckedid,$pluckeduser);
            $numberoflanguages = count($languagesinmodule);
            
            
            //Display only modules that have content in all languages
            //All the modules for the googlemaps section
            if($numberoflanguages == 4){
                
                //If language is not 'en' then use the parentmodule mainimage
                if($userlanguage <> 'en'){
                    $mainimagesmap[] = $this->modules->getMainimage($pluckedid,$pluckeduser);
                }
                
                $modulesmap[] = $this->modules->getModule($pluckedid,$pluckeduser,$userlanguage);
                
            }
            
        }
            
        return view('frontend.search',[
            'locations' => $this->search->getLocations(),
            'modulesmap' => $modulesmap, 
            'mainimagesmap' => $mainimagesmap, 
            'userlanguage'  => $userlanguage,
            'monuments' => $this->modules->getMonuments(),
        ]);
    }
    
    public function makeSearch(Request $request)
    {
        //Get the language the user is using
        $apploc = App::getLocale();
        if($userlanguage = Session::get('locale')){
            //echo ("/session locale set:" . $userlanguage);
        }else{
            $userlanguage = Session::put('locale', $apploc);
        }
        
        App::setLocale($userlanguage);
        Session::put('locale', $userlanguage);
        
        if (!$userlanguage)
            $userlanguage = 'en';
        
        $searchbylocation = array();
        $searchbycategory = array();
        $searchbytag = array();
        $searchbyterm = array();
        $modulesbylang = array();
        $modules = array();
        $mainimages = array();
        $finalresults = array();
        
        //When a search term has been entered
        if ($request['search']){
            
            $modulesterm = Module::select ('id','idModule','user_id')
            ->where('modulename','like','%'.$request['search'].'%')
            ->where('idLang', $userlanguage)
            ->where('public', '1')
            ->where('enabled', '1')
            ->inRandomOrder()
            ->get();
            
        }else{
            
            $modulesterm = Module::select ('id','idModule','user_id')
            ->where('idLang', 'en')
            ->where('public', '1')
            ->where('enabled', '1')
            ->inRandomOrder()
            ->get();

        }
        
        foreach ($modulesterm as $key => $value){
            
            if (!in_array($value['id'],$modules)){
                
                $languagesinmodule = $this->modules->getLangInModules($value['idModule'],$value['user_id']);
                $numberoflanguages = count($languagesinmodule);
                
                //Display only modules that have content in all languages
                if($numberoflanguages == 4){
                    //If language is not 'en' then use the parentmodule mainimage
                    if($userlanguage <> 'en'){
                        $searchbyterm = $this->search->getParentId($value['idModule'],$value['user_id'])->toArray();
                        $modules[] = $searchbyterm['id'];
                    }else{
                        $modules[] = $value['id'];
                    }
                }
            }
        }
        
        //locations are always in the english entry
        if ($request['location']){

            foreach ($request['location'] as $key => $value){
                
                $moduleslocation[] = Module::select ('id','idModule','user_id')
                ->where('city', $key)
                ->where('idLang', 'en')
                ->where('public', '1')
                ->where('enabled', '1')
                ->inRandomOrder()
                ->get();
            }
            
            foreach ($moduleslocation as $key => $value){
                
                foreach ($value as $key1 => $value1){
                    
                    if (in_array($value1['id'],$modules)){

                            $searchbylocation[] = $value1['id'];
                    }
                }
            }
        }
        
        //$request -> checkboxes with name category
        if ($request['category']){
            
            
            foreach ($request['category'] as $key => $value){
                
                $modulescategory[] = Category::select ('idModule')
                ->where('idCategory', $key)
                ->inRandomOrder()
                ->get();
            }
            
            foreach ($modulescategory as $key => $value){
                
                foreach ($value as $key1 => $value1){
                    
                    if (in_array($value1['idModule'],$modules)){

                            $searchbycategory[] = $value1['idModule'];

                    }
                }
            }
            
            
        }
        
        //$request -> checkboxes with name tag
        if ($request['tag']){
            
            
            foreach ($request['tag'] as $key => $value){
                
                $modulestags[] = Tag::select ('idModule')
                ->where('idTag', $key)
                ->inRandomOrder()
                ->get();
            }
            
            foreach ($modulestags as $key => $value){
                
                foreach ($value as $key1 => $value1){
                    
                    if (in_array($value1['idModule'],$modules)){
                        
                        $searchbytag[] = $value1['idModule'];
                    }
                }
            }
            
            
        }
        
        //There are four arrays with results if there is a search term and all the filters are on
        //Arrays: modules, searchbylocation, searchbycategory, searchbytags
        
        //If not all the filters on then we must fill these arrays with the same values as $modules
        if(count($searchbylocation) == 0){
            $searchbylocation = $modules;
        }
        if(count($searchbycategory) == 0){
            $searchbycategory = $modules;
        }
        if(count($searchbytag) == 0){
            $searchbytag = $modules;
        }
        
        //The modules in all arrays go to the final results
        
        foreach ($modules as $module)
        {
            if (in_array($module,$searchbylocation) AND in_array($module,$searchbycategory) AND in_array($module,$searchbytag)){
                $finalresults[] = $module;
            }
        }
        
        $results = count($finalresults);
        
        //If there are no results then get all the modules
        //enabled, public and that are in all languages
        if($results == 0){
            
            //Get modules from the modules table (Random order)
            $modules = $this->modules->getModules();
            
            //For each record check if the content is in all four languages
            
            foreach ( $modules as $value )
            {
                $realidmodule = $value['id'];
                $pluckedid = $value['idModule'];
                $pluckeduser = $value['user_id'];
                $languagesinmodule = $this->modules->getLangInModules($pluckedid,$pluckeduser);
                $numberoflanguages = count($languagesinmodule);
               
                
                //Display only modules that have content in all languages
                //All the modules for the googlemaps section
                if($numberoflanguages == 4){
                    
                    //If language is not 'en' then use the parentmodule mainimage
                    if($userlanguage <> 'en'){
                        $searchbyterm = $this->search->getParentId($value['idModule'],$value['user_id'])->toArray();
                        $modules[] = $searchbyterm['id'];
                    }else{
                        $modules[] = $value['id'];
                    }
                    
                    if($userlanguage <> 'en'){
                        $searchbyterm = $this->search->getParentId($value['idModule'],$value['user_id'])->toArray();
                        $finalresults[] = $searchbyterm['id'];
                    }else{
                        $finalresults[] = $value['id'];
                    }
                }
                
            }
        
        
        }  
        
        //Get the modules info in the userlanguage for the $finalresults array
        //This array is whether the result of the search or all the available modules if no results
        
        foreach ( $finalresults as $value )
        {

            $module = $this->search->getModule($value,$userlanguage);
            $modulesbylang[] = $module;
            
            if($userlanguage <> 'en'){
                $mainimages[] = $this->modules->getMainimage($module->idModule,$module->user_id);
            }
            
            
        }

        return view('frontend.search',[
            'locations' => $this->search->getLocations(),

            'modulesmap' => $modulesbylang,
            'mainimagesmap' => $mainimages,
            'userlanguage'  => $userlanguage,
            
            'results' => $results,
            
            'request' => $request,
            'monuments' => $this->modules->getMonuments(),
        ]);
    }
    
}
