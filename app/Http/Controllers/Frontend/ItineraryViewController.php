<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Validator;
use DateTime;

use App\Itinerary;
use App\Repositories\ItineraryViewRepository;
use App\Repositories\ItineraryModuleRepository;

use Lang;
use App;
use Session;
use DB;
use Auth;
use Image;
use PDF;

use App\User;
use App\Module;
use App\Category;
use App\Tag;
use App\Gallery;
Use App\ItineraryModule;

use Mail;
use App\Mail\ItineraryConfirmation;
use App\Jobs\ItineraryConfirmationEmail;
use App\Mail\ItineraryCancel;
use App\Jobs\ItineraryCancelEmail;
use App\Mail\EvaluationLink;
use App\Jobs\EvaluationLinkEmail;
use App\Mail\EvaluationLinkAgency;
use App\Jobs\EvaluationLinkAgencyEmail;



class ItineraryViewController extends Controller
{
   
    /**
     * The itinerary repository instance.
     *
     * @var ItineraryRepository
     */
    protected $itineraries;
    protected $primaryKey = 'id';
    protected $table = 'itineraries';

    /**
     * Create a new controller instance.
     * 
     * @param  ItineraryRepository  $itineraries
     * @return void
     */
    public function __construct(ItineraryViewRepository $itineraries, ItineraryModuleRepository $itinerarymodules)
    {
       
        $this->itineraries = $itineraries;
        $this->itinerarymodules = $itinerarymodules;
        
    }
    
    
    /**
     * Display a list of all of the public itineraries.
     *
     * @param  Request  $request
     * @return Response
     */
    public function index()
    {
        
        //Get the language the user is using
        $apploc = App::getLocale();
        if($userlanguage = Session::get('locale')){
            //echo ("/session locale set:" . $userlanguage);
        }else{
            $userlanguage = Session::put('locale', $apploc);
        }
        
        App::setLocale($userlanguage);
        Session::put('locale', $userlanguage);
        
        if (!$userlanguage)
            $userlanguage = 'en';
            
        //Get all the public modules with status >= 4 from the itineraries table
        $itineraries = $this->itineraries->getPublicItineraries();
        
        $mainimages = array();
        $itinerariestoshow = array();
        
        if (count($itineraries)>0){
            foreach ( $itineraries as $itinerary )
            {
                
                //For each record check if the content is in all four languages
                $languagesinitinerary = $this->itineraries->getLangInItinerary($itinerary['idItinerary'],$itinerary['user_id']);
                $numberoflanguages = count($languagesinitinerary);

                //Display only modules that have content in all languages
                if($numberoflanguages == 4){
                    //If language is not 'en' then use the parentmodule mainimage
                    
                    if($userlanguage <> 'en'){
                        $mainimages[] = $this->itinerarymodules->getMainimage($itinerary['idItinerary'],$itinerary['user_id']);
                    }
                    
                    $itinerariestoshow[] = $this->itineraries->getItinerary($itinerary['idItinerary'],$itinerary['user_id'],$userlanguage);
                }
            }
        }
        

        return view('frontend.itineraries', [
            'itineraries' => $itinerariestoshow, 'mainimages' => $mainimages, 'userlanguage' => $userlanguage
        ]);
    }
    

    public function showItinerary($itineraryid)
    {
        //Get the language the user is using
        $apploc = App::getLocale();
        if($userlanguage = Session::get('locale')){
            //echo ("/session locale set:" . $userlanguage);
        }else{
            $userlanguage = Session::put('locale', $apploc);
        }
        
        App::setLocale($userlanguage);
        Session::put('locale', $userlanguage);
        
        if (!$userlanguage)
            $userlanguage = 'en';
        
        $itinerary = Itinerary::findOrFail($itineraryid);
        
        //For each record check if the content is in all four languages
        $languagesinitinerary = $this->itineraries->getLangInItinerary($itineraryid,$itinerary['user_id']);
        $numberoflanguages = count($languagesinitinerary);
        
        //Check that the itinerary is public, enabled and that exists in all four languages
        if ($itinerary['public'] == 1 AND $itinerary['enabled'] == 1 AND $itinerary['status'] > 3 AND $itinerary['status'] < 6 AND $numberoflanguages == 4){
            
            $itineraryname = $itinerary['itineraryname'];
            
            
            $agency = User::findOrFail($itinerary['user_id']);
            
            if ($itinerary->idLang == 'en'){
                $itineraryFrom = $itinerary['dateFrom'];
                $itineraryTo = $itinerary['dateTo'];
            }else{
                $itinerary = $this->itineraries->getItinerary($itinerary['idItinerary'],$itinerary['user_id'],$userlanguage);
                $itineraryFrom = $itinerary['dateFrom'];
                $itineraryTo = $itinerary['dateTo'];
            }
            
            $itineraryFrom = new DateTime($itineraryFrom);
            $itineraryFrom = $itineraryFrom->format('d. m. Y H:i');
            
            $itineraryTo = new DateTime($itineraryTo);
            $itineraryTo = $itineraryTo->format('d. m. Y H:i');
            
            $itinerarymodulesid = new ItineraryModule;
            $moduleParent = new Module;
            $categories = new Category;
            $moduleChild = array();
            
            $itinerarymodulesid = $this->itinerarymodules->getItineraryModules($itineraryid);
            $userlanguage = Session::get('locale');
            
            foreach ( $itinerarymodulesid as $value )
            {
                $idparentmodule = $value['idModule'];
                $moduleParent[$idparentmodule] = Module::findOrFail($idparentmodule);
                //Get all the categories for each module
                $categories[$idparentmodule] = $this->itinerarymodules->getModuleCategories($idparentmodule);
                
                if($userlanguage != 'en'){
                    $pluckedid = $moduleParent[$idparentmodule]['idModule'];
                    $pluckeduser = $moduleParent[$idparentmodule]['user_id'];
                    $moduleChild[$idparentmodule] = $this->itinerarymodules->getModule($pluckedid,$pluckeduser,$userlanguage);
                    
                }
                
            }
            
            return view('frontend.itineraryview', [
                'itinerarymodules' => $itinerarymodulesid, 'moduleParent' => $moduleParent,
                'categories' => $categories, 'moduleChild' => $moduleChild, 'userlanguage' => $userlanguage, 'itineraryFrom' => $itineraryFrom,
                'itineraryTo' => $itineraryTo, 'itinerary' => $itinerary, 'agency' => $agency,
            ]);
        }else{
            
            return redirect('/packages');
            
        }
    }
    
}
