<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Validator;

use App\Module;
use App\ModCategory;
use App\Repositories\ModCategoryRepository;
use App\Repositories\FrontpageRepository;

use App\Policies\ModulePolicy;

use Lang;
use App;
use Session;
use DB;
use Auth;

use App\User;
use App\Category;
use App\Tag;
use App\Gallery;


class ModCategoryController extends Controller
{
   
    /**
     * The module repository instance.
     *
     * @var ModuleRepository
     */
    /**
     * Create a new controller instance.
     * 
     * Add the language to the session 
     *
     * @param  ModuleRepository  $modules
     * @return void
     */
    public function __construct(ModCategoryRepository $modules, ModCategoryRepository $categories, FrontpageRepository $frontend)
    {
        $this->modules = $modules;
        $this->categories = $categories;
        $this->frontend = $frontend;
        
    }
    
    
    /**
     * Display a list of all of the user's modules.
     *
     * @param  Request  $request
     * @return Response
     */
    public function makeIndex($cat)
    {

        //Get the language the user is using
        $apploc = App::getLocale();
        if($userlanguage = Session::get('locale')){
            //echo ("/session locale set:" . $userlanguage);
        }else{
            $userlanguage = Session::put('locale', $apploc);
        }
        
        App::setLocale($userlanguage);
        Session::put('locale', $userlanguage);

        $userlanguage = $this->modules->getLang();
        
        if (!$userlanguage)
            $userlanguage = 'en';
        
        //Get the modules for this category from the categories table
        $categories = $this->categories->getModulesInCategory($cat);

        $mainimages = array();
        $modulesincategory = array();
        
        foreach ( $categories as $value )
        {
            $realidmodule = $this->modules->getRealModuleId($value['idModule']);
            $pluckedid = $realidmodule->pluck('idModule')->filter()->first();
            $pluckeduser = $realidmodule->pluck('user_id')->filter()->first();
            $pluckedpublic = $realidmodule->pluck('public')->filter()->first();
            $pluckedenabled = $realidmodule->pluck('enabled')->filter()->first();
            
            //Modules must be public and enabled to be seen in the frontpages
            if($pluckedpublic == '1' && $pluckedenabled == '1'){
                //For each record check if the content is in all four languages
                $languagesinmodule = $this->modules->getLangInModules($pluckedid,$pluckeduser);
                $numberoflanguages = count($languagesinmodule);
                
                //Display only modules that have content in all languages
                if($numberoflanguages == 4){
                //If language is not 'en' then use the parentmodule mainimage
                
                    if($userlanguage <> 'en'){
                        $mainimages[] = $this->modules->getMainimage($pluckedid,$pluckeduser);
                    }
                    
                    $modulesincategory[] = $this->modules->getModule($pluckedid,$pluckeduser,$userlanguage);
                }
            }
        } 
        
        return view('frontend.modcategory', [
            'modules' => $modulesincategory, 
            'cat' => $cat, 
            'mainimages' => $mainimages, 
            'userlanguage' => $userlanguage,
            'monuments' => $this->frontend->getMonuments(),
        ]);
    }
    
    public function index1()
    {
        
        $cat=1;
        return $this->makeIndex($cat);
        
    }
    
    public function index2()
    {

        $cat=2;
        return $this->makeIndex($cat);
        
    }
    
    public function index3()
    {
        
        $cat=3;
        return $this->makeIndex($cat);
        
    }
    
}
