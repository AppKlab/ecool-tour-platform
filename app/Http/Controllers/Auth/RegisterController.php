<?php

namespace App\Http\Controllers\Auth;

use App\Notifications\UserRegisteredSuccessfully;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

use Notifiable;

use Illuminate\Support\Facades\Hash;
use Illuminate\Auth\Events\Registered;
use App\Jobs\SendVerificationEmail;
use App\Jobs\SendEnableEmail;


use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Mail;
use App\Mail\EmailVerification;
use App\Mail\UserVerification;

use Session;
use App;
use Image;

class RegisterController extends Controller
{
    /*
     |--------------------------------------------------------------------------
     | Register Controller
     |--------------------------------------------------------------------------
     |
     | This controller handles the registration of new users as well as their
     | validation and creation. By default this controller uses a trait to
     | provide this functionality without requiring any additional code.
     |
     */
    
    use RegistersUsers;
    
    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $username = 'username';
    protected $users;
    protected $primaryKey = 'id';
    protected $table = 'users';
    
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }
    
    public function showRegistrationForm()
    {
        die();
    }
    
    /**
     * Register new account.
     *
     * @param Request $request
     * @return User
     */
    protected function register(Request $request)
    {
        
        /** @var User $user */
        
        if ($request->usertype < 4){
            $validatedData = $request->validate([
                'username'  => 'required|max:255|unique:users',
                'email'     => 'required|email|max:255|unique:users',
                'password'  => 'required|confirmed|min:6',
                'title'     => 'required|max:255',
                'address'   => 'required|max:255',
                'logo'      => 'required',
                'telephone' => 'required|max:15',
                'cellphone' => 'required|max:15',
                'www'       => 'required|url',
                'usertype'  => 'required',
            ]);
        }else{
            $validatedData = $request->validate([
                'username'  => 'required|max:255|unique:users',
                'email'     => 'required|email|max:255|unique:users',
                'password'  => 'required|confirmed|min:6',
                'title'     => 'required|max:255',
                'cellphone' => 'required|max:15',
                'usertype'  => 'required',
            ]);
        }
        
        $validatedData['password']        = bcrypt(array_get($validatedData, 'password'));
        $validatedData['activation_code'] = str_random(30).time();
        $validatedData['enable_code'] = str_random(30).time();
        
        if($idLang = Session::get ('locale')){
            $validatedData['idLang']        = $idLang;
        }else{
            $idLang = App::getLocale();
            $validatedData['idLang']        = $idLang;
        }
        
        if ($request->usertype < 4){
            try {
        
                
                /*FIRST TRY TO UPLOAD FILE*/
                $file = $request['logo'];
                if ($request->hasFile('logo')) {
                    //echo '</br>File OK';
                }else{
                    //echo '</br>No file';
                    return redirect()->back()->with('message', trans('auth.filenotfound'));
                }
                
                //Move Uploaded File
                $destinationPath = '/storage/app/public/logos';
                $path = public_path();
                $path1 = base_path().'/public/upload_media/logos';
                $timeforname = time();
                $filename = 'logo' . '-' . time() . '-' . $file->getClientOriginalName();
                $img = Image::make($request->file('logo')->getRealPath());
                // read width of image to see if needs to be resized
                $width = $img->width();
                
                if ($width > 500){
                    // resize the image to a width of 500 and constrain aspect ratio (auto height)
                    $img->resize(500, null, function ($constraint) {
                        $constraint->aspectRatio();
                    });
                }
                $img->save(base_path('/public/upload_media/logos/').$filename);
                // resize the image to a width of 100 and constrain aspect ratio (auto height)
                // for THUMBS
                $img->resize(100, null, function ($constraint) {
                    $constraint->aspectRatio();
                });
                $img->save(base_path('/public/upload_media/logos_thumbs/').$filename);
                
                $validatedData['logo']  = $filename;
                
            } catch (\Exception $exception) {
                logger()->error($exception);
                return redirect()->back()->with('message', trans('auth.nousercreated'));
            }
        
        }
        /*WHEN FILE IS UPLOADED (if is not a group) THEN CREATE USER*/
        $user = app(User::class)->create($validatedData);
        
        //After creating the user an email notification is sent to the mail address
        //for email validation
        $activation_code = $validatedData['activation_code'];
        
        Mail::to($request->email)
        ->send(new EmailVerification($activation_code));
        
        return redirect()->back()->with('message', trans('auth.successreg'));
        
    }
    
    /**
     * Activate the user with given activation code.
     * @param string $activationCode
     * @return string
     */
    public function activateUser(string $activationCode)
    {
        
        try {
            $user = app(User::class)->where('activation_code', $activationCode)->first();
            if (!$user) {
                
                return view('mail.emailconfirm')->with('error', trans('forms.notverifiedmail'));

            }
            $user->status          = 1;
            $user->activation_code = null;
            $user->save();

        } catch (\Exception $exception) {
            logger()->error($exception);
            
            return view('mail.emailconfirm')->with('error', trans('errors.whoops'));
            
        }
        
        $enabling_code = $user->enable_code;
        $userType = $user->userType;
        $username = $user->title;
        $email = $user->email;
        $address = $user->address;
        /* Must send mail to admin so he reviews the user and the account is enabled */
        Mail::to('admin@ecooltour.eu')
        ->send(new UserVerification($enabling_code,$userType,$username,$email,$address));
        
        return view('mail.emailconfirm')->with('message', trans('forms.verifiedmail'));
    }
    
    public function enableUser(string $enablingCode)
    {
        
        try {
            $user = app(User::class)->where('enable_code', $enablingCode)->first();
            if (!$user) {
                
                return view('mail.userconfirm')->with('error', trans('forms.notenabledmail'));
                
            }
            $user->enabled  = 1;
            $user->enable_code = null;
            $user->save();
            
        } catch (\Exception $exception) {
            logger()->error($exception);
            
            return view('mail.userconfirm')->with('error', trans('errors.whoops'));
            
        }
        
        return view('mail.userconfirm')->with('message', trans('forms.verifieduser'));
    }
    
    
}