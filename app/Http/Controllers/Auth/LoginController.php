<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

/*use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;*/
use App\User;
use Session;
use Auth;
use Closure;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/authhome';
    protected $username = 'username';
    
    
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
        $this->middleware('checkstatus');
    }
    
    /**
     * Check either username or email.
     * @return string
     */
    public function username()
    {
        $fieldName = 'username';
        return $fieldName;
    }
    
    /**
     * Validate the user login.
     * @param Request $request
     */
    protected function validateLogin(Request $request)
    {
       
        $this->validate(
            $request,
            [
                'username' => 'required|string',
                'password' => 'required|string',
            ]
            );
       
        
       //The rest is done in the middleware
        
    }
    
    /**
     * @param Request $request
     * @throws ValidationException
     */
    protected function sendFailedLoginResponse(Request $request)
    {
        $request->session()->put('login_error', trans('auth.failed'));
        throw ValidationException::withMessages(
            [
                'error' => [trans('auth.failed')],
            ]
            );
    }


    public function logout() {
    	Auth::logout();
    	Session::forget('isAdmin');
    	
    	return redirect('/');
    	
    }
    
    protected function doLogin(Request $request)
    {
        return redirect('/');
        
        $this->validate(
            $request,
            [
                'username' => 'required|string',
                'password' => 'required|string',
            ]
            );
        
        if (Auth::attempt(['username'=>$request['username'],'password'=>$request['password'], 'status'=>1 ])){
            //print("registered & validated");
        }elseif  (Auth::attempt(['username'=>$request['username'],'password'=>$request['password'], 'status'=>0 ])){
            //print("registered & NOT validated");
        }else{
            //print("failed");
        }
        
        return redirect('/login')->with('message', trans('auth.successreg'));
        
        
    }
    
    
}
