<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;


use App\Notifications\UserRegisteredSuccessfully;
use App\User;


use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

use Notifiable;

use Illuminate\Support\Facades\Hash;
use Illuminate\Auth\Events\Registered;
use App\Jobs\SendResetEmail;
use App\Mail\SendResetPassword;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Mail;

class PasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use SendsPasswordResetEmails;

    /**
     * Create a new password controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }
    
    public function sendResetLinkEmail(Request $request)
    {
     
        try {
            $user = app(User::class)->where('email', $request['email'])->first();
            if (!$user) {
                
                return view('auth.passwords.email')->with('error', trans('forms.noemailindb'));

            }
            $user->resetmail_token  = str_random(30).time();

            $user->save();

        } catch (\Exception $exception) {
            logger()->error($exception);
            
            return view('auth.passwords.email')->with('error', trans('errors.whoops'));
            
        }
        
        //After creating the user an email notification is sent to the mail address
        //for email validation
        $resetmail_token = $user['resetmail_token'];
        $mail = encrypt($request->email);
        $mailto = $request->email;
        
        Mail::to($mailto)
        ->send(new SendResetPassword($resetmail_token,$mail));
        
        return view('auth.emails.emailconfirm')->with('message', trans('forms.confirmyourmail'));
        
        
    }
    
    
    public function showEmailForm()
    {
        return view('auth.passwords.email');
    }
    
    public function showResetForm($token,$email)
    {
        $decryptedmail = decrypt($email);
        
        try {
            $user = app(User::class)->where('email', $decryptedmail)->where('resetmail_token', $token)->first();
            if (!$user) {
                
                return view('auth.passwords.reset')->with('error', trans('forms.notoken'));
                
            }
            
        } catch (\Exception $exception) {
            logger()->error($exception);
            
            return view('auth.passwords.reset')->with('error', trans('errors.whoops'));
            
        }

        return view('auth.passwords.reset')->with('message', 'ok')->with('token', $token);
    }
    
    public function reset(Request $request)
    {
        $validatedData = $request->validate([
            'email'     => 'required|email|max:255',
            'password'  => 'required|confirmed|min:6',
        ]);

        try {
            $user = app(User::class)->where('email', $request['email'])->where('resetmail_token', $request['token'])->first();
            if (!$user) {
                
                return view('auth.passwords.reset')->with('error', trans('auth.passnotchanged'));
                
            }
            
            $user->password   = bcrypt($request['password']);
            $user->resetmail_token = null;
            $user->save();
            
        } catch (\Exception $exception) {
            logger()->error($exception);
            
            return view('auth.passwords.reset')->with('error', trans('errors.whoops'));
            
        }
        
        return view('auth.passwords.reset')->with('message', 'success');
    }
    
    
}
